<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Util
{
    public function createEncrypt($length = 6)
    {

        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $encrypt ='';
        for ($i = 0; $i < $length; $i++) {
            $encrypt .= $chars[ mt_rand(0, strlen($chars) - 1) ];
        }
        return $encrypt;
    }

    public function error($statusCode, $message)
    {
        header('Content-Type: application/json');
        echo json_encode(array('status' => $statusCode,'message' => $message), JSON_UNESCAPED_UNICODE);       
        exit();
    }

    public function success($data = null, $message = 'OK')
    {
        $response['status'] = 200;
        $response['message'] = $message;
        // if ($total != null) {
        //     $response['total'] = $total;
        // }
        
        if ($data != null) {
            $response['data'] = $data;
        }
        $this->arrayKeysToCamelCase($response);

        header('Content-Type: application/json');
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        exit();
    }

    public function log($message)
    {
        if (DEBUG_MODE) {
            var_dump($message);
        }
    }

    /**
     * Set params
     * @param Object &$param The object to be generated
     * @param array  $input  A dictionary that contains all request parameters
     * @param array  $mapper Mapper object provides the mapping in the below format
     *
     * If the field name remain unchanged, null should be provided
     *
     * ['inputParam' => 'outputParam', "unchangedParam" => null]
     */

    public function setParams(&$param, array $input, array $mapper = null)
    {
        if (empty($input)) {
            return;
        }

        foreach ($input as $from => $value) {
            if (!empty($mapper)) {
                if (array_key_exists($from, $mapper) && !empty($mapper[$from])) {
                    $param[$mapper[$from]] = $value;
                } else {
                    $param[$from] = $value;
                }
            } else {
                $param[$from] = $value;
            }
        }
    }


    public function toSnakeCase($input, $case = 'upper')
    {
        $num = preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];

        foreach ($ret as &$match) {
            $match = $case == 'lower' ? strtolower($match) : strtoupper($match);
        }

        return implode('_', $ret);
    }

    public function toCamelCase($input)
    {
        if (strtoupper($input) == 'ID') {
            return 'ID';
        }
        if (strpos($input, '_') == true || $input == strtoupper($input)) {
            $input = lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', strtolower($input)))));
        }
        
        if ((substr($input, -2)  === 'Id' || substr($input, -2)  === 'id') && substr($input, -4)  !== 'Paid') {
            $input = substr_replace($input, 'ID', -2);
        }
        return $input;
    }


    public function arrayKeysToCamelCase(array &$array)
    {
        foreach ($array as $key => $val) {
            //Get camelCase key from snake_case
            $new_key = $this->toCamelCase($key);

            // Change 'id' or 'Id' into 'ID'
            // TODO: tmp fix for "paid"
            if ((substr($new_key, -2)  === 'Id' || substr($new_key, -2)  === 'id')
                && substr($new_key, -4) !== 'Paid') {
                $new_key = substr_replace($new_key, 'ID', -2);
            }

            $array[$new_key] = $val;

            //Unset previous key and value pairs
            if ($new_key != $key) {
                unset($array[$key]);
            }
            
            //Recusively call the function itself if $val is an array
            if (is_array($val)) {
                $array[$new_key] = $this->arrayKeysToCamelCase($val);
            }
        }
        return $array;
    }

    public function arrayKeysToSnakeCase(array &$array, $case = 'upper')
    {
        foreach ($array as $key => $val) {
            //Get snake_case key from camelCase
            $new_key = $this->toSnakeCase($key);

            $array[$new_key] = $val;

            //Unset previous key and value pairs
            if ($new_key != $key) {
                unset($array[$key]);
            }
            
            //Recusively call the function itself if $val is an array
            if (is_array($val)) {
                $array[$new_key] = $this->arrayKeysToSnakeCase($val);
            }
        }
        return $array;
    }

    /**
     * Named-Param vsprintf()
     *
     * positional-params based on key name, much the same as positional in sprintf()
     *
     * @link http://php.net/manual/en/function.sprintf.php
     * @link http://www.php.net/manual/en/function.vsprintf.php
     * @link https://gist.github.com/onyxraven/1902197
     *
     * @param string $str format string - replacements are in %KEY$x format
     *     where KEY is the array key from $args and x is the sprintf format
     * @param array $args key-value associative array of replacements
     * @return string
     */
    public function nvsprintf($str, array $args)
    {
        $i = 1;
        foreach ($args as $k => $v) {
            $str = str_replace("%{$k}$", "%{$i}$", $str);
            $i++;
        }
        return vsprintf($str, array_values($args));
    }
}
