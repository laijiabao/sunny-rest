<?php
class Form_model extends MY_Model {

    public function __construct()
    {
        $this->load->database();
    }

	public function getForms($param)
	{
		$pagination = empty($param['pagination']) ? 1 : $param['pagination'];
		$lang = empty($param['lang']) ? 'ch' : $param['lang'];
		$search = $param['search'];

		$query = 'SELECT f.formId, f.formName,f.type, f.formUrl,f.createTime,f.updateTime';
		$query .= ' FROM form f';
		$condition = ' WHERE 1 ';
		if($search != null){
			$condition .= ' AND (f.formName LIKE \'%'.$search.'%\')';
		}
		$query2 = 'SELECT COUNT(*) AS total FROM form f ';
		$query2 .= $condition;

		$condition .= ' ORDER BY f.formId DESC';
		$condition .= ' LIMIT '.(($pagination - 1) * RECORD_PER_PAGE).','.RECORD_PER_PAGE;
		$query .= $condition;


		$data['total'] = $this->db->query($query2)->row()->total;
		$data['data'] = $this->db->query($query)->result();

		return $data;

	}


	public function getFormDetail($formId, $lang)
	{

		$query = 'SELECT s.formId, s.color,';
		if($lang != 'en' && !empty($lang))
			$query .= 's.chFormName AS formName';
		else
			$query .= 's.enFormName AS formName';
		$query .= ' FROM form s WHERE formId = ? ';

		// $this->log($query);


		$data['data'] = $this->db->query($query, $formId)->result();
		return $data;
	}


	public function createForm($forms)
	{
		$count = 0;
        //Insert Into Database
        $this->db->trans_start();
        
        foreach ($forms as $form) {
        	$this->db->insert('form',$form);
	        if ($this->db->trans_status() === FALSE){
	          $this->db->trans_rollback();
	          $count = 0;
	        } else {

	          $this->db->trans_commit();
	          $count = $count + 1;
			}
        }  
		
		return $count;
	}

	public function editForm($form)
	{
		//Check 
		if(empty($form['formId']))
			$this->error(204, 'Form ID cannot be empty');

		$this->db->where('formId', $form['formId']);
		$this->db->update('form', $form);

		$result = $this->db->affected_rows();

		return $result;

	}

	public function deleteForms($forms)
	{
        $forms = "'" . str_replace(",", "','", $forms) . "'";

        $sql = "SELECT formUrl FROM form WHERE formId IN (".$forms.")";
        $formsUrlToDelete = $this->db->query($sql)->result();

        $sql = "DELETE FROM form WHERE formId IN (".$forms.")";
        $this->db->query($sql);
        $result = $this->db->affected_rows();

        if($result > 0){

        	foreach ($formsUrlToDelete as $file) {
        		unlink($file->formUrl);
        	}
        }
		return $result;

	}

}