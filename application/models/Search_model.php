<?php
class Search_model extends MY_Model {

    public function __construct()
    {
        $this->load->database();
    }

	public function getStatuses($param)
	{
		$pagination = empty($param['pagination']) ? 1 : $param['pagination'];
		$lang = empty($param['lang']) ? 'ch' : $param['lang'];
		$search = $param['search'];

		$query = 'SELECT s.statusId, s.color,';
		if($lang != 'en' && !empty($lang))
			$query .= 's.chStatusName AS statusName';
		else
			$query .= 's.enStatusName AS statusName';
		$query .= ' FROM status s';
		$condition = ' WHERE 1 ';
		if($search != null){
			$condition .= ' AND (s.chStatusName LIKE \'%'.$search.'%\' OR s.enStatusName LIKE \'%'.$search.'%\')';
		}
		$query2 = 'SELECT COUNT(*) AS total FROM status s ';
		$query2 .= $condition;

		$condition .= ' ORDER BY s.statusId ASC';
		$condition .= ' LIMIT '.(($pagination - 1) * RECORD_PER_PAGE).','.RECORD_PER_PAGE;
		$query .= $condition;


		$data['total'] = $this->db->query($query2)->row()->total;
		$data['data'] = $this->db->query($query)->result();

		return $data;

	}


	public function clientList($q, $lang = 'ch')
	{

		$query = 'SELECT clientId, clientCode, enClientName, chClientName,CONCAT(clientCode, " ", enClientName) AS name FROM client WHERE (enClientName LIKE \'%'.rawurldecode($q).'%\' OR chClientName LIKE \'%'.rawurldecode($q).'%\' OR clientCode LIKE \'%'.rawurldecode($q).'%\') LIMIT 10';

		$data = $this->db->query($query)->result();
		return $data;
	}

	public function staffList($q, $lang = 'ch')
	{
		$query = 'SELECT staffId, alias, CONCAT(staffNo, " ", alias) AS name FROM staff WHERE (alias LIKE \'%'.rawurldecode($q).'%\' OR enName LIKE \'%'.rawurldecode($q).'%\' OR chName LIKE \'%'.rawurldecode($q).'%\') LIMIT 10';
		$data = $this->db->query($query)->result();
		return $data;
	}

	public function taskList($q, $lang = 'ch')
	{
		$query = 'SELECT taskId, enTaskName, chTaskName,CONCAT(taskCode, " ", enTaskName) AS name FROM task WHERE (enTaskName LIKE \'%'.rawurldecode($q).'%\' OR chTaskName LIKE \'%'.rawurldecode($q).'%\') LIMIT 10';
		$data = $this->db->query($query)->result();
		return $data;
	}

}