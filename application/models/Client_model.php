<?php
class Client_model extends MY_Model {

    public function __construct()
    {
        $this->load->database();
    }

	public function getClients($param = null)
	{
		$pagination = empty($param['pagination']) ? 1 : $param['pagination'];
		$lang = empty($param['lang']) ? 'ch' : $param['lang'];
		$search = $param['search'];
		$filterBy = $param['filterBy'];
		$role = $this->session->userdata('userData')['role'];
		$staffId = $this->session->userdata('userData')['staffId'];
		

		// if($role != 'A' && $role != 'M'){

		// 	$clientIds = $this->getClientsByStaffId($staffId);
		// }

		$query = 'SELECT c.clientId, c.clientCode, c.agencyName,c.chClientName,c.enClientName,c.status,';
		$query .=' (SELECT tc.'.$lang.' FROM type_code tc WHERE tc.code = c.status AND tc.type = "clientStatus") AS statusName';

		$query .= ' FROM client c INNER JOIN client_detail AS cl ON c.clientId = cl.clientId LEFT JOIN company AS cy ON c.companyNo = cy.companyId ';
		$condition = ' WHERE 1 ';

		if($search != null){
			$condition .= ' AND (c.chClientName LIKE \'%'.$search.'%\' OR c.enClientName LIKE \'%'.$search.'%\' OR c.clientCode LIKE \'%'.$search.'%\')';
		}
		if($filterBy != null)
		{
			if ($filterBy == 'clientName') 
			{
				$condition .= " (c.chClientName = '$search' OR c.enClientName = '$search')";
			}
			elseif ($filterBy == 'clientAddress') 
			{
				$condition .= " (
					c.enRegisteredAddress1 LIKE \'%'.$search.'%\' OR 
					c.enRegisteredAddress2 LIKE \'%'.$search.'%\' OR 
					c.enRegisteredAddress3 LIKE \'%'.$search.'%\' OR 
					c.chRegisteredAddress1 LIKE \'%'.$search.'%\' OR 
					c.chRegisteredAddress2 LIKE \'%'.$search.'%\' OR 
					c.chRegisteredAddress3 LIKE \'%'.$search.'%\')";
			}
			elseif ($filterBy == 'companyNo') 
			{
				$condition .= " c.companyNo = '$search' )";
			}
			elseif ($filterBy == 'clientCode') 
			{
				$condition .= " c.clientCode = '$search' )";
			}
			elseif ($filterBy == 'companyType') 
			{
				$condition .= " cy.companyType = '$search' )";
			}
			elseif ($filterBy == 'socialMediaContact') 
			{
				$condition .= " c.socialMediaContact = '$search' )";
			}
			elseif ($filterBy == 'dateOfIncorporation') 
			{
				$condition .= " c.dateOfIncorporation = '$search' )";
			}
			elseif ($filterBy == 'yearEnd') 
			{
				$condition .= " DATE_FORMAT('%m',cl.yearEndedDate) = '$search' )";
			}
			elseif ($filterBy == 'yearEnd') 
			{
				$condition .= " DATE_FORMAT('%m',cl.yearEndedDate) = '$search' )";
			}
			
		}

		// if(!empty($clientIds))
		// 	$condition .= " AND cl.clientId IN ('$clientIds')";

		$query2 = 'SELECT COUNT(*) AS total FROM client c ';
		$query2 .= $condition;

		$condition .= ' ORDER BY c.clientCode';
		$condition .= ' LIMIT '.(($pagination - 1) * RECORD_PER_PAGE).','.RECORD_PER_PAGE;
		$query .= $condition;

		$data['total'] = $this->db->query($query2)->row()->total;
		$queryResult = $this->db->query($query);
		$data['data'] = $queryResult->result();
		if (array_key_exists('indicator', $param) && $param['indicator'] == 'CLIENT LIST REPORT') 
		{
			return $queryResult;
		}
		return $data;

	}

	public function getClients_origin($param = null)
	{
		$pagination = empty($param['pagination']) ? 1 : $param['pagination'];
		$lang = empty($param['lang']) ? 'ch' : $param['lang'];
		$search = $param['search'];

		$role = $this->session->userdata('userData')['role'];
		$staffId = $this->session->userdata('userData')['staffId'];
		

		// if($role != 'A' && $role != 'M'){

		// 	$clientIds = $this->getClientsByStaffId($staffId);
		// }

		$query = 'SELECT cl.clientId, cl.clientCode, cl.agencyName,cl.chClientName,cl.enClientName,cl.status,';
		$query .=' (SELECT c.'.$lang.' FROM type_code c WHERE c.code = cl.status AND c.type = "clientStatus") AS statusName';

		$query .= ' FROM client cl';
		$condition = ' WHERE 1 ';

		if($search != null){
			$condition .= ' AND (cl.chClientName LIKE \'%'.$search.'%\' OR cl.enClientName LIKE \'%'.$search.'%\' OR cl.clientCode LIKE \'%'.$search.'%\')';
		}

		// if(!empty($clientIds))
		// 	$condition .= " AND cl.clientId IN ('$clientIds')";

		$query2 = 'SELECT COUNT(*) AS total FROM client cl ';
		$query2 .= $condition;

		$condition .= ' ORDER BY cl.clientCode';
		$condition .= ' LIMIT '.(($pagination - 1) * RECORD_PER_PAGE).','.RECORD_PER_PAGE;
		$query .= $condition;

		$data['total'] = $this->db->query($query2)->row()->total;
		$data['data'] = $this->db->query($query)->result();

		return $data;

	}

	public function getClientDetail($clientId, $lang)
	{

		$query = 'SELECT cl.clientId, cl.chClientName, cl.enClientName, cl.clientCode,cl.companyNo, cl.brNo, cl.country, cl.attn, cl.type, cl.clientGroup, cl.agencyName, cl.enMailingAddress1, cl.enMailingAddress2, cl.enMailingAddress3, cl.chMailingAddress1, cl.chMailingAddress2, cl.chMailingAddress3, cl.enRegisteredAddress1, cl.enRegisteredAddress2, cl.enRegisteredAddress3, cl.chRegisteredAddress1, cl.chRegisteredAddress2, cl.chRegisteredAddress3, cld.contactPerson1, cld.contactPerson2, cld.position1, cld.position2, cld.email1, cld.email2, cld.telNo1, cld.telNo2, cld.telNo3, cld.faxNo1, cld.faxNo2, cld.faxNo3 ';

		$query .= ' FROM client cl LEFT JOIN client_detail cld ON cl.clientId = cld.clientId WHERE cl.clientId = ?';
		$data['data'] = $this->db->query($query, $clientId)->result();
		return $data;
	}


	public function createClient($client, $clientDetail)
	{

        //Insert Into Database
        $this->db->trans_start();
        $this->db->insert('client',$client);        
        $insert_id = $this->db->insert_id();
        $clientDetail['clientId'] = $insert_id;
        $this->db->insert('client_detail',$clientDetail);

        if ($this->db->trans_status() === FALSE){
          $this->db->trans_rollback();
          $insert_id = 0;
        } else {
          $this->db->trans_commit();
		}
		
		return $insert_id;
	}

	public function editClient($client, $clientDetail)
	{
		if(empty($client['clientId']))
			$this->error(204, 'Client ID cannot be empty');

		$this->db->trans_start();

		$this->db->where('clientId', $client['clientId']);
		$this->db->update('client', $client);

		$result = $this->db->affected_rows();

		$this->db->where('clientId', $client['clientId']);
		$this->db->update('client_detail', $clientDetail);
		$result += $this->db->affected_rows();


        if ($this->db->trans_status() === FALSE){
          $this->db->trans_rollback();
          $this->error(204, 'Edit Client Failed');
        } else {
          $this->db->trans_commit();
		}

		return $result;

	}

	public function deleteClients($clients)
	{
        $clients = "'" . str_replace(",", "','", $clients) . "'";

        $sql = "UPDATE client SET status = 'I' WHERE clientId IN (".$clients.")";

        $this->db->query($sql);
        $result = $this->db->affected_rows();
		return $result;
	}

	public function restoreClients($clients)
	{
        $clients = "'" . str_replace(",", "','", $clients) . "'";

        $sql = "UPDATE client SET status = 'A' WHERE clientId IN (".$clients.")";

        $this->db->query($sql);
        $result = $this->db->affected_rows();
		return $result;
	}

	public function getInvoice($params = null){

	}

	public function getClientsByStaffId($staffId){


		if(!is_numeric($staffId))
			$this->error(204, 'invalid staff ID');

		$query = "SELECT group_concat(clientId separator ',') as `clientIds` FROM task_management WHERE staffId = $staffId ";
		$result = $this->db->query($query)->row()->clientIds;

		return $result;
	}

	public function getClientTasks($clientId, $staffId = null, $lang = 'ch'){

		$query = 'SELECT tm.taskMagId, sf.alias AS staffName, tm.startDate, tm.endDate, tm.dueDate, tm.taskAttachments,tm.manHours,st.color AS statusColor, tm.value,u.userName,';
		if($lang != 'en' && !empty($lang))
			$query .= 'st.chStatusName AS status,';
		else
			$query .= 'st.enStatusName AS status,';
		if($lang != 'en' && !empty($lang))
			$query .= 't.chTaskName AS taskName, t.enTaskDescription,';
		else
			$query .= 't.enTaskName AS taskName, t.chTaskDescription,';
		// if($lang != 'en' && !empty($lang))
		// 	$query .= 'cl.chClientName AS clientName';
		// else
			$query .= 'cl.enClientName AS clientName';

		$query .= ' FROM task_management tm LEFT JOIN task t ON tm.taskId = t.taskId';
		$query .= ' LEFT JOIN client cl ON tm.clientId = cl.clientId';
		$query .= ' LEFT JOIN staff sf ON tm.staffId = sf.staffId';
		$query .= ' LEFT JOIN status st ON tm.statusId = st.statusId';
		$query .= ' LEFT JOIN user u ON tm.userId = u.userId';
		$query .= ' WHERE tm.clientId = ?';
		if(!empty($staffId))
			$query .= ' AND tm.staffId = '.$staffId;
		$query .= ' ORDER BY tm.dueDate DESC LIMIT 50';


		$result = $this->db->query($query, $clientId)->result();
		return $result;
	}

	public function getClientInvoiceInfo($clientId, $lang = 'ch'){

		$query = 'SELECT cl.clientId, cl.clientCode, cl.enClientName, cl.chClientName, cl.attn, cld.contactPerson1, cld.contactPerson2, cld.email1, cld.email2';
		$query .= ' FROM client cl LEFT JOIN client_detail cld ON cl.clientId = cld.clientId';
		$query .= ' WHERE cl.clientId = ?';

		$result = $this->db->query($query, $clientId)->row();
		return $result;

	}


	public function createInvoice($invoices){

		$query1 = "SELECT MAX(count) AS count FROM invoice WHERE DATE_FORMAT(NOW(),'%Y-%m-%d') = DATE_FORMAT(invoice.createTime,'%Y-%m-%d')";
		$result = $this->db->query($query1)->row();

		if(empty($result->count))
			$count = 1;
		else
			$count = $result->count + 1;

		$invoiceNo = date("ym").sprintf("%03d", $count);

		foreach ($invoices as &$invoice) {
			$invoice['count'] = $count;
			$invoice['invoiceNo'] = $invoiceNo;
		}

		$this->db->insert_batch('invoice',$invoices);        

		return $invoiceNo;

	}

	public function invoiceSummary($param, $user = null)
	{
		$year = empty($param['year']) ? $year = date("Y") : $param['year'];
		$month = empty($param['month']) ? $month = date("m") : $param['month'];

		$startDate = date('Y-m-01', strtotime("$year-$month"));
		$endDate = date('Y-m-t', strtotime("$year-$month"));

		$fields = "
			i.clientCode,
			i.invoiceNo,
			DATE_FORMAT(i.createTime,'%d/%m/%y') AS date,
			i.staffNo,
			'11000' AS receivableAccount,
			i.numberOfTasks,
			i.description,
			i.taskCode AS glAccount,
			'1' AS taxType,
			(-1 * i.amount) AS amount,
			i.clientCode AS jobId
		";

		$table = "
			invoice i
		";

		$condition = " i.createTime >= '$startDate' AND i.createTime <= '$endDate'";

		$query = "SELECT $fields FROM $table WHERE $condition";

		$result = $this->db->query($query)->result_array();
		return $result;

	}

}