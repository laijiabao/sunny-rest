<?php
class Task_model extends MY_Model {

    public function __construct()
    {
        $this->load->database();
    }

	public function getTasks($param = null)
	{
		$pagination = empty($param['pagination']) ? 1 : $param['pagination'];
		$lang = empty($param['lang']) ? 'ch' : $param['lang'];
		$search = $param['search'];
		//t.bankConfirmDate,
		$query = 'SELECT cl.taxReturnNo, cl.yearEndedDate, tm.value, tm.manHours, tm.taskMagId,    t.taskId, t.taskCode, t.taskType AS taskTypeCode, cp.alias, t.color,';
		if($lang != 'en' && !empty($lang))
			$query .= 't.chTaskName AS taskName,';
		else
			$query .= 't.enTaskName AS taskName,';

		$query .=' (SELECT c.'.$lang.' FROM type_code c WHERE c.code = t.taskType AND c.type = "taskType") AS taskType,';
		$query .=' (SELECT c.'.$lang.' FROM type_code c WHERE c.code = t.reminderStartTime AND c.type = "reminderStartTime") AS reminderStartTime,';
		$query .=' (SELECT c.'.$lang.' FROM type_code c WHERE c.code = t.reminderFrequency AND c.type = "reminderFrequency") AS reminderFrequency';


		$query .= ' FROM task t LEFT JOIN company cp ON t.companyId = cp.companyId INNER JOIN task_management AS tm ON t.taskId = tm.taskId LEFT JOIN client AS c ON tm.clientId = c.clientId LEFT JOIN client_detail AS cl ON tm.clientId =cl.clientId';
		$condition = ' WHERE 1 ';

		if($search != null){
			$condition .= ' AND (t.chTaskName LIKE \'%'.$search.'%\' OR t.enTaskName LIKE \'%'.$search.'%\')';
		}
		// new filter.
		$filterBy = $param['filterBy'];
		if($filterBy != null)
		{
			if ($filterBy == 'taskName') 
			{
				$condition .= " (t.chTaskName = '$search' OR t.enTaskName = '$search')";
			}
			elseif ($filterBy == 'date') 
			{
				$condition .= " (DATE_FORMAT('%Y-%m-%d', t.createTime) = '$search' )";
			}
			elseif ($filterBy == 'status') 
			{
				$condition .= " t.status = '$search' )";
			}
		}

		$query2 = 'SELECT COUNT(*) AS total FROM task t ';
		$query2 .= $condition;

		$condition .= ' ORDER BY t.taskId ASC';
		$condition .= ' LIMIT '.(($pagination - 1) * RECORD_PER_PAGE).','.RECORD_PER_PAGE;
		$query .= $condition;

		$data['total'] = $this->db->query($query2)->row()->total;
		$queryResult = $this->db->query($query);
		$data['data'] = $queryResult->result();
		if (array_key_exists('indicator', $param) && $param['indicator'] == 'TASK LIST REPORT') 
		{
			return $queryResult;
		}
		return $data;

	}

	public function getTaskList_origin($lang = 'ch')
	{
		if($lang != 'en' && !empty($lang))
			$query = 'SELECT chTaskName AS taskName,taskId FROM task GROUP BY chTaskName';
		else
			$query = 'SELECT enTaskName AS taskName,taskId FROM task GROUP BY enTaskName';

		$data['data'] = $this->db->query($query)->result();

		return $data;
	}

	public function getTaskList($lang = 'ch')
	{
		if($lang != 'en' && !empty($lang))
			$query = 'SELECT chTaskName AS taskName,taskId FROM task GROUP BY chTaskName';
		else
			$query = 'SELECT enTaskName AS taskName,taskId FROM task GROUP BY enTaskName';

		$data['data'] = $this->db->query($query)->result();

		return $data;
	}


	public function getTaskDetail($taskId, $lang)
	{

		$query = 'SELECT t.taskId, t.taskCode, cp.companyId, cp.alias AS name, t.taskType AS taskType, t.reminderStartTime AS reminderStartTime, t.reminderFrequency AS reminderFrequency,';
		if($lang != 'en' && !empty($lang))
			$query .= 't.chTaskName AS taskName';
		else
			$query .= 't.enTaskName AS taskName';

		// $query .=' (SELECT c.'.$lang.' FROM type_code c WHERE c.code = t.taskType AND c.type = "taskType") AS taskTypeNameString,';
		// $query .=' (SELECT c.'.$lang.' FROM type_code c WHERE c.code = t.reminderStartTime AND c.type = "reminderStartTime") AS reminderStartTimeString,';
		// $query .=' (SELECT c.'.$lang.' FROM type_code c WHERE c.code = t.reminderFrequency AND c.type = "reminderFrequency") AS reminderFrequencyString';

		$query .= ' FROM task t INNER JOIN company cp ON t.companyId = cp.companyId WHERE t.taskId = ?';
		$data['data'] = $this->db->query($query, $taskId)->result();
		return $data;
	}


	public function createTask($task)
	{

        //Insert Into Database
        $this->db->trans_start();
        $this->db->insert('task',$task);
        $insert_id = $this->db->insert_id();

        if ($this->db->trans_status() === FALSE){
          $this->db->trans_rollback();
          $insert_id = 0;
        } else {
          $this->db->trans_commit();
		}
		
		return $insert_id;
	}

	public function editTask($task)
	{
		//Check 
		if(empty($task['taskId']))
			$this->error(204, 'Task ID cannot be empty');

		$this->db->where('taskId', $task['taskId']);
		$this->db->update('task', $task);

		$result = $this->db->affected_rows();

		return $result;

	}

	public function deleteTasks($tasks)
	{
        $tasks = "'" . str_replace(",", "','", $tasks) . "'";

        $sql = "DELETE FROM task WHERE taskId IN (".$tasks.")";

        $this->db->query($sql);
        $result = $this->db->affected_rows();
		return $result;
	}


	public function assignTask($task)
	{

        $this->db->trans_start();
        $this->db->insert('task_management',$task);
        $insert_id = $this->db->insert_id();

        if ($this->db->trans_status() === FALSE){
          $this->db->trans_rollback();
          $insert_id = 0;
        } else {
          $this->db->trans_commit();
		}
		
		return $insert_id;		
	}

	public function editAssignedTask($task)
	{

		if(empty($task['taskMagId']))
			$this->error(204, 'Task ID cannot be empty');

		$this->db->where('taskMagId', $task['taskMagId']);
		$this->db->update('task_management', $task);

		$result = $this->db->affected_rows();

		return $result;

	}

	public function getAssignedTasks($param = null)
	{

		$pagination = empty($param['pagination']) ? 1 : $param['pagination'];
		$lang = empty($param['lang']) ? 'ch' : $param['lang'];
		$search = $param['search'];
		$clientId = empty($param['clientId']) ? 0 : $param['clientId'];
		$staffId = empty($param['staffId']) ? 0 : $param['staffId'];
		$taskId = empty($param['taskId']) ? 0 : $param['taskId'];
		

		$query = 'SELECT tm.taskMagId, sf.alias AS staffName, tm.startDate, tm.endDate, tm.dueDate, tm.taskAttachments,tm.manHours,st.color AS statusColor, st.statusId, tm.value,tm.lastUpdate,u.userName,';
		if($lang != 'en' && !empty($lang))
			$query .= 'st.chStatusName AS status,';
		else
			$query .= 'st.enStatusName AS status,';
		if($lang != 'en' && !empty($lang))
			$query .= 't.chTaskName AS taskName,';
		else
			$query .= 't.enTaskName AS taskName,';
		// if($lang != 'en' && !empty($lang))
		// 	$query .= 'cl.chClientName AS clientName';
		// else
			$query .= 'cl.enClientName AS clientName';

		$query .= ' FROM task_management tm LEFT JOIN task t ON tm.taskId = t.taskId';
		$query .= ' LEFT JOIN client cl ON tm.clientId = cl.clientId';
		$query .= ' LEFT JOIN staff sf ON tm.staffId = sf.staffId';
		$query .= ' LEFT JOIN status st ON tm.statusId = st.statusId';
		$query .= ' LEFT JOIN user u ON tm.userId = u.userId';
		$condition = ' WHERE 1 ';

		if($search != null){
			$condition .= ' AND (t.chTaskName LIKE \'%'.$search.'%\' OR t.enTaskName LIKE \'%'.$search.'%\'';
			$condition .= ' OR cl.chClientName LIKE \'%'.$search.'%\' OR cl.enClientName LIKE \'%'.$search.'%\' OR cl.clientCode LIKE \'%'.$search.'%\'';
			$condition .= ' OR sf.alias LIKE \'%'.$search.'%\')';
		}

		if(!empty($clientId) && is_numeric($clientId)){
			$condition .= ' AND tm.clientId = '.$clientId;
		}


		$role = $this->session->userdata('userData')['role'];

		if($role == 'A' || $role == 'M'){
			if(!empty($staffId)){
				$condition .= ' AND tm.staffId = '.$staffId;
			}
		}else{
			$staffId = $this->session->userdata('userData')['staffId'];
			$condition .= ' AND tm.staffId = '.$staffId;
		}

		if(is_numeric($staffId) && !empty($staffId)){
			
		}

		if(!empty($taskId) && is_numeric($taskId)){
			$condition .= ' AND tm.taskId = '.$taskId;
		}


		$query2 = 'SELECT COUNT(*) AS total ';
		$query2 .= ' FROM task_management tm LEFT JOIN task t ON tm.taskId = t.taskId';
		$query2 .= ' LEFT JOIN client cl ON tm.clientId = cl.clientId';
		$query2 .= ' LEFT JOIN staff sf ON tm.staffId = sf.staffId';
		$query2 .= ' LEFT JOIN status st ON tm.statusId = st.statusId';
		$query2 .= ' LEFT JOIN user u ON tm.userId = u.userId';
		$query2 .= $condition;

		if($role == 'A' || $role == 'M')
			$condition .= ' ORDER BY FIELD(st.statusId, 3) DESC, FIELD(st.statusId, 4, 5), tm.dueDate ASC';
		else
			$condition .= ' ORDER BY FIELD(st.statusId, 3) DESC, FIELD(st.statusId, 4, 5), tm.dueDate ASC';
			// $condition .= ' ORDER BY FIELD(st.statusId, 3, 4, 5), tm.dueDate ASC';
		$condition .= ' LIMIT '.(($pagination - 1) * RECORD_PER_PAGE).','.RECORD_PER_PAGE;
		$query .= $condition;

		$data['total'] = $this->db->query($query2)->row()->total;
		$data['data'] = $this->db->query($query)->result();

		return $data;		
	}

	public function addAttachment($attachment)
	{
        // $this->db->trans_start();

		$query = "SELECT * FROM task_management WHERE taskMagId = ?";
		$oldAttachment = $this->db->query($query, $attachment['taskMagId'])->row()->taskAttachments;

		$oldAttachment = json_decode($oldAttachment, JSON_UNESCAPED_UNICODE);

		if(!empty($oldAttachment)){

			foreach($oldAttachment as $att){

				$attachment['taskAttachments'][] = $att;
			}
		}
		$attachment['taskAttachments'] = json_encode($attachment['taskAttachments'], JSON_UNESCAPED_UNICODE);

		$this->db->where('taskMagId', $attachment['taskMagId']);
		$this->db->update('task_management', $attachment);

        // if ($this->db->trans_status() === FALSE){
        //   $this->db->trans_rollback();
        // } else {
        //   $this->db->trans_commit();
          $result = $this->db->affected_rows();	
		// }
	

		return $result;	
	}

	public function getAssignedTaskDetail($taskMagId, $lang = 'ch')
	{
		$query = 'SELECT tm.taskMagId, sf.alias AS staffName, sf.staffNo,sf.staffId,cl.clientCode,cl.clientId,cl.chClientName,cl.enClientName, t.taskCode,t.taskId, tm.startDate, tm.endDate, tm.dueDate, tm.taskAttachments,tm.manHours, tm.value,tm.lastUpdate,';

		if($lang != 'en' && !empty($lang))
			$query .= 't.chTaskName AS taskName';
		else
			$query .= 't.enTaskName AS taskName';
		$query .= ' FROM task_management tm LEFT JOIN task t ON tm.taskId = t.taskId';
		$query .= ' LEFT JOIN client cl ON tm.clientId = cl.clientId';
		$query .= ' LEFT JOIN staff sf ON tm.staffId = sf.staffId';
		$query .= ' WHERE taskMagId = ?';

		$data = $this->db->query($query, $taskMagId)->result();
		return $data;

	}


	public function deleteAttachment($taskMagId, $attachment)
	{
		$query = "SELECT * FROM task_management WHERE taskMagId = ?";
		$oldAttachment = $this->db->query($query, $taskMagId)->row()->taskAttachments;

		$oldAttachment = json_decode($oldAttachment, JSON_UNESCAPED_UNICODE);

		if(!empty($oldAttachment)){

			for($i = 0; $i < count($oldAttachment); $i++){

				if($oldAttachment[$i]['formName'] == $attachment['formName']){
					unset($oldAttachment[$i]);
					unlink($attachment['formUrl']);
					break;
				}
			}
		}	
		$attachments['taskAttachments'] = json_encode($oldAttachment, JSON_UNESCAPED_UNICODE);

		$this->db->where('taskMagId', $taskMagId);
		$this->db->update('task_management', $attachments);

        // if ($this->db->trans_status() === FALSE){
        //   $this->db->trans_rollback();
        // } else {
        //   $this->db->trans_commit();
          $result = $this->db->affected_rows();	
		// }
	

		return $result;		
	}

	public function setStatus($taskMagId, $statusId, $userId)
	{
		$this->db->where('taskMagId', $taskMagId);
		$this->db->update('task_management', array('statusId' => $statusId,  'userId' => $userId));

		
		$query = "SELECT staffId FROM task_management WHERE taskMagId = ?";
		$result = $this->db->query($query, $taskMagId)->row()->staffId; 

		return $result;		
	}

	public function deleteAssignedTask($tasks)
	{
		$tasks = "'" . str_replace(",", "','", $tasks) . "'";

        $sql = "DELETE FROM task_management WHERE taskMagId IN (".$tasks.")";

        $this->db->query($sql);
        $result = $this->db->affected_rows();
		return $result;
	}

	public function getDisbursementList(){

		$query = 'SELECT DISTINCT(enTaskDescription), taskCode FROM task WHERE taskCode = 48000';

		$result = $this->db->query($query)->result();
		return $result;
	}

	public function getInvoiceTasks($tasks)
	{

		$tasks = "'" . str_replace(",", "','", $tasks) . "'";

		$query = 'SELECT cl.clientCode, t.taskCode, sf.staffNo, tm.startDate, tm.endDate, tm.dueDate, tm.taskAttachments, tm.value, t.enTaskDescription ';

		$query .= ' FROM task_management tm LEFT JOIN task t ON tm.taskId = t.taskId';
		$query .= ' LEFT JOIN staff sf ON tm.staffId = sf.staffId';
		$query .= ' LEFT JOIN client cl ON tm.clientId = cl.clientId';
		$query .= ' WHERE taskMagId IN ('.$tasks.')';

		$result = $this->db->query($query)->result();

		return $result;

	}

	public function getOngoingTasks(){


		$query = "SELECT tm.taskMagId, sf.alias, t.enTaskName AS taskName, st.chStatusName, st.statusId, tm.createTime, tm.dueDate, tm.manHours, IF(DATEDIFF(tm.dueDate, NOW())>0,DATEDIFF(tm.dueDate, NOW()), 0) AS remainDays FROM task_management tm LEFT JOIN task t ON tm.taskId = t.taskId LEFT JOIN client cl ON tm.clientId = cl.clientId LEFT JOIN staff sf ON tm.staffId = sf.staffId LEFT JOIN status st ON tm.statusId = st.statusId WHERE tm.statusId < 4";

		$role = $this->session->userdata('userData')['role'];
		$staffId = $this->session->userdata('userData')['staffId'];

		if(is_numeric($staffId) && $staffId != 0 && $role != 'A' && $role != 'M'){
			$query .= " AND tm.staffId = $staffId";
		}



		$query .= " ORDER BY FIELD(st.statusId, 3), remainDays";



		$result = $this->db->query($query)->result();
		return $result;

	}


	public function test()
	{
		return $this->db->query('SELECT * FROM test')->result_array();
	}

}