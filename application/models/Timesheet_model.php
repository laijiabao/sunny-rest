<?php
class Timesheet_model extends MY_Model {

    public function __construct()
    {
        $this->load->database();
    }

	public function getTimesheet($param = null)
	{
		$pagination = empty($param['pagination']) ? 1 : $param['pagination'];
		$lang = empty($param['lang']) ? 'ch' : $param['lang'];
		$search = $param['search'];
		$startDate = empty($param['startDate']) ? date("Y-m-d H:i:s") : $param['startDate'];
		$staffId = $param['staffId'];


		$role = $this->session->userdata('userData')['role'];	

		if($role == 'S'){
			$staffId = $this->session->userdata('userData')['staffId'];
		}

		if(empty($staffId) || !is_numeric($staffId))
			$this->error(204, 'Invalid Staff ID');

		if(empty($param['endDate']))
			$endDate = date("Y-m-d H:i:s");
		else{
			$time = strtotime($param['endDate']);
		    $time_p1 = strtotime('+1 Day',$time);
		    $endDate = date('Y-m-d H:i:s',$time_p1);
		} 

		$query = 'SELECT ts.timesheetId, ts.hours, ts.date, ts.isChargeable,ts.leaveType AS leaveTypeCode, t.color,';
		$query .=' (SELECT c.'.$lang.' FROM type_code c WHERE c.code = ts.leaveType AND c.type = "leaveType") AS leaveType,';
		$query .=' ts.taskId, ';
		if($lang != 'en' && !empty($lang))
			$query .= 't.chTaskName AS taskName,';
		else
			$query .= 't.enTaskName AS taskName,';
		$query .=' ts.clientId, cl.enClientName ';
		$query .= ' FROM timesheet ts LEFT JOIN task t ON ts.taskId = t.taskId LEFT JOIN client cl ON ts.clientId = cl.clientId LEFT JOIN staff s ON ts.staffId = s.staffId ';
		$condition = ' WHERE ts.staffId = '.$staffId.' AND date >= \''.$startDate.'\' AND date <= \''.$endDate.'\'';

		if(!empty($search)){
			$condition .= ' AND (ts.chTaskName LIKE \'%'.$search.'%\' OR ts.enTaskName LIKE \'%'.$search.'%\')';
		}

		$query2 = 'SELECT COUNT(*) AS total FROM timesheet ts ';
		$query2 .= $condition;

		$condition .= ' ORDER BY ts.createTime DESC';
		// $condition .= ' LIMIT '.(($pagination - 1) * RECORD_PER_PAGE).','.RECORD_PER_PAGE;
		$query .= $condition;

		$data['total'] = $this->db->query($query2)->row()->total;
		$data['data'] = $this->db->query($query)->result();

		return $data;

	}

	public function createTask($task)
	{

        //Insert Into Database
        $this->db->trans_start();
        $this->db->insert('timesheet',$task);
        $insert_id = $this->db->insert_id();

        if ($this->db->trans_status() === FALSE){
          $this->db->trans_rollback();
          $insert_id = 0;
        } else {
          $this->db->trans_commit();
		}
		
		return $insert_id;
	}

	public function editTask($task)
	{
		//Check 
		if(empty($task['timesheetId']))
			$this->error(204, 'Task ID cannot be empty');

		if(!empty($task['leaveType'])){
			$task['taskId'] = 0;
			$task['clientId'] = 0;
			$task['isChargeable'] = 'N';
		}


		$this->db->where('timesheetId', $task['timesheetId']);
		$this->db->update('timesheet', $task);

		$result = $this->db->affected_rows();

		return $result;

	}

	public function deleteTask($timesheetId)
	{

		if(empty($timesheetId))
			$this->error(204, 'Timesheet ID cannot be empty');

        $sql = "DELETE FROM timesheet WHERE timesheetId = ?";

        $this->db->query($sql, $timesheetId);
        $result = $this->db->affected_rows();
		return $result;
	}	


	public function printTimesheet($param)
	{

		if(empty($param))
			$this->error(204, 'param cannot be empty');

		$startDate = $param['startDate'];
		$endDate = $param['endDate'];
		$staffId = $param['staffId'];

		if(empty($staffId))
			$this->error(204, 'Staff ID cannot be empty');

		if(empty($startDate))
			$this->error(204, 'Start Date cannot be empty');

		if(empty($endDate))
			$this->error(204, 'End Date cannot be empty');

		$_startDate = strtotime($startDate);
		$_endDate = strtotime($endDate);
		$datediff = round(($_endDate - $_startDate) / (60 * 60 * 24));

		if($datediff > 31)
			$this->error(204, 'Please get not more than 1 month records');

		$query1 = 'DROP TABLE IF EXISTS _tmp;';


		//Client Time sheet
		$query2 = 'CREATE TABLE _tmp SELECT cl.enClientName, cl.clientCode,  SUM(ts.hours) AS Total';

		for ($i=0; $i <= $datediff; $i++) { 
			$days = '+'.$i.'day';
			$query2 .= ',SUM(IF(ts.date = "'.date('Y-m-d',strtotime($days,strtotime($startDate))).'", ts.hours, 0)) AS "D'.($i+1).'"';
		}
		$query2 .= ' FROM timesheet ts INNER JOIN CLIENT cl ON ts.clientId = cl.clientId';
    	$query2 .= ' WHERE staffId = '.$staffId.' AND ts.date > "'.$startDate.'" AND ts.date <= "'.$endDate.'" GROUP BY
    cl.clientCode;';

    	$query3 = '(SELECT * FROM _tmp )';
    	$query3 .= 'UNION ALL';

    	//Dummy Row
    	$query3 .= '(SELECT " ", " ", " "';
		for ($i=0; $i <= $datediff; $i++) { 
			$days = '+'.$i.'day';
			$query3 .= '," "';
		}
		$query3 .= ') ';
		$query3 .= 'UNION ALL';

		//Total Chargeable Hours
    	$query3 .= '(SELECT "Total Chargeable Hours", " ", SUM(ts.hours) AS Total';
		for ($i=0; $i <= $datediff; $i++) { 
			$days = '+'.$i.'day';
			$query3 .= ',SUM(IF(ts.date = "'.date('Y-m-d',strtotime($days,strtotime($startDate))).'", ts.hours, 0)) AS "D'.($i+1).'"';
		}
		$query3 .= ' FROM timesheet ts INNER JOIN CLIENT cl ON ts.clientId = cl.clientId';
    	$query3 .= ' WHERE staffId = '.$staffId.' AND ts.date > "'.$startDate.'" AND ts.date <= "'.$endDate.'" AND ts.ischargeable = "Y") ';

    	$query3 .= 'UNION ALL';

    	//Dummy Row
    	$query3 .= '(SELECT " ", " ", " "';
		for ($i=0; $i <= $datediff; $i++) { 
			$days = '+'.$i.'day';
			$query3 .= '," "';
		}
		$query3 .= ') ';
		$query3 .= 'UNION ALL';

		//Total Non-Chargeable Hours
    	$query3 .= '(SELECT "Total Non-Chargeable Hours", " ", SUM(ts.hours) AS Total';
		for ($i=0; $i <= $datediff; $i++) { 
			$days = '+'.$i.'day';
			$query3 .= ',SUM(IF(ts.date = "'.date('Y-m-d',strtotime($days,strtotime($startDate))).'", ts.hours, 0)) AS "D'.($i+1).'"';
		}
		$query3 .= ' FROM timesheet ts INNER JOIN CLIENT cl ON ts.clientId = cl.clientId';
    	$query3 .= ' WHERE staffId = '.$staffId.' AND ts.date > "'.$startDate.'" AND ts.date <= "'.$endDate.'" AND ts.ischargeable = "N") ';

    	$query3 .= 'UNION ALL';

    	//Dummy Row
    	$query3 .= '(SELECT " ", " ", " "';
		for ($i=0; $i <= $datediff; $i++) { 
			$days = '+'.$i.'day';
			$query3 .= '," "';
		}
		$query3 .= ') ';
		$query3 .= 'UNION ALL';

		//Total Hours
    	$query3 .= '(SELECT "Total Hours", " ", SUM(_tmp.Total) AS Total';
		for ($i=0; $i <= $datediff; $i++) { 
			$days = '+'.$i.'day';
			$query3 .= ',SUM(_tmp.D'.($i+1).')';
		}
		$query3 .= ' FROM _tmp);';


		//Standard Hours
		$query4 = 'SELECT "Standard Hours", " ", "TotalHours"';
		for ($i=0; $i <= $datediff; $i++) { 
			$days = '+'.$i.'day';
			// $query4 .= ',8.0 - SUM(IF(ts.date = "'.date('Y-m-d',strtotime($days,strtotime($startDate))).'" AND ts.taskId = 0, ts.hours, 0)) AS "D'.($i+1).'"';
			$date = date('Y-m-d',strtotime($days,strtotime($startDate)));

			$query4 .= ', (IF( WEEKDAY("'.$date.'") > 4, 0, ';
			$query4 .= '(8.0 - SUM(CASE WHEN ts.date = "'.$date.'" AND ts.taskId = 0 AND WEEKDAY("'.$date.'") <= 4
			 THEN ts.hours ELSE 0 END)))) AS "D'.($i+1).'"';
		}
		$query4 .= ' FROM timesheet ts';
		$query4 .= ' WHERE ts.staffId = '.$staffId.' AND ts.date > "'.$startDate.'" AND ts.date <= "'.$endDate.'";';

		// Annual Leave
		$query5 = "SELECT IF(ts.leaveType = 'AL', 'Annual Leave', 'Day Off') AS leaveType, ' ',SUM(ts.hours) AS Total ";
		for ($i=0; $i <= $datediff; $i++) { 
			$days = '+'.$i.'day';
			$query5 .= ',SUM(IF(ts.date = "'.date('Y-m-d',strtotime($days,strtotime($startDate))).'", ts.hours, 0)) AS "D'.($i+1).'"';
		}
		$query5 .= ' FROM timesheet ts ';
    	$query5 .= ' WHERE staffId = '.$staffId.' AND ts.date > "'.$startDate.'" AND ts.date <= "'.$endDate.'" AND ts.leaveType != \'\' GROUP BY
    ts.leaveType = \'AL\' ORDER BY ts.leaveType = \'AL\'';
    	// $query .= 'UNION ALL';

  //   	$query .= '(SELECT "Less: Overtime Hours", " ", (SUM(_tmp.Total) - SUM(B.COST_PRICE)) AS Total';
		// for ($i=0; $i <= $datediff; $i++) { 
		// 	$days = '+'.$i.'day';
		// 	$query .= ',SUM(_tmp.'.($i+1).')';
		// }
		// $query .= ' FROM _tmp);';

		// $query5 = "DROP table _tmp;";

		//Get leave hours
				// SELECT
		//     SUM(IF(ts.date = "2018-04-01" AND ts.taskId = 0, ts.hours,0)) AS D1, 
		//     SUM(IF(ts.date = "2018-04-02" AND ts.taskId = 0, ts.hours,0)) AS D2, 
		//     SUM(IF(ts.date = "2018-04-03" AND ts.taskId = 0, ts.hours,0)) AS D3, 
		//     SUM(IF(ts.date = "2018-04-04" AND ts.taskId = 0, ts.hours,0)) AS D4
		//     ts.date
		// FROM
		//     timesheet ts
		// WHERE
		//     ts.staffId = 7 AND ts.date > "2018-4-1" AND ts.date <= "2018-4-30"


		// SELECT SUM(hours) AS leaveHours, date FROM timesheet ts WHERE ts.staffId = 7 AND ts.date > "2018-4-1" AND ts.date <= "2018-4-30" And ts.taskId = 0 GROUP BY ts.date

		// echo $query1.$query2.$query3.$query4;
		// echo $query4;

		// echo $query5; die();

		$this->db->trans_start();
		$this->db->query($query1);
		$this->db->query($query2);
		$data['timesheet'] = $this->db->query($query3)->result();
		$data['standardHours'] = $this->db->query($query4)->row();
		$data['leaves'] = $this->db->query($query5)->result();

        if ($this->db->trans_status() === FALSE){
          $this->db->trans_rollback();
        } else {
          $this->db->trans_commit();
          return $data;
		}
	}

}
