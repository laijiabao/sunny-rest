<?php
class Code_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }


    public function getCodeList($type = 'all'){

    	if($type == 'all')
    		$query = $this->db->get('type_code');
    	else 
    		$query = $this->db->where('type',$type)->get('type_code');

    	$result['data'] = $query->result();
    	$result['status'] = 200;

    	return $result;
    }

    public function getCodes($types, $lang = 'ch'){

        // $sql = $this->db->where('parent_id',$parent_id)->get('address_code');

        $types = "'" . str_replace(",", "','", $types) . "'";

        if($lang == 'en')
            $sql = 'SELECT codeId, type, code, en AS name FROM type_code WHERE type IN ('.$types.')';
        else
            $sql = 'SELECT codeId, type, code, ch AS name FROM type_code WHERE type IN ('.$types.')';

        $result['data'] = $this->db->query($sql)->result();
        $result['status'] = 200;

        return $result;
    }

    public function getCountries($lang = 'ch')
    {   
        if($lang == 'en')
            $sql = 'SELECT countryId, enName AS name FROM country';
        else
            $sql = 'SELECT countryId, tcName AS name FROM country';

        $countries = $this->db->query($sql)->result();
        return $countries;
    }

    public function getDepartments($lang = 'ch')
    {
        if($lang == 'en')
            $sql = 'SELECT departmentId, enName AS name FROM department';
        else
            $sql = 'SELECT departmentId, tcName AS name FROM department';

        $departments = $this->db->query($sql)->result();
        return $departments;        
    }

    public function getPositions($lang = 'ch')
    {
        if($lang == 'en')
            $sql = 'SELECT positionId, enName AS name FROM position';
        else
            $sql = 'SELECT positionId, tcName AS name FROM position';

        $positions = $this->db->query($sql)->result();
        return $positions;        
    }

    public function getBanks($lang = 'ch')
    {
        if($lang == 'en')
            $sql = 'SELECT bankId, enName AS name FROM bank';
        else
            $sql = 'SELECT bankId, tcName AS name FROM bank';

        $banks = $this->db->query($sql)->result();
        return $banks;        
    }

    public function getCompanies($lang = 'ch'){
        if($lang == 'en')
            $sql = 'SELECT companyId, alias, enName AS name FROM company';
        else
            $sql = 'SELECT companyId, alias, chName AS name FROM company';

        $companies = $this->db->query($sql)->result();
        return $companies;     
    }

    public function getClientGroups(){
            
        $sql = 'SELECT DISTINCT(clientGroup) FROM client';

        $clientGroups = $this->db->query($sql)->result();
        return $clientGroups;             

    }

    public function getClientTypes(){
            
        $sql = 'SELECT DISTINCT(type) FROM client';

        $types = $this->db->query($sql)->result();
        return $types;             

    }

}