<?php
class Notification_model extends MY_Model {

    public function __construct()
    {
        $this->load->database();
    }

	public function getNotifications(){

		$staffId = $this->session->userdata('userData')['staffId'];

		$query = "SELECT taskMagId, staffId, type, chMessage, enMessage,createTime FROM notification WHERE DATEDIFF(NOW(),createTime) < 7 AND staffId = $staffId";

		$result = $this->db->query($query)->result();

		return $result;
	}


	public function createNotification($notification)
	{

        $this->db->trans_start();
        $this->db->insert('notification',$notification);
        $insertId = $this->db->insert_id();

        if ($this->db->trans_status() === FALSE){
          $this->db->trans_rollback();
          $insertId = 0;
        } else {
          $this->db->trans_commit();
		}
		
		return $insertId;
	}

	public function getUnreadNotifications()
	{
		$staffId = $this->session->userdata('userData')['staffId'];
		$query = "SELECT taskMagId, staffId, type, chMessage, enMessage,createTime FROM notification WHERE isRead = 0 AND staffId = ?";

		$result = $this->db->query($query, $staffId)->result();

		$this->db->set('isRead', 1);
		$this->db->where('staffId', $staffId);
		$this->db->update('notification');

		return $result;
	}

	public function checkReminders()
	{
		$query = "SELECT taskMagId, staffId, DATEDIFF(dueDate, NOW()) AS remainingDays FROM task_management WHERE (statusId = 1 OR statusId = 2) AND (DATEDIFF(dueDate, NOW()) = 1 OR DATEDIFF(dueDate, NOW()) = 3 OR DATEDIFF(dueDate, NOW()) = 7 OR DATEDIFF(dueDate, NOW()) = 14)";

		$result = $this->db->query($query)->result();

		return $result;
	}

}