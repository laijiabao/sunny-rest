<?php
class User_model extends MY_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function setLang($userId, $lang){

    	$result = $this->db->set('lang', $lang)->where('userId', $userId)->update('user');

    	if($this->db->affected_rows() == 1)
    		return true;
    	else
    		return false;
    }


    public function createUser($params)
    {
    	$username = $params['username'];
	    $role = $params['role'];
	    $staffId = $params['staffId'];
	    $password = $params['password'];
	    $confirmPassword = $params['confirmPassword'];

	    if(empty($username) || empty($role) || empty($password))
	      $this->error(405,'Invalid Input');


	    $this->load->library('form_validation');
	    $this->load->helper('security');
	    $this->form_validation->set_rules('username','username','trim|required|min_length[3]|trim|xss_clean');
	    $this->form_validation->set_rules('role','Role','trim|required|max_length[1]|trim|xss_clean');
	    $this->form_validation->set_rules('password','Password','trim|required|xss_clean');
	    $this->form_validation->set_rules('confirmPassword','Password Confirm','trim|required|xss_clean|matches[password]');

	    if($this->form_validation->run()){

	      $salt = $this->createEncrypt(6);
	      $password = md5(md5($password).$salt);

	      $user = array(
	        'username'    => $username,
	        'staffId'    => $staffId,
	        'role'    => $role,
	        'password'   => $password,
	        'salt'   => $salt,
	        'token'    => $this->createEncrypt(32),
	        'ip'   => $_SERVER["REMOTE_ADDR"]
	       );
	    }
	    else
      		$this->error(405,$this->form_validation->error_array());
    	if($user == null)
    		$this->error(204, 'Create failed');

        $this->db->insert('user',$user);
        $insertId = $this->db->insert_id();

        if($insertId > 0)
			return $insertId;
		else
			$this->error(204, 'Create failed');
    }


    public function editUser($data = null)
    {
    	$userEntry = array();
		if(empty($data) || empty($data['userId']))
			$this->error(204, 'Edit User Failed');

	    	if(!empty($data['staffId']))
	       		$userEntry['staffId'] =  $data['staffId'];
	       	if(!empty($data['role']))
	        	$userEntry['role'] =  $data['role'];
	       	if(!empty($data['status']))
	        	$userEntry['status'] =  $data['status'];
	        if(!empty($userEntry ))
	       		$this->db->set($userEntry)->where('userId', $data['userId'])->update('user');

	        return $data['userId'];
    }

    public function editPassword($data = null)
    {
		if(empty($data) || empty($data['userId']))
			$this->error(204, 'Edit User Password Failed');

		$userId = $data['userId'];
    	$oldPassword = $data['oldPassword'];
    	$newPassword = $data['newPassword'];
    	$confirmedNewPassword = $data['confirmedNewPassword'];

    	$result = $this->db->select('userId, password, salt')->where('userId', $userId)->get('user')->row();
    	$_oldPassword = $result->password;
    	$_salt = $result->salt;

    	if(empty($result))
    		$this->error(204, 'User Not Found');

    	if(md5(md5($oldPassword).$_salt) != $_oldPassword)
    		$this->error(204, 'Old Password Not Correct');

    	// Type two times of new password.
    	if ($confirmedNewPassword != $newPassword) 
    		$this->error(204, 'New password does not match with the cofirmed one');

    	$salt = $this->createEncrypt(6);
    	$_newPassword = md5(md5($newPassword).$salt);

   		$this->db->set('salt', $salt);
    	$this->db->set('password', $_newPassword);
		$this->db->where('userId', $data['userId']);
		$this->db->update('user');

        return $data['userId'];

    } 


	public function getUsers($param)
	{
		$page = empty($param['page']) ? 1 : $param['page'];
		$search = empty($param['search']) ? null : $param['search'];
		$lang = empty($param['lang']) ? 'ch' : $param['lang'];
		$startDate = empty($param['startDate']) ? '1970-1-1' : $param['startDate'];
		$orderBy = empty($param['orderBy']) ? 'userId' : $param['orderBy'];
		$direction = empty($param['direction']) ? 'DESC' : $param['direction'];
		if(empty($param['endDate']))
			$endDate = date("Y-m-d H:i:s");
		else{
			$time = strtotime($param['endDate']);
		    $time_p1 = strtotime('+1 Day',$time);
		    $endDate = date('Y-m-d H:i:s',$time_p1);
		} 
		$query = 'SELECT u.userId, u.username, u.lastLogin, u.ip, u.status,';
		if($lang == null || $lang == "en")
			$query .= ' r.enName AS role, ';
		else
			$query .= ' r.chName AS role, ';

		$query .=' (SELECT c.'.$lang.' FROM type_code c WHERE c.code = u.status AND c.type = "userStatus") AS statusName';

		$query .= ' FROM user u INNER JOIN role r ON u.role = r.role';

		$condition = ' WHERE u.lastLogin > \''.$startDate.'\' AND u.lastLogin < \''.$endDate.'\'';

		if($search != null){
			$condition .= ' AND u.username LIKE \'%'.$search.'%\'';
		}

		$query2 = 'SELECT COUNT(*) AS total FROM user u INNER JOIN role r ON u.role = r.role';
		$query2 .= $condition;

		$condition .= " ORDER BY u.".$orderBy." $direction";
		$condition .= ' LIMIT '.(($page - 1) * RECORD_PER_PAGE).','.RECORD_PER_PAGE;
		$query .= $condition;
		$data['total'] = $this->db->query($query2)->row()->total;
		$data['list'] = $this->db->query($query)->result();
		return $data;

	}

	public function getRoles($lang)
	{
		if(empty($lang))
			$lang = 'ch';
		if($lang == 'en')
			$result = $this->db->select('role,enName AS name')->get('role')->result();
		else
			$result = $this->db->select('role,chName AS name')->get('role')->result();
		return $result;
	}

	public function deleteUsers($params){
		if (!array_key_exists('ID',$params) || empty($params['ID'])) 
			return 'OK';
		$users = $params['ID'];
		if (is_array($users)) 
			$users = implode(',', $users );
        $users = "'" . str_replace(",", "','", $users) . "'";

        $sql = "UPDATE user SET status = 'D' WHERE userId IN (".$users.")";

        $result = $this->db->query($sql);

        return $result;

	}


}