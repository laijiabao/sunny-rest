<?php
class Status_model extends MY_Model {

    public function __construct()
    {
        $this->load->database();
    }

	public function getStatuses($param)
	{
		$pagination = empty($param['pagination']) ? 1 : $param['pagination'];
		$lang = empty($param['lang']) ? 'ch' : $param['lang'];
		$search = $param['search'];

		$query = 'SELECT s.statusId, s.color,';
		if($lang != 'en' && !empty($lang))
			$query .= 's.chStatusName AS statusName';
		else
			$query .= 's.enStatusName AS statusName';
		$query .= ' FROM status s';
		$condition = ' WHERE 1 ';
		if($search != null){
			$condition .= ' AND (s.chStatusName LIKE \'%'.$search.'%\' OR s.enStatusName LIKE \'%'.$search.'%\')';
		}
		$query2 = 'SELECT COUNT(*) AS total FROM status s ';
		$query2 .= $condition;

		$condition .= ' ORDER BY s.statusId ASC';
		$condition .= ' LIMIT '.(($pagination - 1) * RECORD_PER_PAGE).','.RECORD_PER_PAGE;
		$query .= $condition;


		$data['total'] = $this->db->query($query2)->row()->total;
		$data['data'] = $this->db->query($query)->result();

		return $data;

	}


	public function getStatusDetail($statusId, $lang)
	{

		$query = 'SELECT s.statusId, s.color,';
		if($lang != 'en' && !empty($lang))
			$query .= 's.chStatusName AS statusName';
		else
			$query .= 's.enStatusName AS statusName';
		$query .= ' FROM status s WHERE statusId = ? ';

		// $this->log($query);


		$data['data'] = $this->db->query($query, $statusId)->result();
		return $data;
	}


	public function createStatus($status)
	{

        //Insert Into Database
        $this->db->trans_start();
        $this->db->insert('status',$status);
        $insert_id = $this->db->insert_id();

        if ($this->db->trans_status() === FALSE){
          $this->db->trans_rollback();
          $insert_id = 0;
        } else {
          $this->db->trans_commit();
		}
		
		return $insert_id;
	}

	public function editStatus($status)
	{
		//Check 
		if(empty($status['statusId']))
			$this->error(204, 'Status ID cannot be empty');

		$this->db->where('statusId', $status['statusId']);
		$this->db->update('status', $status);

		$result = $this->db->affected_rows();

		return $result;

	}

	public function deleteStatuses($statuss)
	{
        $statuss = "'" . str_replace(",", "','", $statuss) . "'";

        $sql = "DELETE FROM status WHERE statusId IN (".$statuss.")";

        $this->db->query($sql);
        $result = $this->db->affected_rows();
		return $result;
	}

}