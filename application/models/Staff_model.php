<?php
class Staff_model extends MY_Model {

    public function __construct()
    {
        $this->load->database();
    }

	public function getStaffs($param)
	{
		$pagination = empty($param['pagination']) ? 1 : $param['pagination'];
		$search = empty($param['search']) ? null : $param['search'];
		$lang = empty($param['lang']) ? 'ch' : $param['lang'];
		// Get available date of this month.
		date_default_timezone_set('Australia/Melbourne');
		$today = date('Y-m-d', time());
		$monthEnd = date('Y-m-t',time());
		$remainDay = (strtotime($monthEnd) - strtotime($today))/86400;
		$thisYear = date("Y");

		$query = "SELECT s.salary/8/'$remainDay' AS chargeOutrate, s.staffId, s.staffNo, s.alias,s.status,tmp.totalValue, tmp.totalHours,";
		$query .=' (SELECT c.'.$lang.' FROM type_code c WHERE c.code = s.status AND c.type = "staffStatus") AS statusName,';
		if($lang == null || $lang == "en")
			$query .= ' s.enName AS name ';
		else
			$query .= ' s.chName AS name ';

		$query .= ' FROM staff s LEFT OUTER JOIN (SELECT s.staffId, SUM(IFNULL(tm.value,0)) AS totalValue, SUM(IFNULL(tm.manHours,0)) AS totalHours FROM staff s LEFT JOIN task_management tm ON s.staffId = tm.staffId GROUP BY s.staffId) tmp ON s.staffId = tmp.staffId';
		$condition = ' WHERE 1 ';

		if($search != null){
			$condition .= ' AND (s.chName LIKE \'%'.$search.'%\' OR s.enName LIKE \'%'.$search.'%\' OR s.alias LIKE \'%'.$search.'%\' OR s.staffNo LIKE \'%'.$search.'%\')';
		}

		$query2 = 'SELECT COUNT(*) AS total FROM staff s ';
		$query2 .= $condition;

		$condition .= ' ORDER BY s.staffId DESC';
		$condition .= ' LIMIT '.(($pagination - 1) * RECORD_PER_PAGE).','.RECORD_PER_PAGE;
		$query .= $condition;

		$data['total'] = $this->db->query($query2)->row()->total;
		$data['data'] = $this->db->query($query)->result();

		// Get expenses of each staff.
		$sql1 = "SELECT SUM(tm.value) AS expenses FROM task_management AS tm INNER JOIN staff AS s ON tm.staffId = s.staffId WHERE YEAR(tm.dueDate) = '$thisYear' GROUP BY s.staffId";

		// Get CPD of each staff.
		$sql2 = "SELECT SUM(tm.manHours) AS CPD FROM task_management AS tm INNER JOIN staff AS s ON tm.staffId = s.staffId INNER JOIN task AS t ON t.taskId = tm.taskId WHERE t.enTaskName = 'Continuing Professional Development' GROUP BY s.staffId";
		$data['expenses'] = $this->db->query($sql1)->result();
		$data['CPD'] = $this->db->query($sql2)->result();

		return $data;

	}


	public function getStaff($staffId, $lang = 'ch')
	{

		$query = 'SELECT s.staffId, s.staffNo, s.enName, s.chName, s.alias,s.status, s.lineNo, s.mobileNo, s.gender, s.inTime, s.leaveTime, s.birthday, s.MPFDate, s.remark,';
		$query .= 'TIMESTAMPDIFF(YEAR, s.birthday, CURDATE()) AS age,';
		if($lang == null || $lang == "en")
			$query .= ' d.enName AS department,d.enName AS department, p.enName AS position, c.enName AS country, b.enName AS bank,';
		else
			$query .= ' d.tcName AS department, p.tcName AS position, c.tcName AS country, b.tcName AS bank,';
		$query .= ' s.departmentId, p.positionId, c.countryId, b.bankId, s.bankAccount, s.email, s.directNo, s.pagingNo, s.homeTel, s.faxNo, s.identityNo, s.urgentContactName, s.urgentContactNo, s.enAddress1,s.enAddress2,s.enAddress3,s.chAddress1,s.chAddress2,s.chAddress3 ';
		$query .= ' FROM staff s LEFT OUTER JOIN department d ON s.departmentId = d.departmentId LEFT OUTER JOIN position p ON s.positionId = p.positionId LEFT OUTER JOIN country c ON s.countryId = c.countryId LEFT OUTER JOIN bank b ON s.bankId = b.bankId  ';
		$condition = ' WHERE staffId = '.$staffId;
		$query .= $condition;

		// $this->log($query);

		$data['data'] = $this->db->query($query)->result();
		return $data;
	}

	public function getRoles($lang)
	{
		if(empty($lang))
			$lang = 'ch';
		if($lang == 'en')
			$result = $this->db->select('role,enName AS name')->get('role')->result();
		else
			$result = $this->db->select('role,chName AS name')->get('role')->result();
		return $result;
	}

	public function deleteUsers($users){

        $users = "'" . str_replace(",", "','", $users) . "'";

        $sql = "UPDATE user SET status = 'D' WHERE userId IN (".$users.")";

        $result = $this->db->query($sql);

        return $result;

	}


	public function createStaff($staff)
	{

		//Check Duplication
		$result = $this->db->select('1')->where('staffNo', $staff['staffNo'])->get('staff')->num_rows();

		if($result != 0)
			$this->error(204, 'Duplicated Staff No.!');



        //Insert Into Database
        $this->db->trans_start();
        $this->db->insert('staff',$staff);
        $insert_id = $this->db->insert_id();


		$salt = $this->createEncrypt(6);
		$_password = $this->createEncrypt(8);
		$password = md5(md5($_password).$salt);

		$user = array(
			'userName'    => $staff['alias'].rand(1, 100),
			'staffId' => $insert_id,
			'role'    => 'S',
			'password'   => $password,
			'salt'   => $salt,
			'session'    => $this->createEncrypt(32),
			'ip'   => $_SERVER["REMOTE_ADDR"]
		);  

  		$this->db->insert('user',$user);

        if ($this->db->trans_status() === FALSE){
          $this->db->trans_rollback();
          $insert_id = 0;
        } else {
          $this->db->trans_commit();
		}
		
		return $insert_id;
	}

	public function editStaff($staff)
	{
		//Check 
		if(empty($staff['staffId']))
			$this->error(204, 'Staff ID cannot be empty');

		//Check Duplication
		if(!empty($staff['staffNo'])){
			$result = $this->db->select('staffId, staffNo')->where('staffNo', $staff['staffNo'])->get('staff');
			$rows = $result->num_rows();
			if($rows > 0){
				$staffId = $result->row()->staffId;
				if($staffId != $staff['staffId'])
					$this->error(204, 'Duplicated Staff No.!');
			}
				
		}

		$this->db->where('staffId', $staff['staffId']);
		$this->db->update('staff', $staff);

		$result = $this->db->affected_rows();

		return $result;

	}

	public function deleteStaffs($staffs)
	{
        $staffs = "'" . str_replace(",", "','", $staffs) . "'";

        $sql = "UPDATE staff SET status = 'D' WHERE staffId IN (".$staffs.")";

        $this->db->query($sql);
        $result = $this->db->affected_rows();
		return $result;
	}


	public function restoreStaffs($staffs)
	{
        $staffs = "'" . str_replace(",", "','", $staffs) . "'";

        $sql = "UPDATE staff SET status = 'A' WHERE staffId IN (".$staffs.")";

        $this->db->query($sql);
        $result = $this->db->affected_rows();
		return $result;
	}

	public function getStaffList($param)
	{
		$pagination = empty($param['pagination']) ? 1 : $param['pagination'];
		$search = empty($param['search']) ? null : $param['search'];
		$lang = empty($param['lang']) ? 'ch' : $param['lang'];

		$query = 'SELECT s.staffId, s.staffNo, s.alias,s.status,';
		$query .=' (SELECT c.'.$lang.' FROM type_code c WHERE c.code = s.status AND c.type = "staffStatus") AS statusName,';
		if($lang == null || $lang == "en")
			$query .= ' s.enName AS name ';
		else
			$query .= ' s.chName AS name ';

		$query .= ' FROM staff s';
		$condition = ' WHERE 1 ';

		if($search != null){
			$condition .= ' AND (s.chName LIKE \'%'.$search.'%\' OR s.enName LIKE \'%'.$search.'%\' OR s.alias LIKE \'%'.$search.'%\')';
		}

		$condition .= ' ORDER BY s.staffId DESC';
		$condition .= ' LIMIT '.(($pagination - 1) * RECORD_PER_PAGE).','.RECORD_PER_PAGE;
		$query .= $condition;

		$data['data'] = $this->db->query($query)->result();

		return $data;		
	}

	public function getStaffIdByUserId(){

		$userId = $this->session->userdata('userData')['userId'];

		if(empty($userId))
			return 0;

    	$result = $this->db->select('staffId')->get('staff')->where('userId', $userId)->row()->userId;

    	return $result;

	}

}