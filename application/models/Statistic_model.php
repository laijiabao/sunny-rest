<?php
class Statistic_model extends MY_Model {

    public function __construct()
    {
        $this->load->database();
    }

	public function getRevenue($startDate = null, $endDate = null, $companyId = 0){

		if(empty($endDate))
			$endDate = date('Y-m-d');
		if(empty($startDate))
			$startDate = date('Y-m-d',strtotime($endDate."- 1 year"));

		$query = "SELECT DATE_FORMAT(tm.dueDate,'%Y-%m') AS date, SUM(tm.value) AS revenue FROM task_management tm LEFT JOIN task t ON t.taskId = tm.taskId WHERE 1";

		if(!empty($startDate))
			$query .= " AND tm.dueDate >= '$startDate'";
		if(!empty($endDate))
			$query .= " AND tm.dueDate <= '$endDate'";
		if(!empty($companyId))
			$query .= " AND t.companyId = $companyId";


		$query .= "  GROUP BY MONTH(tm.dueDate) ORDER BY tm.dueDate DESC";

		$data = $this->db->query($query)->result();
		return $data;
	}


	public function getClients($startDate = null, $endDate = null, $companyId = 0){

		if(empty($endDate))
			$endDate = date('Y-m-d');
		if(empty($startDate))
			$startDate = date('Y-m-d',strtotime($endDate."- 1 year"));


		$query = "SELECT DATE_FORMAT(tm.dueDate,'%Y-%m') AS date, COUNT(DISTINCT(tm.clientId)) AS clients FROM task_management tm LEFT JOIN task t ON t.taskId = tm.taskId WHERE 1";

		if(!empty($startDate))
			$query .= " AND tm.dueDate >= '$startDate'";
		if(!empty($endDate))
			$query .= " AND tm.dueDate <= '$endDate'";
		if(!empty($companyId))
			$query .= " AND t.companyId = $companyId";

		$query .= "  GROUP BY MONTH(tm.dueDate) ORDER BY tm.dueDate DESC";


		$data = $this->db->query($query)->result();
		return $data;
	}

	public function getTasks($startDate = null, $endDate = null, $companyId = 0){

		if(empty($endDate))
			$endDate = date('Y-m-d');
		if(empty($startDate))
			$startDate = date('Y-m-d',strtotime($endDate."- 1 year"));

		$query = "SELECT DATE_FORMAT(tm.dueDate,'%Y-%m') AS date, COUNT(tm.taskMagId) AS tasks FROM task_management tm LEFT JOIN task t ON t.taskId = tm.taskId WHERE 1";


		if(!empty($startDate))
			$query .= " AND tm.dueDate >= '$startDate'";
		if(!empty($endDate))
			$query .= " AND tm.dueDate <= '$endDate'";
		if(!empty($companyId))
			$query .= " AND t.companyId = $companyId";

		$query .= "  GROUP BY MONTH(tm.dueDate) ORDER BY tm.dueDate DESC";

		$data = $this->db->query($query)->result();
		return $data;		
	}

	public function getManhours($startDate = null, $endDate = null, $companyId = 0){

		if(empty($endDate))
			$endDate = date('Y-m-d');
		if(empty($startDate))
			$startDate = date('Y-m-d',strtotime($endDate."- 1 year"));

		$query = "SELECT DATE_FORMAT(tm.dueDate,'%Y-%m') AS date, SUM(tm.manHours) AS manHours FROM task_management tm LEFT JOIN task t ON t.taskId = tm.taskId WHERE 1";

		if(!empty($startDate))
			$query .= " AND tm.dueDate >= '$startDate'";
		if(!empty($endDate))
			$query .= " AND tm.dueDate <= '$endDate'";
		if(!empty($companyId))
			$query .= " AND t.companyId = $companyId";

		$query .= "  GROUP BY MONTH(tm.dueDate) ORDER BY tm.dueDate DESC";

		$data = $this->db->query($query)->result();
		return $data;		
	}


	public function getStaffProfit($pagination = 1, $startDate = null, $endDate = null, $companyId = 0){

		if(empty($endDate))
			$endDate = date('Y-m-d');
		if(empty($startDate))
			$startDate = date('Y-m-d',strtotime($endDate."- 1 year"));

		$condition = "tm.dueDate >= '$startDate' AND tm.dueDate <= '$endDate'";
		if(!empty($companyId))
			$condition .= " AND t.companyId = $companyId";



		$subquery = "(SELECT sf.staffNo, sf.alias AS name, SUM(IF($condition, tm.value, 0)) AS value, SUM(IF($condition, tm.manHours, 0)) AS manHours FROM staff sf LEFT JOIN task_management tm ON sf.staffId = tm.staffId LEFT JOIN task t ON t.taskId = tm.taskId GROUP BY sf.staffId) tmp";

		$query = "SELECT staffNo, name, value, manHours, FORMAT(IFNULL(value/manHours,0),2) AS average FROM $subquery ORDER BY IFNULL(value/manHours,0) DESC";

		if(empty($pagination))
			$pagination = 1;

		$query .= ' LIMIT '.(($pagination - 1) * RECORD_PER_PAGE).','.RECORD_PER_PAGE;

		$query2 = "SELECT COUNT(staffId) AS total FROM staff";

		$data['data'] = $this->db->query($query)->result();
		$data['total'] = $this->db->query($query2)->row()->total;
		return $data;		

	}

	public function getTaskProfit($pagination = 1, $startDate = null, $endDate = null, $companyId = 0){


		if(empty($endDate))
			$endDate = date('Y-m-d');
		if(empty($startDate))
			$startDate = date('Y-m-d',strtotime($endDate."- 1 year"));

		$condition = "tm.dueDate >= '$startDate' AND tm.dueDate <= '$endDate'";
		if(!empty($companyId))
			$condition .= " AND t.companyId = $companyId";

		$subquery = "(SELECT t.taskCode, t.enTaskName AS taskName, SUM(IF($condition, tm.value, 0)) AS value, SUM(IF($condition, tm.manHours, 0)) AS manHours FROM task t LEFT JOIN task_management tm ON t.taskId = tm.taskId GROUP BY t.enTaskName) tmp";

		$query = "SELECT taskCode, taskName, value, manHours, FORMAT(IFNULL(value/manHours,0),2) AS average FROM $subquery ORDER BY IFNULL(value/manHours,0) DESC";

		if(empty($pagination))
			$pagination = 1;

		$query .= ' LIMIT '.(($pagination - 1) * RECORD_PER_PAGE).','.RECORD_PER_PAGE;
		$query2 = "SELECT COUNT(taskId) AS total FROM task";

		$data['data'] = $this->db->query($query)->result();
		$data['total'] = $this->db->query($query2)->row()->total;
		return $data;			

	}

	public function getClientProfit($pagination = 1, $startDate = null, $endDate = null, $companyId = 0){

		if(empty($endDate))
			$endDate = date('Y-m-d');
		if(empty($startDate))
			$startDate = date('Y-m-d',strtotime($endDate."- 1 year"));

		$condition = "tm.dueDate >= '$startDate' AND tm.dueDate <= '$endDate'";
		if(!empty($companyId))
			$condition .= " AND t.companyId = $companyId";

		$subquery = "(SELECT cl.clientCode, cl.enClientName AS clientName, SUM(IF($condition, tm.value, 0)) AS value, SUM(IF($condition, tm.manHours, 0)) AS manHours FROM client cl LEFT JOIN task_management tm ON cl.clientId = tm.clientId LEFT JOIN task t ON t.taskId = tm.taskId GROUP BY cl.clientCode) tmp";

		$query = "SELECT clientCode, clientName, value, manHours, FORMAT(IFNULL(value/manHours,0),2) AS average FROM $subquery ORDER BY IFNULL(value/manHours,0) DESC";

		if(empty($pagination))
			$pagination = 1;

		$query .= ' LIMIT '.(($pagination - 1) * RECORD_PER_PAGE).','.RECORD_PER_PAGE;
		$query2 = "SELECT COUNT(clientId) AS total FROM client";

		$data['data'] = $this->db->query($query)->result();
		$data['total'] = $this->db->query($query2)->row()->total;
		return $data;			

	}

	public function getTopNStaffs($n = 10, $startDate = null, $endDate = null, $companyId = 0){

		if(empty($n))
			$n = 10;
		if(empty($endDate))
			$endDate = date('Y-m-d');
		if(empty($startDate))
			$startDate = date('Y-m-d',strtotime($endDate."- 1 year"));

		$condition = "tm.dueDate >= '$startDate' AND tm.dueDate <= '$endDate'";
		if(!empty($companyId))
			$condition .= " AND t.companyId = $companyId";

		$query = "SELECT sf.staffNo, sf.alias AS name, SUM(IF($condition, tm.value, 0)) AS value, SUM(IF($condition, tm.manHours, 0)) AS manHours FROM staff sf LEFT JOIN task_management tm ON sf.staffId = tm.staffId LEFT JOIN task t ON t.taskId = tm.taskId GROUP BY sf.staffId ORDER BY value DESC LIMIT ?";

		$subquery = "(SELECT sf.staffNo, sf.alias AS name, SUM(IF($condition, tm.value, 0)) AS value, SUM(IF($condition, tm.manHours, 0)) AS manHours FROM staff sf LEFT JOIN task_management tm ON sf.staffId = tm.staffId LEFT JOIN task t ON t.taskId = tm.taskId GROUP BY sf.staffId) tmp";

		$query2 = "SELECT staffNo, name, value, manHours, IFNULL(value/manHours,0) AS average FROM $subquery ORDER BY IFNULL(value/manHours,0) DESC LIMIT ?";

		$data['total'] = $this->db->query($query, $n)->result();
		$data['average'] = $this->db->query($query2, $n)->result();

		return $data;		
	}

	public function getTopNClients($n = 10, $startDate = null, $endDate = null, $companyId = 0){

		if(empty($n))
			$n = 10;
		if(empty($endDate))
			$endDate = date('Y-m-d');
		if(empty($startDate))
			$startDate = date('Y-m-d',strtotime($endDate."- 1 year"));

		$condition = "tm.dueDate >= '$startDate' AND tm.dueDate <= '$endDate'";
		if(!empty($companyId))
			$condition .= " AND t.companyId = $companyId";

		$query = "SELECT cl.clientCode AS name, SUM(IF($condition, tm.value, 0)) AS value, SUM(IF($condition, tm.manHours, 0)) AS manHours FROM client cl LEFT JOIN task_management tm ON cl.clientId = tm.clientId LEFT JOIN task t ON t.taskId = tm.taskId GROUP BY cl.clientId ORDER BY value DESC LIMIT ?";

		$subquery = "(SELECT cl.clientCode AS name, SUM(IF($condition, tm.value, 0)) AS value, SUM(IF($condition, tm.manHours, 0)) AS manHours FROM client cl LEFT JOIN task_management tm ON cl.clientId = tm.clientId LEFT JOIN task t ON t.taskId = tm.taskId GROUP BY cl.clientId) tmp";

		$query2 = "SELECT name, value, manHours, IFNULL(value/manHours,0) AS average FROM $subquery ORDER BY IFNULL(value/manHours,0) DESC LIMIT ?";

		$data['total'] = $this->db->query($query, $n)->result();
		$data['average'] = $this->db->query($query2, $n)->result();

		return $data;		
	}

	public function getTopNTasks($n = 10, $startDate = null, $endDate = null, $companyId = 0){

		if(empty($n))
			$n = 10;
		if(empty($endDate))
			$endDate = date('Y-m-d');
		if(empty($startDate))
			$startDate = date('Y-m-d',strtotime($endDate."- 1 year"));

		$condition = "tm.dueDate >= '$startDate' AND tm.dueDate <= '$endDate'";
		if(!empty($companyId))
			$condition .= " AND t.companyId = $companyId";

		$query = "SELECT t.enTaskName AS name, IFNULL(SUM(tm.value),0) AS value, IFNULL(SUM(tm.manHours) ,0) AS manHours, tm.dueDate FROM task t LEFT JOIN task_management tm ON t.taskId = tm.taskId GROUP BY t.enTaskName HAVING tm.dueDate >= '$startDate' AND tm.dueDate <= '$endDate' OR value = 0 ORDER BY value DESC LIMIT ?";

		$subquery = "(SELECT t.enTaskName AS name, SUM(IF($condition, tm.value, 0)) AS value, SUM(IF($condition, tm.manHours, 0)) AS manHours FROM task t LEFT JOIN task_management tm ON t.taskId = tm.taskId GROUP BY t.enTaskName) tmp";

		$query2 = "SELECT name, value, manHours,IFNULL(value/manHours,0) AS average FROM $subquery ORDER BY IFNULL(value/manHours,0) DESC LIMIT ?";

		$data['total'] = $this->db->query($query, $n)->result();
		$data['average'] = $this->db->query($query2, $n)->result();

		return $data;		
	}

}