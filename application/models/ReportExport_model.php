<?php
require_once FCPATH.'vendor/tecnickcom/tcpdf/tcpdf.php';
class ReportExport_model extends MY_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->model("Task_model");
		$this->load->model("Client_model");
	}
	// retrieve task data and send to get_csv().
	public function exportTaskReport($params)
	{
		$paramsForGetTask['search'] = $params['search'];
		$paramsForGetTask['filterBy'] = $params['filterBy'];
		$paramsForGetTask['indicator'] = 'TASK LIST REPORT';
		$tasks = $this->Task_model->getTasks($paramsForGetTask);
		// echo json_encode($tasks->result_array());
		// exit();
		$fileName = 'task';
		$this->get_csv($tasks,$fileName);
	}

	// retrieve task data and send to get_csv().
	public function exportClientReport($params)
	{
		$paramsForGetClient['search'] = $params['search'];
		$paramsForGetClient['filterBy'] = $params['filterBy'];
		$paramsForGetTask['indicator'] = 'CLIENT LIST REPORT';
		$clients = $this->Client_model->getClients($paramsForGetClient);
		$fileName = 'client';
		$this->get_csv($clients,$fileName);
	}

	public function get_csv($params,$fielName)
	{
		$this->load->dbutil();
		$this->load->helper('file');
    		$this->load->helper('download');
   	 	$delimiter = ",";
   		$newline = "\r\n";
		$file_name = $fielName.date("Y-m-d").'.csv';

		$result = $params;
		$data = $this->dbutil->csv_from_result($result);
   		if(!force_download($file_name, $data))
			$this->error(400,'download files failed!');
	}	


}

