<?php
class Auth_model extends MY_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function register($user)
    {
        // new 
        $username = $user['username'];
        $password = $user['password'];
        // Check username and password empty or not
        if(empty($username))
            $this->error(422, $this->lang('error_auth_username_empty'));
        if(empty($password))
            $this->error(422, $this->lang('error_auth_passward_empty'));        

        // Check user existance
        $query = "SELECT 1 FROM user WHERE username = ?";
        $result = $this->db->query($query, $username)->row_array();
        if($result != null)
            $this->error(422, $this->lang->line('error_auth_username_exist'));        

        // Filter unnecessary fields
        $this->fieldsFilteredByEntity($user, 'user');

        // Encrypt password before insert into database
        $user['salt'] = $this->createEncrypt(6);

        $user['password'] = md5(md5($user['password']).$user['salt']);

        // Transaction Start
        $this->db->trans_start();

       // Create User
        $this->db->insert('user', $user);
        $userID = $this->db->insert_id();

        $this->commit($this->lang('error_auth_register_failed'));

        return $userID;
    }

    public function login($username, $password)
    {

        $lastLogin = date('Y-m-d H:i:s');
        $token = $this->createEncrypt(32);
        $flag = $this->db->where('username',$username)->update('user',array('lastLogin' => $lastLogin, 'token' => $token));


        if($flag)
        	$result = $this->db->select('userId, staffId, username, role,token,lang')->from('user')->where('token',$token)->get()->row();
        else 
        	$result = null;

        return $result;


        // new
        if(empty($username))
            $this->error(422, $this->lang->line('error_auth_username_empty'));
        if(empty($password))
            $this->error(422, $this->lang->line('error_auth_passward_empty'));  

        // Check user existance
        $query = "SELECT userId, password, salt FROM user WHERE username = ?";
        $result = $this->db->query($query, $username)->row_array();

        if(empty($result))
            $this->error(422, $this->lang->line('error_auth_username_not_found'));

        // Validate password
        $_password = $result['password'];
        $salt = $result['salt'];
        $password = md5(md5($password).$salt);

        if($_password != $password)
            $this->error(400, $this->lang->line('error_auth_passward_wrong'));

        $user = array();
        $user['lastLogin'] = date('Y-m-d H:i:s');
        $user['token'] = $this->createEncrypt(32);

        // Update login info
        $this->db->trans_start();
        $this->db->where('username',$username);
        $this->db->update('user', $user);
        $result = $this->db->select('userId, staffId, username, role,token,lang')->from('user')->where('token',$token)->get()->row();
        $this->commit($this->lang('error_auth_login_failed'));

        return $result;

    }
    public function auth()
    {
        $token = $this->input->get_request_header('Authorization', TRUE);

        if(empty($token))    
            $this->error(401, $this->lang->line('error_unauthorized'));

        // Select user info 
        $query = "SELECT userId AS ID, username, role, token, lang FROM user WHERE token = ? ";
        $user = $this->db->query($query, $token)->row_array();
        
        // If user not exist
        if(empty($user))    
            $this->error(401, $this->lang->line('error_unauthorized'));

        // If token expired, need to login again
        // $expireTime = $user['expireTime'];
        // if($this->dateDifference($expireTime) > 0)
        //     $this->error(401, $this->lang->line('error_unauthorized'));
        // else{
        //     $expireTime = date("Y-m-d H:i:s", strtotime('+3 day'));  // Token expired 3 days later
        // }

        // $this->db->trans_start();
        // $this->db->where('token',$token);
        // $this->db->update('user', array('expireTime'=> $expireTime));

        // $this->commit($this->lang('error_auth_login_failed'));

        return $user;     
    }
}