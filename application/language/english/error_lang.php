<?php
/**
 * 
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['response_success'] = 'OK';
$lang['response_bad_request'] = 'Bad Request';
$lang['response_Unauthorized'] = 'Unauthorized';
$lang['response_invalid_input'] = 'Invalid Input';

$lang['error_auth_username_empty'] = 'User Name cannot be Empty';
$lang['error_auth_username_notfound'] = 'Cannot Find User';
$lang['error_auth_username_exist'] = 'Existing User';
$lang['error_auth_passward_empty'] = 'Password Cannot be empty';
$lang['error_auth_passward_wrong'] = 'Wrong Password';
$lang['error_auth_login_failed'] = 'Login Failed';
$lang['error_auth_register_failed'] = 'Register Failed';

$lang['error_user_invalid_id'] = 'Invalid User ID';
$lang['error_user_invalid_lang'] = 'Language Not Selected';
$lang['error_user_lang_failed'] = 'Set language Failed';
$lang['error_user_list_failed'] = 'Get User List Failed';