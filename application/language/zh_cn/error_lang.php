<?php
/**
 * 
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['response_success'] = 'OK';
$lang['response_bad_request'] = '错误的调用方法';
$lang['response_Unauthorized'] = '未授权';
$lang['response_invalid_input'] = '错误的输入';

$lang['error_auth_username_empty'] = '用户名不能為空';
$lang['error_auth_username_notfound'] = '找不到用户';
$lang['error_auth_username_exist'] = '用户名已被註册';
$lang['error_auth_passward_empty'] = '密码不能為空';
$lang['error_auth_passward_wrong'] = '密码错误';
$lang['error_auth_login_failed'] = '登录失败';
$lang['error_auth_register_failed'] = '註册失败';

$lang['error_user_invalid_id'] = '错误的用户ID';
$lang['error_user_invalid_lang'] = '未选择语言';
$lang['error_user_lang_failed'] = '设置语言失败';
$lang['error_user_list_failed'] = '获取用户列表失败';
