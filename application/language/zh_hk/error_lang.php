<?php
/**
 * 
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['response_success'] = 'OK';
$lang['response_bad_request'] = '錯誤的調用方法';
$lang['response_Unauthorized'] = '未授權';
$lang['response_invalid_input'] = '錯誤的輸入';

$lang['error_auth_username_empty'] = '用戶名不能為空';
$lang['error_auth_username_notfound'] = '找不到用戶';
$lang['error_auth_username_exist'] = '用戶名已被註冊';
$lang['error_auth_passward_empty'] = '密碼不能為空';
$lang['error_auth_passward_wrong'] = '密碼錯誤';
$lang['error_auth_login_failed'] = '登錄失敗';
$lang['error_auth_register_failed'] = '註冊失敗';

$lang['error_user_invalid_id'] = '錯誤的用戶ID';
$lang['error_user_invalid_lang'] = '未選擇語言';
$lang['error_user_lang_failed'] = '設置語言失敗';
$lang['error_user_list_failed'] = '獲取用戶列表失敗';
