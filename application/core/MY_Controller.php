<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Headers: Authorization,Content-Type,token');
require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Shared\Date;


class MY_Controller extends CI_Controller {
	 public $user;

    function __construct($auth = true)
    {
        parent::__construct();
        
        $this->lang->load('error', 'zh_hk');

        $this->load->library('util');
        $this->load->model('auth_model');

        if($auth){
            $user = $this->auth_model->auth();
            $this->arrayKeysToCamelCase($user);
            $this->user = $user;

            // Block customer role if auth = true
            if($user['role'] == 'C')
                $this->error(403, 'error_permission_denied');

            switch ($user['lang']) {
                case 'en': 
                    $language = 'english';
                    break;
                case 'cn':
                    $language = 'zh_cn';
                    break;
                case 'zh':
                    $language = 'zh_hk';
                    break;
                default:
                    $language = 'zh_hk';
                    break;
            }
        }else{
            $language = "zh_cn";
        }
        
        $this->lang->load('error', $language);

    }

    public function setValidators(array $requirements)
    {
        foreach ($requirements as $field => $validator) {
            $this->form_validation->set_rules($field, ucfirst(strtolower($field)), $validator);
        }
    }

    public function createEncrypt($length = 6)
    {
        return $this->util->createEncrypt($length);
    }

    public function error($statusCode, $message)
    {
        $this->util->error($statusCode, $message);
    }

    public function success($data = null, $message = 'OK')
    {
        $this->util->success($data, $this->lang->line('response_success'));
    }

    public function log($message)
    {
        $this->util->log($message);
    }

    // public function checkPermissions($roles = '', $permissions = '', $dependOnAccessLevel = true)
    // {
    //     // $this->authentication->checkPermissions($this->user, $roles, $permissions, $dependOnAccessLevel);
    //     if (empty($this->user)) {
    //         return true;
    //     }

    //     if (!$this->authentication->checkPermissions($this->user, $roles, $permissions, $dependOnAccessLevel)) {
    //         return $this->error(403, $this->lang->line('error_permission_denied'));
    //     }
    // }

    public function setParams(&$param, array $input, array $mapper = null)
    {
        $this->util->setParams($param, $input, $mapper);
    }

    public function toSnakeCase($input, $case = 'upper')
    {
        return $this->util->toSnakeCase($input, $case);
    }

    public function authenticate()
    {
        $headers = (array) $this->input->request_headers();
        $rs = $this->auth->validateToken($headers);
        if (!$rs) {
            $this->error(403, "Unauthorized");
        } else {
            $this->success(["valid" => true]);
        }
    }

    public function toCamelCase($input)
    {
        return $this->util->toCamelCase($input);
    }

    public function arrayKeysToCamelCase(array &$array)
    {
        $this->util->arrayKeysToCamelCase($array);
    }

    public function arrayKeysToSnakeCase(array &$array, $case = 'upper')
    {
        $this->util->arrayKeysToSnakeCase($array);
    }

    public function lang($key)
    {
        return $this->lang->line($key);
    }

    protected function commit($errorMsg)
    {
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $this->error(400, $errorMsg);
        } else {
            $this->db->trans_commit();
        }
    }

    public function sendEmail($subject, $content, $to, $from = '')
    {
        return $this->email_sender->sendEmail($subject, $content, $to, $from = '');
    }

    public function loadSubject($type)
    {
        return $this->email_sender->loadSubject($type);
    }

    public function loadEmailTemplate($type, $param)
    {
        return $this->email_sender->loadEmailTemplate($type, $param);
    }

    protected function processAttachments(&$param)
    {
        $attachment = array();
        if (array_key_exists('attachment', $param)) {
            $attachment = $param['attachment'];
            unset($param['attachment']);
        }
        return $attachment;
    }

    private function array2csv(array &$array)
    {
        if (count($array) == 0) {
            return null;
        }
        ob_start();
        $df = fopen("php://output", 'w');
        fwrite($df, chr(0xEF).chr(0xBB).chr(0xBF));
        fputcsv($df, array_keys(reset($array)));
        foreach ($array as $row) {
            fputcsv($df, $row);
        }
        fclose($df);
        return ob_get_clean();
    }

    private function download_send_headers($filename) {
        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download  
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header('Content-Type: text/csv;charset=utf-8');
        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
    }

    protected function exportCSV($fileName, $data)
    {
        $this->download_send_headers($fileName);
        echo $this->array2csv($data);
        die();
    }

    protected function processReportHeader(array &$array)
    {
        foreach ($array as $key => $val) {
            //Get snake_case key from camelCase
            // $new_key = $this->toSnakeCase($key);
            $new_key = ucwords(str_replace('_', ' ', strtolower($key)));

            if ((substr($new_key, -2)  === 'Id')) {
                $new_key = substr_replace($new_key, 'ID', -2);
            }

            // if ((substr($new_key, -3)  === 'fyp') || (substr($new_key, -3)  === 'Fyp')) {
            //     $new_key = substr_replace($new_key, 'FYP', -3);
            // }          
            // if ((substr($new_key, -3)  === 'fyc') || (substr($new_key, -3)  === 'Fyc')) {
            //     $new_key = substr_replace($new_key, 'FYC', -3);
            // }
            $array[$new_key] = $val;

            //Unset previous key and value pairs
            if ($new_key != $key) {
                unset($array[$key]);
            }
            
            //Recusively call the function itself if $val is an array
            if (is_array($val)) {
                $array[$new_key] = $this->processReportHeader($val);
            }
        }
        return $array;
    }

    public function checkPermissions($roles, $IDs = null)
    {
        $user = $this->auth_model->auth();

        $this->arrayKeysToCamelCase($user);

        $this->user = $user;

        if($this->user == null || empty($this->user['role']))
            $this->error(403, $this->lang('error_permission_denied'));

        // Convert roles to array if it is not array
        if(!is_array($roles))
            $roles = array($roles);

        // If user role not in array, return error
        if(!in_array($this->user['role'], $roles))
            $this->error(403, $this->lang('error_permission_denied'));

        if(!empty($IDs)){

            // Convert roles to array if it is not array
            if(!is_array($IDs))
                $IDs = array($IDs);        

            // If user ID not in array, return error
            if(!in_array($this->user['ID'], $IDs))
                $this->error(403, $this->lang('error_permission_denied'));
        }
    }

    public function getUser()
    {
        return $this->user;
    }


	// sendNotification.
	public function sendNotification($taskMagId, $staffId, $type, $days = 1){

		$this->load->model('notification_model');
		$user = $this->auth_model->auth();
		$userId = $user['ID'];

		if(empty($userId))
			$userId = 0;
		$lang = $user['lang'];

		if(empty($taskMagId) || empty($staffId) || empty($type)){
			return;
		}

		switch ($type) {

			case 'NT':	//New Task

				$chMessage = "你有新的任務(Task ID: $taskMagId)";
				$enMessage = "You have new task (Task ID: $taskMagId)";
				break;
			case 'ET':	//Edit Task
				$chMessage = "你的任務被修改(Task ID: $taskMagId)";
				$enMessage = "Your task has been edited (Task ID: $taskMagId)";
				break;			
			case 'R':	//Reminder
				$chMessage = "你的任務(Task ID: $taskMagId)離deadline還有 $days 天";
				$enMessage = "Your task(Task ID: $taskMagId) is $days days away from deadline";
				break;		
			case 'TA':	//Task Approved
				$chMessage = "你的任務(Task ID: $taskMagId)已經審批通過";
				$enMessage = "Your task(Task ID: $taskMagId) has been approved";
				break;	
			default:
				$chMessage = '';
				$enMessage = '';
				break;
		}


		$notification['taskMagId'] = $taskMagId;
		$notification['staffId'] = $staffId;
		$notification['userId'] = $userId;
		$notification['chMessage'] = $chMessage;
		$notification['enMessage'] = $enMessage;
		$notification['type'] = $type;

		$this->notification_model->createNotification($notification);


	}

	public function setEmail(){

	}

    protected function exportExcel($fileName, $data, $headers = null)
    {
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

        $row = 1;
        $column = 1;

        if(empty($headers)){
        	$headers = array_keys(reset($data));
        }

        $char = 'A';
        foreach ($headers as $header) {
            // Check to make sure column index is in range
            $last = strlen($char) - 1;
            $byte = ord($char[$last]);
            if ($byte > 59 && $byte < 90) { // Within Range
                $char[$last] = chr($byte + 1);
            } elseif (strlen($char) > 1) {
            } else { // is last element in array and out of range
                for ($i = 0; $i < strlen($char); $i++) {
                    $char[$i] = 'A';
                }
                $char .= 'A';
            }
            // end of checking
            $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow($column, $row, $header);
            $spreadsheet->getActiveSheet()->getColumnDimension($char)->setAutoSize(true);
            $column = $column + 1;
        }

        $row = 2;
        $column = 1;

        foreach ($data as $line) {
            foreach ($line as $value) {
                $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow($column, $row, $value);
                $column = $column + 1;
            }
            $column = 1;
            $row = $row + 1;
        }

        
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=$fileName.xls");
        header('Cache-Control: max-age=0');
        $writer = IOFactory::createWriter($spreadsheet, "Xls");
        $writer->save("php://output");
    }

    public function checkHttpMethod($expectMethod)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != $expectMethod) {
            $this->error(405, $this->lang->line('error_bad_request'));
        }
    }

}
