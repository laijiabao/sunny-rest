<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {

	protected $tableName;
    protected $validationRules = array();

    public function __construct(string $tableName = "")
    {
        $this->tableName = $tableName;
        $this->load->database();
        $this->load->library(array('Util'));
    }


	// public function createEncrypt( $length = 6 ) 
	// { 

	// 	$chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'; 
	// 	$encrypt =''; 
	// 	for ( $i = 0; $i < $length; $i++ ) 
	// 	{ 
	// 		$encrypt .= $chars[ mt_rand(0, strlen($chars) - 1) ]; 
	// 	} 
	// 	return $encrypt; 
	// } 

	public function exitWithError($statusCode, $message)
	{
		echo json_encode(array('status' => $statusCode,'message' => $message),JSON_UNESCAPED_UNICODE);
		exit();
	}

	// public function error($statusCode, $message)
	// {
	// 	echo json_encode(array('status' => $statusCode,'message' => $message),JSON_UNESCAPED_UNICODE);
	// 	exit();
	// }

	// public function success($data = null)
	// {
	// 	$response['status'] = 200;
	// 	$response['message'] = 'Success';
	// 	if($data != null)
	// 		$response['data'] = $data;

	// 	echo json_encode($response);
	// 	exit();
	// }
	
	// public function log($message)
	// {
	// 	if(DEBUG_MODE)
	// 		echo '<br/>'.$message;
	// }
	// ---------------------------------------------------------------------------
	// All new FIT_Methods.
    public function struct()
    {
        $query= $this->db->query("desc $this->tableName");
        return $query->result();
    }

    // ================ General functions ================ //

    public function createEncrypt($length = 6)
    {
        return $this->util->createEncrypt($length);
    }

    public function error($statusCode, $message)
    {
        $this->util->error($statusCode, $message);
    }

    public function success($data = null, $message = 'OK')
    {
        $this->util->success($data, $this->lang->line('success'));
    }
    
    public function log($message)
    {
        $this->util->log($message);
    }

    public function setParams(&$param, array $input, array $mapper = null)
    {
        $this->util->setParams($param, $input, $mapper);
    }

    public function toSnakeCase($input, $case = 'upper')
    {
        return $this->util->toSnakeCase($input, $case);
    }

    public function toCamelCase($input)
    {
        return $this->util->toCamelCase($input);
    }

    public function arrayKeysToCamelCase(array &$array)
    {
        $this->util->arrayKeysToCamelCase($array);
    }

    public function arrayKeysToSnakeCase(array &$array, $case = 'upper')
    {
        $this->util->arrayKeysToSnakeCase($array);
    }
    public function lang($key)
    {
        return $this->lang->line($key);
    }

    // ================ DB related functions ================ //

    public function checkTableExist($tableName)
    {
        if ($this->db->table_exists($tableName) === false) {
            $this->error(400, $tableName);
        }
    }

    protected function checkFieldsExist($tableName, $item)
    {
        // if (empty($item)) {
            // $this->error(422, $this->lang->line('error_invalid_input'), $tableName);
            // $this->error(422, $tableName);
        // }

        $fields = $this->getTableFields($tableName, '', '', false);
        if (is_array($item)) {
            foreach ($item as $key => $value) {
                if (!in_array($key, $fields)) {
                    $this->error(422, "Invalid field $key");
                }
            }
        } elseif (is_string($item)) {
            if (!in_array($item, $fields)) {
                $this->error(422, "Invalid field $item");
            }
        } else {
            // $this->error(422, $this->lang->line('error_invalid_input'), $tableName);
            $this->error(422, $tableName);
        }
    }

    public function getTableFields($tableName)
    {
        $this->checkTableExist($tableName);
        $columns = $this->db->list_fields($tableName);

        return $columns;
    }

    public function getTableFieldsMeta($tableName)
    {
        $this->checkTableExist($tableName);
        return $this->db->field_data($tableName);
    }

    
    /**
     * @param string $tableName     Table name
     * @param string $alias         Table alias
     * @param string/array $omits   Columns in omits will not return
     * @param bool $returnString    If true, implode the columns by ',', otherwise returns columns array
     */
    public function tableColumnsToString($tableName, $alias = '', $omits = '', $returnString = true)
    {
        $this->checkTableExist($tableName);
        $columns = $this->db->list_fields($tableName);

        if (!is_array($columns)) {
            $this->error(422, $this->lang('error_invalid_input'));
        }

        if (!is_array($omits)) {
            $omits = array($omits);
        }

        foreach ($omits as &$o) {
            $o = $this->toSnakeCase($o);
        }

        $columns = array_diff($columns, $omits);

        if (!empty($alias)) {
            foreach ($columns as &$c) {
                $c = $alias.'.'.$c;
            }
        }
        if ($returnString) {
            $columns = implode(',', $columns);
        }

        return $columns;
    }


    protected function getVersion($tableName, $ID)
    {
        $this->checkTableExist($tableName);
        return $this->db->select('VERSION')->from($tableName)->where('ID', $ID)->get()->row()->VERSION;
    }

    public function stripAttributes(array &$item)
    {
        unset($item['ID']);
        unset($item['createTime']);
        unset($item['updateTime']);
        unset($item['createUser']);
        unset($item['entity']);

        unset($item["workFlow"]);
        unset($item["workFlowStep"]);
        unset($item["policyStatus"]);

        return $item;
    }

    /**
     * Map the associated objects to result
     * @param  array  $associate    The Foreign Table
     * @param  array  &$target      The ResultSet
     * @param  string $associateKey Foreign Key
     * @param  string $result       Output Key
     * @param  string $targetKey    Associated Key in ResultSet
     * @return array               Array of Mapped ResultSet
     */
    protected function mapAssociateToTarget(
        array $associate,
        array &$target,
        string $associateKey,
        string $result,
        string $targetKey = 'ID'
    ) {
        $remaining = $associate;
        $associateKey = $this->toSnakeCase($associateKey);

        foreach ($target as &$tar) {
            $rs = array_values(
                array_filter(
                    $remaining,
                    function ($item) use ($associateKey, $targetKey, &$tar) {
                        return $item[$associateKey] === $tar[$targetKey];
                    }
                )
            );
            $tar[$result] = $rs;

            // Filter matched objects to minimize workload
            $remaining = array_udiff($remaining, $rs, function ($a, $b) use ($associateKey) {
                return $a[$associateKey] === $b[$associateKey];
            });
        }
        
        return $target;
    }

    protected function implodeIDs($list, $returnString = true)
    {
        $IDs_array = array_map(function ($item) {
            return $item["ID"];
        }, $list);

        $IDs_array = array_unique($IDs_array);

        if ($returnString) {
            $IDs = implode(',', $IDs_array);
            return $IDs;
        }

        return $IDs_array;
    }

    protected function convertKeyToLang(&$result, $key = null, $universal = '')
    {
        foreach ($result as &$row) {
            if (!empty($key)) {
                $content = $row[$key];
            } else {
                $content = $universal;
            }
            
            $row["CONTENT"] = $this->util->nvsprintf($this->lang($content), $row);
        }

        return $result;
    }

    public function delete($param)
    {
        $ID = $this->processParam($param['ID'], 'ID');
        $userID = $this->getUser()['ID'];

        $this->db->trans_start();

        $query = "UPDATE $this->tableName SET updateUser = $ID, version = 0 WHERE ID IN ($ID)";

        $this->db->query($query);

        $this->commit($this->lang('error_delete'));
        return $ID;
    }

    public function create($param)
    {
        $this->validate();
        $this->stripAttributes($param);
        
        $this->checkFieldsExist($this->tableName, $param);

        $this->db->trans_start();
        $this->insert($this->tableName, $param);

        $ID = $this->db->insert_id();

        $this->commit($this->lang('error_create'));

        return $ID;
    }

    public function edit($param)
    {
        $ID = $this->processParam($param['ID'], 'required', 'ID');
        $this->validate();

        $this->stripAttributes($param);

        $this->checkFieldsExist($this->tableName, $param);

        $this->db->trans_start();

        $this->db->where('ID', $ID);

        $this->update($this->tableName, $param);

        $this->commit($this->lang('error_edit'));

        return $ID;
    }

    public function detail($ID)
    {
        if (empty($ID) || !is_numeric($ID)) {
            $this->error(422, $this->lang('error_invalid_input'));
        }

        $query = "SELECT * FROM ".$this->tableName;

        $condition = " WHERE VERSION >= 1 AND ID = ?";
        $query .= $condition;

        $data = $this->db->query($query, $ID)->row_array();

        if (empty($data)) {
            $this->error(422, $this->lang('error_result_not_found'));
        }

        return $data;
    }

    protected function getScopedAgentID($user, $imploded = true, $getUser = false)
    {
        return $this->authentication->getScopedAgentID($user, $imploded, $getUser);
    }

    
    public function processParam(&$value, $type = '', $fieldName = '')
    {
        $result = $value;

        $typeArray = explode('|', $type);

        $validationRule['field'] = $fieldName;
        $rule = array(
            // "required": false,
            // "minLength": 1,
            // "maxLength": 99999999,
            // "minValue": 0,
            // "maxValue": 99999999,
            // "numeric": false,
            // "decimal": false,
            // "email": false,
            // "regex": ''
        );
        $validationRule['rule'] = $rule;

        foreach ($typeArray as $type) {
            if ($type == 'page') {
                //If page not defined or page is not a number or page is less than 1, set page = 1
                $result = (empty($value) || !is_numeric($value) || $value < 1) ? 1 : $value;
                $rule['numeric'] = true;
            } elseif ($type == 'count') {
                //Limit number of records to 200 per page
                $result = (empty($value) || !is_numeric($value) || $value < 1) ? 10 : $value;
                $result = $result > 200 ? 200 : $result;
                $rule['numeric'] = true;
            } elseif ($type == 'direction') {
                if (strtolower($value) != "asc" && strtolower($value) != "desc") {
                    $result = "";
                }
            } elseif ($type == 'ID') {
                //Parse ID array into string
                if (is_array($value)) {
                    foreach ($value as $v) {
                        if (!is_numeric($v)) {
                            $errorMessage = sprintf($this->lang('error_invalid_number'), $fieldName);
                            $this->error(422, $errorMessage);
                        }
                    }
                    $result = implode(',', $value);
                } else {
                    if (isset($value) && !is_numeric($value)) {
                        $errorMessage = sprintf($this->lang('error_invalid_number'), $fieldName);
                        $this->error(422, $errorMessage);
                    }
                }
            } elseif ($type == 'type') {
                if (is_array($value)) {
                    $result = "'${implode("','", $value)}'";
                } elseif (!empty($value)) {
                    $result = "'$value'";
                }
            } elseif ($type == 'date') {
                if (!strtotime($value) && !empty($value)) {
                    if (!empty($fieldName)) {
                        $errorMessage = sprintf($this->lang('error_invalid_date'), $fieldName);
                        $this->error(422, $errorMessage);
                    } else {
                        $this->error(422, $this->lang('error_invalid_input'));
                    }
                }
            } elseif ($type == 'number') {
                if (!is_numeric($value) && !empty($value)) {
                    if (!empty($fieldName)) {
                        $errorMessage = sprintf($this->lang('error_invalid_number'), $fieldName);
                        $this->error(422, $errorMessage);
                    } else {
                        $this->error(422, $this->lang('error_invalid_input'));
                    }
                }
                $rule['decimal'] = true;
            } elseif ($type == 'integer') {
                if (filter_var($value, FILTER_VALIDATE_INT) === false && !empty($value)) {
                    if (!empty($fieldName)) {
                        $errorMessage = sprintf($this->lang('error_invalid_integer'), $fieldName);
                        $this->error(422, $errorMessage);
                    } else {
                        $this->error(422, $this->lang('error_invalid_input'));
                    }
                }
                $rule['numeric'] = true;
            } elseif ($type == 'email') {
                if (filter_var($value, FILTER_VALIDATE_EMAIL) === false && !empty($value)) {
                    $this->error(422, $this->lang('error_invalid_email'));
                }
                $rule['email'] = true;
            } elseif ($type == 'required') {
                if (!isset($value)) {
                    if (!empty($fieldName)) {
                        $errorMessage = sprintf($this->lang('error_is_required'), $fieldName);
                        $this->error(422, $errorMessage);
                    } else {
                        $this->error(422, $this->lang('error_invalid_input'));
                    }
                }
                $rule['required'] = true;
            } elseif ($type == 'boolean') {
                if (empty($value)) {
                    $result = false;
                } else {
                    $result = ($value === 'true');
                }
            } elseif (substr($type, 0, 4) == 'max-') {
                $tmp = explode('-', $type);
                if (!isset($tmp[1])) {
                    continue;
                }

                $max = $tmp[1];

                if (in_array('number', $typeArray) || in_array('integer', $typeArray) || in_array('decimal', $typeArray)) {
                    $rule['maxValue'] = $max;

                    if($value > $max){
                        if (!empty($fieldName)) {
                            $errorMessage = sprintf($this->lang('error_over_max'), $fieldName, $max);
                            $this->error(422, $errorMessage);
                        } else {
                            $this->error(422, $this->lang('error_invalid_input'));
                        }                      
                    }

                } else {
                    $rule['maxLength'] = $max;

                    if(strlen($value) > $max){
                        if (!empty($fieldName)) {
                            $errorMessage = sprintf($this->lang('error_over_max_length'), $fieldName, $max);
                            $this->error(422, $errorMessage);
                        } else {
                            $this->error(422, $this->lang('error_invalid_input'));
                        }                      
                    }
                }
            } elseif (substr($type, 0, 4) == 'min-') {
                $tmp = explode('-', $type);
                if (!isset($tmp[1])) {
                    continue;
                }

                $min = $tmp[1];

                if (in_array('number', $typeArray) || in_array('integer', $typeArray) || in_array('decimal', $typeArray)) {
                    $rule['minValue'] = $min;

                    if($value < $min){
                        if (!empty($fieldName)) {
                            $errorMessage = sprintf($this->lang('error_below_min'), $fieldName, $min);
                            $this->error(422, $errorMessage);
                        } else {
                            $this->error(422, $this->lang('error_invalid_input'));
                        }                      
                    }

                } else {
                    $rule['minLength'] = $min;

                    if(strlen($value) < $min){
                        if (!empty($fieldName)) {
                            $errorMessage = sprintf($this->lang('error_below_min_length'), $fieldName, $min);
                            $this->error(422, $errorMessage);
                        } else {
                            $this->error(422, $this->lang('error_invalid_input'));
                        }                      
                    }

                }
            }elseif (substr($type, 0, 7) == 'length-') {
                $tmp = explode('-', $type);
                if (!isset($tmp[1])) {
                    continue;
                }
                $length = $tmp[1];

                $rule['length'] = $length;

                if($value == '')
                    continue;

                if(strlen($value) != $length){
                    if (!empty($fieldName)) {
                        $errorMessage = sprintf($this->lang('error_length_must_be'), $fieldName, $length);
                        $this->error(422, $errorMessage);
                    } else {
                        $this->error(422, $this->lang('error_invalid_input'));
                    }                      
                }
            }else {
                $result = empty($value) ? '' : $value;
            }
        }
        $validationRule['rule'] = $rule;

        // check duplication
        $duplicated = false;
        foreach ($this->validationRules as $rule) {
            if ($fieldName == $rule['field']) {
                $duplicated = true;
            }
        }

        if (!$duplicated) {
            $this->validationRules[] = $validationRule;
        }

        return $result;
    }
    public function batchProcessParam(array $requirements, $target)
    {
        foreach ($requirements as $field => $requirement) {
            $tmp = $this->processParam($target[$field], $requirement, $field);
            if(isset($tmp))
             $result[$field] = $tmp;
        }

        return $result;
    }

    protected function processAttachments(&$param)
    {
        $attachment = array();

        if (array_key_exists('attachment', $param)) {
            $attachment = $param['attachment'];
            unset($param['attachment']);
        }
        return $attachment;
    }

    protected function commit($errorMsg)
    {
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $this->error(400, $errorMsg);
        } else {
            $this->db->trans_commit();
        }
    }

    public function fieldsFilteredByEntity($param, $tableName, $isCamelCase = true)
    {
        $tableFields = $this->getTableFields($tableName);



        if($isCamelCase == true){
            foreach ($tableFields as &$field) {
                $field = $this->toCamelCase($field);
            }
        }
        $filtered = array_filter($param, function ($item) use ($tableFields) {
            return in_array($item, $tableFields);
        }, ARRAY_FILTER_USE_KEY);

        return $filtered;
    }

    protected function getPremiumTypeFactor($premiumType)
    {
        switch ($premiumType) {
            case 'A':
            $factor = 1;
            break;
            case 'S':
            $factor = 2;
            break;
            case 'Q':
            $factor = 4;
            break;
            case 'M':
            $factor = 12;
            break;
            default:
            $factor = 1;
            break;
        }
        return $factor;
    }

    protected function unsetColumns(&$array, $toBeUnset)
    {
        if (!is_array($toBeUnset)) {
            $toBeUnset = array($toBeUnset);
        }
        foreach ($toBeUnset as &$key) {
            // echo $array["$key"];
            unset($array["$key"]);
        }

        return $array;
    }

    public function sendEmail($subject, $content, $to, $from = '')
    {
        return $this->email_sender->sendEmail($subject, $content, $to, $from = '');
    }

    public function loadSubject($type)
    {
        return $this->email_sender->loadSubject($type);
    }

    public function loadEmailTemplate($type, $param)
    {
        return $this->email_sender->loadEmailTemplate($type, $param);
    }

    public function validate()
    {
        $validate = $this->input->get('validate');
        if (empty($validate)) {
            return;
        }

        $rules = array_values(array_filter($this->validationRules, function ($item) {
            return !empty($item['field']);
        }));

        $this->success($rules);
    }

    protected function processUploads(&$param, $key)
    {
        $uploads = array();

        if (array_key_exists('attachment', $param)) {
            $attachment = $param['attachment'];
            unset($param['attachment']);
        }
        return $attachment;
    }


    protected function uploadImage($key, $limit = 5, $host = '')
    {
        sleep(1);

        $images = array();

        $dir = 'uploadfile/'.date("Y").'/'.date("md").'/';
        $path = $host.$dir;

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        $config["upload_path"] = $path;
        $config["allowed_types"] = 'gif|jpg|png|jpeg';
        $config['encrypt_name'] = true;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if (isset($_FILES[$key])) {
            $fileCount = count($_FILES[$key]['name']);
            $files = $_FILES[$key];

            if ($fileCount > $limit) {
                $this->error('422', $this->lang('error_too_much_files'));
            }

            if ($fileCount == 0) {
                return '';
            }

            // Single file
            if (!is_array($files['name'])) {
                $_FILES['file']['name']     = $files['name'];
                $_FILES['file']['type']     = $files["type"];
                $_FILES['file']['tmp_name'] = $files['tmp_name'];
                $_FILES['file']['error']    = $files['error'];
                $_FILES['file']['size']     = $files['size'];

                if ($this->upload->do_upload('file')) {
                    $data = $this->upload->data();

                    $image = array();
                    $image['imageUrl'] = $dir.$data["file_name"];
                    $image['imageName'] = $data["orig_name"];
                    $image['imageSize'] = (int)$data["file_size"];
                    $images[] = $image;
                } else {
                    $this->error('422', $this->lang('error_file_upload'));
                }
            } else {
                // Multiple files

                foreach ($files['name'] as $index => $fileName) {
                    $_FILES['file']['name']     = $fileName;
                    $_FILES['file']['type']     = $files["type"][$index];
                    $_FILES['file']['tmp_name'] = $files['tmp_name'][$index];
                    $_FILES['file']['error']    = $files['error'][$index];
                    $_FILES['file']['size']     = $files['size'][$index];

                    if ($this->upload->do_upload('file')) {
                        $data = $this->upload->data();

                        $image = array();
                        $image['imageUrl'] = $dir.$data["file_name"];
                        $image['imageName'] = $data["orig_name"];
                        $image['imageSize'] = (int)$data["file_size"];
                        $images[] = $image;
                    } else {
                        $this->error('422', $this->lang('error_file_upload'));
                    }
                }
            }
        } else {
        }
        return $images;
    }

    protected function insert($table, $param)
    {
        unset($param['ID']);
        unset($param['createTime']);
        unset($param['updateTime']);
        unset($param['createUser']);
        unset($param['updateUser']);

        $ci =& get_instance();

        $param['createUser'] = isset($ci->user['ID']) ? $ci->user['ID'] : 0 ;
        $param['updateUser'] = isset($ci->user['ID']) ? $ci->user['ID'] : 0 ;

        $this->db->insert($table, $param);
        return $this->db->insert_id();
    }

    protected function update($table, $param = null)
    {
        if(empty($param)){
            $ci =& get_instance();
            $this->db->set('updateUser', isset($ci->user['ID']) ? $ci->user['ID'] : 0);
            $this->db->update($table);            
        }else{

            unset($param['updateTime']);
            unset($param['createUser']);
            unset($param['updateUser']);

            $ci =& get_instance();
            
            $param['updateUser'] = isset($ci->user['ID']) ? $ci->user['ID'] : 0;
            $this->db->update($table, $param);
        }
    }

    protected function getUser()
    {
        $ci =& get_instance();
        return $ci->getUser();
    }

    protected function isIdentityValid($identity, $type)
    {
        // Mainland ID card
        if($type == 'M')
            $preg='/^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$/isu';
        elseif($type == 'H')
            $preg='/^[A-MP-Z]{1,2}[0-9]{6}[0-9A]$/isu';
        else
            return false;

        return preg_match($preg,$identity);
    }

    protected function isMobileValid($mobile, $type)
    {
        if($type == 'M')
            $preg='/^1[34578]\d{9}$/ims';
        elseif($type == 'H')
            $preg='/^[1-9]\d{7}$/ims';
        else 
            return false;

        return preg_match($preg,$mobile);

    }

    protected function randomNumbers($length = 6){
        $chars = '0123456789';
        $encrypt ='';
        for ($i = 0; $i < $length; $i++) {
            $encrypt .= $chars[ mt_rand(0, strlen($chars) - 1) ];
        }
        return $encrypt;        
    }

    protected function dateDifference($startDate, $endDate = null)
    {
        if(empty($endDate))
            $endDate = date("Y-m-d");
        return round((strtotime($endDate)-strtotime($startDate))/3600/24);
    }
}
