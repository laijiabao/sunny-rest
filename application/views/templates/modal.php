<!-- </section> -->

<div class="modal fade" id="MD_clientModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <!-- form start -->                
        <div class="box-body">     
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" aria-expanded="true">{{labels.clientList}}</a></li>
              <li class="pull-right"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></li>
            </ul>
            <div class="tab-content">
              <!-- /.tab-pane -->                      
              <div class="tab-pane active">
                <div class="box-header">         
                  <div class="col-xs-12">
                    <div class="input-group">
                      <input type="text" class="form-control" v-model="param.search" v-on:keyup.enter="" id="MD_searchClient">
                      <span class="input-group-btn">
                        <button type="button" class="btn btn-info" v-on:click="search">{{labels.search}}</button>
                      </span>
                    </div>
                    <typeahead v-model="client" target="#MD_searchClient" async-src="/accountant/search/client/" async-key="data" item-key="name" debounce="200"/>   
                  </div>

                </div>
                <div class="box-body">
                  <div class="col-xs-12">
                    <table class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          <th>{{labels.clientId}}</th>
                          <th width="20%">{{labels.clientCode}}</th>
                          <th>{{labels.enClientName}}</th>
                          <th>{{labels.status}}</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr v-for="item in items" v-on:click="selectItem(item)">
                          <td>{{item.clientId}}</td>
                          <td>{{item.clientCode}}</td>
                          <td>{{item.enClientName}}</td>
                          <td>
                            <span class="badge bg-green" v-if="item.status == 'A'">{{item.statusName}}</span>
                            <span class="badge" v-else-if="item.status == 'D'">{{item.statusName}}</span>
                            <span class="badge bg-red" v-else>{{item.statusName}}</span>
                          </td>
                        </tr>   
                      </tbody>

                    </table>
                  </div>
                </div>
                <div class="box-footer clearfix">
                  <div class="col-xs-3">
                    <span>{{paginationPrefix}}<b>{{total}}</b>{{paginationSuffix}}</span>
                  </div>
                  <div class="col-xs-9">
                    <pagination v-model="currentPage" :total-page="totalPage" size="sm" align="right" boundary-links style="margin-top:-20px; margin-bottom:-20px;"/>
                  </div> 
                </div>
              </div>
              <!-- /.tab-pane -->  
            </div>                    
            <!-- /.box-body -->
          </div>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<script>
  
  var MD_clientModal = new Vue({
    el: '#MD_clientModal',
    data: {     
      labels:{
        "search": lang('searchBtn'),
        "clientList": lang('list_client'),
        "clientId": lang('client_clientId'),
        "clientCode": lang('client_clientCode'),
        "chClientName":lang('client_chClientName'),
        "enClientName":lang('client_enClientName'),
        "agencyName":lang('client_agencyName'),
        "status":lang('client_status'),
        "actions":lang('actions')
      },
      paginationPrefix: lang('paginationPrefix'),
      paginationSuffix: lang('paginationSuffix'),
      total: 0,
      totalPage: 1,
      currentPage: 1,
      element: '',
      items: [],
      client:'',
      param: {
        "pagination":1,
        "search":'',
        "lang":SYSTEM_LANGUAGE
      }
    },
    methods:{
      refresh: function(){
        getTableData(this,this,URLS['GET_CLIENTS']);
      },
      search: function(){
        this.param.pagination = 1;
        this.param.search = '';    
      },
      selectItem: function(item){
        console.log(item);

        this.element.onClientSelected(item);
        $('#MD_clientModal').modal('hide');
      },
      show:function(element){
        this.refresh();
        $('#MD_clientModal').modal('show');
        this.element = element;
        
      }
    },
    created:function(){
      // getTableData(this,this,URLS['GET_CLIENTS']);
    },
    watch:{
      currentPage: {
        handler(newValue, oldValue){
          this.param.pagination = newValue;
        },
      },
      param: {
        handler(newValue, oldValue){
          getTableData(this,this,URLS['GET_CLIENTS']);
        },
        deep: true 
      }
    }
  }) 
</script>


<div class="modal fade" id="MD_staffModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <!-- form start -->                
        <div class="box-body">     
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" aria-expanded="true">{{labels.staffList}}</a></li>
              <li class="pull-right"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></li>
            </ul>
            <div class="tab-content">
              <!-- /.tab-pane -->                      
              <div class="tab-pane active">
                <div class="box-header">         
                  <div class="col-xs-12">
                    <div class="input-group">
                      <input type="text" class="form-control" v-model="param.search" v-on:keyup.enter="" id="MD_searchStaff">
                      <span class="input-group-btn">
                        <button type="button" class="btn btn-info" v-on:click="search">{{labels.search}}</button>
                      </span>
                    </div>
                    <typeahead v-model="staff" target="#MD_searchStaff" async-src="/accountant/search/staff/" async-key="data" item-key="name" debounce="200"/>   
                  </div>

                </div>
                <div class="box-body">
                  <div class="col-xs-12">
                    <table class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          <th>{{labels.staffId}}</th>
                          <th width="20%">{{labels.staffNo}}</th>
                          <th>{{labels.alias}}</th>
                          <th>{{labels.status}}</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr v-for="item in items" v-on:click="selectItem(item)">
                          <td>{{item.staffId}}</td>
                          <td>{{item.staffNo}}</td>
                          <td>{{item.alias}}</td>
                          <td>
                            <span class="badge bg-green" v-if="item.status == 'A'">{{item.statusName}}</span>
                            <span class="badge" v-else-if="item.status == 'D'">{{item.statusName}}</span>
                            <span class="badge bg-red" v-else>{{item.statusName}}</span>
                          </td>
                        </tr>
                        
                      </tbody>

                    </table>
                  </div>
                </div>
                <div class="box-footer clearfix">
                  <div class="col-xs-3">
                    <span>{{paginationPrefix}}<b>{{total}}</b>{{paginationSuffix}}</span>
                  </div>
                  <div class="col-xs-9">
                    <pagination v-model="currentPage" :total-page="totalPage" size="sm" align="right" boundary-links style="margin-top:-20px; margin-bottom:-20px;"/>
                  </div> 
                </div>
              </div>
              <!-- /.tab-pane -->  
            </div>                    
            <!-- /.box-body -->
          </div>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<script>
  
  var MD_staffModal = new Vue({
    el: '#MD_staffModal',
    data: {     
      labels:{
        "search": lang('searchBtn'),
        "staffList": lang('list_staff'),
        "staffId": lang('staff_staffId'),
        "staffNo": lang('staff_staffNo'),
        "alias":lang('staff_alias'),
        "status":lang('staff_status')
      },
      paginationPrefix: lang('paginationPrefix'),
      paginationSuffix: lang('paginationSuffix'),
      total: 0,
      totalPage: 1,
      currentPage: 1,
      element: '',
      items: [],
      staff:'',
      param: {
        "pagination":1,
        "search":'',
        "lang":SYSTEM_LANGUAGE
      }
    },
    methods:{
      refresh: function(){
        getTableData(this,this,URLS['GET_STAFFS']);
      },
      search: function(){
        this.param.pagination = 1;
        this.param.search = '';    
      },
      selectItem: function(item){
        console.log(item);

        this.element.onStaffSelected(item);
        $('#MD_staffModal').modal('hide');
      },
      show:function(element){
        this.refresh();
        $('#MD_staffModal').modal('show');
        this.element = element;
        
      }
    },
    created:function(){
      // getTableData(this,this,URLS['GET_STAFFS']);
    },
    watch:{
      currentPage: {
        handler(newValue, oldValue){
          this.param.pagination = newValue;
        },
      },
      param: {
        handler(newValue, oldValue){
          getTableData(this,this,URLS['GET_STAFFS']);
        },
        deep: true 
      }
    }
  }) 
</script>

<div class="modal fade" id="MD_taskModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <!-- form start -->                
        <div class="box-body">     
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" aria-expanded="true">{{labels.taskList}}</a></li>
              <li class="pull-right"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></li>
            </ul>
            <div class="tab-content">
              <!-- /.tab-pane -->                      
              <div class="tab-pane active">
                <div class="box-header">         
                  <div class="col-xs-12">
                    <div class="input-group">
                      <input type="text" class="form-control" v-model="param.search" v-on:keyup.enter="" id="MD_searchTask">
                      <span class="input-group-btn">
                        <button type="button" class="btn btn-info" v-on:click="search">{{labels.search}}</button>
                      </span>
                    </div>
                    <typeahead v-model="task" target="#MD_searchTask" async-src="/accountant/search/task/" async-key="data" item-key="name" debounce="200"/>   
                  </div>

                </div>
                <div class="box-body">
                  <div class="col-xs-12">
                    <table class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          <th>{{labels.taskId}}</th>
                          <th>{{labels.taskCode}}</th>
                          <th>{{labels.taskName}}</th>
                          <th>{{labels.company}}</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr v-for="item in items" v-on:click="selectItem(item)">
                          <td>{{item.taskId}}</td>
                          <td>{{item.taskCode}}</td>
                          <td>{{item.taskName}}</td>
                          <td>{{item.alias}}</td>
                        </tr>
                        
                      </tbody>

                    </table>
                  </div>
                </div>
                <div class="box-footer clearfix">
                  <div class="col-xs-3">
                    <span>{{paginationPrefix}}<b>{{total}}</b>{{paginationSuffix}}</span>
                  </div>
                  <div class="col-xs-9">
                    <pagination v-model="currentPage" :total-page="totalPage" size="sm" align="right" boundary-links style="margin-top:-20px; margin-bottom:-20px;"/>
                  </div> 
                </div>
              </div>
              <!-- /.tab-pane -->  
            </div>                    
            <!-- /.box-body -->
          </div>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<script>
  
  var MD_taskModal = new Vue({
    el: '#MD_taskModal',
    data: {     
      labels:{
        "search": lang('searchBtn'),
        "taskList": lang('list_task'),
        "taskId": lang('task_taskId'),
        "taskCode":lang('task_taskCode'),
        "taskName":lang('task_name'),
        "company":lang('task_company')
      },
      paginationPrefix: lang('paginationPrefix'),
      paginationSuffix: lang('paginationSuffix'),
      total: 0,
      totalPage: 1,
      currentPage: 1,
      element: '',
      items: [],
      task:'',
      param: {
        "pagination":1,
        "search":'',
        "lang":SYSTEM_LANGUAGE
      }
    },
    methods:{
      refresh: function(){
        getTableData(this,this,URLS['GET_TASKS']);
      },
      search: function(){
        this.param.pagination = 1;
        this.param.search = '';    
      },
      selectItem: function(item){
        console.log(item);

        this.element.onTaskSelected(item);
        $('#MD_taskModal').modal('hide');
      },
      show:function(element){
        this.refresh();
        $('#MD_taskModal').modal('show');
        this.element = element;
        
      }
    },
    created:function(){
      // getTableData(this,this,URLS['GET_TASKS']);
    },
    watch:{
      currentPage: {
        handler(newValue, oldValue){
          this.param.pagination = newValue;
        },
      },
      param: {
        handler(newValue, oldValue){
          getTableData(this,this,URLS['GET_TASKS']);
        },
        deep: true 
      }
    }
  }) 

</script>


<div class="modal fade" id="MD_invoiceModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <!-- form start -->                
        <div class="box-body">     
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" aria-expanded="true">{{labels.outputInvoice}}</a></li>
              <li class="pull-right"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></li>
            </ul>
            <div class="tab-content">
              <!-- /.tab-pane -->                      
              <div class="tab-pane active">
                <div class="box-header">   
                  <h4>{{labels.companyInfo}}</h4>      
                  <hr>
                  <div class="row">          
                    <div class="col-xs-3 form-group">
                      <label>{{labels.clientCode}}</label>  
                      <input type="text" class="form-control" v-model="inputs.clientCode" placeholder="" disabled="disabled">
                    </div>
                    <div class="col-xs-3 form-group">
                      <label>{{labels.attn}}</label>  
                      <input type="text" class="form-control" v-model="inputs.attn" placeholder="">
                    </div> 
                  </div> 
                  <div class="row">
                    <div class="col-xs-6 form-group">
                      <label>{{labels.enClientName}}</label>  
                      <input type="text" class="form-control" v-model="inputs.enClientName" placeholder="" disabled="disabled">
                    </div>
                    <div class="col-xs-6 form-group">
                      <label>{{labels.chClientName}}</label>  
                      <input type="text" class="form-control" v-model="inputs.chClientName" placeholder="" disabled="disabled">
                    </div>
                  </div>
                  <div class="row">          
                    <div class="col-xs-4 form-group">
                      <label>{{labels.contactPerson1}}</label>  
                      <input type="text" class="form-control" v-model="inputs.contactPerson1" placeholder="">
                    </div> 
                    <div class="col-xs-6 form-group">
                      <label>{{labels.email1}}</label>  
                      <input type="text" class="form-control" v-model="inputs.email1" placeholder="">
                    </div>
                  </div>
<!--                   <div class="col-xs-12 form-group">
                    <label class="checkbox-inline"><input type="checkbox">Alert with IRD accessment completed</label>
                  </div>   -->
                  <div class="row">          
                    <div class="col-xs-4 form-group">
                      <label>{{labels.contactPerson2}}</label>  
                      <input type="text" class="form-control" v-model="inputs.contactPerson2" placeholder="">
                    </div> 
                    <div class="col-xs-6 form-group">
                      <label>{{labels.email2}}</label>  
                      <input type="text" class="form-control" v-model="inputs.email2" placeholder="">
                    </div> 
                  </div> 
<!--                   <div class="col-xs-12 form-group">
                    <label class="checkbox-inline"><input type="checkbox">Alert with IRD accessment completed</label>
                  </div> -->
                </div>
                <div class="row">          
                  <div class="col-xs-8 form-group">
                    <label>{{labels.company}}</label>  
                    <select class="form-control" v-model="inputs.companyId" >
                      <option v-for="company in companies" v-bind:value="company.companyId">{{company.name}}</option>
                    </select>
                  </div>
                </div>
                <div class="box-body">
                  <h4>{{labels.taskList}}</h4>      
                  <hr>
                  <div class="row">     
                    <div class="col-xs-12">
                      <table class="table table-bordered table-hover">
                        <thead>
                          <tr>
                            <th></th>
                            <th>{{labels.taskName}}</th>
                            <th>{{labels.dueDate}}</th>
                            <th>{{labels.value}}</th>
                            <th>{{labels.assignedTo}}</th>
                            <th>{{labels.status}}</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr v-for="item in items">
                            <td>
                              <input type="checkbox" v-model="selected" :value="item.taskMagId">
                            </td>
                            <td>{{item.taskName}}</td>
                            <td>{{item.dueDate}}</td>
                            <td>{{item.value}}</td>
                            <td>{{item.staffName}}</td>
                            <td>{{item.status}}</td>
                          </tr>                    
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <h4>{{labels.disbursement}}</h4>      
                  <hr>
                  <div class="row">  
                    <div class="col-xs-12">
                      <table class="table table-bordered table-hover">
                        <thead>
                          <tr>
                            <th>{{labels.disbursement}}</th>
                            <th>{{labels.value}}</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr v-for="d in disbursements">
                            <td>
                              <select class="form-control" v-model="d.disbursement">
                                <option v-for="disbursement in disbursementList" v-bind:value="disbursement.enTaskDescription">
                                  {{disbursement.enTaskDescription}}
                                </option>
                              </select>
                            </td>
                            <td><input type="text" class="form-control" v-model="d.value"></td>

                          </tr>
                          <tr>
                            <td colspan="2">
                              <a class="btn btn-warning btn-flat col-xs-12" v-on:click="addDisbursement()"><i class="fa fa-plus"></i></a>
                            </td>
                          </tr>
                        </tbody>                 
                      </table>
                      
                      
                    </div>
                  </div>
                </div>
                <div class="box-footer clearfix">
                <a class="btn btn-success pull-right" v-on:click.prevent="outputInvoice()" :disabled="uploading">{{labels.outputInvoice}}</a>
                </div>
              </div>
              <!-- /.tab-pane -->  
            </div>                    
            <!-- /.box-body -->
          </div>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<script>
  
  var MD_invoiceModal = new Vue({
    el: '#MD_invoiceModal',
    data: {     
      labels:{
        "outputInvoice": lang('client_outputInvoice'),
        "clientCode": lang('client_clientCode'),
        "chClientName":lang('client_chClientName'),
        "enClientName":lang('client_enClientName'),
        "attn":lang('client_attn'),
        "companyNo":lang('client_companyNo'),
        "companyInfo":lang('client_companyInfo'),
        "brNo":lang('client_brNo'),
        "taskList":lang('client_taskList'),
        "disbursement":lang('client_disbursement'),
        "taskName":lang('client_taskName'),
        "dueDate":lang('client_dueDate'),
        "value":lang('client_value'),
        "assignedTo":lang('client_assignedTo'),
        "status":lang('client_status'),
        "contactPerson1":lang('client_contactPerson1'),
        "contactPerson2":lang('client_contactPerson2'),
        "email1":lang('client_email1'),
        "email2":lang('client_email2'),
        "company":lang('client_companyInvoice')
        
      },
      disbursements:[{"disbursement":"", "value":"", "taskCode":"48000"}],
      disbursementList:[],
      items: [],
      inputs: {},
      selected: [],
      companies: [{"companyId":1, "name":"W.K. Pang & Co."}, {"companyId":2, "name":"CHK Corporate Services Limited."}],
      client: '',
      uploading:false
    },
    methods:{
      outputInvoice: function(){

        this.inputs.disbursements = this.disbursements;
        this.inputs.selected = this.selected;

        $.ajax({
          url: HOST + '/accountant/client/outputInvoice/',
          type: 'POST',
          dataType: 'json',
          data:this.inputs,
          timeout:AJAX_TIMEOUT,
          success:function(data)
          {
            if(checkStatus(data) == false)
              return;  

            $("#MD_invoiceModal").modal('hide');
            // MD_invoiceModal.items = data.data.tasks;
            // MD_invoiceModal.inputs = data.data.client;
            // MD_invoiceModal.disbursementList = data.data.disbursementList;

            var url = HOST+'/accountant/'+data.data;
            console.log(url);
            var win = window.open(url, '_blank');

          }
        });  
      },      
      addDisbursement: function(){
        this.disbursements.push({"disbursement":"", "value":"","taskCode":"48000"}); 
      }
      // selectItem: function(item){
      //   console.log(item);

      //   this.element.onClientSelected(item);
      //   $('#MD_clientModal').modal('hide');
      // },
      // show:function(element){
      //   this.refresh();
      //   $('#MD_clientModal').modal('show');
      //   this.element = element;
        
      // }
    },
    created:function(){
      // getTableData(this,this,URLS['GET_CLIENTS']);
    },
    watch:{

    }
  }) 
</script>


<script>
  $("#MD_staffModal #MD_clientModal #MD_taskModal").on("hidden.bs.modal", function () {
    $(".modal-backdrop").fadeOut("fast");
  });
</script>
