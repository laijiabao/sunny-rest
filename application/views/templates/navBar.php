<header class="main-header" id="Nav_Header">
  <!-- Logo -->
  <a href="CMS" class="logo">
    <!-- mini logo for Nav_Sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>{{nav_title_short}}</b></span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>{{nav_title}}</b></span>
  </a>

  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top">
    <a href="#" class="Nav_Sidebar-toggle" data-toggle="push-menu" role="button">
    </a>
    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
      <!-- Notifications: style can be found in dropdown.less -->
      <li class="dropdown notifications-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <i class="fa fa-bell-o"></i>
          <span class="label label-warning">{{notifications.length}}</span>
        </a>
        <ul class="dropdown-menu">
          <li class="header">{{labels.numberOfNotifications}} {{notifications.length}}</li>
          <li>
            <!-- inner menu: contains the actual data -->
            <ul class="menu">              
              <li v-for="notification in notifications">
                <a v-on:click="Nav_Sidebar.onTabClicked('task')">
                  <i v-if="notification.type == 'TA'" class="fa fa-check-circle text-green" ></i> 
                  <i v-else-if="notification.type == 'NT'" class="fa fa-tasks text-info" ></i> 
                  <i v-else-if="notification.type == 'R'" class="fa fa-warning text-red" ></i> 
                    {{notification.chMessage}}
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </li>
      <!-- Tasks: style can be found in dropdown.less -->
      <li class="dropdown tasks-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <i class="fa fa-flag-o"></i>
          <span class="label label-danger">{{tasks.length}}</span>
        </a>
        <ul class="dropdown-menu">
          <li class="header">{{labels.numberOfTasks}} {{tasks.length}}</li>
          <li>
            <!-- inner menu: contains the actual data -->
            <ul class="menu">
              <li v-for="task in tasks"><!-- Task item -->
                <a v-on:click="Nav_Sidebar.onTabClicked('task')">
                  <h3>
                    {{task.taskName}}
                    <small class="pull-right" v-if="task.statusId != 3">{{task.remainDays}} {{labels.hoursLeft}}</small>
                    <small class="pull-right" v-else>{{labels.waitingApproval}}</small>
                  </h3>
                  <div class="progress xs">
                    <progress-bar v-model="task.percentage"  v-bind:type="getProgressBarType(task.remainDays, task.statusId)"/>
                  </div>
                </a>
              </li>
              <!-- end task item -->
            </ul>
          </li>
          <li class="footer">
            <a v-on:click="Nav_Sidebar.onTabClicked('task')">{{labels.viewAll}}</a>
          </li>
        </ul>
      </li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="fa fa-user-o fa-fw"></i>
          </a>
          <ul class="dropdown-menu dropdown-user">
            <li><a href="javascript:void(0)" v-on:click="showEditPassword()"><i class="fa fa-lock fa-fw"></i> Edit Password</a>
            </li>
            <li><a href="javascript:void(0)" onclick="logout()"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
            </li>
          </ul>
          <!-- /.dropdown-user -->
        </li>
        <li>
          <!-- <a href="#" data-toggle="control-Nav_Sidebar"><i class="fa fa-gears"></i></a> -->
        </li>
      </ul>
    </div>
  </nav>
</header>

<div class="modal fade" id="Nav_editPasswordModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <!-- form start -->                
        <div class="box-body">     
          <form role="form">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active"><a href="" data-toggle="tab" aria-expanded="true">{{labels.editPassword}}</a></li>
                <li class="pull-right"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></li>
              </ul>
              <div class="tab-content">
                <!-- /.tab-pane -->                      
                <div class="tab-pane active">
                  <input type="hidden" class="form-control" name="id" v-model="inputs.userId">
                  <!-- <input type="hidden" class="form-control" name="version"> --> 
                  <div class="row">          
                    <div class="col-xs-6 form-group">
                      <label>{{labels.oldPassword}}</label>  
                      <input type="password" class="form-control" name="oldPassword" v-model="inputs.oldPassword">
                    </div> 
                  </div>   
                  <div class="row">          
                    <div class="col-xs-6 form-group">
                      <label>{{labels.newPassword}}</label>  
                      <input type="password" class="form-control" name="newPassword" v-model="inputs.newPassword">
                    </div> 
                  </div>  
                  <div class="row">          
                    <div class="col-xs-6 form-group">
                      <label>{{labels.newPasswordConf}}</label>  
                      <input type="password" class="form-control" name="newPasswordConf" v-model="inputs.newPasswordConf">
                    </div> 
                  </div>                                      
                </div>
                <!-- /.tab-pane -->  
              </div>                    
              <!-- /.box-body -->
            </div>
            <div class="box-footer">
              <button class="btn btn-primary pull-right" v-on:click.prevent="editUserPassword" :disabled="uploading">{{labels.update}}</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<script>
  var Nav_Header = new Vue({
    el: '#Nav_Header',
    data: {
      labels:{
        "numberOfTasks":lang('nav_numberOfTasks'),
        "viewAll":lang('nav_viewAllTask'),
        "hoursLeft":lang('nav_hoursLeft'),
        "waitingApproval": lang('nav_waitingApproval'),
        "numberOfNotifications": lang('nav_numberOfNotifications')
      },
      nav_title_short:lang('nav_title_short'),
      nav_title: lang('nav_title'),
      tasks:[],
      notifications:[],
      timer:''
    },
    methods:{
      updateTaskList:function(){
        $.ajax({
          url: HOST + '/accountant/task/getOngoingTasks/',
          type: 'GET',
          dataType: 'json',
          data:this.inputs,
          timeout:AJAX_TIMEOUT,
          success:function(data)
          {
            if(checkStatus(data) == false)
              return;  

            Nav_Header.tasks = data.data;
            // labels.numberOfTasks = documment.write();

            for(var i = 0; i < Nav_Header.tasks.length; i++){

              var remainManhours = Nav_Header.tasks[i].remainDays * 8;
              var manHours = Nav_Header.tasks[i].manHours;

              var diff = manHours - remainManhours;

              if(remainManhours == 0){
                Nav_Header.tasks[i].percentage = 100;
              }else if(remainManhours > manHours){
                Nav_Header.tasks[i].percentage = 0;
              }else{
                Nav_Header.tasks[i].percentage = 100 - Math.ceil(remainManhours/manHours * 100);

                console.log(remainManhours/manHours); 
              }             
            }

          }
        });
      },
      updateNotificationList:function(){
        $.ajax({
          url: HOST + '/accountant/notification/getNotifications/',
          type: 'GET',
          dataType: 'json',
          timeout:AJAX_TIMEOUT,
          success:function(data)
          {
            if(checkStatus(data) == false)
              return;  

            Nav_Header.notifications = data.data.list;

            var newNotification = data.data.new;

            if(newNotification.length > 0){

              for (var i = 0; i < newNotification.length; i++) {
               
                switch(newNotification[i].type){       
                  case 'ET':
                  case 'TA':
                    Notify.notify('info', lang('notify_info'), newNotification[i].chMessage, 0);
                    break;
                  case 'NT':
                  case 'R':
                    Notify.notify('warning', lang('notify_warning'), newNotification[i].chMessage, 0);
                    break;
                }

                  
              }
            }

          }
        });        
      },
      getNewNotifications:function(){

      },
      getProgressBarType:function(remainDays, statusId){

        if(statusId == 3)
          return 'success';

        if(remainDays < 3)
          return 'danger';
        else if(remainDays < 7)
          return 'warning';
        else 
          return 'info';
      },
      showEditPassword:function(){
        $("#Nav_editPasswordModal").modal('show');
      }

    },
    created: function(){
      this.timer = setInterval(this.updateNotificationList, 60000);
    }
  });
  var Nav_editPasswordModal = new Vue({
    el: '#Nav_editPasswordModal',
    data: {
      labels:{
        "editPassword":lang('user_editPassword'),
        "update": lang('user_update'),
        "oldPassword":lang('user_oldPassword'),
        "newPassword":lang('user_newPassword'),
        "newPasswordConf":lang('user_newPasswordConf')
      },
      inputs:{},
      uploading: false
    },
    methods:{
      editUserPassword: function(){
        this.uploading = true;
        $.ajax({
          url: HOST + '/accountant/user/editPassword/',
          type: 'POST',
          dataType: 'json',
          data: this.inputs,
          timeout:AJAX_TIMEOUT,
          error:function(){
            Nav_editPasswordModal.uploading = false;
          },
          success:function(data)
          {
            Nav_editPasswordModal.uploading = false;
            if(checkStatus(data) == false)
              return;     
            else {
              $("#Nav_editPasswordModal").modal('hide');
              Notify.notify('success', lang('notify_success'), lang('notify_successMessage'));
            }
            Nav_editPasswordModal.inputs = {};
            tableData.refresh(); 
          }
        }); 
      },
    },
    created: function () {
      // initOptions();
    }
  }) 
</script>