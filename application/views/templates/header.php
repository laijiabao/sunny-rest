<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>W.K Pang任務管理系統</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">



  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">

  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/skins/_all-skins.min.css">

  <!-- Calendar -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.css">



  <script src="https://cdn.jsdelivr.net/npm/vue"></script>
  <!-- <script src="https://unpkg.com/vue-router/dist/vue-router.js"></script> -->
  <script src="https://unpkg.com/uiv"></script>
  <!-- <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script> -->
  
  <script src="<?php echo base_url();?>assets/plugins/JavaScript-MD5-master/js/md5.js"></script>

  <script src="<?php echo base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script>
  <script src="<?php echo base_url();?>assets/bower_components/js-cookie/src/js.cookie.js"></script>
  <script src="<?php echo base_url();?>js/lang.js?random=<?php echo uniqid(); ?>"></script>
  <script src="<?php echo base_url();?>js/sidebarMenu.js?random=<?php echo uniqid(); ?>"></script>
  <script src="<?php echo base_url();?>js/functions.js?random=<?php echo uniqid(); ?>"></script>
<!--   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.js"></script> -->
 <!--  <script src="<?php echo base_url();?>assets/bower_components/typeahead.js-master/dist/bloodhound.js"></script>
  <script src="<?php echo base_url();?>assets/bower_components/typeahead.js-master/dist/typeahead.bundle.js"></script>
  <script src="<?php echo base_url();?>assets/bower_components/typeahead/src/bootstrap-typeahead.js"></script> -->
  <!-- <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/jquery-typeahead-2.10.3/src/jquery.typeahead.css"> -->
  <!-- <script src="<?php echo base_url();?>assets/bower_components/jquery-typeahead-2.10.3/src/jquery.typeahead.js"></script> -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <script src="<?php echo base_url();?>assets/bower_components/moment/min/moment.min.js"></script>
  <script src="<?php echo base_url();?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>  
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/twbs-pagination/1.4.1/jquery.twbsPagination.min.js"></script> -->


  <!-- Calendar -->
<!--     <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.js"></script>  
 -->    
  <script src="<?php echo base_url();?>assets/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>  

  <!-- <script src="<?php echo base_url();?>assets/bower_components/jquery-validation-master/dist/jquery.validate.js"></script>   -->
  <!-- <script src="<?php echo base_url();?>assets/bower_components/chart.js/Chart.min.js"></script>   -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>

  <style type="text/css">
    .fileUpload {
      position: relative;
      overflow: hidden;
      margin: 10px;
    }
    .fileUpload input.upload {
      position: absolute;
      top: 0;
      right: 0;
      margin: 0;
      padding: 0;
      font-size: 20px;
      cursor: pointer;
      opacity: 0;
      filter: alpha(opacity=0);
    }
    .btn-default{
      background-color: #ffffff;
    }
    .alert {
      z-index: 5000;
    }

  </style>


  <link rel="stylesheet"
  href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">    