<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header" id="contentHeader">
    <h1 id="header">
      {{header}}
    </h1>
<!--     <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li v-for="item in breadcrumb"><a href="#" v-on:click="sidebar.onTabClicked(item)"> {{item}}</a></li>
    </ol> -->
  </section>   
  <!-- <div id="Notify"></div> -->


  <section class="content" id="content">
  </section>
  <div id="Notify" style="z-index:-1;position:relative;"></div>

</div>
<div class="control-sidebar-bg"></div>

<script>
  var contentHeader = new Vue({
    el: '#contentHeader',
    data: {
      header: 'Dashboard',
      breadcrumb: ['dashboard']  
    }
  })

  var pageContent = new Vue({
    el: '#content',
    data: {
      content:''
    },
    created: function(){

    }
  })

  var Notify = new Vue({
    el: '#Notify',
    data: {
    },
    methods:{
      notify:function(type, title, content = '', duration = 5000){

        if(type != 'success' && type != 'info' && type != 'danger' && type != 'warning')
          return;
        this.$notify({
          type: type,
          title: title,
          content: content,
          duration: duration,
          offsetY: 80  

        })
      }
    }

  });

</script>

<script>
  $(document).ready(function(){

    var page = "<?php echo $page?>";

    Nav_Sidebar.onTabClicked(page);

    console.log("page = "+page);

    window.addEventListener('popstate', function(e) {

      if(Nav_Sidebar.stack.length > 1){
        Nav_Sidebar.stack.pop();
        console.log(Nav_Sidebar.stack);
        var lastPageIndex = Nav_Sidebar.stack.length - 1;
        Nav_Sidebar.popState(Nav_Sidebar.stack[lastPageIndex]);
      }
    }); 

    Nav_Header.updateTaskList();
    Nav_Header.updateNotificationList();

    // $("#content").load("CMS/view/dashboard");

    function logout(){
      self.location = "<?php echo base_url();?>admin";
    }

  });
</script>
