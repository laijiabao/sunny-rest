
<script type="text/javascript">
  // function loadContent(content,e){
  //   // $("#content").hide();
  //   $("#content").html("");
  //   $("#content").load("CMS/view/"+content,'',function(){

  //     $("#header").html(e.find("span").html());
  //     $("#content").fadeIn();
  //     $("#breadcrumb > li:last").remove();
  //     if(e.find("span").html())
  //       $("#breadcrumb").append('<li class="active">'+e.find("span").html()+'</li>');
  //   });
  // }

  function logout(){
    self.location = "<?php echo base_url();?>admin";

    $.ajax({
      url: '<?php echo base_url();?>auth/logout',
      type: 'GET',
      dataType: 'json',
      success: function (data) {
      }

    });     
  }

  var menuArray = getMenus();


  function changeLang(lang){

    if(lang == SYSTEM_LANGUAGE)
      return;

    var userId = Cookies.get("userId");

    $.ajax({
      url: '<?php echo base_url();?>user/setLang/'+userId+'/'+lang,
      type: 'GET',
      dataType: 'json',
      success: function (data) {
        if(data.status == 200){
          Cookies.set('lang',lang);
          location.reload();
        }
      }         
    }); 
  }

</script>

<aside class="main-sidebar" id="Nav_Sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <!-- search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header"></li>
      <li v-if="menuArray.indexOf('dashboard') >= 0" v-bind:class="[activeTab == 'dashboard' ? 'active' : '']">
        <a href="javascript:void(0)" v-on:click="onTabClicked('dashboard')" >
          <i class="fa fa-dashboard"></i> <span>{{menu.dashboard}}</span>
        </a>
      </li>
      <li v-if="menuArray.indexOf('user') >= 0" v-bind:class="[activeTab == 'user' ? 'active' : '']">
        <a href="javascript:void(0)" v-on:click="onTabClicked('user')">
          <i class="fa fa-user-plus"></i> <span>{{menu.user}}</span>
        </a>
      </li>
      <li v-if="menuArray.indexOf('staff') >= 0" v-bind:class="[activeTab == 'staff' ? 'active' : '']">
        <a href="javascript:void(0)" v-on:click="onTabClicked('staff')">
          <i class="fa fa-user"></i> <span>{{menu.staff}}</span>
        </a>
      </li>
      <li v-if="menuArray.indexOf('task') >= 0" v-bind:class="[activeTab == 'task' ? 'active' : '']">
        <a href="javascript:void(0)" v-on:click="onTabClicked('task')">
          <i class="fa fa-tasks"></i> <span>{{menu.task}}</span>
        </a>
      </li>
      <li v-if="menuArray.indexOf('timesheet') >= 0" v-bind:class="[activeTab == 'timesheet' ? 'active' : '']">
        <a href="javascript:void(0)" v-on:click="onTabClicked('timesheet')">
          <i class="fa fa-clock-o"></i> <span>{{menu.timesheet}}</span>
        </a>
      </li>
      <li v-if="menuArray.indexOf('client') >= 0" v-bind:class="[activeTab == 'client' ? 'active' : '']">
        <a href="javascript:void(0)" v-on:click="onTabClicked('client')">
          <i class="fa fa-dollar"></i> <span>{{menu.client}}</span>
        </a>
      </li>
      <li v-if="menuArray.indexOf('status') >= 0" v-bind:class="[activeTab == 'status' ? 'active' : '']">
        <a href="javascript:void(0)" v-on:click="onTabClicked('status')">
          <i class="fa  fa-files-o"></i> <span>{{menu.status}}</span>
        </a>
      </li>
      <li v-if="menuArray.indexOf('form') >= 0" v-bind:class="[activeTab == 'form' ? 'active' : '']">
        <a href="javascript:void(0)" v-on:click="onTabClicked('form')">
          <i class="fa fa-edit"></i> <span>{{menu.form}}</span>
        </a>
      </li>
      <li v-if="menuArray.indexOf('system') >= 0" v-bind:class="[activeTab == 'system' ? 'active' : '','treeview']">
        <a href="#">
          <i class="fa fa-gear"></i> <span>{{menu.system}}</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="javascript:void(0)" onclick="changeLang('ch')" ><span>中文</span></a></li>
          <li><a href="javascript:void(0)" onclick="changeLang('en')"><span>English</span></a></li>
        </ul>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>

<script type="text/javascript">

  // const router = new VueRouter({
  //   routes: [
  //     { path: '/accountant/CMS/:tab' }
  //   ]
  // })



  var Nav_Sidebar = new Vue({
    el: '#Nav_Sidebar',
    // router,
    data: {
      activeTab:'dashboard',
      stack:[],
      menu:{
        "dashboard":lang('menu_dashboard'),
        "user": lang('menu_user'),
        "task":lang('menu_task'),
        "timesheet":lang('menu_timesheet'),
        "client":lang('menu_client'),
        "status":lang('menu_status'),
        "form":lang('menu_form'),
        "staff":lang('menu_staff'),
        "system":lang('menu_system')
      }    
    },
    methods:{
      onTabClicked: function(content){
        this.activeTab = content;
        contentHeader.header = this.menu[content];
        contentHeader.breadcrumb.pop();
        contentHeader.breadcrumb.push(content);

        window.history.pushState(null, null, '/accountant/CMS/'+content);
        this.stack.push(content);

        $("#content").html("");
        $("#content").load("/accountant/CMS/view/"+content,'',function(){
          $("#content").fadeIn();
        });
      },
      popState: function(content){
        this.activeTab = content;
        contentHeader.header = this.menu[content];
        contentHeader.breadcrumb.pop();
        contentHeader.breadcrumb.push(content);

        $("#content").html("");
        $("#content").load("/accountant/CMS/view/"+content,'',function(){
          $("#content").fadeIn();
        });
      }
    }





  })

</script>
