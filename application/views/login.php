<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>W.K Pang任務管理系統 | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/iCheck/square/blue.css">

  <script src="<?php echo base_url();?>assets/bower_components/MD5/js/md5.js"></script>
  <script src="<?php echo base_url();?>assets/bower_components/js-cookie/src/js.cookie.js"></script>

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
  <div class="login-box">
    <div class="login-logo">
      W.K Pang任務管理系統
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">

      <form action="" method="post">
        <div class="form-group has-feedback">
          <input type="text" class="form-control" placeholder="User Name" id="userName">
          <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <input type="password" class="form-control" placeholder="Password" id="password">
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="text-danger" style="display: none" id="error"></div>
        <div class="row">
          <div class="col-xs-8">
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat" id="login">登入</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url();?>assets/plugins/iCheck/icheck.min.js"></script>

<script>

  $(document).ready(function(){

    $("#login").click(function(event) {
      event.preventDefault(); 

      $.ajax({
        url: '<?php echo base_url();?>auth/login',
        type: 'POST',
        data: {"userName":$("#userName").val(),"password":$("#password").val()},
        dataType: 'json',
        success: function (data) {
          if(data.status == 200){
              //success

              // Cookies.set('admin_id', data.admin_id);
              // Cookies.set('admin_token', data.token);

              // console.log(data);
              // SYSTEM_LANGUAGE = data.data.lang;
              // console.log(SYSTEM_LANGUAGE);
              self.location = "<?php echo base_url();?>CMS";
              Cookies.set('lang', data.data.lang);
              Cookies.set('userId', data.data.userId);
              Cookies.set('role', data.data.role);
              // console.log("<?php echo base_url();?>CMS");


            }
            else{

              // if(data.status == 204){
                $("#error").html('Error: '+data.message);
                $("#error").show();
              // }

            }
          }
          
        });       
    });

  });

</script>
</body>
</html>
