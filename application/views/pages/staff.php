

<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">     
        <div class="pull-left">
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createModal">
            <i class="fa fa-plus"></i>
          </button>
          <button type="button" class="btn btn-danger" onclick="tableData.deleteStaffs()">
            <i class="fa fa-trash"></i>
          </button>
        </div>    
        <div class="col-xs-5" id="searchBar">
          <div class="input-group">
            <input type="text" class="form-control" v-model="search" v-on:keyup.enter="searchUser">
            <span class="input-group-btn">
              <button type="button" class="btn btn-info" v-on:click="searchUser">{{searchBtn}}</button>
            </span>
          </div>
        </div>
      </div>          

      <!-- /.box-header -->
      <div class="box-body">
        <table id="table" class="table table-bordered table-hover">

          <thead>
            <tr>
              <th><input type="checkbox" :checked="false" v-model="selectAll"></th>
              <th>{{labels.staffId}}</th>
              <th>{{labels.staffNo}}</th>
              <th>{{labels.name}}</th>
              <th>{{labels.alias}}</th>
              <th>{{labels.totalManHours}}</th>
              <th>{{labels.totalValues}}</th>
              <th>{{labels.status}}</th>
              <th>{{labels.actions}}</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="item in items">
              <td>
                <input type="checkbox" v-model="selected" :value="item.staffId">
              </td>
              <td>{{item.staffId}}</td>
              <td>{{item.staffNo}}</td>
              <td>{{item.name}}</td>
              <td>{{item.alias}}</td>
              <td>{{item.totalHours}}</td>
              <td>{{item.totalValue}}</td>
              <td>
                <span class="badge bg-green" v-if="item.status == 'A'">{{item.statusName}}</span>
                <span class="badge" v-else-if="item.status == 'D'">{{item.statusName}}</span>
                <span class="badge bg-red" v-else>{{item.statusName}}</span>
              </td>
              <td>
                <div class="">
                  <!-- <a class="btn-xs" v-on:click="viewItem(item.staffId)"><i class="fa fa-eye"></i></a> -->
                  <a class="btn-xs" v-on:click="editForm.getStaffDetail(item.staffId)"><i class="fa fa-edit"></i></a>
                  <a v-if="item.status == 'D'" class="btn-xs" v-on:click="restoreItem(item.staffId)"><i class="fa fa-refresh"></i></a>
                  <a v-else class="btn-xs" v-on:click="deleteItem(item.staffId)"><i class="fa fa-trash"></i></a>

                </div>
              </td>
            </tr>
            
          </tbody>

        </table>
      </div>
      <div class="box-footer clearfix" id="tableFooter">
        <div class="col-xs-3">
          <span>{{paginationPrefix}}<b>{{total}}</b>{{paginationSuffix}}</span>
        </div>
        <div class="col-xs-9">
          <pagination v-model="currentPage" :total-page="totalPage" size="sm" align="right" boundary-links style="margin-top:-20px; margin-bottom:-20px;"/>
        </div> 
      </div>
    </div>
    <!-- /.box -->
  </div>
</div>
<!-- /.row -->


<div class="modal fade" id="createModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <!-- form start -->                
        <div class="box-body">     
          <form role="form" id="createForm">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#c_tab_1" data-toggle="tab" aria-expanded="true" id="c_n_tab_1">{{labels.new}}</a></li>
                <li class="pull-right"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></li>
              </ul>
              <div class="tab-content" id="c_tab_content">
                <!-- /.tab-pane -->                      
                <div class="tab-pane active" id="c_tab_1">
                  <h4>{{labels.basic}}</h4>
                  <br />
                  <!-- <input type="hidden" class="form-control" name="id"> -->
                  <!-- <input type="hidden" class="form-control" name="version"> -->
                  <div class="row">          
                    <div class="col-xs-3 form-group">
                      <label>{{labels.staffNo}}</label>  
                      <input type="text" class="form-control" v-model="inputs.staffNo" placeholder="S001">
                    </div> 
                    <div class="col-xs-4 form-group">
                      <label>{{labels.identityNo}}</label>  
                      <input type="text" class="form-control" v-model="inputs.identityNo" placeholder="M123456-1">
                    </div>
                    <div class="col-xs-4 form-group">
                      <label>{{labels.country}}</label>  
                      <select class="form-control" v-model="inputs.countryId" >
                        <option v-for="country in countries" v-bind:value="country.countryId">{{country.name}}</option>
                      </select>
                    </div>  
                  </div>  
                  <div class="row">          
                    <div class="col-xs-3 form-group">
                      <label>{{labels.chName}}</label>  
                      <input type="text" class="form-control" v-model="inputs.chName" placeholder="陳大文">
                    </div> 
                    <div class="col-xs-3 form-group">
                      <label>{{labels.alias}}</label>  
                      <input type="text" class="form-control" v-model="inputs.alias" placeholder="Sunny">          
                    </div> 
                    <div class="col-xs-3 form-group">
                      <label>{{labels.enName}}</label>  
                      <input type="text" class="form-control" v-model="inputs.enName" placeholder="Chan Tai Man">
                    </div>
                    <div class="col-xs-2 form-group">
                      <label>{{labels.gender}}</label>  
                      <select class="form-control" v-model="inputs.gender" >
                        <!-- <option v-for="role in roles" v-bind:value="role.role">{{role.name}}</option> -->
                        <option value="M">M</option>
                        <option value="F">F</option>
                      </select>
                    </div>
                  </div>

                  <div class="row">          
                    <div class="col-xs-5 form-group">
                      <label>{{labels.department}}</label>  
                      <select class="form-control" v-model="inputs.departmentId" >
                        <option v-for="department in departments" v-bind:value="department.departmentId">{{department.name}}</option>
                      </select>
                    </div> 
                    <div class="col-xs-5 form-group">
                      <label>{{labels.position}}</label>  
                      <select class="form-control" v-model="inputs.positionId" >
                        <option v-for="position in positions" v-bind:value="position.positionId">{{position.name}}</option>
                      </select>
                    </div> 
                  </div>   
                  <div class="row">          
                    <div class="col-xs-5 form-group">
                      <label>{{labels.bank}}</label>  
                      <select class="form-control" v-model="inputs.bankId" >
                        <option v-for="bank in banks" v-bind:value="bank.bankId">{{bank.name}}</option>
                      </select>
                    </div> 
                    <div class="col-xs-5 form-group">
                      <label>{{labels.bankAccount}}</label>  
                      <input type="text" class="form-control" v-model="inputs.bankAccount">
                    </div> 
                  </div>   
                  <div class="row">          
                    <div class="col-xs-5 form-group">
                      <label>{{labels.birthday}}</label>  
                        <dropdown class="form-group">
                          <div class="input-group">
                            <input class="form-control" type="text" v-model="inputs.birthday">
                            <div class="input-group-btn">
                              <btn class="dropdown-toggle"><i class="glyphicon glyphicon-calendar"></i></btn>
                            </div>
                          </div>
                          <template slot="dropdown">
                            <li>
                              <date-picker v-model="inputs.birthday" :today-btn="false" :clear-btn="false"/>
                            </li>
                          </template>
                        </dropdown>
                    </div> 
                    <div class="col-xs-5 form-group">
                      <label>{{labels.inTime}}</label>  
                      <dropdown class="form-group">
                        <div class="input-group">
                          <input class="form-control" type="text" v-model="inputs.inTime">
                          <div class="input-group-btn">
                            <btn class="dropdown-toggle"><i class="glyphicon glyphicon-calendar"></i></btn>
                          </div>
                        </div>
                        <template slot="dropdown">
                          <li>
                            <date-picker v-model="inputs.inTime" :today-btn="false" :clear-btn="false"/>
                          </li>
                        </template>
                      </dropdown>
                    </div> 
                  </div> 
                  <div class="row">          
                    <div class="col-xs-5 form-group">
                      <label>{{labels.leaveTime}}</label>  
                      <dropdown class="form-group">
                        <div class="input-group">
                          <input class="form-control" type="text" v-model="inputs.leaveTime">
                          <div class="input-group-btn">
                            <btn class="dropdown-toggle"><i class="glyphicon glyphicon-calendar"></i></btn>
                          </div>
                        </div>
                        <template slot="dropdown">
                          <li>
                            <date-picker v-model="inputs.leaveTime" :today-btn="false" :clear-btn="false"/>
                          </li>
                        </template>
                      </dropdown>
                    </div> 
                    <div class="col-xs-5 form-group">
                      <label>{{labels.MPFDate}}</label>  
                      <dropdown class="form-group">
                        <div class="input-group">
                          <input class="form-control" type="text" v-model="inputs.MPFDate">
                          <div class="input-group-btn">
                            <btn class="dropdown-toggle"><i class="glyphicon glyphicon-calendar"></i></btn>
                          </div>
                        </div>
                        <template slot="dropdown">
                          <li>
                            <date-picker v-model="inputs.MPFDate" :today-btn="false" :clear-btn="false"/>
                          </li>
                        </template>
                      </dropdown>
                    </div> 
                  </div>
                  <hr /> 
                  <h4>{{labels.contacts}}</h4>
                  <br />
                  <div class="row">          
                    <div class="col-xs-5 form-group">
                      <label>{{labels.email}}</label>  
                      <input type="text" class="form-control" v-model="inputs.email">
                    </div> 
                    <div class="col-xs-4 form-group">
                      <label>{{labels.mobileNo}}</label>  
                      <input type="text" class="form-control" v-model="inputs.mobileNo">
                    </div> 
                  </div>
                  <div class="row">          
                    <div class="col-xs-3 form-group">
                      <label>{{labels.urgentContactName}}</label>  
                      <input type="text" class="form-control" v-model="inputs.urgentContactName">
                    </div> 
                    <div class="col-xs-4 form-group">
                      <label>{{labels.urgentContactNo}}</label>  
                      <input type="text" class="form-control" v-model="inputs.urgentContactNo">
                    </div>
                  </div>  
                  <div class="row">          
                    <div class="col-xs-6 form-group">
                      <label>{{labels.enAddress}}</label>  
                      <input type="text" class="form-control" v-model="inputs.enAddress1" style="margin:3px;">
                      <input type="text" class="form-control" v-model="inputs.enAddress2" style="margin:3px;">
                      <input type="text" class="form-control" v-model="inputs.enAddress3" style="margin:3px;">
                    </div> 
                    <div class="col-xs-6 form-group">
                      <label>{{labels.chAddress}}</label>  
                      <input type="text" class="form-control" v-model="inputs.chAddress1" style="margin:3px;">
                      <input type="text" class="form-control" v-model="inputs.chAddress2" style="margin:3px;">
                      <input type="text" class="form-control" v-model="inputs.chAddress3" style="margin:3px;">
                    </div> 
                  </div>                    

                </div>
                <!-- /.tab-pane -->  

              </div>                    
              <!-- /.box-body -->
            </div>
            <div class="box-footer">
              <button class="btn btn-primary pull-right" v-on:click.prevent="createStaff" :disabled="uploading">{{labels.new}}</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>


<div class="modal fade" id="editModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <!-- form start -->                
        <div class="box-body">     
          <form role="form" id="editForm">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#c_tab_1" data-toggle="tab" aria-expanded="true" id="c_n_tab_1">{{labels.edit}}</a></li>
                <li class="pull-right"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></li>
              </ul>
              <div class="tab-content" id="c_tab_content">
                <!-- /.tab-pane -->                      
                <div class="tab-pane active" id="c_tab_1">
                  <div class="row">  
                    <div class="col-xs-9 form-group">        
                     <h4>{{labels.basic}}</h4>
                    </div>
                    <div class="col-xs-3 form-group">  
                      <button class="btn btn-success pull-right" v-on:click.prevent="editStaff" :disabled="uploading">{{labels.edit}}</button>       
                    </div>      
                  </div>
                  <br />
                  <input type="hidden" class="form-control" v-model="inputs.staffId">
                  <!-- <input type="hidden" class="form-control" name="version"> -->
                  <div class="row">          
                    <div class="col-xs-3 form-group">
                      <label>{{labels.staffNo}}</label>  
                      <input type="text" class="form-control" v-model="inputs.staffNo" placeholder="S001">
                    </div> 
                    <div class="col-xs-4 form-group">
                      <label>{{labels.identityNo}}</label>  
                      <input type="text" class="form-control" v-model="inputs.identityNo" placeholder="M123456-1">
                    </div>
                    <div class="col-xs-4 form-group">
                      <label>{{labels.country}}</label>  
                      <select class="form-control" v-model="inputs.countryId" >
                        <option v-for="country in countries" v-bind:value="country.countryId">{{country.name}}</option>
                      </select>
                    </div>  
                  </div>  
                  <div class="row">          
                    <div class="col-xs-3 form-group">
                      <label>{{labels.chName}}</label>  
                      <input type="text" class="form-control" v-model="inputs.chName" placeholder="陳大文">
                    </div> 
                    <div class="col-xs-3 form-group">
                      <label>{{labels.alias}}</label>  
                      <input type="text" class="form-control" v-model="inputs.alias" placeholder="Sunny">          
                    </div> 
                    <div class="col-xs-3 form-group">
                      <label>{{labels.enName}}</label>  
                      <input type="text" class="form-control" v-model="inputs.enName" placeholder="Chan Tai Man">
                    </div>
                    <div class="col-xs-2 form-group">
                      <label>{{labels.gender}}</label>  
                      <select class="form-control" v-model="inputs.gender" >
                        <!-- <option v-for="role in roles" v-bind:value="role.role">{{role.name}}</option> -->
                        <option value="M">M</option>
                        <option value="F">F</option>
                      </select>
                    </div>
                  </div>

                  <div class="row">          
                    <div class="col-xs-5 form-group">
                      <label>{{labels.department}}</label>  
                      <select class="form-control" v-model="inputs.departmentId" >
                        <option v-for="department in departments" v-bind:value="department.departmentId">{{department.name}}</option>
                      </select>
                    </div> 
                    <div class="col-xs-5 form-group">
                      <label>{{labels.position}}</label>  
                      <select class="form-control" v-model="inputs.positionId" >
                        <option v-for="position in positions" v-bind:value="position.positionId">{{position.name}}</option>
                      </select>
                    </div> 
                  </div>   
                  <div class="row">          
                    <div class="col-xs-5 form-group">
                      <label>{{labels.bank}}</label>  
                      <select class="form-control" v-model="inputs.bankId" >
                        <option v-for="bank in banks" v-bind:value="bank.bankId">{{bank.name}}</option>
                      </select>
                    </div> 
                    <div class="col-xs-5 form-group">
                      <label>{{labels.bankAccount}}</label>  
                      <input type="text" class="form-control" v-model="inputs.bankAccount">
                    </div> 
                  </div>   
                  <div class="row">          
                    <div class="col-xs-5 form-group">
                      <label>{{labels.birthday}}</label>  
                        <dropdown class="form-group">
                          <div class="input-group">
                            <input class="form-control" type="text" v-model="inputs.birthday">
                            <div class="input-group-btn">
                              <btn class="dropdown-toggle"><i class="glyphicon glyphicon-calendar"></i></btn>
                            </div>
                          </div>
                          <template slot="dropdown">
                            <li>
                              <date-picker v-model="inputs.birthday" :today-btn="false" :clear-btn="false"/>
                            </li>
                          </template>
                        </dropdown>
                    </div> 
                    <div class="col-xs-5 form-group">
                      <label>{{labels.inTime}}</label>  
                        <dropdown class="form-group">
                          <div class="input-group">
                            <input class="form-control" type="text" v-model="inputs.inTime">
                            <div class="input-group-btn">
                              <btn class="dropdown-toggle"><i class="glyphicon glyphicon-calendar"></i></btn>
                            </div>
                          </div>
                          <template slot="dropdown">
                            <li>
                              <date-picker v-model="inputs.inTime" :today-btn="false" :clear-btn="false"/>
                            </li>
                          </template>
                        </dropdown>
                    </div> 
                  </div> 
                  <div class="row">          
                    <div class="col-xs-5 form-group">
                      <label>{{labels.leaveTime}}</label>  
                      <dropdown class="form-group">
                        <div class="input-group">
                          <input class="form-control" type="text" v-model="inputs.leaveTime">
                          <div class="input-group-btn">
                            <btn class="dropdown-toggle"><i class="glyphicon glyphicon-calendar"></i></btn>
                          </div>
                        </div>
                        <template slot="dropdown">
                          <li>
                            <date-picker v-model="inputs.leaveTime" :today-btn="false" :clear-btn="false"/>
                          </li>
                        </template>
                      </dropdown>
                    </div> 
                    <div class="col-xs-5 form-group">
                      <label>{{labels.MPFDate}}</label>  
                      <dropdown class="form-group">
                        <div class="input-group">
                          <input class="form-control" type="text" v-model="inputs.MPFDate">
                          <div class="input-group-btn">
                            <btn class="dropdown-toggle"><i class="glyphicon glyphicon-calendar"></i></btn>
                          </div>
                        </div>
                        <template slot="dropdown">
                          <li>
                            <date-picker v-model="inputs.MPFDate" :today-btn="false" :clear-btn="false"/>
                          </li>
                        </template>
                      </dropdown>
                    </div> 
                  </div>
                  <hr /> 
                  <h4>{{labels.contacts}}</h4>
                  <br />
                  <div class="row">          
                    <div class="col-xs-5 form-group">
                      <label>{{labels.email}}</label>  
                      <input type="text" class="form-control" v-model="inputs.email">
                    </div> 
                    <div class="col-xs-4 form-group">
                      <label>{{labels.mobileNo}}</label>  
                      <input type="text" class="form-control" v-model="inputs.mobileNo">
                    </div> 
                  </div>
                  <div class="row">          
                    <div class="col-xs-3 form-group">
                      <label>{{labels.urgentContactName}}</label>  
                      <input type="text" class="form-control" v-model="inputs.urgentContactName">
                    </div> 
                    <div class="col-xs-4 form-group">
                      <label>{{labels.urgentContactNo}}</label>  
                      <input type="text" class="form-control" v-model="inputs.urgentContactNo">
                    </div>
                  </div>  
                  <div class="row">          
                    <div class="col-xs-6 form-group">
                      <label>{{labels.enAddress}}</label>  
                      <input type="text" class="form-control" v-model="inputs.enAddress1" style="margin:3px;">
                      <input type="text" class="form-control" v-model="inputs.enAddress2" style="margin:3px;">
                      <input type="text" class="form-control" v-model="inputs.enAddress3" style="margin:3px;">
                    </div> 
                    <div class="col-xs-6 form-group">
                      <label>{{labels.chAddress}}</label>  
                      <input type="text" class="form-control" v-model="inputs.chAddress1" style="margin:3px;">
                      <input type="text" class="form-control" v-model="inputs.chAddress2" style="margin:3px;">
                      <input type="text" class="form-control" v-model="inputs.chAddress3" style="margin:3px;">
                    </div> 
                  </div>                    

                </div>
                <!-- /.tab-pane -->  

              </div>                    
              <!-- /.box-body -->
            </div>
            <div class="box-footer">
              <button class="btn btn-success pull-right" v-on:click.prevent="editStaff" :disabled="uploading">{{labels.edit}}</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>



<script>
  function initOptions()
  {
    $.ajax({
      url: HOST + '/accountant/code/getStaffOptions/'+SYSTEM_LANGUAGE,
      type: 'GET',
      dataType: 'json',
      success: function (data) {
        if(data.status == 200){
          for(var i = 0; i < data.data.countries.length; i++){
              createForm.countries.push(data.data.countries[i]);
              editForm.countries.push(data.data.countries[i]);
          }
          for(var i = 0; i < data.data.departments.length; i++){
              createForm.departments.push(data.data.departments[i]);
              editForm.departments.push(data.data.departments[i]);
          }
          for(var i = 0; i < data.data.positions.length; i++){
              createForm.positions.push(data.data.positions[i]);
              editForm.positions.push(data.data.positions[i]);
          }
          for(var i = 0; i < data.data.banks.length; i++){
              createForm.banks.push(data.data.banks[i]);
              editForm.banks.push(data.data.banks[i]);
          }
        }
        else{
          alert(data.message);    
        }
      }
    });       
  }

  var searchBar = new Vue({
    el:'#searchBar',
    data: {
      searchBtn: lang('searchBtn'),    
      search:''
    },
    methods:{
      searchUser: function(){
        tableData.param.search = this.search;
      }
    }
  })

  var tableData = new Vue({
    el: '#table',
    data: {     
      labels:{
        "staffId": lang('staff_staffId'),
        "staffNo": lang('staff_staffNo'),
        "name":lang('staff_name'),
        "alias":lang('staff_alias'),
        "status":lang('staff_status'),
        "actions":lang('staff_actions'),
        "totalManHours":lang('staff_totalManHours'),
        "totalValues":lang('staff_totalValues')
      },
      items: [],
      selected:[],
      param: {
        "pagination":1,
        "search":searchBar.search,
        "lang":SYSTEM_LANGUAGE
      }
    },
    methods:{
      refresh: function(){
        getTableData(this,pagination,URLS['GET_STAFFS']);
      },
      checkAll: function(){
      },
      deleteStaffs: function(){
        
        var result;  
        result = confirm(lang('q_confirmDelete'));  

        if(result == 0)
          return;

        $.ajax({
          url: HOST + '/accountant/staff/deleteStaffs/',
          type: 'GET',
          dataType: 'json',
          data: {"staffs":this.selected},
          timeout:AJAX_TIMEOUT,
          success:function(data)
          {
            if(checkStatus(data) == false)
              return;  
            tableData.selected = [];
            tableData.refresh(); 
          }
        });         
      },
      deleteItem: function(id){
        
        var result;  
        result = confirm(lang('q_confirmDelete'));  

        if(result == 0)
          return;

        $.ajax({
          url: HOST + '/accountant/staff/deleteStaffs/',
          type: 'GET',
          dataType: 'json',
          data: {"staffs":[id]},
          timeout:AJAX_TIMEOUT,
          success:function(data)
          {
            if(checkStatus(data) == false)
              return;  
            tableData.selected = [];
            tableData.refresh(); 
          }
        });         
      },
      restoreItem: function(id){

        var result;  
        result = confirm(lang('q_confirmRestore'));  

        if(result == 0)
          return;

        $.ajax({
          url: HOST + '/accountant/staff/restoreStaffs/',
          type: 'GET',
          dataType: 'json',
          data: {"staffs":[id]},
          timeout:AJAX_TIMEOUT,
          success:function(data)
          {
            if(checkStatus(data) == false)
              return;  
            tableData.selected = [];
            tableData.refresh(); 
          }
        });         
      }, 

      editItem: function(itemId){
        $("#editModal").modal('show');
        editForm.inputs.staffId = itemId;
        console.log(itemId);

      }
    },
    computed: {
      selectAll: {
        get: function () {
          return this.items ? this.selected.length == this.items.length : false;
        },
        set: function (value) {
          var selected = [];

          if (value) {
              this.items.forEach(function (item) {
                  selected.push(item.userId);
              });
          }
          this.selected = selected;
        }
      }
    },
    watch:{
      param: {
        handler(newValue, oldValue){
          getTableData(tableData,pagination,URLS['GET_STAFFS']);
        },
        deep: true 
      }
    },
    created: function(){
      initOptions();
    }
  })

  var pagination = new Vue({
    el: '#tableFooter',
    data: {
      paginationPrefix: lang('paginationPrefix'),
      paginationSuffix: lang('paginationSuffix'),
      total: 0,
      totalPage: 1,
      currentPage: 1
    },
    methods:{
      // refreshPagination: function(){
      //   refreshPagination($('#pagination'), this);
      // },
      // onPageClick: function(page){
      //   tableData.param.pagination = page;
      //   console.log("page"+page);
      // }
    },
    created:function(){
      getTableData(tableData,this,URLS['GET_STAFFS']);
      // console.log(URLS);
      // initOptions();
      // initRolesOptions();
    },
    watch:{
      currentPage: {
        handler(newValue, oldValue){
          tableData.param.pagination = newValue;
        },
      }
    }
  }) 

  var createForm = new Vue({
    el: '#createModal',
    data: {
      labels:{
        "basic":lang('staff_basic'),
        "contacts":lang('staff_contacts'),
        "new":lang('staff_new'),
        "staffNo":lang('staff_staffNo'),
        "alias":lang('staff_alias'),
        "chName": lang('staff_chName'),
        "enName":lang('staff_enName'),
        "gender":lang('staff_gender'),
        "department":lang('staff_department'),
        "position":lang('staff_position'),
        "email":lang('staff_email'),
        "birthday":lang('staff_birthday'),
        "identityNo":lang('staff_identityNo'),
        "urgentContactName":lang('staff_urgentContactName'),
        "urgentContactNo":lang('staff_urgentContactNo'),
        "bank":lang('staff_bank'),
        "bankAccount":lang('staff_bankAccount'),
        "country":lang('staff_country'),
        "enAddress":lang('staff_enAddress'),
        "chAddress":lang('staff_chAddress'),
        "mobileNo":lang('staff_mobileNo'),
        "homeNo":lang('staff_homeNo'),
        "faxNo":lang('staff_faxNo'),
        "inTime":lang('staff_inTime'),  
        "leaveTime":lang('staff_leaveTime'),
        "MPFDate":lang('staff_MPFDate')       
      },
      inputs:{},
      countries:[],
      departments:[],
      banks:[],
      positions:[],
      uploading: false
    },
    methods:{
      createStaff: function(){
        this.uploading = true;
        $.ajax({
          url: HOST + '/accountant/staff/createStaff/',
          type: 'POST',
          dataType: 'json',
          data: this.inputs,
          timeout:AJAX_TIMEOUT,
          error:function(){
            createForm.uploading = false;
          },
          success:function(data)
          {
            console.log('hello');
            createForm.uploading = false;
            if(checkStatus(data) == false)
              return;  
            else {
              $("#createModal").modal('hide');
              Notify.notify('success', lang('notify_success'), lang('notify_successMessage'));
            }
            tableData.refresh(); 
          }
        }); 
      },
    },
    created: function () {

    }
  }) 

  var editForm = new Vue({
    el: '#editModal',
    data: {
      labels:{
        "edit":lang('staff_edit'),
        "basic":lang('staff_basic'),
        "contacts":lang('staff_contacts'),
        "new":lang('staff_new'),
        "staffNo":lang('staff_staffNo'),
        "alias":lang('staff_alias'),
        "chName": lang('staff_chName'),
        "enName":lang('staff_enName'),
        "gender":lang('staff_gender'),
        "department":lang('staff_department'),
        "position":lang('staff_position'),
        "email":lang('staff_email'),
        "birthday":lang('staff_birthday'),
        "identityNo":lang('staff_identityNo'),
        "urgentContactName":lang('staff_urgentContactName'),
        "urgentContactNo":lang('staff_urgentContactNo'),
        "bank":lang('staff_bank'),
        "bankAccount":lang('staff_bankAccount'),
        "country":lang('staff_country'),
        "enAddress":lang('staff_enAddress'),
        "chAddress":lang('staff_chAddress'),
        "mobileNo":lang('staff_mobileNo'),
        "homeNo":lang('staff_homeNo'),
        "faxNo":lang('staff_faxNo'),
        "inTime":lang('staff_inTime'),  
        "leaveTime":lang('staff_leaveTime'),
        "MPFDate":lang('staff_MPFDate')       
      },
      inputs:{},
      countries:[],
      departments:[],
      banks:[],
      positions:[],
      uploading: false
    },
    methods:{
      editStaff: function(){
        this.uploading = true;
        $.ajax({
          url: HOST + '/accountant/staff/editStaff/',
          type: 'POST',
          dataType: 'json',
          data: this.inputs,
          timeout:AJAX_TIMEOUT,
          error:function(){
            editForm.uploading = false;
          },
          success:function(data)
          {
            console.log('hello');
            editForm.uploading = false;
            if(checkStatus(data) == false)
              return;  
            else {
              $("#editModal").modal('hide');
              Notify.notify('success', lang('notify_success'), lang('notify_successMessage'));
            }
            tableData.refresh(); 
          }
        }); 
      },
      getStaffDetail:function(id){
        getDetail(id, URLS['GET_STAFFS_DETAIL'],function(data){
          editForm.inputs = data.data[0];
          $("#editModal").modal('show');
        });    
      }
    }
  }) 



</script>
