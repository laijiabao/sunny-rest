<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header" id="searchBar">     
        <div class="pull-left" v-if="ROLE == 'A' || ROLE == 'M'">
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createModal">
            <i class="fa fa-plus"></i>
          </button>
          <button type="button" class="btn btn-danger" onclick="tableData.deleteClients()">
            <i class="fa fa-trash"></i>
          </button>
        </div>    
        <div class="col-xs-5">
          <div class="input-group">
            <input id="searchClient" type="text" class="form-control" v-model="search" v-on:keyup.enter="searchUser">
            <span class="input-group-btn">
              <button type="button" class="btn btn-info" v-on:click="searchUser">{{searchBtn}}</button>
            </span>     
          </div>
          <typeahead v-model="model" target="#searchClient" async-src="/accountant/search/client/" async-key="data" item-key="name"/>   
        </div>
        <div class="col-xs-2">
          <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exportInvoiceModal">{{labels.exportSummary}}</button>
        </div>
      </div>          

      <!-- /.box-header -->
      <div class="box-body">
        <table id="table" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th v-if="ROLE == 'A' || ROLE == 'M'"><input type="checkbox" :checked="false" v-model="selectAll"></th>
              <th>{{labels.clientId}}</th>
              <th>{{labels.clientCode}}</th>
              <th>{{labels.chClientName}}</th>
              <th>{{labels.enClientName}}</th>
              <th>{{labels.agencyName}}</th>
              <th>{{labels.status}}</th>
              <th>{{labels.actions}}</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="item in items">
              <td v-if="ROLE == 'A' || ROLE == 'M'">
                <input type="checkbox" v-model="selected" :value="item.clientId">
              </td>
              <td>{{item.clientId}}</td>
              <td>{{item.clientCode}}</td>
              <td>{{item.chClientName}}</td>
              <td>{{item.enClientName}}</td>
              <td>{{item.agencyName}}</td>
              <td>
                <span class="badge bg-green" v-if="item.status == 'A'">{{item.statusName}}</span>
                <span class="badge" v-else-if="item.status == 'D'">{{item.statusName}}</span>
                <span class="badge bg-red" v-else>{{item.statusName}}</span>
              </td>
              <td>
                <div class="">
                  <!-- <a class="btn-xs" v-on:click="viewItem(item.clientId)"><i class="fa fa-eye"></i></a> -->
                  <a class="btn-xs" v-on:click="editForm.getClientDetail(item.clientId)"><i class="fa fa-edit"></i></a>
                  <a class="btn-xs" v-on:click="outputInvoice(item.clientId)" title="Output Invoice"><i class="fa fa-dollar"></i></a>
                  <span v-if="ROLE == 'A' || ROLE == 'M'">
                    <a v-if="item.status == 'I'" class="btn-xs" v-on:click="restoreItem(item.clientId)"><i class="fa fa-refresh"></i></a>
                    <a v-else class="btn-xs" v-on:click="deleteItem(item.clientId)"><i class="fa fa-trash"></i></a>
                  </span>
                </div>
              </td>
            </tr>
            
          </tbody>

        </table>
      </div>
      <div class="box-footer clearfix" id="tableFooter">
        <div class="col-xs-3">
          <span>{{paginationPrefix}}<b>{{total}}</b>{{paginationSuffix}}</span>
        </div>
        <div class="col-xs-9">
          <pagination v-model="currentPage" :total-page="totalPage" size="sm" align="right" boundary-links style="margin-top:-20px; margin-bottom:-20px;"/>
        </div> 
      </div>
    </div>
    <!-- /.box -->
  </div>
</div>
<!-- /.row -->

<div class="modal fade" id="createModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <!-- form start -->                
        <div class="box-body">     
          <form role="form" id="createForm" autocomplete="off">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#c_tab_1" data-toggle="tab" aria-expanded="true" id="c_n_tab_1">{{labels.new}}</a></li>
                <li class="pull-right"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></li>
              </ul>
              <div class="tab-content" id="c_tab_content">
                <!-- /.tab-pane -->                      
                <div class="tab-pane active" id="c_tab_1">
                  <!-- <input type="hidden" class="form-control" name="id"> -->
                  <!-- <input type="hidden" class="form-control" name="version"> -->
                  <div class="row">          
                    <div class="col-xs-3 form-group">
                      <label>{{labels.clientCode}}</label>  
                      <input type="text" class="form-control" v-model="inputs.clientCode" placeholder="">
                    </div> 
                    <div class="col-xs-4 form-group">
                      <label>{{labels.companyNo}}</label>  
                      <input type="text" class="form-control" v-model="inputs.companyNo" placeholder="">
                    </div>
                    <div class="col-xs-4 form-group">
                      <label>{{labels.brNo}}</label>  
                      <input type="text" class="form-control" v-model="inputs.brNo" placeholder="">
                    </div>
                  </div> 
                  <div class="row">          
                    <div class="col-xs-6 form-group">
                      <label>{{labels.enClientName}}</label>  
                      <input type="text" class="form-control" v-model="inputs.enClientName" placeholder="">
                    </div>
                    <div class="col-xs-6 form-group">
                      <label>{{labels.chClientName}}</label>  
                      <input type="text" class="form-control" v-model="inputs.chClientName" placeholder="">
                    </div>
                  </div> 
                  <div class="row">
                    <div class="col-xs-4 form-group">
                      <label>{{labels.country}}</label>  
                      <input type="text" class="form-control" id="c_countryInput" v-on:keyup.enter="">
                      <typeahead v-model="selectedCountry" :data="countries" target="#c_countryInput" item-key="name"/> 
                    </div>                    
                    <div class="col-xs-4 form-group">
                      <label>{{labels.type}}</label>  
                      <input type="text" class="form-control" id="c_typeInput" v-on:keyup.enter="">
                      <typeahead v-model="selectedType" :data="types" target="#c_typeInput" item-key="type"/> 
                    </div>
                    <div class="col-xs-4 form-group">
                      <label>{{labels.clientGroup}}</label>  
                      <input type="text" class="form-control" id="c_clientGroupInput" v-on:keyup.enter="">
                      <typeahead v-model="selectedClientGroup" :data="clientGroups" target="#clientGroupInput" item-key="clientGroup"/> 
                    </div>
                  </div> 
                  <div class="row">          
                    <div class="col-xs-4 form-group">
                      <label>{{labels.attn}}</label>  
                      <input type="text" class="form-control" v-model="inputs.attn" placeholder="">
                    </div> 
                    <div class="col-xs-4 form-group">
                      <label>{{labels.agencyName}}</label>  
                      <input type="text" class="form-control" v-model="inputs.agencyName" placeholder="">
                    </div> 
                  </div>

                  <hr /> 

                  <div class="row">          
                    <div class="col-xs-3 form-group">
                      <label>{{labels.contactPerson1}}</label>  
                      <input type="text" class="form-control" v-model="inputs.contactPerson1" placeholder="">
                    </div> 
                    <div class="col-xs-4 form-group">
                      <label>{{labels.position1}}</label>  
                      <input type="text" class="form-control" v-model="inputs.position1" placeholder="">
                    </div> 
                    <div class="col-xs-5 form-group">
                      <label>{{labels.email1}}</label>  
                      <input type="text" class="form-control" v-model="inputs.email1" placeholder="">
                    </div> 
                  </div>
                  <div class="row">          
                    <div class="col-xs-3 form-group">
                      <label>{{labels.contactPerson2}}</label>  
                      <input type="text" class="form-control" v-model="inputs.contactPerson2" placeholder="">
                    </div> 
                    <div class="col-xs-4 form-group">
                      <label>{{labels.position2}}</label>  
                      <input type="text" class="form-control" v-model="inputs.position2" placeholder="">
                    </div> 
                    <div class="col-xs-5 form-group">
                      <label>{{labels.email2}}</label>  
                      <input type="text" class="form-control" v-model="inputs.email2" placeholder="">
                    </div> 
                  </div>
                  <div class="row">          
                    <div class="col-xs-4 form-group">
                      <label>{{labels.telNo1}}</label>  
                      <input type="text" class="form-control" v-model="inputs.telNo1" placeholder="">
                    </div> 
                    <div class="col-xs-4 form-group">
                      <label>{{labels.telNo2}}</label>  
                      <input type="text" class="form-control" v-model="inputs.telNo2" placeholder="">
                    </div> 
                    <div class="col-xs-4 form-group">
                      <label>{{labels.telNo3}}</label>  
                      <input type="text" class="form-control" v-model="inputs.telNo3" placeholder="">
                    </div> 
                  </div> 

                  <div class="row">          
                    <div class="col-xs-4 form-group">
                      <label>{{labels.faxNo1}}</label>  
                      <input type="text" class="form-control" v-model="inputs.faxNo1" placeholder="">
                    </div> 
                    <div class="col-xs-4 form-group">
                      <label>{{labels.faxNo2}}</label>  
                      <input type="text" class="form-control" v-model="inputs.faxNo2" placeholder="">
                    </div> 
                    <div class="col-xs-4 form-group">
                      <label>{{labels.faxNo3}}</label>  
                      <input type="text" class="form-control" v-model="inputs.faxNo3" placeholder="">
                    </div> 
                  </div> 

                  <hr /> 

                  <div class="row">          
                    <div class="col-xs-6 form-group">
                      <label>{{labels.enMailingAddress}}</label>  
                      <input type="text" class="form-control" v-model="inputs.enMailingAddress1" style="margin:3px;">
                      <input type="text" class="form-control" v-model="inputs.enMailingAddress2" style="margin:3px;">
                      <input type="text" class="form-control" v-model="inputs.enMailingAddress3" style="margin:3px;">
                    </div> 
                    <div class="col-xs-6 form-group">
                      <label>{{labels.chMailingAddress}}</label>  
                      <input type="text" class="form-control" v-model="inputs.chMailingAddress1" style="margin:3px;">
                      <input type="text" class="form-control" v-model="inputs.chMailingAddress2" style="margin:3px;">
                      <input type="text" class="form-control" v-model="inputs.chMailingAddress3" style="margin:3px;">
                    </div> 
                  </div>                    
                  <div class="row">          
                    <div class="col-xs-6 form-group">
                      <label>{{labels.enRegisterAddress}}</label>  
                      <input type="text" class="form-control" v-model="inputs.enRegisteredAddress1" style="margin:3px;">
                      <input type="text" class="form-control" v-model="inputs.enRegisteredAddress2" style="margin:3px;">
                      <input type="text" class="form-control" v-model="inputs.enRegisteredAddress3" style="margin:3px;">
                    </div> 
                    <div class="col-xs-6 form-group">
                      <label>{{labels.chRegisterAddress}}</label>  
                      <input type="text" class="form-control" v-model="inputs.chRegisteredAddress1" style="margin:3px;">
                      <input type="text" class="form-control" v-model="inputs.chRegisteredAddress2" style="margin:3px;">
                      <input type="text" class="form-control" v-model="inputs.chRegisteredAddress3" style="margin:3px;">
                    </div> 
                  </div> 
                </div>
                <!-- /.tab-pane -->  

              </div>                    
              <!-- /.box-body -->
            </div>
            <div class="box-footer">
              <a class="btn btn-primary pull-right" v-on:click.prevent="createClient" :disabled="uploading">{{labels.new}}</a>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>


<div class="modal fade" id="editModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <!-- form start -->                
        <div class="box-body">     
          <form role="form" id="editForm" autocomplete="off">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#c_tab_1" data-toggle="tab" aria-expanded="true" id="c_n_tab_1">{{labels.edit}}</a></li>
                <li class="pull-right"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></li>
              </ul>
              <div class="tab-content" id="c_tab_content">
                <!-- /.tab-pane -->                      
                <div class="tab-pane active" id="c_tab_1">
                  <input type="hidden" class="form-control" name="id">
                  <div class="row">          
                    <div class="col-xs-3 form-group">
                      <label>{{labels.clientCode}}</label>  
                      <input type="text" class="form-control" v-model="inputs.clientCode" placeholder="">
                    </div> 
                    <div class="col-xs-4 form-group">
                      <label>{{labels.companyNo}}</label>  
                      <input type="text" class="form-control" v-model="inputs.companyNo" placeholder="">
                    </div>
                    <div class="col-xs-4 form-group">
                      <label>{{labels.brNo}}</label>  
                      <input type="text" class="form-control" v-model="inputs.brNo" placeholder="">
                    </div>
                  </div> 
                  <div class="row">          
                    <div class="col-xs-6 form-group">
                      <label>{{labels.enClientName}}</label>  
                      <input type="text" class="form-control" v-model="inputs.enClientName" placeholder="">
                    </div>
                    <div class="col-xs-6 form-group">
                      <label>{{labels.chClientName}}</label>  
                      <input type="text" class="form-control" v-model="inputs.chClientName" placeholder="">
                    </div>
                  </div> 
                  <div class="row">
                    <div class="col-xs-4 form-group">
                      <label>{{labels.country}}</label>  
                      <input type="text" class="form-control" id="e_countryInput" v-on:keyup.enter="">
                      <typeahead v-model="selectedCountry" :data="countries" target="#e_countryInput" item-key="name"/> 
                    </div>                    
                    <div class="col-xs-4 form-group">
                      <label>{{labels.type}}</label>  
                      <input type="text" class="form-control" id="e_typeInput" v-on:keyup.enter="">
                      <typeahead v-model="selectedType" :data="types" target="#e_typeInput" item-key="type"/> 
                    </div>
                    <div class="col-xs-4 form-group">
                      <label>{{labels.clientGroup}}</label>  
                      <input type="text" class="form-control" id="e_clientGroupInput" v-on:keyup.enter="">
                      <typeahead v-model="selectedClientGroup" :data="clientGroups" target="#e_clientGroupInput" item-key="clientGroup"/> 
                    </div>
                  </div> 
                  <div class="row">          
                    <div class="col-xs-4 form-group">
                      <label>{{labels.attn}}</label>  
                      <input type="text" class="form-control" v-model="inputs.attn" placeholder="">
                    </div> 
                    <div class="col-xs-4 form-group">
                      <label>{{labels.agencyName}}</label>  
                      <input type="text" class="form-control" v-model="inputs.agencyName" placeholder="">
                    </div> 
                  </div>

                  <hr /> 

                  <div class="row">          
                    <div class="col-xs-3 form-group">
                      <label>{{labels.contactPerson1}}</label>  
                      <input type="text" class="form-control" v-model="inputs.contactPerson1" placeholder="">
                    </div> 
                    <div class="col-xs-4 form-group">
                      <label>{{labels.position1}}</label>  
                      <input type="text" class="form-control" v-model="inputs.position1" placeholder="">
                    </div> 
                    <div class="col-xs-5 form-group">
                      <label>{{labels.email1}}</label>  
                      <input type="text" class="form-control" v-model="inputs.email1" placeholder="">
                    </div> 
                  </div>
                  <div class="row">          
                    <div class="col-xs-3 form-group">
                      <label>{{labels.contactPerson2}}</label>  
                      <input type="text" class="form-control" v-model="inputs.contactPerson2" placeholder="">
                    </div> 
                    <div class="col-xs-4 form-group">
                      <label>{{labels.position2}}</label>  
                      <input type="text" class="form-control" v-model="inputs.position2" placeholder="">
                    </div> 
                    <div class="col-xs-5 form-group">
                      <label>{{labels.email2}}</label>  
                      <input type="text" class="form-control" v-model="inputs.email2" placeholder="">
                    </div> 
                  </div>
                  <div class="row">          
                    <div class="col-xs-4 form-group">
                      <label>{{labels.telNo1}}</label>  
                      <input type="text" class="form-control" v-model="inputs.telNo1" placeholder="">
                    </div> 
                    <div class="col-xs-4 form-group">
                      <label>{{labels.telNo2}}</label>  
                      <input type="text" class="form-control" v-model="inputs.telNo2" placeholder="">
                    </div> 
                    <div class="col-xs-4 form-group">
                      <label>{{labels.telNo3}}</label>  
                      <input type="text" class="form-control" v-model="inputs.telNo3" placeholder="">
                    </div> 
                  </div> 

                  <div class="row">          
                    <div class="col-xs-4 form-group">
                      <label>{{labels.faxNo1}}</label>  
                      <input type="text" class="form-control" v-model="inputs.faxNo1" placeholder="">
                    </div> 
                    <div class="col-xs-4 form-group">
                      <label>{{labels.faxNo2}}</label>  
                      <input type="text" class="form-control" v-model="inputs.faxNo2" placeholder="">
                    </div> 
                    <div class="col-xs-4 form-group">
                      <label>{{labels.faxNo3}}</label>  
                      <input type="text" class="form-control" v-model="inputs.faxNo3" placeholder="">
                    </div> 
                  </div> 

                  <hr /> 

                  <div class="row">          
                    <div class="col-xs-6 form-group">
                      <label>{{labels.enMailingAddress}}</label>  
                      <input type="text" class="form-control" v-model="inputs.enMailingAddress1" style="margin:3px;">
                      <input type="text" class="form-control" v-model="inputs.enMailingAddress2" style="margin:3px;">
                      <input type="text" class="form-control" v-model="inputs.enMailingAddress3" style="margin:3px;">
                    </div> 
                    <div class="col-xs-6 form-group">
                      <label>{{labels.chMailingAddress}}</label>  
                      <input type="text" class="form-control" v-model="inputs.chMailingAddress1" style="margin:3px;">
                      <input type="text" class="form-control" v-model="inputs.chMailingAddress2" style="margin:3px;">
                      <input type="text" class="form-control" v-model="inputs.chMailingAddress3" style="margin:3px;">
                    </div> 
                  </div>                    
                  <div class="row">          
                    <div class="col-xs-6 form-group">
                      <label>{{labels.enRegisterAddress}}</label>  
                      <input type="text" class="form-control" v-model="inputs.enRegisteredAddress1" style="margin:3px;">
                      <input type="text" class="form-control" v-model="inputs.enRegisteredAddress2" style="margin:3px;">
                      <input type="text" class="form-control" v-model="inputs.enRegisteredAddress3" style="margin:3px;">
                    </div> 
                    <div class="col-xs-6 form-group">
                      <label>{{labels.chRegisterAddress}}</label>  
                      <input type="text" class="form-control" v-model="inputs.chRegisteredAddress1" style="margin:3px;">
                      <input type="text" class="form-control" v-model="inputs.chRegisteredAddress2" style="margin:3px;">
                      <input type="text" class="form-control" v-model="inputs.chRegisteredAddress3" style="margin:3px;">
                    </div> 
                  </div> 
                </div>
                <!-- /.tab-pane -->  

              </div>                  
              <!-- /.box-body -->
            </div>
            <div class="box-footer">
              <a class="btn btn-success pull-right" v-on:click.prevent="editClient" :disabled="uploading" v-if="ROLE == 'A' || ROLE == 'M'">{{labels.edit}}</a>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="exportInvoiceModal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body">
        <!-- form start -->                
        <div class="box-body">     
          <form role="form" autocomplete="off">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#c_tab_1" data-toggle="tab" aria-expanded="true" id="c_n_tab_1">{{labels.exportSummary}}</a></li>
                <li class="pull-right"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></li>
              </ul>
              <div class="tab-content" id="c_tab_content">
                <!-- /.tab-pane -->                      
                <div class="tab-pane active" id="c_tab_1">
                  <input type="hidden" class="form-control" name="id">
                  <div class="row">
                    <div class="col-xs-5 col-xs-offset-1 form-group">
                      <label>{{labels.year}}</label>  
                      <input type="text" class="form-control" v-model="year">
                    </div>  
                    <div class="col-xs-5 form-group">
                      <label>{{labels.month}}</label>  
                      <input type="text" class="form-control" v-model="month">
                    </div>                       
                  </div> 
                </div>
                <!-- /.tab-pane -->  

              </div>                  
              <!-- /.box-body -->
            </div>
            <div class="box-footer">
              <a class="btn btn-success pull-right" v-on:click.prevent="printInvoiceSummary">{{labels.exportSummary}}</a>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<script>
  function initOptions()
  {
    $.ajax({
      url: HOST + '/accountant/code/getClientOptions/'+SYSTEM_LANGUAGE,
      type: 'GET',
      dataType: 'json',
      success: function (data) {
        if(data.status == 200){
          // for(var i = 0; i < data.data.countries.length; i++){
          //   createForm.countries 
          //     // createForm.countries.push(data.data.countries[i]);
          //     // editForm.countries.push(data.data.countries[i]);
          // }
          // for(var i = 0; i < data.data.clientGroups.length; i++){
          //     createForm.clientGroups.push(data.data.clientGroups[i]);
          //     // editForm.departments.push(data.data.clientGroups[i]);
          // }
          // for(var i = 0; i < data.data.types.length; i++){
          //     createForm.types.push(data.data.types[i]);
          //     // editForm.positions.push(data.data.types[i]);
          // }
          createForm.countries = data.data.countries;
          createForm.clientGroups = data.data.clientGroups;
          createForm.types = data.data.types;

          editForm.countries = data.data.countries;
          editForm.clientGroups = data.data.clientGroups;
          editForm.types = data.data.types;          
        }
        else{
          alert(data.message);    
        }
      }
    });       
  }

  var searchBar = new Vue({
    el:'#searchBar',
    data: {
      labels:{
        "exportSummary":lang('client_exportSummary')
      },
      searchBtn: lang('searchBtn'),    
      search:'',
      model: ''
    },
    methods:{
      searchUser: function(){
        if(typeof this.model === 'string' || this.model instanceof String)
          tableData.param.search = this.search;
        else
          tableData.param.search = this.model.clientCode;

        console.log(this.model);
      }
    },
    watch:{
      search: {
        handler(newValue, oldValue){
          // tableData.param.search = newValue;
        },
        deep: true 
      }
    }
  })

  var tableData = new Vue({
    el: '#table',
    data: {     
      labels:{
        "clientId": lang('client_clientId'),
        "clientCode": lang('client_clientCode'),
        "chClientName":lang('client_chClientName'),
        "enClientName":lang('client_enClientName'),
        "agencyName":lang('client_agencyName'),
        "status":lang('client_status'),
        "actions":lang('actions')
      },
      items: [],
      selected:[],
      param: {
        "pagination":1,
        "search":searchBar.search,
        "lang":SYSTEM_LANGUAGE
      }
    },
    methods:{
      refresh: function(){
        getTableData(this,pagination,URLS['GET_CLIENTS']);
      },
      checkAll: function(){
      },
      deleteClients: function(){
        
        var result;  
        result = confirm(lang('q_confirmDelete'));  

        if(result == 0)
          return;

        $.ajax({
          url: HOST + '/accountant/client/deleteClients/',
          type: 'GET',
          dataType: 'json',
          data: {"clients":this.selected},
          timeout:AJAX_TIMEOUT,
          success:function(data)
          {
            if(checkStatus(data) == false)
              return;  
            tableData.selected = [];
            tableData.refresh(); 
          }
        });         
      },
      deleteItem: function(id){
        
        var result;  
        result = confirm(lang('q_confirmDelete'));  

        if(result == 0)
          return;

        $.ajax({
          url: HOST + '/accountant/client/deleteClients/',
          type: 'GET',
          dataType: 'json',
          data: {"clients":[id]},
          timeout:AJAX_TIMEOUT,
          success:function(data)
          {
            if(checkStatus(data) == false)
              return;  
            tableData.selected = [];
            tableData.refresh(); 
          }
        });         
      },
      restoreItem: function(id){

        var result;  
        result = confirm(lang('q_confirmRestore'));  

        if(result == 0)
          return;

        $.ajax({
          url: HOST + '/accountant/client/restoreClients/',
          type: 'GET',
          dataType: 'json',
          data: {"clients":[id]},
          timeout:AJAX_TIMEOUT,
          success:function(data)
          {
            if(checkStatus(data) == false)
              return;  
            tableData.selected = [];
            tableData.refresh(); 
          }
        });         
      },  
      editItem: function(itemId){
        $("#editModal").modal('show');
        editForm.inputs.clientId = itemId;
        console.log(itemId);

      },
      outputInvoice: function(itemId){
        $.ajax({
          url: HOST + '/accountant/client/getClientInvoiceInfo/'+itemId,
          type: 'GET',
          dataType: 'json',
          timeout:AJAX_TIMEOUT,
          success:function(data)
          {
            if(checkStatus(data) == false)
              return;  

            $("#MD_invoiceModal").modal('show');
            MD_invoiceModal.items = data.data.tasks;
            MD_invoiceModal.inputs = data.data.client;
            MD_invoiceModal.disbursementList = data.data.disbursementList;
          }
        });      

        
      }
    },
    computed: {
      selectAll: {
        get: function () {
          return this.items ? this.selected.length == this.items.length : false;
        },
        set: function (value) {
          var selected = [];

          if (value) {
              this.items.forEach(function (item) {
                  selected.push(item.userId);
              });
          }
          this.selected = selected;
        }
      }
    },
    watch:{
      param: {
        handler(newValue, oldValue){
          getTableData(tableData,pagination,URLS['GET_CLIENTS']);
        },
        deep: true 
      }
    },
    created: function(){

    }
  })

  var pagination = new Vue({
    el: '#tableFooter',
    data: {
      paginationPrefix: lang('paginationPrefix'),
      paginationSuffix: lang('paginationSuffix'),
      total: 0,
      totalPage: 1,
      currentPage: 1
    },
    methods:{

    },
    created:function(){
      getTableData(tableData,this,URLS['GET_CLIENTS']);
    },
    watch:{
      currentPage: {
        handler(newValue, oldValue){
          tableData.param.pagination = newValue;
        },
      }
    }
  }) 

  var createForm = new Vue({
    el: '#createModal',
    data: {
      labels:{
        "basic":lang('client_basic'),
        "contacts":lang('client_contacts'),
        "new":lang('client_new'),
        "clientCode":lang('client_clientCode'),
        "chClientName": lang('client_chClientName'),
        "enClientName":lang('client_enClientName'),
        "companyNo":lang('client_companyNo'),
        "brNo":lang('client_brNo'),
        "clientGroup":lang('client_clientGroup'),
        "type":lang('client_type'),
        "agencyName":lang('client_agencyName'),
        "status":lang('client_status'),
        "country":lang('client_country'),
        "enRegisterAddress":lang('client_enRegisterAddress'),
        "chRegisterAddress":lang('client_chRegisterAddress'),
        "enMailingAddress":lang('client_enMailingAddress'),
        "chMailingAddress":lang('client_chMailingAddress'),
        "attn":lang('client_attn'),
        "activeDate":lang('client_activeDate'),
        "inactiveDate":lang('client_inactiveDate'),
        "telNo1":lang('client_telNo1'),
        "telNo2":lang('client_telNo2'),
        "telNo3":lang('client_telNo3'),
        "faxNo1":lang('client_faxNo1'),  
        "faxNo2":lang('client_faxNo2'),
        "faxNo3":lang('client_faxNo3'),
        "contactPerson1":lang('client_contactPerson1'),
        "contactPerson2":lang('client_contactPerson2'),
        "position1":lang('client_position1'),
        "position2":lang('client_position2'),
        "email1":lang('client_email1'),
        "email2":lang('client_email2')

      },
      inputs:{},
      countries:[],
      clientGroups:[],
      types:[],
      positions:[],
      selectedCountry: '',
      selectedType: '',
      selectedClientGroup: '',
      uploading: false
    },
    methods:{
      createClient: function(){
        this.uploading = true;
        this.inputs.country = this.selectedCountry.name;
        this.inputs.type = this.selectedType.type;
        this.inputs.clientGroup = this.selectedClientGroup.clientGroup;

        $.ajax({
          url: HOST + '/accountant/client/createClient/',
          type: 'POST',
          dataType: 'json',
          data: this.inputs,
          timeout:AJAX_TIMEOUT,
          success:function(data)
          {
            console.log('hello');
            createForm.uploading = false;
            if(checkStatus(data) == false)
              return;  
            else{ 
              $("#createModal").modal('hide');
              Notify.notify('success', lang('notify_success'), lang('notify_successMessage'));
            }
            tableData.refresh(); 
          }
        }); 
      },
    },
    created: function () {
      initOptions();
    }
  }) 

  var editForm = new Vue({
    el: '#editModal',
    data: {
      labels:{
        "basic":lang('client_basic'),
        "edit":lang('client_edit'),
        "contacts":lang('client_contacts'),
        "new":lang('client_new'),
        "clientCode":lang('client_clientCode'),
        "chClientName": lang('client_chClientName'),
        "enClientName":lang('client_enClientName'),
        "companyNo":lang('client_companyNo'),
        "brNo":lang('client_brNo'),
        "clientGroup":lang('client_clientGroup'),
        "type":lang('client_type'),
        "agencyName":lang('client_agencyName'),
        "status":lang('client_status'),
        "country":lang('client_country'),
        "enRegisterAddress":lang('client_enRegisterAddress'),
        "chRegisterAddress":lang('client_chRegisterAddress'),
        "enMailingAddress":lang('client_enMailingAddress'),
        "chMailingAddress":lang('client_chMailingAddress'),
        "attn":lang('client_attn'),
        "activeDate":lang('client_activeDate'),
        "inactiveDate":lang('client_inactiveDate'),
        "telNo1":lang('client_telNo1'),
        "telNo2":lang('client_telNo2'),
        "telNo3":lang('client_telNo3'),
        "faxNo1":lang('client_faxNo1'),  
        "faxNo2":lang('client_faxNo2'),
        "faxNo3":lang('client_faxNo3'),
        "contactPerson1":lang('client_contactPerson1'),
        "contactPerson2":lang('client_contactPerson2'),
        "position1":lang('client_position1'),
        "position2":lang('client_position2'),
        "email1":lang('client_email1'),
        "email2":lang('client_email2')

      },
      inputs:{},
      countries:[],
      clientGroups:[],
      types:[],
      positions:[],
      selectedCountry: '',
      selectedType: '',
      selectedClientGroup: '',
      uploading: false
    },
    methods:{
      editClient: function(){
        this.uploading = true;
        this.inputs.country = $("#e_countryInput").val();
        this.inputs.type = $("#e_typeInput").val();
        this.inputs.clientGroup = $("#e_clientGroupInput").val();

        $.ajax({
          url: HOST + '/accountant/client/editClient/',
          type: 'POST',
          dataType: 'json',
          data: this.inputs,
          timeout:AJAX_TIMEOUT,
          error:function(){
            editForm.uploading = false;
          },
          success:function(data)
          {
            editForm.uploading = false;
            if(checkStatus(data) == false)
              return;  
            else {
              $("#editModal").modal('hide');
              Notify.notify('success', lang('notify_success'), lang('notify_successMessage'));
            }
            tableData.refresh(); 
          }
        }); 
      },
      getClientDetail:function(id){
        getDetail(id, URLS['GET_CLIENT_DETAIL'],function(data){
          editForm.inputs = data.data[0];

          $("#e_countryInput").val(editForm.inputs.country);
          $("#e_typeInput").val(editForm.inputs.type);
          $("#e_clientGroupInput").val(editForm.inputs.clientGroup);

          $("#editModal").modal('show');
        });    
      }
    }
  }) 

  var exportInvoiceModal = new Vue({
    el: '#exportInvoiceModal',
    data: {
      labels:{
        "exportSummary":lang('client_exportSummary'),
        "year": lang('client_year'),
        "month": lang('client_month')
      },
      year:moment().year(),
      month:moment().month()+1,
      inputs:{}
    },
    methods:{
      printInvoiceSummary:function(){
        var url = HOST+'/accountant/timesheet/printTimesheet?year='+this.year+'&month='+this.month;
        var win = window.open(url, '_blank');
        if (win) {
          win.focus();
        } else {

        }
        // window.open(url,'_blank');
        $('#exportInvoiceModal').modal('hide');
        Notify.notify('success', lang('notify_success'), lang('notify_successMessage'));
      }
    }
  }) 

</script>
