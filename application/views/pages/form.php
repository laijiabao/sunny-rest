<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header" id="searchBar">     
        <div class="pull-left" v-if="ROLE == 'A' || ROLE == 'M'">
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createModal">
            <i class="fa fa-plus"></i>
          </button>
          <button type="button" class="btn btn-danger" onclick="tableData.deleteForms()">
            <i class="fa fa-trash"></i>
          </button>
        </div>    
        <div class="col-xs-5" >
          <div class="input-group">
            <input type="text" class="form-control" v-model="search" v-on:keyup.enter="searchUser">
            <span class="input-group-btn">
              <button type="button" class="btn btn-info" v-on:click="searchUser">{{searchBtn}}</button>
            </span>
          </div>
        </div>
      </div>          

      <!-- /.box-header -->
      <div class="box-body">
        <table id="table" class="table table-bordered table-hover">

          <thead>
            <tr>
              <th v-if="ROLE == 'A' || ROLE == 'M'"><input type="checkbox" :checked="false" v-model="selectAll"></th>
              <th>{{labels.formId}}</th>
              <th>{{labels.formName}}</th>
              <th>{{labels.createTime}}</th>
              <!-- <th>{{labels.updateTime}}</th> -->
              <th>{{labels.actions}}</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="item in items">
              <td v-if="ROLE == 'A' || ROLE == 'M'">
                <input type="checkbox" v-model="selected" :value="item.formId">
              </td>
              <td>{{item.formId}}</td>
              <td><a :href="HOST+'/accountant/'+item.formUrl" :download="item.formName">{{item.formName}}</a></td>
              <td>{{item.createTime}}</td>
              <!-- <td>{{item.updateTime}}</td> -->
              <td>
                <div class="">
<!--                   <a class="btn-xs" v-on:click="viewItem(item.formId)"><i class="fa fa-eye"></i></a>
                  <a class="btn-xs" v-on:click="editForm.getFormDetail(item.formId)"><i class="fa fa-edit"></i></a> -->
                  <a class="btn-xs" :href="HOST+'/accountant/'+item.formUrl" :download="item.formName"><i class="fa fa-download"></i></a>
                  <a v-if="ROLE == 'A' || ROLE == 'M'" class="btn-xs" v-on:click="deleteItem(item.formId)"><i class="fa fa-trash"></i></a>
                </div>
              </td>
            </tr>
            
          </tbody>

        </table>
      </div>
      <div class="box-footer clearfix" id="tableFooter">
        <div class="col-xs-3">
          <span>{{paginationPrefix}}<b>{{total}}</b>{{paginationSuffix}}</span>
        </div>
        <div class="col-xs-9">
          <pagination v-model="currentPage" :total-page="totalPage" size="sm" align="right" boundary-links style="margin-top:-20px; margin-bottom:-20px;"/>
        </div> 
      </div>
    </div>
    <!-- /.box -->
  </div>
</div>
<!-- /.row -->

<div class="modal fade" id="createModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <!-- form start -->                
        <div class="box-body">     
          <form role="form" id="createForm">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#c_tab_1" data-toggle="tab" aria-expanded="true" id="c_n_tab_1">{{labels.new}}</a></li>
                <li class="pull-right"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></li>
              </ul>
              <div class="tab-content" id="c_tab_content">
                <!-- /.tab-pane -->                      
                <div class="tab-pane active" id="c_tab_1">
                    <div class="tab-pane" id="c_tab_2">
                      <div class="row">
                        <div class="col-xs-12 form-group">
                          <!-- <label>{{labels.selectFile}}</label> -->
                          <div class="fileUpload btn btn-warning">
                            <span>{{labels.selectFile}}</span>
                            <i class="fa fa-upload"></i>
                            <input type="file" class="upload" multiple v-on:change="onChanged"/>
                          </div>                          
                        </div>  
                        <div class="col-xs-12">
                          <div v-for="file in filesToUpload">
                            <p>{{file}}</p>
                          </div>
                        </div>

                        <div class="col-xs-12"><label class="text-info" v-if="uploading">{{labels.uploading}}</label></div>                            
                      </div>
                    </div>
                <!-- /.tab-pane -->  

              </div>                    
              <!-- /.box-body -->
            </div>
            <div class="box-footer">
              <button class="btn btn-primary pull-right" v-on:click.prevent="createForm" :disabled="uploading">{{labels.new}}</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>


<div class="modal fade" id="editModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <!-- form start -->                
        <div class="box-body">     
          <form role="form" id="editForm">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#c_tab_1" data-toggle="tab" aria-expanded="true" id="c_n_tab_1">{{labels.edit}}</a></li>
                <li class="pull-right"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></li>
              </ul>
              <div class="tab-content" id="c_tab_content">
                <!-- /.tab-pane -->                      
                <div class="tab-pane active" id="c_tab_1">
                  <div class="row">  
                    <div class="col-xs-9 form-group">        
                     <h4>{{labels.basic}}</h4>
                    </div>
                    <div class="col-xs-3 form-group">  
                      <button class="btn btn-success pull-right" v-on:click.prevent="editForm" :disabled="uploading">{{labels.edit}}</button>       
                    </div>      
                  </div>
                  <br />
                  <input type="hidden" class="form-control" v-model="inputs.formId">
                  <!-- <input type="hidden" class="form-control" name="version"> -->
                                    

                </div>
                <!-- /.tab-pane -->  

              </div>                    
              <!-- /.box-body -->
            </div>
            <div class="box-footer">
              <button class="btn btn-success pull-right" v-on:click.prevent="editForm" :disabled="uploading">{{labels.edit}}</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>



<script>

  var searchBar = new Vue({
    el:'#searchBar',
    data: {
      searchBtn: lang('searchBtn'),    
      search:''
    },
    methods:{
      searchUser: function(){
        tableData.param.search = this.search;
      }
    }
  })

  var tableData = new Vue({
    el: '#table',
    data: {     
      labels:{
        "new":lang('form_new'),
        "formId":lang('form_formId'),
        "formName":lang('form_formName'),
        "createTime":lang('form_createTime'),
        "updateTime":lang('form_updateTime'),
        "actions":lang('actions')
      },
      items: [],
      selected:[],
      param: {
        "pagination":1,
        "search":searchBar.search,
        "lang":SYSTEM_LANGUAGE
      }
    },
    methods:{
      refresh: function(){
        getTableData(this,pagination,URLS['GET_FORMS']);
      },
      checkAll: function(){
      },
      deleteForms: function(){
        
        var result;  
        result = confirm(lang('q_confirmDelete'));  

        if(result == 0)
          return;

        $.ajax({
          url: HOST + '/accountant/form/deleteForms/',
          type: 'GET',
          dataType: 'json',
          data: {"forms":this.selected},
          timeout:AJAX_TIMEOUT,
          success:function(data)
          {
            if(checkStatus(data) == false)
              return;  
            tableData.selected = [];
            tableData.refresh(); 
          }
        });         
      },
      deleteItem: function(id){
        
        var result;  
        result = confirm(lang('q_confirmDelete'));  

        if(result == 0)
          return;

        $.ajax({
          url: HOST + '/accountant/form/deleteForms/',
          type: 'GET',
          dataType: 'json',
          data: {"forms":[id]},
          timeout:AJAX_TIMEOUT,
          success:function(data)
          {
            if(checkStatus(data) == false)
              return;  
            tableData.selected = [];
            tableData.refresh(); 
          }
        });         
      },


      editItem: function(itemId){
        $("#editModal").modal('show');
        editForm.inputs.formId = itemId;
        console.log(itemId);

      }
    },
    computed: {
      selectAll: {
        get: function () {
          return this.items ? this.selected.length == this.items.length : false;
        },
        set: function (value) {
          var selected = [];

          if (value) {
              this.items.forEach(function (item) {
                  selected.push(item.userId);
              });
          }
          this.selected = selected;
        }
      }
    },
    watch:{
      param: {
        handler(newValue, oldValue){
          getTableData(tableData,pagination,URLS['GET_FORMS']);
        },
        deep: true 
      }
    },
    created: function(){

    }
  })

  var pagination = new Vue({
    el: '#tableFooter',
    data: {
      paginationPrefix: lang('paginationPrefix'),
      paginationSuffix: lang('paginationSuffix'),
      total: 0,
      totalPage: 1,
      currentPage: 1
    },
    methods:{

    },
    created:function(){
      getTableData(tableData,this,URLS['GET_FORMS']);
    },
    watch:{
      currentPage: {
        handler(newValue, oldValue){
          tableData.param.pagination = newValue;
        },
      }
    }
  }) 

  var createForm = new Vue({
    el: '#createModal',
    data: {
      labels:{
        "new":lang('form_new'),
        "uploading":lang('uploading'),
        "selectFile":lang('selectFile'), 
        "upload":lang('uploadFile')    
      },
      inputs:{},
      filesToUpload: [],
      uploading: false
    },
    methods:{
      createForm: function(){

        if(this.filesToUpload.length == 0){
          alert(lang('error_pleaseSelectFile'));
          return;
        }

        this.uploading = true;
        console.log(this.inputs.get("files[]"));
        $.ajax({
          url: HOST + '/accountant/form/createForm/',
          type: 'POST',
          dataType: 'json',
          data: this.inputs,
          processData:false,
          contentType:false,
          cache:false,
          timeout:AJAX_TIMEOUT,
          error:function()
          {
            alert(lang("error_uploadFailed"));
            this.uploading = false;
          },
          success:function(data)
          {
            console.log('hello');
            createForm.uploading = false;
            if(checkStatus(data) == false)
              return;  
            else {
              $("#createModal").modal('hide');
              Notify.notify('success', lang('notify_success'), lang('notify_successMessage'));
            }
            tableData.refresh(); 
          }
        }); 
      },
      onChanged:function(e){
        var files = e.target.files || e.dataTransfer.files;
        if (!files.length)
          return;

        formData = new FormData();
        this.filesToUpload = [];
        for(var count = 0; count<files.length; count++)
        {
          formData.append("files[]", files[count]);
          this.filesToUpload.push(files[count].name);
        }
        // formData.append("version", 0);
        this.inputs = formData;
        // console.log(this.inputs.get("files[]"));
      }
    },
    created: function () {

    }
  }) 

  var editForm = new Vue({
    el: '#editModal',
    data: {
      labels:{
        "edit":lang('form_edit'),
        "basic":lang('form_basic'),
        "contacts":lang('form_contacts'),
        "new":lang('form_new'),
        "formNo":lang('form_formNo'),
        "alias":lang('form_alias'),
        "chName": lang('form_chName'),
        "enName":lang('form_enName'),
        "gender":lang('form_gender'),
        "department":lang('form_department'),
        "position":lang('form_position'),
        "email":lang('form_email'),
        "birthday":lang('form_birthday'),
        "identityNo":lang('form_identityNo'),
        "urgentContactName":lang('form_urgentContactName'),
        "urgentContactNo":lang('form_urgentContactNo'),
        "bank":lang('form_bank'),
        "bankAccount":lang('form_bankAccount'),
        "country":lang('form_country'),
        "enAddress":lang('form_enAddress'),
        "chAddress":lang('form_chAddress'),
        "mobileNo":lang('form_mobileNo'),
        "homeNo":lang('form_homeNo'),
        "faxNo":lang('form_faxNo'),
        "inTime":lang('form_inTime'),  
        "leaveTime":lang('form_leaveTime'),
        "MPFDate":lang('form_MPFDate')       
      },
      inputs:{},
      countries:[],
      departments:[],
      banks:[],
      positions:[],
      uploading: false
    },
    methods:{
      editForm: function(){
        this.uploading = true;
        $.ajax({
          url: HOST + '/accountant/form/editForm/',
          type: 'POST',
          dataType: 'json',
          data: this.inputs,
          timeout:AJAX_TIMEOUT,
          error:function(){
            editForm.uploading = false;
          },
          success:function(data)
          {
            console.log('hello');
            editForm.uploading = false;
            if(checkStatus(data) == false)
              return;  
            else {
              $("#editModal").modal('hide');
              Notify.notify('success', lang('notify_success'), lang('notify_successMessage'));
            }
            tableData.refresh(); 
          }
        }); 
      },
      getFormDetail:function(id){
        getDetail(id, URLS['GET_FORMS_DETAIL'],function(data){
          editForm.inputs = data.data[0];
          $("#editModal").modal('show');
        });    
      }
    }
  }) 



</script>
