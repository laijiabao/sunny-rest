

<div class="row">
  <div class="col-md-12">
    <div class="box" id="Stat_timeRange">
      <div class="box-header with-border">
        <h3 class="box-title">{{labels.title}}</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">

        <div class="row">
          <div class="col-md-3 col-md-offset-1">
            <label>{{labels.startDate}}</label>  
            <dropdown class="form-group">
              <div class="input-group">
                <input class="form-control" type="text" v-model="startDate">
                <div class="input-group-btn">
                  <btn class="dropdown-toggle"><i class="glyphicon glyphicon-calendar"></i></btn>
                </div>
              </div>
              <template slot="dropdown">
                <li>
                  <date-picker v-model="startDate" :today-btn="false" :clear-btn="false"/>
                </li>
              </template>
            </dropdown>
          </div>
          <div class="col-md-3">
            <label>{{labels.endDate}}</label>  
            <dropdown class="form-group">
              <div class="input-group">
                <input class="form-control" type="text" v-model="endDate">
                <div class="input-group-btn">
                  <btn class="dropdown-toggle"><i class="glyphicon glyphicon-calendar"></i></btn>
                </div>
              </div>
              <template slot="dropdown">
                <li>
                  <date-picker v-model="endDate" :today-btn="false" :clear-btn="false"/>
                </li>
              </template>
            </dropdown>
          </div>
          <div class="col-md-3 col-md-offset-1 form-group">
            <div class="row">
              <label>{{labels.chooseCompany}}</label>   
<!--               <btn-group>
                <btn @click="setCompanyId(0)">{{labels.allCompany}}</btn>
                <btn @click="setCompanyId(1)">{{labels.wkpang}}</btn>
                <btn @click="setCompanyId(2)">{{labels.chk}}</btn>
              </btn-group> -->
            </div>
            <div class="row">
              <div class="btn-group">
                <a class="dropdown-toggle btn btn-default" data-toggle="dropdown" aria-expanded="false" style="width: 200px">{{chosenCompany}}</a> 
                <ul class="dropdown-menu">
                  <li>
                    <a @click="setCompanyId(1)">{{labels.wkpang}}</a>
                  </li>
                  <li>
                    <a @click="setCompanyId(2)">{{labels.chk}}</a>
                  </li>
                  <li>
                    <a @click="setCompanyId(0)">{{labels.allCompany}}</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>

        </div>
      </div>
      <!-- ./box-body -->
      <div class="box-footer">

        <!-- /.row -->
      </div>
      <!-- /.box-footer -->
    </div>
    <!-- /.box -->
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="box" id="Stat_Revenue">
      <div class="box-header with-border">
        <h3 class="box-title">{{labels.title}}</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col-md-8">
            <h4 class="text-center">
              <!-- <strong>{{startDate}} to {{endDate}}</strong> -->
            </h4>

            <div class="chart" style="width: 100%;">
              <!-- Sales Chart Canvas -->
              <canvas id="chart_income"></canvas>
            </div>
            <!-- /.chart-responsive -->
          </div>
          <!-- /.col -->
          <div class="col-md-4">
            <h4 class="text-center">
            <strong>{{labels.statistic}}</strong>
            </h4>
            <div class="row">
              <div class="col-sm-12 col-xs-6">
                <div class="description-block border-right">
                  <!-- <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 17%</span> -->
                  <h5 class="description-header">${{totalRevenue}}</h5>
                  <span class="description-text">{{labels.totalRevenue}}</span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
              <div class="col-sm-12 col-xs-6">
                <div class="description-block border-right">
                  <!-- <span class="description-percentage text-yellow"><i class="fa fa-caret-left"></i> 0%</span> -->
                  <h5 class="description-header">{{totalClients}}</h5>
                  <span class="description-text">{{labels.totalClients}}</span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
              <div class="col-sm-12 col-xs-6">
                <div class="description-block border-right">
                  <!-- <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 20%</span> -->
                  <h5 class="description-header">{{totalManHours}}</h5>
                  <span class="description-text">{{labels.totalManHours}}</span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
              <div class="col-sm-12 col-xs-6">
                <div class="description-block">
                  <!-- <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> 18%</span> -->
                  <h5 class="description-header">{{totalTasks}}</h5>
                  <span class="description-text">{{labels.totalTasks}}</span>
                </div>
                <!-- /.description-block -->
              </div>
            </div>  
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- ./box-body -->
      <div class="box-footer">

        <!-- /.row -->
      </div>
      <!-- /.box-footer -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
<div class="row">
  <div class="col-md-12">
    <div class="box" id="Stat_Task">
      <div class="box-header with-border">
        <h3 class="box-title">{{labels.title}}</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col-md-6">
            <div class="box box-info">
              <!-- /.box-header -->
              <div class="box-body">
                <div class="table-responsive">
                  <table class="table no-margin">
                    <thead>
                    <tr>
                      <th>{{labels.taskCode}}</th>
                      <th>{{labels.taskName}}</th>
                      <th>{{labels.value}}</th>
                      <th>{{labels.hours}}</th>
                      <th>{{labels.average}}</th>
                    </tr>
                    </thead>
                    <tbody>
                      <tr v-for="item in items">
                        <td>{{item.taskCode}}</td>
                        <td>{{item.taskName}}</td>
                        <td>{{item.value}}</td>
                        <td>{{item.manHours}}</td>
                        <td>{{item.average}}</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.box-body -->
              <div class="box-footer clearfix">
                <pagination v-model="param.pagination" :total-page="totalPage" size="sm" align="right" boundary-links style="margin-top:-20px; margin-bottom:-20px;"/>
              </div>
              <!-- /.box-footer -->
            </div>
            <!-- /.chart-responsive -->
          </div>
          <div class="col-md-6">
            <div class="btn-group pull-right">
              <button type="button" class="btn btn-default" v-on:click="changeChart(1)">{{labels.average}}</button>
              <button type="button" class="btn btn-default" v-on:click="changeChart(2)">{{labels.total}}</button>
            </div>
            <canvas id="chart_task"></canvas>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- ./box-body -->
      <!-- /.box-footer -->
    </div>
    <!-- /.box -->
  </div>  
</div>

<div class="row">
  <div class="col-md-12">
    <div class="box" id="Stat_Client">
      <div class="box-header with-border">
        <h3 class="box-title">{{labels.title}}</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col-md-6">
            <div class="box box-danger">
              <!-- /.box-header -->
              <div class="box-body">
                <div class="table-responsive">
                  <table class="table no-margin">
                    <thead>
                    <tr>
                      <th>{{labels.clientCode}}</th>
                      <th>{{labels.clientName}}</th>
                      <th>{{labels.value}}</th>
                      <th>{{labels.hours}}</th>
                      <th>{{labels.average}}</th>
                    </tr>
                    </thead>
                    <tbody>
                      <tr v-for="item in items">
                        <td>{{item.clientCode}}</td>
                        <td>{{item.clientName}}</td>
                        <td>{{item.value}}</td>
                        <td>{{item.manHours}}</td>
                        <td>{{item.average}}</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.box-body -->
              <div class="box-footer clearfix">
                <pagination v-model="param.pagination" :total-page="totalPage" size="sm" align="right" boundary-links style="margin-top:-20px; margin-bottom:-20px;"/>
              </div>
              <!-- /.box-footer -->
            </div>
            <!-- /.chart-responsive -->
          </div>
          <div class="col-md-6">
            <div class="btn-group pull-right">
              <button type="button" class="btn btn-default" v-on:click="changeChart(1)">{{labels.average}}</button>
              <button type="button" class="btn btn-default" v-on:click="changeChart(2)">{{labels.total}}</button>
            </div>
            <canvas id="chart_client"></canvas>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- ./box-body -->
      <!-- /.box-footer -->
    </div>
    <!-- /.box -->
  </div>  
</div>

<div class="row">
  <div class="col-md-12">
    <div class="box" id="Stat_Staff">
      <div class="box-header with-border">
        <h3 class="box-title">{{labels.title}}</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col-md-6">
            <div class="box box-success">
              <!-- /.box-header -->
              <div class="box-body">
                <div class="table-responsive">
                  <table class="table no-margin">
                    <thead>
                    <tr>
                      <th>{{labels.staffNo}}</th>
                      <th>{{labels.staffName}}</th>
                      <th>{{labels.value}}</th>
                      <th>{{labels.hours}}</th>
                      <th>{{labels.average}}</th>
                    </tr>
                    </thead>
                    <tbody>
                      <tr v-for="item in items">
                        <td>{{item.staffNo}}</td>
                        <td>{{item.name}}</td>
                        <td>{{item.value}}</td>
                        <td>{{item.manHours}}</td>
                        <td>{{item.average}}</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.box-body -->
              <div class="box-footer clearfix">
                <pagination v-model="param.pagination" :total-page="totalPage" size="sm" align="right" boundary-links style="margin-top:-20px; margin-bottom:-20px;"/>
              </div>
              <!-- /.box-footer -->
            </div>
            <!-- /.chart-responsive -->
          </div>
          <div class="col-md-6">
            <div class="btn-group pull-right">
              <button type="button" class="btn btn-default" v-on:click="changeChart(1)">{{labels.average}}</button>
              <button type="button" class="btn btn-default" v-on:click="changeChart(2)">{{labels.total}}</button>
            </div>
            <canvas id="chart_staff"></canvas>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- ./box-body -->
      <!-- /.box-footer -->
    </div>
    <!-- /.box -->
  </div>  
</div>




<script>

  var GLOBAL_DATA;

  function loadDashboard(startDate, endDate, companyId){
    $.ajax({
      url: HOST + '/accountant/statistic/loadDashboard',
      type: 'GET',
      dataType: 'json',
      data:{"startDate":startDate, "endDate":endDate, "companyId": companyId},
      timeout:AJAX_TIMEOUT,
      error:function(){
      },
      success:function(data)
      {
        if(checkStatus(data) == false)
          return; 

        if(Stat_timeRange.isFirstLoad){
          createRevenueChart(data.data.revenue);
          createStaffChart(data.data.topStaffs);
          createTaskChart(data.data.topTasks);
          createClientChart(data.data.topClients);
        }

            GLOBAL_DATA = data;


        updateStaffChart(data.data.topStaffs);
        updateTaskChart(data.data.topTasks);
        updateClientChart(data.data.topClients);
        updateRevenueCharts(data.data.revenue);

        updateStaffStats(data.data.staffs);
        updateTaskStats(data.data.tasks);
        updateClientStats(data.data.clients);

        Stat_timeRange.isFirstLoad = false;

        Stat_Staff.param.pagination = 1;
        Stat_Task.param.pagination = 1;
        Stat_Client.param.pagination = 1;
      }
    });   
  }


  function createRevenueChart(data){

    var ctx_revenue = document.getElementById("chart_income");
    Stat_Revenue.chart = new Chart(ctx_revenue, {
        type: 'line',
        data: {
            labels: Stat_Revenue.x,
            datasets: [{ 
              data: Stat_Revenue.y,
              borderColor: "#3e95cd",
              backgroundColor: "rgba(62, 149, 205, 0.2)",
              fill: true
          }]
        },
        options: {
          title: {
            display:true,
            text: Stat_Revenue.startDate + ' to ' + Stat_Revenue.endDate
          },
          legend: { display: false },
            scales: {
                xAxes:[{
                  gridLines: {
                      display:false
                  }
                }],
                yAxes: [{
                  gridLines: {
                      display:true
                  },
                  ticks: {
                      beginAtZero:true
                  }
                }]
            }
        }
    });    
  }
  function createStaffChart(data){

    // var ctx_revenue = document.getElementById("chart_income");
    var ctx_staff = document.getElementById("chart_staff");
    ctx_staff.height = 200;
    Stat_Staff.chart = new Chart(ctx_staff, {
      type: 'horizontalBar',
      data: {
        labels: Stat_Staff.x1,
        datasets: [
          {
            label: lang('dashboard_averageOrder'),
            backgroundColor: ["#3cba9f","#3cba9f","#3cba9f","#3cba9f","#3cba9f","#3cba9f","#3cba9f","#3cba9f","#3cba9f","#3cba9f"],
            data: Stat_Staff.y1
          }
        ]
      },
      options: {
        legend: { display: false },
        title: {
          display: true,
          text: lang('dashboard_averageOrder')
        },
        scales: {
            xAxes:[{
              gridLines: {
                  display:true
              }
            }],
            yAxes: [{
              gridLines: {
                  display:false
              },
              ticks: {
                  beginAtZero:true
              }
            }]
        }
      }
    });    
  }

  function createTaskChart(data){

    // var ctx_revenue = document.getElementById("chart_income");
    var ctx_task = document.getElementById("chart_task");
    ctx_task.height = 200;
    Stat_Task.chart = new Chart(ctx_task, {
      type: 'horizontalBar',
      data: {
        labels: Stat_Task.x1,
        datasets: [
          {
            label: lang('dashboard_averageOrder'),
            backgroundColor: ["#3e95cd","#3e95cd","#3e95cd","#3e95cd","#3e95cd","#3e95cd","#3e95cd","#3e95cd","#3e95cd","#3e95cd"],
            data: Stat_Task.y1
          }
        ]
      },
      options: {
        legend: { display: false },
        title: {
          display: true,
          text: lang('dashboard_averageOrder')
        },
        scales: {
            xAxes:[{
              gridLines: {
                  display:true
              }
            }],
            yAxes: [{
              gridLines: {
                  display:false
              },
              ticks: {
                  beginAtZero:true
              }
            }]
        }
      }
    });    
  }
  function createClientChart(data){

    // var ctx_revenue = document.getElementById("chart_income");
    var ctx_client = document.getElementById("chart_client");
    ctx_client.height = 200;
    Stat_Client.chart = new Chart(ctx_client, {
      type: 'horizontalBar',
      data: {
        labels: Stat_Client.x1,
        datasets: [
          {
            label: lang('dashboard_averageOrder'),
            backgroundColor: ["#c45850","#c45850","#c45850","#c45850","#c45850","#c45850","#c45850","#c45850","#c45850","#c45850"],
            data: Stat_Client.y1
          }
        ]
      },
      options: {
        legend: { display: false },
        title: {
          display: true,
          text: lang('dashboard_averageOrder')
        },
        scales: {
            xAxes:[{
              gridLines: {
                  display:true
              }
            }],
            yAxes: [{
              gridLines: {
                  display:false
              },
              ticks: {
                  beginAtZero:true
              }
            }]
        }
      }
    });    
  }

  function updateRevenueCharts(data){
    Stat_Revenue.x = [];
    Stat_Revenue.y = [];

            console.log(data);
            // console.log(data.data.topStaffs);

    var r_data = data.value;
    var numOfData = r_data.length;

    Stat_Revenue.startDate = Stat_timeRange.startDate;
    Stat_Revenue.endDate = Stat_timeRange.endDates;

    for(var i = numOfData - 1 ; i >= 0; i--){

      Stat_Revenue.x.push(r_data[i].date);
      Stat_Revenue.y.push(r_data[i].revenue);

    }

    Stat_Revenue.totalRevenue = data.totalRevenue;
    Stat_Revenue.totalClients = data.totalClients;
    Stat_Revenue.totalManHours = data.totalManHours;
    Stat_Revenue.totalTasks = data.totalTasks;   

    Stat_Revenue.updateChart(); 
  }

  function updateStaffChart(data){  

    Stat_Staff.total = data.total;
    Stat_Staff.average = data.average;

    var numOfData = data.total.length;

    Stat_Staff.x1 = [];
    Stat_Staff.x2 = [];
    Stat_Staff.y1 = [];
    Stat_Staff.y2 = [];

    for(var i = 0 ; i < numOfData; i++){

      Stat_Staff.x1.push(Stat_Staff.average[i].name);
      Stat_Staff.x2.push(Stat_Staff.total[i].name);
      Stat_Staff.y1.push(Stat_Staff.average[i].average);
      Stat_Staff.y2.push(Stat_Staff.total[i].value);

    }

    Stat_Staff.updateChart();

  } 

  function updateStaffStats(data){

    Stat_Staff.items = data.data;
    Stat_Staff.totalPage = Math.ceil(parseInt(data.total)/RECORDS_PER_PAGE);  

  }

  function getStaffStats(param){

    $.ajax({
      url: HOST + '/accountant/statistic/getStaffStats',
      type: 'GET',
      dataType: 'json',
      data: param,
      timeout:AJAX_TIMEOUT,
      error:function(){
      },
      success:function(data)
      {
        if(checkStatus(data) == false)
          return; 
        updateStaffStats(data.data.staffs);
      }
    });    
  }


  function updateTaskChart(data){  

    Stat_Task.total = data.total;
    Stat_Task.average = data.average;

    var numOfData = data.total.length;

    Stat_Task.x1 = [];
    Stat_Task.x2 = [];
    Stat_Task.y1 = [];
    Stat_Task.y2 = [];

    for(var i = 0 ; i < numOfData; i++){

      Stat_Task.x1.push(Stat_Task.average[i].name);
      Stat_Task.x2.push(Stat_Task.total[i].name);
      Stat_Task.y1.push(Stat_Task.average[i].average);
      Stat_Task.y2.push(Stat_Task.total[i].value);

    }

    Stat_Task.updateChart();

  } 

  function updateTaskStats(data){

    Stat_Task.items = data.data;
    Stat_Task.totalPage = Math.ceil(parseInt(data.total)/RECORDS_PER_PAGE);  

  }

  function getTaskStats(param){

    $.ajax({
      url: HOST + '/accountant/statistic/getTaskStats',
      type: 'GET',
      dataType: 'json',
      data: param,
      timeout:AJAX_TIMEOUT,
      error:function(){
      },
      success:function(data)
      {
        if(checkStatus(data) == false)
          return; 
        updateTaskStats(data.data.tasks);
      }
    });    
  }

  function updateClientChart(data){  

    Stat_Client.total = data.total;
    Stat_Client.average = data.average;

    var numOfData = data.total.length;

    Stat_Client.x1 = [];
    Stat_Client.x2 = [];
    Stat_Client.y1 = [];
    Stat_Client.y2 = [];

    for(var i = 0 ; i < numOfData; i++){

      Stat_Client.x1.push(Stat_Client.average[i].name);
      Stat_Client.x2.push(Stat_Client.total[i].name);
      Stat_Client.y1.push(Stat_Client.average[i].average);
      Stat_Client.y2.push(Stat_Client.total[i].value);

    }

    Stat_Client.updateChart();

  } 

  function updateClientStats(data){

    Stat_Client.items = data.data;
    Stat_Client.totalPage = Math.ceil(parseInt(data.total)/RECORDS_PER_PAGE);  

  }

  function getClientStats(param){

    $.ajax({
      url: HOST + '/accountant/statistic/getClientStats',
      type: 'GET',
      dataType: 'json',
      data: param,
      timeout:AJAX_TIMEOUT,
      error:function(){
      },
      success:function(data)
      {
        if(checkStatus(data) == false)
          return; 
        updateClientStats(data.data.clients);
      }
    });    
  }


  var Stat_timeRange = new Vue({
    el: '#Stat_timeRange',
    data: {     
      labels:{
        "startDate":lang('dashboard_startDate'),
        "endDate":lang('dashboard_endDate'),
        "title":lang('dashboard_timeRange'),
        "chooseCompany": lang('dashboard_chooseCompany'),
        "allCompany": lang('dashboard_allCompany'),
        "wkpang": lang('dashboard_wkpang'),
        "chk": lang('dashboard_chk')
      },
      startDate: moment().subtract(1, 'Y').format('YYYY-MM-DD'),
      endDate: moment().format('YYYY-MM-DD'),
      companyId: 1,
      chosenCompany:lang('dashboard_wkpang'),
      isFirstLoad: true
    },
    methods:{ 
      setCompanyId:function(companyId){
        this.companyId = companyId;
        if(companyId == 1){
          this.chosenCompany = this.labels.wkpang;
        }else if(companyId == 2){
          this.chosenCompany = this.labels.chk;
        }else{
          this.chosenCompany = this.labels.allCompany;
        }    
      }   

    },
    created:function(){

      loadDashboard(this.startDate, this.endDate, this.companyId);

    },
    watch:{
      startDate: {
        handler(newValue, oldValue){
          loadDashboard(this.startDate, this.endDate, this.companyId);
        }
      },
      endDate:{
        handler(newValue, oldValue){
          loadDashboard(this.startDate, this.endDate, this.companyId);
        }       
      },
      companyId:{
        handler(newValue, oldValue){  
          loadDashboard(this.startDate, this.endDate, this.companyId);
        }           
      }
    }
  });

  var Stat_Revenue = new Vue({
    el: '#Stat_Revenue',
    data: {     
      labels:{
        "statistic":lang('dashboard_statistic'),
        "totalRevenue":lang('dashboard_totalRevenue'),
        "totalClients":lang('dashboard_totalClients'),
        "totalManHours":lang('dashboard_totalManHours'),
        "totalTasks":lang('dashboard_totalTasks'),
        "title":lang('dashboard_revenue_title')
      },
      chart:null,
      x:[],
      y:[],
      startDate: moment().subtract(1, 'Y').format('YYYY-MM-DD'),
      endDate: moment().format('YYYY-MM-DD'),
      totalRevenue: 0,
      totalClients: 0,
      totalManHours:0,
      totalTasks: 0
    },
    methods:{    
      updateChart: function(){
        this.chart.data.labels = this.x;
        this.chart.data.datasets[0].data = this.y;
        this.chart.options.title.text = Stat_timeRange.startDate + ' to ' + Stat_timeRange.endDate;
        this.chart.update();
      }
    },
    created:function(){

    },
    watch:{

    }
  });

  var Stat_Staff = new Vue({
    el: '#Stat_Staff',
    data: {     
      labels:{
        "total":lang('dashboard_total'),
        "average":lang('dashboard_average'),
        "hours":lang('dashboard_hours'),
        "value":lang('dashboard_value'),
        "staffNo":lang('dashboard_staffNo'),
        "staffName":lang('dashboard_staffName'),
        "title":lang('dashboard_staffPerformance')
      },
      startDate:'',
      endDate:'',
      total:[],
      average:[],
      items:[],
      x1:[],
      x2:[],
      y1:[],
      y2:[],
      chart:null,
      chartType: 1,
      param: {
        "pagination":1,
        "startDate":Stat_timeRange.startDate,
        "endDate":Stat_timeRange.endDate
      },
      currentPage: 1,
      totalPage: 1
    },
    methods:{    
      changeChart: function(type){
        if(type == this.chartType || this.chart == null){
          return;
        }else if(type == 1){
          this.chartType = type;
          this.chart.data.labels = this.x1;
          this.chart.data.datasets[0].data = this.y1;
          this.chart.options.title.text = lang('dashboard_averageOrder');
        }else{
          this.chartType = type;
          this.chart.data.labels = this.x2;
          this.chart.data.datasets[0].data = this.y2;
          this.chart.options.title.text = lang('dashboard_totalOrder');
        }
        this.chart.update();
      },
      updateChart: function(){
        this.chart.data.labels = this.x1;
        this.chart.data.datasets[0].data = this.y1;
        this.chart.options.title.text = lang('dashboard_averageOrder');
        this.chart.update();
      }
    },
    created:function(){   
      // getStaffStats(this.param);   
    },
    watch:{
      param: {
        handler(newValue, oldValue){
          getStaffStats(this.param);
        },
        deep:true
      }
    }
  });

  var Stat_Task = new Vue({
    el: '#Stat_Task',
    data: {     
      labels:{
        "total":lang('dashboard_total'),
        "average":lang('dashboard_average'),
        "hours":lang('dashboard_hours'),
        "value":lang('dashboard_value'),
        "taskCode":lang('dashboard_taskCode'),
        "taskName":lang('dashboard_taskName'),
        "title":lang('dashboard_taskPerformance')
      },
      startDate:'',
      endDate:'',
      total:[],
      average:[],
      items:[],
      x1:[],
      x2:[],
      y1:[],
      y2:[],
      chart:null,
      chartType: 1,
      param: {
        "pagination":1,
        "startDate":Stat_timeRange.startDate,
        "endDate":Stat_timeRange.endDate
      },
      currentPage: 1,
      totalPage: 1
    },
    methods:{    
      changeChart: function(type){
        if(type == this.chartType || this.chart == null){
          return;
        }else if(type == 1){
          this.chartType = type;
          this.chart.data.labels = this.x1;
          this.chart.data.datasets[0].data = this.y1;
          this.chart.options.title.text = lang('dashboard_averageOrder');
        }else{
          this.chartType = type;
          this.chart.data.labels = this.x2;
          this.chart.data.datasets[0].data = this.y2;
          this.chart.options.title.text = lang('dashboard_totalOrder');
        }
        this.chart.update();
      },
      updateChart: function(){
        this.chart.data.labels = this.x1;
        this.chart.data.datasets[0].data = this.y1;
        this.chart.options.title.text = lang('dashboard_averageOrder');
        this.chart.update();
      }
    },
    created:function(){   

    },
    watch:{
      param: {
        handler(newValue, oldValue){
          getTaskStats(this.param);
        },
        deep:true
      }
    }
  });


  var Stat_Client = new Vue({
    el: '#Stat_Client',
    data: {     
      labels:{
        "total":lang('dashboard_total'),
        "average":lang('dashboard_average'),
        "hours":lang('dashboard_hours'),
        "value":lang('dashboard_value'),
        "clientCode":lang('dashboard_clientCode'),
        "clientName":lang('dashboard_clientName'),
        "title":lang('dashboard_clientPerformance')
      },
      startDate:'',
      endDate:'',
      total:[],
      average:[],
      items:[],
      x1:[],
      x2:[],
      y1:[],
      y2:[],
      chart:null,
      chartType: 1,
      param: {
        "pagination":1,
        "startDate":Stat_timeRange.startDate,
        "endDate":Stat_timeRange.endDate
      },
      currentPage: 1,
      totalPage: 1
    },
    methods:{    
      changeChart: function(type){
        if(type == this.chartType || this.chart == null){
          return;
        }else if(type == 1){
          this.chartType = type;
          this.chart.data.labels = this.x1;
          this.chart.data.datasets[0].data = this.y1;
          this.chart.options.title.text = lang('dashboard_averageOrder');
        }else{
          this.chartType = type;
          this.chart.data.labels = this.x2;
          this.chart.data.datasets[0].data = this.y2;
          this.chart.options.title.text = lang('dashboard_totalOrder');
        }
        this.chart.update();
      },
      updateChart: function(){
        this.chart.data.labels = this.x1;
        this.chart.data.datasets[0].data = this.y1;
        this.chart.options.title.text = lang('dashboard_averageOrder');
        this.chart.update();
      }
    },
    created:function(){   
      // getStaffStats(this.param);   
    },
    watch:{
      param: {
        handler(newValue, oldValue){
          getClientStats(this.param);
        },
        deep:true
      }
    }
  });

</script>

