<script>

  // // Init Date input
  $('#startDate').daterangepicker({singleDatePicker: true, format: 'YYYY-MM-DD',"timePicker24Hour": true, locale:{format: 'YYYY-MM-DD'}},function(start, end, label) {
    console.log(start.format('YYYY-MM-DD'));
    tableData.param.startDate = start.format('YYYY-MM-DD');
  });
  $('#startDate').data('daterangepicker').setStartDate('2016-01-01');
  $('#startDate').data('daterangepicker').setEndDate('2016-01-01');

  $('#endDate').daterangepicker({singleDatePicker: true, format: 'YYYY-MM-DD',"timePicker24Hour": true,locale:{format: 'YYYY-MM-DD'}},function(start, end, label) {
    console.log(start);
    tableData.param.startDate = start.format('YYYY-MM-DD');
    tableData.param.endDate = start.format('YYYY-MM-DD');
  });

  function initRolesOptions()
  {
    $.ajax({
      url: HOST + '/accountant/user/getRoles/'+SYSTEM_LANGUAGE,
      type: 'GET',
      dataType: 'json',
      success: function (data) {
        if(data.status == 200){
          createForm.roles = data.data;
          editForm.roles = data.data;
        }
        else{
          alert(data.message);    
        }
      }
    });       
  }

  function initOptions()
  {
    $.ajax({
      url: HOST + '/accountant/code/getCodes/'+SYSTEM_LANGUAGE+'?options[]=userStatus',
      type: 'GET',
      dataType: 'json',
      success: function (data) {
        if(data.status == 200){
          for(var i = 0; i < data.data.length; i++){
            if(data.data[i].type == 'userStatus')
              editForm.statuses.push(data.data[i]);
          }
        }
        else{
          alert(data.message);    
        }
      }
    });       
  }



</script>

<!-- /.row -->
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">     
        <div class="pull-left">
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createModal">
            <i class="fa fa-plus"></i>
          </button>
          <button type="button" class="btn btn-danger" onclick="tableData.deleteUsers()">
            <i class="fa fa-trash"></i>
          </button>
        </div>    
        <div class="col-xs-4" id="searchBar">
          <div class="input-group">
            <input type="text" class="form-control" v-model="search" v-on:keyup.enter="searchUser">
            <span class="input-group-btn">
              <button type="button" class="btn btn-info" v-on:click="searchUser">{{searchBtn}}</button>
            </span>
          </div>
        </div>
        <div class="col-xs-3">
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input type="text" class="form-control pull-right" id="startDate">
          </div>
        </div>

        <div class="col-xs-3">
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input type="text" class="form-control pull-right" id="endDate">
          </div>
        </div>
      </div>          

      <!-- /.box-header -->
      <div class="box-body">
        <table id="table" class="table table-bordered table-hover">

          <thead>
            <tr>
              <th><input type="checkbox" :checked="false" v-model="selectAll"></th>
              <th>{{userId}}</th>
              <th>{{userName}}</th>
              <th>{{role}}</th>
              <th>{{ip}}</th>
              <th>{{lastLogin}}</th>
              <th>{{status}}</th>
              <th>{{actions}}</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="item in items">
              <td>
                <input type="checkbox" v-model="selected" :value="item.userId">
              </td>
              <td>{{item.userId}}</td>
              <td>{{item.userName}}</td>
              <td>{{item.role}}</td>
              <td>{{item.ip}}</td>
              <td>{{item.lastLogin}}</td>
              <td>
                <span class="badge bg-green" v-if="item.status == 'A'">{{item.statusName}}</span>
                <span class="badge" v-else-if="item.status == 'D'">{{item.statusName}}</span>
                <span class="badge bg-red" v-else>{{item.statusName}}</span>
              </td>
              <td>
                <div class="">
                  <!-- <a class="btn-xs" v-on:click="editItem"><i class="fa fa-eye"></i></a> -->
                  <a class="btn-xs" v-on:click="editItem(item.userId)"><i class="fa fa-edit"></i></a>
                  <a class="btn-xs" v-on:click="editUserPassword(item.userId)"><i class="fa fa-lock"></i></a>
                  <!-- <a class="btn-xs" v-on:click="editItem(item.userId)"><i class="fa fa-ban"></i></a> -->
                </div>
              </td>
            </tr>
            
          </tbody>

        </table>
      </div>
      <div class="box-footer clearfix" id="tableFooter">
        <div class="col-xs-3">
          <span>{{paginationPrefix}}<b>{{total}}</b>{{paginationSuffix}}</span>
        </div>
          <!-- <ul class="pagination pagination-sm no-margin pull-right" id="pagination"></ul> -->
        <div class="col-xs-9">
            <pagination v-model="currentPage" :total-page="totalPage" size="sm" align="right" boundary-links/>
        </div> 
      </div>
    </div>
    <!-- /.box -->
  </div>
</div>
<!-- /.row -->

<div class="modal fade" id="createModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <!-- form start -->                
        <div class="box-body">     
          <form role="form" id="createForm">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#c_tab_1" data-toggle="tab" aria-expanded="true" id="c_n_tab_1">{{labels.new}}</a></li>
                <li class="pull-right"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></li>
              </ul>
              <div class="tab-content" id="c_tab_content">
                <!-- /.tab-pane -->                      
                <div class="tab-pane active" id="c_tab_1">
                  <!-- <input type="hidden" class="form-control" name="id"> -->
                  <!-- <input type="hidden" class="form-control" name="version"> -->
                  <div class="row">          
                    <div class="col-xs-4 form-group">
                      <label>{{labels.userName}}</label>  
                      <input type="text" class="form-control" v-model="inputs.userName">
                    </div> 
                    <div class="col-xs-6 form-group">
                      <label>{{labels.selectStaff}}</label>  
                      <div class="input-group">
                        <input type="text" class="form-control" placeholder="" id="c_staffName">
                        <span class="input-group-btn">
                          <button type="button" class="btn btn-warning" v-on:click="showStaffList()">{{labels.selectStaff}}
                          </button>
                        </span>
                      </div>
                      <typeahead v-model="staff" target="#c_staffName" async-src="/accountant/search/staff/" async-key="data" item-key="name" debounce="200"/>   
  
                    </div>
                  </div>  
                  <div class="row">          
                    <div class="col-xs-3 form-group">
                      <label>{{labels.role}}</label>  
                      <select class="form-control" v-model="inputs.role" >
                        <option v-for="role in roles" v-bind:value="role.role">{{role.name}}</option>
                      </select>
                    </div> 
                  </div>   
                  <div class="row">          
                    <div class="col-xs-6 form-group">
                      <label>{{labels.password}}</label>  
                      <input type="password" class="form-control" v-model="inputs.password">
                    </div> 
                  </div> 
                  <div class="row">          
                    <div class="col-xs-6 form-group">
                      <label>{{labels.passwordConf}}</label>  
                      <input type="password" class="form-control" name="passwordConfirm" v-model="inputs.passwordConfirm">
                    </div> 
                  </div>                                         
                </div>
                <!-- /.tab-pane -->  

              </div>                    
              <!-- /.box-body -->
            </div>
            <div class="box-footer">
              <button class="btn btn-primary pull-right" v-on:click.prevent="createUser" :disabled="uploading">{{labels.new}}</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>


<div class="modal fade" id="editModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <!-- form start -->                
        <div class="box-body">     
          <form role="form" id="editForm">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#c_tab_1" data-toggle="tab" aria-expanded="true">{{labels.edit}}</a></li>
                <li class="pull-right"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></li>
              </ul>
              <div class="tab-content">
                <!-- /.tab-pane -->                      
                <div class="tab-pane active">
                  <input type="hidden" class="form-control" name="id" v-model="inputs.userId">
                  <!-- <input type="hidden" class="form-control" name="version"> --> 
                  <div class="row">          
                    <div class="col-xs-3 form-group">
                      <label>{{labels.role}}</label>  
                      <select class="form-control" v-model="inputs.role" >
                        <option v-for="role in roles" v-bind:value="role.role">{{role.name}}</option>
                      </select>
                    </div> 
                  </div>   
                  <div class="row">          
                    <div class="col-xs-3 form-group">
                      <label>{{labels.status}}</label>  
                      <select class="form-control" v-model="inputs.status" >
                        <option v-for="status in statuses" v-bind:value="status.code">{{status.name}}</option>
                      </select>
                    </div> 
                  </div>                                       
                </div>
                <!-- /.tab-pane -->  
              </div>                    
              <!-- /.box-body -->
            </div>
            <div class="box-footer">
              <button class="btn btn-primary pull-right" v-on:click.prevent="editUser" :disabled="uploading">{{labels.update}}</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="editPasswordModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <!-- form start -->                
        <div class="box-body">     
          <form role="form" id="editPasswordForm">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active"><a href="" data-toggle="tab" aria-expanded="true">{{labels.editPassword}}</a></li>
                <li class="pull-right"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></li>
              </ul>
              <div class="tab-content">
                <!-- /.tab-pane -->                      
                <div class="tab-pane active">
                  <input type="hidden" class="form-control" name="id" v-model="inputs.userId">
                  <!-- <input type="hidden" class="form-control" name="version"> --> 
                  <div class="row">          
                    <div class="col-xs-6 form-group">
                      <label>{{labels.oldPassword}}</label>  
                      <input type="text" class="form-control" name="oldPassword" v-model="inputs.oldPassword">
                    </div> 
                  </div>   
                  <div class="row">          
                    <div class="col-xs-6 form-group">
                      <label>{{labels.newPassword}}</label>  
                      <input type="text" class="form-control" name="newPassword" v-model="inputs.newPassword">
                    </div> 
                  </div>  
                  <div class="row">          
                    <div class="col-xs-6 form-group">
                      <label>{{labels.newPasswordConf}}</label>  
                      <input type="text" class="form-control" name="newPasswordConf" v-model="inputs.newPasswordConf">
                    </div> 
                  </div>                                      
                </div>
                <!-- /.tab-pane -->  
              </div>                    
              <!-- /.box-body -->
            </div>
            <div class="box-footer">
              <button class="btn btn-primary pull-right" v-on:click.prevent="editUserPassword" :disabled="uploading">{{labels.update}}</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>


<script>

  var searchBar = new Vue({
    el:'#searchBar',
    data: {
      searchBtn: lang('searchBtn'),    
      search:''
    },
    methods:{
      searchUser: function(){
        tableData.param.search = this.search;
      }
    }
  })

  var tableData = new Vue({
    el: '#table',
    data: {     
      userId: lang('user_userId'),
      userName: lang('user_userName'),
      role:lang('user_role'),
      ip:lang('user_ip'),
      lastLogin:lang('user_lastLogin'),
      status:lang('user_status'),
      actions:lang('user_actions'),
      items: [],
      selected:[],
      param: {
        "pagination":1,
        "search":searchBar.search,
        "lang":SYSTEM_LANGUAGE,
        "startDate":$('#startDate').data('daterangepicker').startDate.format('YYYY-MM-DD'),
        "endDate":$('#endDate').data('daterangepicker').endDate.format('YYYY-MM-DD')
      }
    },
    methods:{
      refresh: function(){
        getTableData(this,pagination,URLS['GET_USERS']);
      },
      checkAll: function(){
        console.log(this.selected);
        // for(item in this.selected)
        //   item.checked=true;
      },
      deleteUsers: function(){
        
        var result;  
        result = confirm(lang('q_confirmDelete'));  

        if(result == 0)
          return;

        $.ajax({
          url: HOST + '/accountant/user/deleteUsers/',
          type: 'GET',
          dataType: 'json',
          data: {"users":this.selected},
          timeout:AJAX_TIMEOUT,
          success:function(data)
          {
            createForm.uploading = false;
            if(checkStatus(data) == false)
              return;  
            tableData.selected = [];
            tableData.refresh(); 
          }
        });         
      },
      editItem: function(itemId){
        $("#editModal").modal('show');
        editForm.inputs.userId = itemId;
        console.log(itemId);

      },
      editUserPassword: function(itemId){
        $("#editPasswordModal").modal('show');
        editPasswordForm.inputs.userId = itemId;
        console.log(itemId);

      }
    },
    computed: {
      selectAll: {
        get: function () {
          return this.items ? this.selected.length == this.items.length : false;
        },
        set: function (value) {
          var selected = [];

          if (value) {
              this.items.forEach(function (item) {
                  selected.push(item.userId);
              });
          }
          this.selected = selected;
        }
      }
    },
    watch:{
      param: {
        handler(newValue, oldValue){
          getTableData(this,pagination,URLS['GET_USERS']);
        },
        deep: true 
      }
    },
    created: function(){

    }
  })

  var pagination = new Vue({
    el: '#tableFooter',
    data: {
      paginationPrefix: lang('paginationPrefix'),
      paginationSuffix: lang('paginationSuffix'),
      total: 0,
      totalPage: 1,
      currentPage: 1
    },
    methods:{
    },
    created:function(){
      getTableData(tableData,this,URLS['GET_USERS']);
      initOptions();
      initRolesOptions();
    },
    watch:{
      currentPage: {
        handler(newValue, oldValue){
          tableData.param.agination = newValue;
        },
      }
    }
  }) 

  var createForm = new Vue({
    el: '#createModal',
    data: {
      labels:{
        "new":lang('user_new'),
        "userName": lang('user_userName'),
        "selectStaff": lang('selectStaff'),
        "role":lang('user_role'),
        "password":lang('user_password'),
        "passwordConf":lang('user_passwordConf'),
        "staffList":lang('list_staff'), 
        "staffName": lang('taskMag_staffName')
      },
      inputs:{},
      roles:[],
      staff:{"name":"", "staffNo":""},
      staffName:'',
      uploading: false
    },
    methods:{
      createUser: function(){
        this.uploading = true;
        $.ajax({
          url: HOST + '/accountant/user/createUser/',
          type: 'POST',
          dataType: 'json',
          data: this.inputs,
          timeout:AJAX_TIMEOUT,
          error:function(){
            createForm.uploading = false;
          },
          success:function(data)
          {
            console.log('hello');
            createForm.uploading = false;
            if(checkStatus(data) == false)
              return;  
            else {
              $("#createModal").modal('hide');
              Notify.notify('success', lang('notify_success'), lang('notify_successMessage'));
            }
            tableData.refresh(); 
          }
        }); 
      },
      showStaffList: function(){
        MD_staffModal.show(this); 
      },
      onStaffSelected: function(staff){
        this.staff = staff;
        this.staff.name = staff.staffNo+' '+staff.alias;
        this.inputs.staffId = staff.staffId;
      }
    },
    created: function () {

    }
  }) 

  var editForm = new Vue({
    el: '#editModal',
    data: {
      labels:{
        "edit":lang('user_edit'),
        "update": lang('user_update'),
        "selectStaff": lang('selectStaff'),
        "role":lang('user_role'),
        "status":lang('user_status')
      },
      inputs:{},
      roles:[],
      statuses:[],
      uploading: false
    },
    methods:{
      editUser: function(){
        this.uploading = true;
        $.ajax({
          url: HOST + '/accountant/user/editUser/',
          type: 'POST',
          dataType: 'json',
          data: this.inputs,
          timeout:AJAX_TIMEOUT,
          error:function(){
            editForm.uploading = false;
          },
          success:function(data)
          {
            this.uploading = false;
            if(checkStatus(data) == false)
              return;     
            else {
              $("#editModal").modal('hide');
              Notify.notify('success', lang('notify_success'), lang('notify_successMessage'));
            }
            tableData.refresh(); 
          }
        }); 
      },
    },
    created: function () {
      // initOptions();
    }
  }) 

  var editPasswordForm = new Vue({
    el: '#editPasswordModal',
    data: {
      labels:{
        "editPassword":lang('user_editPassword'),
        "update": lang('user_update'),
        "oldPassword":lang('user_oldPassword'),
        "newPassword":lang('user_newPassword'),
        "newPasswordConf":lang('user_newPasswordConf')
      },
      inputs:{},
      uploading: false
    },
    methods:{
      editUserPassword: function(){
        this.uploading = true;
        $.ajax({
          url: HOST + '/accountant/user/editPassword/',
          type: 'POST',
          dataType: 'json',
          data: this.inputs,
          timeout:AJAX_TIMEOUT,
          success:function(data)
          {
            this.uploading = false;
            if(checkStatus(data) == false)
              return;     
            else {
              $("#editPasswordModal").modal('hide');
              Notify.notify('success', lang('notify_success'), lang('notify_successMessage'));
            }
            tableData.refresh(); 
          }
        }); 
      },
    },
    created: function () {
      // initOptions();
    }
  }) 


</script>
