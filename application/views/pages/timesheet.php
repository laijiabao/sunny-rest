<style type="text/css">

.fc-content{
  text-align: center;
  font-size: 1.5rem;
  padding-top: 3rem;
  padding-bottom: 3rem;
}

</style>

<div class="row">

  <div class="col-xs-12">
    <div class="box">
      <div class="box-header" style="padding-bottom: 0px !important;">  


        <div style="float:left;">
          <div class="col-xs-10 form-group" id="timesheetStafflist" v-if="ROLE == 'A' || ROLE == 'M'">
            <label >{{labels.staffName}}</label>  
            <div class="input-group" style="width: 60%;">
            <input type="text" class="form-control" placeholder="" id="e_staffName" v-model="staff.name">
            <span class="input-group-btn">
            <button type="button" class="btn btn-warning" v-on:click="showStaffList()">{{labels.staffList}}</button>
            </span>
            </div>
            <typeahead v-model="staff" target="#e_staffName" async-src="/accountant/search/staff/" async-key="data" item-key="name"/>  
          </div>

        </div>
  <div style="float:right;">

      <button id="export_btn" class="btn fc-button" onclick="timesheetModal('csv')" ></button>
<!--       <button class="btn fc-button" onclick="timesheetModal('pdf')" >Export as PDF</button>
 -->
      </div>
    </div>  
    <div class="box-body">
      <div id='calendar'></div>
      <div id='durationSum' class="fc-row fc-widget-header">
        <table style="width: 100%; table-layout: fixed;" >
          <tr>
            <td v-for="day in days" class="text-center" style="font-size: 1.6rem; padding-top: 2rem; padding-bottom: 2rem; border:1px solid rgb(221,221,221); color:#777777;"><span style=" color:#444444;font-weight: bold;"> {{day.duration}} </span>{{labels.hour}}</td>
          </tr>
        </table>
      </div>
    </div>
    <div class="box-footer">
    </div>
  </div>
</div>
</div>


 <!-- Event Panel Modal -->
      <div class="modal fade" id="eventPanel" tabindex="-1" role="dialog" aria-labelledby="eventPanelLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <span class="modal-title" id="eventPanelLabel">{{labels.title}}</span>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">

              <div id="panel">
              <div class="form-group">
              <label>{{labels.eventType}}</label>
              <select v-model="eventType" class="form-control">
              <option value="task" selected>{{labels.task}}</option>
              <option value="leave">{{labels.leave}}</option>
              </select>
              </div>
              <div v-if="eventType=='task'" class="form-group">
              <label>{{labels.task}}</label>
              <select v-model="taskItem" class="form-control">
              <option value="" disabled="disabled">{{labels.taskPlaceholder}}</option>
              <option v-for="task in taskList" :value="task.taskId">{{task.taskName}}</option>
              </select>
              </div>
              <div v-else class="form-group">
              <label>{{labels.leave}}</label>
              <select v-model="leaveType" class="form-control">
              <option value="" disabled="disabled">{{labels.leavePlaceholder}}</option>
              <option v-for="leave in leaveList" :value="leave.code" >{{leave.name}}</option>
              </select>
              </div>
             <div   v-if="eventType=='task'" class="form-group">
           <!--     <label>Company</label>
              <select v-model="clientId" class="form-control">
              <option value="" disabled="disabled">Company List</option>
              <option v-for="client in clientList" :value="client.clientId" >{{client.enClientName}}</option>
              </select>-->
               <label>{{labels.clientName}}</label>  
                     <div class="input-group">
                       <input type="text" class="form-control" placeholder="Company List" id="c_clientName">
                       <span class="input-group-btn">
                        <button type="button" class="btn btn-warning" v-on:click="showClientList()">{{labels.clientList}}</button>


                       </span>
                    </div>
                 <typeahead v-model="client" target="#c_clientName" async-src="/accountant/search/client/" async-key="data" item-key="name"/> 
              </div> 
             <div  class="form-group">
              <label for="exampleInputEmail1">{{labels.timeUsingLabel}}</label>
              <input  type="text" class="form-control" v-model="duration" placeholder="etc. 2.5">
              </div>
              <div v-if="eventType=='task'" class="form-check">
              <label class="form-check-label">
              <input  v-model="isChargeable" id="evnetPanel_checkbox" type="checkbox" class="form-check-input"  checked="checked">
              {{labels.chargeableLabel}}
              </label>
              </div>
              </div> 

            </div>
            <div class="modal-footer">
              <button v-if="modalType == 'Add'" class="btn btn-success" onclick="addEvent()" data-dismiss="modal"> {{labels.create}} </button>
              <div v-else>
              <button class="btn btn-danger" v-on:click="removeEvent(currentEvent);" data-dismiss="modal"> {{labels.delete}} </button>
              <button  class="btn btn-primary" v-on:click="editEvent(currentEvent);" data-dismiss="modal"> {{labels.edit}} </button>
              </div>
            </div>
          </div>
        </div>
      </div>



<!-- TImesheet Print Modal -->

 <div class="modal fade" id="timeSheetPrinter" tabindex="-1" role="dialog" aria-labelledby="timeSheetPrinterLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <span style="font-size: 1.75rem;" class="modal-title" id="timeSheetPrinterLabel" >{{labels.title}}</span>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body ">

           <div class="container" style="padding-top: 1rem; padding-bottom: 1rem;">

    <div class="row">
        <div class='col-sm-6'>
            <div class="form-group py-4">
              <h5 style="display: block; font-weight: 700;"> {{labels.timeRange}} <span style="font-weight: normal !important;">{{labels.timeRangeIndicator}}</span></h5>
                <input id="timesheetRange"  type="text" name="timesheetRange" style="width: 50%; text-align: center; font-size: 1.65rem; padding-top: 5px; padding-bottom: 5px;" />
              <span style="display:block; margin-top: 1.5rem; color: #dd4b39; font-weight: bold;">* {{labels.message}}</span>

            </div>
                         
        </div>
    </div>
</div>


      </div>
        <div class="modal-footer">
          <button class="btn btn-primary" style="padding-left: 2rem; padding-right: 2rem;" onclick="printTimesheet()"> {{labels.download}} </button>
            </div>
          </div>
        </div>
      </div>
  


<script type="text/javascript">

//  Initialization




// var HOST="http://localhost:8888";
var language = SYSTEM_LANGUAGE;

var staffId = 0;

$("#export_btn").html(lang('timesheet_export_csv'));

var eventItem ={
  id : '',
  title: '',
  color:  '',
  start:'',
  duration:'',
  type:'',
  taskId:'',
  leaveType:'',
  clientId:'',
  client:'',
  date:'', // Variable for Start & Id
  isChargeable:"",
};
var eventColor={
  'task':'#0273b7',
  'leave':'#777',
  'addEvent':'rgba(0,0,0,0)',
};

var eventsInitial={
      iconType: 'ion-plus-circled',
      color  : eventColor.addEvent,
      start  : '23:59:50',
      end:'23:59:59',
      type:'addEvent',
};


var durationSum = new Vue({
  el:'#durationSum',
  data:{
    days:[
    {duration:0},
    {duration:0},
    {duration:0},
    {duration:0},
    {duration:0},
    {duration:0},
    {duration:0},
    ],
    labels:{

      'hour':lang('timesheet_hour'),
    }
  }

});

var staffList = new Vue({
   el: '#timesheetStafflist',

   data: {
       labels:{
           "staffList": lang('list_staff'),
           "staffName": lang('taskMag_staffName'),
          


       },
       staff:{
        "staffId": 0,
        "alias": '',
        "name": ''
       }
   },

   methods:{

     showStaffList: function(){
       MD_staffModal.show(this);
     },
     onStaffSelected: function(staff){
       this.staff = staff;
       this.staff.name = this.staff.staffNo+' '+this.staff.alias;
       staffId  = this.staff.staffId;
       getTimesheet();
     }
   }
});


var timeSheetPrinter = new Vue({
  el:'#timeSheetPrinter',
  data:{
    title:'',
    startMonth:'',
    endMonth:'',
    range:'',
    labels:{
      'title':lang('timesheet_export_csv'),
      'timeRange':lang('timesheet_range'),
      'timeRangeIndicator':lang('timesheet_range_indicator'),
      'download':lang('timesheet_download'),
      'message':lang('timesheet_print_message'),
    },

  },

});



var eventPanel = new Vue({
  el:'#eventPanel',
  data:{
    currentEvent :'',
    eventType:'task',
    taskItem:'',
    taskList:[],
    leaveType:'',
    leaveList:[],
    clientList:[],
    clientId:'',
    duration:'',
    date:'',
    id:'',
    modalType:'',
    isChargeable:"Y",
    client:'',
    labels:{
          "clientName":lang('taskMag_clientName'),
          "clientList": lang('list_client'),
           // here
           'title': lang('timesheet_event_panel'),
           'eventType': lang('timesheet_event_type'),
           'task':lang('timesheet_task_label'),
           'leave':lang('timesheet_leave_label'),
           'taskPlaceholder':lang('timesheet_task_placeholder'),
           'leavePlaceholder':lang('timesheet_leave_placeholder'),
           'timeUsingLabel':lang('timesheet_time_using_label'),
           'chargeableLabel':lang('timesheet_chargeable_label'),
           'create':lang('timesheet_create'),
           'delete':lang('timesheet_delete'),
           'edit':lang('timesheet_edit'),
      },
  },
computed:{

},

methods:{
showClientList: function(){
          MD_clientModal.show(this);
      },
        
onClientSelected: function(client){
          this.client = client;
          this.client.name = this.client.clientCode+' '+this.client.enClientName;
      },


 defineEventId: function(date){
    var events =  eventsOfDate(date);
    var numOfEvents = events.length;
    console.log( events );
    return date+"_"+numOfEvents;
  },

   taskName: function(taskItem){
    var name;
    $.each(eventPanel.taskList, function(k,v){
      if(v.taskId == taskItem){
      name = v.taskName;
      }
    });
    return name;
  },

  leaveName: function(leaveType){
   var name;
   $.each(eventPanel.leaveList, function(k,v){
      if(v.code == leaveType){
       name = v.name; 
      // console.log(v.name);
      }
    });
    return name;
  },

  companyName: function(clientId){
    var name;
   $.each(eventPanel.clientList, function(k,v) {
      if(v.clientId == clientId){
          name =v.enClientName; 
      }
    });
    return name;
  },

    title: function(client, taskItem, leaveType){ // ( clientId, taskItem, leaveType )
    if (eventPanel.eventType == 'task'){
        var name = "";
        if(client.enClientName){
          name = client.enClientName;
        }else{
          name = client;
        }
    return name+"\n"+eventPanel.taskName(taskItem)+"\n"+eventPanel.duration+" hrs";
    }else if(eventPanel.eventType == "leave"){
return eventPanel.leaveName(leaveType)+"\n\n"+eventPanel.duration+" hrs";
    }
  },

    color: function(eventType){
    return eventColor[eventType];
  },


},
 
});

$(function() {
        getOptions();

    $('input[name="timesheetRange"]').daterangepicker({
        locale:{
          format: 'MM/YYYY',
        },
    });
        $('#calendar').fullCalendar({
                 header:{
                   left:   'prev today',
                   center: 'title',
                  // right:  'month, basicWeek, basicDay, next'
                  right:  'next'
                },
                  //  Calendar Basic 
                  defaultView:'basicWeek',
                  firstDay : 1 ,
                  displayEventTime: false,
                  eventOrder:'title',
                  businessHours: {
                    dow: [ 1, 2, 3, 4,5 ], 
                  },
                  contentHeight: 720,
                  eventOrder:"id",
                  // Event
                 events:eventsInitial,

              // Calender Methods,
              eventClick: function(event,element) {
                var date =  moment(event.start).format('YYYY-MM-DD');
                    // console.log(event.type);
                switch (event.type){
                  case 'task':
                  case 'leave':
                  // console.log(event);
                  loadCurrentEvent(event);
                  eventPanel.currentEvent = event;
                  eventPanel.modalType = "Edit";
                  break;
                  case 'addEvent':
                  eventPanel.date = date;
                  eventPanel.modalType = "Add"; 
                  // console.log(eventPanel.isChargeable);
                  break; 
                }
                  $('#eventPanel').modal('show');
                // updateDurationSummary(date);

              },

              eventRender: function(event, eventElement) {
                if (event.iconType) {
                  eventElement.find('div.fc-content').prepend("<i class='"+ event.iconType+"' style='color:#20c997; font-size:4rem;'></i>");
                }
              },

              });

               // Week Range Change Listener
              $('.fc-today-button').click(function(){
                changeWeek('today');
              });
              $('.fc-prev-button').click(function(){
                changeWeek('prev');
              });
              $('.fc-next-button').click(function(){
                changeWeek('next');
              });

              // Get TimeSheet
              getTimesheet();
  
});


//  ======================== functions ==================================
function getOptions(){
  $.ajax({
        url: HOST+'/accountant/timesheet/getOptions',
        type: 'GET',
        dataType: 'json',
      })
      .done(function(data) {
          // console.log(data);
          if(data.status==200){
            eventPanel.taskList = data.data.taskList.data;
            eventPanel.leaveList = data.data.leaveOptions.data;
            eventPanel.clientList = data.data.clientList.data;

          }else{
          console.log("Get Option Lists error");
          }
      })
      .fail(function() {
        console.log("Get Option Lists Failed");
      })
    }


function getTimesheet(){
    // console.log(staffId);
    var current = getVisibleStartEnd();
    var startDate = current[0];
    var endDate = current[6];
     $.ajax({
        url: HOST+'/accountant/timesheet/getTimesheet?lang='+language+'&startDate='+startDate+'&endDate='+endDate+'&staffId='+staffId,
        type: 'GET',
        dataType: 'json',
      })
      .done(function(data) {
          // console.log(data.data);
          if(data.status==200){
              var events =[];
              events.push( eventsInitial );
             $.each(data.data,function(k,v){
                var event = getEventObj(v);
                events.push(event);
             });
      
               $("#calendar").fullCalendar('removeEvents'); 
               $("#calendar").fullCalendar('addEventSource', events); 
             // Update  All Duration
                updateDurationSummary();
          }else{
          console.log("GET timeshee error");
          }
      })
      .fail(function() {
        console.log("Get Timesheet Failed");
      })
}

function getEventObj(v){
  var eventObj={};
     eventObj.duration = v.hours;
    if((v.taskId !='0')&&(v.taskId)){
       eventObj.type = 'task';
       eventObj.taskId = v.taskId;
       eventObj.clientId = v.clientId;
       eventObj.client = v['enClientName'];
       eventObj.leaveType = '';
       eventObj.title = v['enClientName']+"\n"+v.taskName+'\n'+eventObj.duration+" "+lang('timesheet_hour');        
    }else{
       eventObj.type = 'leave';
       eventObj.leaveType = v.leaveTypeCode;
       eventObj.taskId = '';
       eventObj.clientId = '';
       eventObj.client = '';
       eventObj.title = v.leaveType+"\n\n"+eventObj.duration+" "+lang('timesheet_hour');
    }
    eventObj.date  = v.date;
    eventObj.start = v.date;
    if(v.color){
      eventObj.color = '#'+v.color;
    }else{
      eventObj.color = eventColor['leave'];
    }
    
    eventObj.id = v.timesheetId;
    eventObj.isChargeable = v.isChargeable;
    return eventObj;
}



function updateTimesheet(action,eventId){
    var current = getVisibleStartEnd();
    var startDate = current[0];
    var endDate = current[6];
    $.ajax({
        url: HOST+'/accountant/timesheet/getTimesheet?lang='+language+'&startDate='+startDate+'&endDate='+endDate+'&staffId='+staffId,
        type: 'GET',
        dataType: 'json',
      })
      .done(function(data) {
          // console.log(data.data);
          if(data.status==200){
              $.each(data.data, function (k,v){
                if (v.timesheetId == eventId){
               var eventObj = getEventObj(v);
               // console.log(eventObj);
            switch (action){
              case "Add":
                $('#calendar').fullCalendar( 'renderEvent', eventObj ,'stick' );
              break;
              case "Edit":
              $("#calendar").fullCalendar('removeEvents', eventId); 
              $('#calendar').fullCalendar( 'renderEvent', eventObj, 'stick' );
              break;
              case "Remove":
              // $("#calendar").fullCalendar('removeEvents', eventId); 
              break;
                  }
            updateDurationSummary(eventObj.date);
                }
              });
          }else{
          console.log("Update timeshee error");
          }
      })
      .fail(function() {
        console.log("Update Timesheet Failed");
      })


}

function addEvent(){
  eventItem.type = eventPanel.eventType;

  if(eventPanel.eventType == 'leave'){
  eventItem.taskId = "";
  eventItem.leaveType = eventPanel.leaveType;
  eventItem.clientId = "";
  eventItem.isChargeable = "N";
  }else {
  eventItem.taskId = eventPanel.taskItem;
  eventItem.leaveType = "";
  eventItem.clientId = eventPanel.client.clientId;
  eventItem.client = eventPanel.client;
  if(eventPanel.isChargeable){
  eventItem.isChargeable = 'Y';
  }else{
    eventItem.isChargeable = 'N';
  }
  
  }
  eventItem.date = eventPanel.date;
  eventItem.start = eventPanel.date;
  // eventItem.color = eventPanel.color(eventItem.type);
  // eventItem.title = eventPanel.title(eventItem.client, eventItem.taskId, eventItem.leaveType);
  eventItem.duration = eventPanel.duration;


   $.ajax({
        url: HOST+'/accountant/timesheet/createTask',
        type: 'POST',
        data:{
          staffId:staffId,
          leaveType:eventItem.leaveType,
          taskId:eventItem.taskId,
          clientId:eventItem.clientId,
          hours: eventItem.duration,
          date:eventItem.date,
          isChargeable:eventItem.isChargeable,
        },
        dataType: 'json',
      })
      .done(function(data) {
          // console.log(data);
          if(data.status==200){
            eventItem.id = data.data;
            updateTimesheet('Add',eventItem.id);
          }else{
          alert(data.message);
          }
      })
      .fail(function() {
        alert(data.message);
      })
}

function editEvent(event){
  // Edit Event:
  event.type = eventPanel.eventType;

  if(event.type == 'leave'){
      event.taskId = "";
      event.leaveType = eventPanel.leaveType;
      event.clientId = "";
      event.client = "";
      event.isChargeable ="N";
  }else {
      event.taskId = eventPanel.taskItem;
      event.leaveType = "";
      event.clientId = eventPanel.client.clientId;
      event.client = eventPanel.client;
  if(eventPanel.isChargeable){
      event.isChargeable = 'Y';
  }else{
      event.isChargeable = 'N';
  }
  }

  event.start = eventPanel.date;
  event.date = eventPanel.date;
  event.id = eventPanel.id;
  event.duration = eventPanel.duration;
  // event.color = eventPanel.color(event.type);
  // event.title = eventPanel.title(event.client, event.taskId, event.leaveType);
  // console.log(event);

   $.ajax({
        url: HOST+'/accountant/timesheet/editTask',
        type: 'POST',
        data:{
          staffId:staffId,
          leaveType:event.leaveType,
          taskId:event.taskId,
          clientId:event.clientId,
          hours: event.duration,
          isChargeable:event.isChargeable,
          timesheetId:event.id,
        },
        dataType: 'json',
      })
      .done(function(data) {
          // console.log(data);
          if(data.status==200){ 
             updateTimesheet('Edit',event.id);
          }else{
          alert(data.message);
          }
      })
      .fail(function() {
        alert(data.message);
      })
}


function removeEvent(event){
  $.ajax({
    url: HOST+'/accountant/timesheet/deleteTask',
    type: 'POST',
    dataType: 'json',
    data: {timesheetId: event.id},
  })
  .done(function(data) {
    if(data.status==200){
      // console.log("Delete Event success");
      var date = event.date;
     $('#calendar').fullCalendar( 'removeEvents', event.id );
     // updateTimesheet('Remove',event.id);
     updateDurationSummary(date);
}else{
  alert(data.message);
}
  })
  .fail(function() {
  alert(data.message);
  })
}

function loadCurrentEvent(event){
  eventPanel.eventType = event.type;
  eventPanel.date = event.date;
  eventPanel.duration =event.duration;
  eventPanel.taskItem =  event.taskId;
  eventPanel.leaveType = event.leaveType;
  eventPanel.clientId = event.clientId;
  eventPanel.client = event.client;
  eventPanel.id = event.id;

  if(event.isChargeable =='Y'){
    eventPanel.isChargeable = true ;
  }else{
eventPanel.isChargeable = false ;
  }
}


function timesheetModal(type){

  // timeSheetPrinter.title="Export as "+ type.toUpperCase();

  $('#timeSheetPrinter').modal('show');

}

function printTimesheet(current){ // YYYY-MM Format

  if(current){
    var currentMonth = current.split('-');
    var year = currentMonth[0];
    var month = currentMonth[1];

   var url = HOST+'/accountant/timesheet/printTimesheet?year='+year+'&from='+month+'&to='+month+'&staffId='+staffId;
    var win = window.open(url, '_blank');
    if (win) {
        //Browser has allowed it to be opened
        win.focus();
    } else {
        //Browser has blocked it
        console.log(lang('timesheet_download_fail'));
    }
    // window.open(url,'_blank');
  $('#timeSheetPrinter').modal('hide');
  Notify.notify('success', lang('notify_success'), lang('notify_successMessage'));
  }else{

  timeSheetPrinter.range =  $('#timesheetRange').val().split('-');

  var startMonth = moment(timeSheetPrinter.range[0], "MM/YYYY");
  var endMonth =  moment(timeSheetPrinter.range[1], "MM/YYYY");
  // var monthValues = [];
  var i =1;
while (endMonth > startMonth || startMonth.format('M') === endMonth.format('M')) {
  // monthValues.push(startMonth.format('YYYY-M'));

setTimeout(function(currentMonth) { //YYYY-MM format
    printTimesheet( currentMonth );     
    // console.log('print'+currentMonth);
}(startMonth.format('YYYY-MM')), i*500);
    startMonth.add(1,'month');
    i++;
    }  
  }
}








function changeWeek(type){
  getTimesheet();
  // updateDurationSummary();
}


function updateDurationSummary(date){
  var current = getVisibleStartEnd();
  // console.log('current: '+ current );
  var duration;
  if(date){
    $.each(current, function(index,value){
      if (date == value ){
       duration = getDurations(value);
       durationSum.days[index].duration = duration;
     }
   });
  }else{
    $.each(current, function(index,value){
       duration = getDurations(value);
       durationSum.days[index].duration = duration;
    });
  }
}


function getDurations(date){
 var resources = eventsOfDate(date);
 var sum = 0;
 // console.log(resources);
 $.each(resources, function(k,v){
   if(v.type=="task"){
   sum += parseFloat(v.duration);
  }
 });
 return sum;
}

function eventsOfDate(date){
  var events = $('#calendar').fullCalendar( 'clientEvents');
  var eventsOfDate = [];

 $.each(events, function(k,v){
   if(v.date == date){
      eventsOfDate.push(v);
  }
 });
 return eventsOfDate;
}


function getDate(start){
  return moment(start, "YYYY-MM-DD");
}

function getVisibleStartEnd(){

  var start  = $('#calendar').fullCalendar('getView').start.format();
  // console.log('start: '+ $('#calendar').fullCalendar('getView').start );
  var startDate = moment(start, "YYYY-MM-DD");
  var weekdays = [];
  var temp_date = "";
  var ondDay = moment.duration(1,'day');
  for (var i = 0 ; i <= 6; i++) {
    temp_date =  moment(  moment(startDate).valueOf() + i*ondDay  ).format("YYYY-MM-DD");
    weekdays.push(temp_date);
  }
  return weekdays; 
}
//  Test Code
</script>
