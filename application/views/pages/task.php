<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header" id="searchBar">     
        <div class="pull-left" v-if="ROLE == 'A' || ROLE == 'M'">
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createModal">
            <i class="fa fa-plus"></i>
          </button>
          <button type="button" class="btn btn-danger" onclick="tableData.deleteTasks()">
            <i class="fa fa-trash"></i>
          </button>
        </div>    
        <div class="col-xs-10" >
          <div class="col-xs-5 form-group">
            <div class="input-group">
              <input type="text" class="form-control" v-model="search" v-on:keyup.enter="searchUser">
              <span class="input-group-btn">
                <button type="button" class="btn btn-info" v-on:click="searchUser">{{searchBtn}}</button>
              </span>
            </div>
          </div>
          <div class="col-xs-5 form-group" v-if="ROLE == 'A' || ROLE == 'M'">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="" id="s_staffName">
              <span class="input-group-btn">
                <button type="button" class="btn btn-warning" v-on:click="showStaffList()">{{labels.staffList}}</button>
              </span>
            </div>
            <typeahead v-model="staff" target="#s_staffName" async-src="/accountant/search/staff/" async-key="data" item-key="name" :loaded="refresh()" debounce="200"/>             
          </div>
        </div>
      </div>          

      <div class="box-body">
        <table id="table" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th v-if="ROLE == 'A' || ROLE == 'M'"><input type="checkbox" :checked="false" v-model="selectAll"></th>
              <th>{{labels.taskMagId}}</th>
              <th width="10%">{{labels.taskName}}</th>
              <!-- <th>{{labels.timeRange}}</th> -->
              <th width="20%">{{labels.clientName}}</th>
              <th>{{labels.value}}</th>
              <th>{{labels.dueDate}}</th>
              <th>{{labels.taskAttachments}}</th>
              <th>{{labels.staffName}}</th>
              <th>{{labels.manHours}}</th>
              <th>{{labels.status}}</th>
              <th v-if="ROLE == 'A' || ROLE == 'M'">{{labels.actions}}</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="item in items">
              <td v-if="ROLE == 'A' || ROLE == 'M'">
                <input type="checkbox" v-model="selected" :value="item.taskMagId">
              </td>
              <td>{{item.taskMagId}}</td>
              <td>{{item.taskName}}</td>
              <!-- <td>{{item.startDate}} - {{item.endDate}} </td> -->
              <td>{{item.clientName}}</td>
              <td>{{item.value}}</td>
              <td>{{item.dueDate}}</td>
              <td>
                <button type="button" class="btn btn-success btn-sm" v-on:click="viewAttachmentModal.viewAttachment(item.taskMagId, item)">
                  {{labels.viewAttachment}}
                </button>   
              </td>
              <td>{{item.staffName}}</td>
              <th>{{item.manHours}}</th>
              <td>
                <div class="btn-group">
                  <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span v-bind:class="['badge', item.statusColor]">{{item.status}}<span class="caret" v-if="ROLE == 'A' || ROLE == 'M' || (ROLE == 'S' && item.statusId != 4)"></span></span></a> 
                  <ul class="dropdown-menu">
                    <li v-for="status in statuses">
                    <a v-if="ROLE == 'A' || ROLE == 'M'" type="button" :class="['btn','btn-flat','btn-block', status.color]" v-on:click="setStatus(status.statusId, item.taskMagId)">{{status.statusName}}</a>
                    <a v-else-if="status.statusId < 4" type="button" :class="['btn','btn-flat','btn-block', status.color]" v-on:click="setStatus(status.statusId, item.taskMagId)">{{status.statusName}}</a>

                    </li>
                  </ul>
                </div>  
                <div><small>{{labels.lastUpdate}}:{{item.userName}}</small></div>
                <div><small>{{item.lastUpdate}}</small></div>
              </td>
              <td v-if="ROLE == 'A' || ROLE == 'M'">
                <div class="">
                  <!-- <a class="btn-xs" v-on:click="viewItem(item.taskMagId)"><i class="fa fa-eye"></i></a> -->
                  <a class="btn-xs" v-on:click="editForm.getTaskDetail(item.taskMagId)"><i class="fa fa-edit"></i></a>
                  <a class="btn-xs" v-on:click="deleteItem(item.taskMagId)"><i class="fa fa-trash"></i></a>
                </div>
              </td>
            </tr>
            
          </tbody>

        </table>
      </div>
      <div class="box-footer clearfix" id="tableFooter">
        <div class="col-xs-3">
          <span>{{paginationPrefix}}<b>{{total}}</b>{{paginationSuffix}}</span>
        </div>
        <div class="col-xs-9">
          <pagination v-model="currentPage" :total-page="totalPage" size="sm" align="right" boundary-links style="margin-top:-20px; margin-bottom:-20px;"/>
        </div> 
      </div>
    </div>
    <!-- /.box -->
  </div>
</div>
<!-- /.row -->

<div class="modal fade" id="createModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <!-- form start -->                
        <div class="box-body">     
          <form role="form" id="createForm" autocomplete="off">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#c_tab_1" data-toggle="tab" aria-expanded="true" id="c_n_tab_1">{{labels.new}}</a></li>
                <li class="pull-right"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></li>
              </ul>
              <div class="tab-content" id="c_tab_content">
                <!-- /.tab-pane -->                      
                <div class="tab-pane active" id="c_tab_1">
                  <div class="row">          
                    <div class="col-xs-10 form-group">
                      <label>{{labels.taskName}}</label>   
                      <div class="input-group">
                        <input type="text" class="form-control" placeholder="" id="c_taskName">
                        <span class="input-group-btn">
                          <button type="button" class="btn btn-warning" v-on:click="showTaskList()">{{labels.taskList}}</button>
                        </span>
                      </div>
                      <typeahead v-model="task" target="#c_taskName" async-src="/accountant/search/task/" async-key="data" item-key="name"/>   
                    </div> 
                  </div> 
                  <div class="row">          
                    <div class="col-xs-10 form-group">
                      <label>{{labels.clientName}}</label>  
                      <div class="input-group">
                        <input type="text" class="form-control" placeholder="" id="c_clientName">
                        <span class="input-group-btn">
                          <button type="button" class="btn btn-warning" v-on:click="showClientList()">{{labels.clientList}}</button>
                        </span>
                      </div>
                      <typeahead v-model="client" target="#c_clientName" async-src="/accountant/search/client/" async-key="data" item-key="name"/>   
                    </div> 
                  </div> 
                  <div class="row">          
                    <div class="col-xs-10 form-group">
                      <label>{{labels.staffName}}</label>  
                      <div class="input-group">
                        <input type="text" class="form-control" placeholder="" id="c_staffName">
                        <span class="input-group-btn">
                          <button type="button" class="btn btn-warning" v-on:click="showStaffList()">{{labels.staffList}}</button>
                        </span>
                      </div>
                      <typeahead v-model="staff" target="#c_staffName" async-src="/accountant/search/staff/" async-key="data" item-key="name"/>   
                    </div> 
                  </div>  
                  <div class="row">          
                    <div class="col-xs-4 form-group">
                      <label>{{labels.dueDate}}</label>  
                      <dropdown class="form-group">
                        <div class="input-group">
                          <input class="form-control" type="text" v-model="inputs.dueDate">
                          <div class="input-group-btn">
                            <btn class="dropdown-toggle"><i class="glyphicon glyphicon-calendar"></i></btn>
                          </div>
                        </div>
                        <template slot="dropdown">
                          <li>
                            <date-picker v-model="inputs.dueDate" :today-btn="false" :clear-btn="false"/>
                          </li>
                        </template>
                      </dropdown>
                    </div>       
                    <div class="col-xs-4 form-group">
                      <label>{{labels.startDate}}</label>  
                      <dropdown class="form-group">
                        <div class="input-group">
                          <input class="form-control" type="text" v-model="inputs.startDate">
                          <div class="input-group-btn">
                            <btn class="dropdown-toggle"><i class="glyphicon glyphicon-calendar"></i></btn>
                          </div>
                        </div>
                        <template slot="dropdown">
                          <li>
                            <date-picker v-model="inputs.startDate" :today-btn="false" :clear-btn="false"/>
                          </li>
                        </template>
                      </dropdown>
                    </div> 
                    <div class="col-xs-4 form-group">
                      <label>{{labels.endDate}}</label>  
                      <dropdown class="form-group">
                        <div class="input-group">
                          <input class="form-control" type="text" v-model="inputs.endDate">
                          <div class="input-group-btn">
                            <btn class="dropdown-toggle"><i class="glyphicon glyphicon-calendar"></i></btn>
                          </div>
                        </div>
                        <template slot="dropdown">
                          <li>
                            <date-picker v-model="inputs.endDate" :today-btn="false" :clear-btn="false"/>
                          </li>
                        </template>
                      </dropdown>
                    </div> 
                  </div>  
                  <div class="row">          
                    <div class="col-xs-4 form-group">
                      <label>{{labels.value}}</label>  
                      <input type="text" class="form-control" v-model="inputs.value" placeholder="">
                    </div> 
                    <div class="col-xs-4 form-group">
                      <label>{{labels.manHours}}</label>  
                      <input type="text" class="form-control" v-model="inputs.manHours" placeholder="">
                    </div> 
                  </div>  

                </div>
                <!-- /.tab-pane -->  

              </div>                    
              <!-- /.box-body -->
            </div>
            <div class="box-footer">
              <button class="btn btn-primary pull-right" v-on:click.prevent="createTask" :disabled="uploading">{{labels.new}}</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>


<div class="modal fade" id="editModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <!-- form start -->                
        <div class="box-body">     
          <form role="form" id="editForm" autocomplete="off">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" aria-expanded="true" id="c_n_tab_1">{{labels.edit}}</a></li>
                <li class="pull-right"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></li>
              </ul>
              <div class="tab-content">
                <!-- /.tab-pane -->                      
                <div class="tab-pane active">
                  <div class="row">          
                    <div class="col-xs-10 form-group">
                      <label>{{labels.taskName}}</label>   
                      <div class="input-group">
                        <input type="text" class="form-control" placeholder="" id="e_taskName"  v-model="task.name">
                        <span class="input-group-btn">
                          <button type="button" class="btn btn-warning" v-on:click="showTaskList()">{{labels.taskList}}</button>
                        </span>
                      </div>
                      <typeahead v-model="task" target="#e_taskName" async-src="/accountant/search/task/" async-key="data" item-key="name"/>   
                    </div> 
                  </div> 
                  <div class="row">          
                    <div class="col-xs-10 form-group">
                      <label>{{labels.clientName}}</label>  
                      <div class="input-group">
                        <input type="text" class="form-control" placeholder="" id="e_clientName" v-model="client.name">
                        <span class="input-group-btn">
                          <button type="button" class="btn btn-warning" v-on:click="showClientList()">{{labels.clientList}}</button>
                        </span>
                      </div>
                      <typeahead v-model="client" target="#e_clientName" async-src="/accountant/search/client/" async-key="data" item-key="name"/>   
                    </div> 
                  </div> 
                  <div class="row">          
                    <div class="col-xs-10 form-group">
                      <label>{{labels.staffName}}</label>  
                      <div class="input-group">
                        <input type="text" class="form-control" placeholder="" id="e_staffName" v-model="staff.name">
                        <span class="input-group-btn">
                          <button type="button" class="btn btn-warning" v-on:click="showStaffList()">{{labels.staffList}}</button>
                        </span>
                      </div>
                      <typeahead v-model="staff" target="#e_staffName" async-src="/accountant/search/staff/" async-key="data" item-key="name"/>   
                    </div> 
                  </div>  
                  <div class="row">          
                    <div class="col-xs-4 form-group">
                      <label>{{labels.dueDate}}</label>  
                      <dropdown class="form-group">
                        <div class="input-group">
                          <input class="form-control" type="text" v-model="inputs.dueDate">
                          <div class="input-group-btn">
                            <btn class="dropdown-toggle"><i class="glyphicon glyphicon-calendar"></i></btn>
                          </div>
                        </div>
                        <template slot="dropdown">
                          <li>
                            <date-picker v-model="inputs.dueDate" :today-btn="false" :clear-btn="false"/>
                          </li>
                        </template>
                      </dropdown>
                    </div>       
                    <div class="col-xs-4 form-group">
                      <label>{{labels.startDate}}</label>  
                      <dropdown class="form-group">
                        <div class="input-group">
                          <input class="form-control" type="text" v-model="inputs.startDate">
                          <div class="input-group-btn">
                            <btn class="dropdown-toggle"><i class="glyphicon glyphicon-calendar"></i></btn>
                          </div>
                        </div>
                        <template slot="dropdown">
                          <li>
                            <date-picker v-model="inputs.startDate" :today-btn="false" :clear-btn="false"/>
                          </li>
                        </template>
                      </dropdown>
                    </div> 
                    <div class="col-xs-4 form-group">
                      <label>{{labels.endDate}}</label>  
                      <dropdown class="form-group">
                        <div class="input-group">
                          <input class="form-control" type="text" v-model="inputs.endDate">
                          <div class="input-group-btn">
                            <btn class="dropdown-toggle"><i class="glyphicon glyphicon-calendar"></i></btn>
                          </div>
                        </div>
                        <template slot="dropdown">
                          <li>
                            <date-picker v-model="inputs.endDate" :today-btn="false" :clear-btn="false"/>
                          </li>
                        </template>
                      </dropdown>
                    </div> 
                  </div>  
                  <div class="row">          
                    <div class="col-xs-4 form-group">
                      <label>{{labels.value}}</label>  
                      <input type="text" class="form-control" v-model="inputs.value" placeholder="">
                    </div> 
                    <div class="col-xs-4 form-group">
                      <label>{{labels.manHours}}</label>  
                      <input type="text" class="form-control" v-model="inputs.manHours" placeholder="">
                    </div> 
                  </div>  

                </div>
                <!-- /.tab-pane -->  

              </div>                    
              <!-- /.box-body -->
            </div>
            <div class="box-footer">
              <button class="btn btn-primary pull-right" v-on:click.prevent="editTask" :disabled="uploading">{{labels.edit}}</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="viewAttachmentModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <!-- form start -->                
        <div class="box-body">     
          <form role="form">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#c_tab_1" data-toggle="tab" aria-expanded="true">{{labels.attachment}}</a></li>
                <li class="pull-right"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></li>
              </ul>
              <div class="tab-content" id="c_tab_content">
                <!-- /.tab-pane -->                      
                <div class="tab-pane active" id="c_tab_1">
                  <div class="row">
                    <div class="col-xs-12 form-group">
                      <table id="table" class="table table-bordered table-hover" v-if="task.taskAttachments">
                        <thead>
                          <tr>
                            <th>{{labels.attachment}}</th>
                            <th>{{labels.uploadTime}}</th>
                            <th>{{labels.actions}}</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr v-for="attachment in task.taskAttachments">
                            <td><a :href="HOST+'/accountant/'+attachment.formUrl" :download="attachment.formName">{{attachment.formName}}</a></td>
                            
                            <td>
                              {{attachment.time}}
                            </td>
                            <td>
                              <div class="">
                                <a class="btn-xs" :href="HOST+'/accountant/'+attachment.formUrl" target= "_blank"><i class="fa fa-eye"></i></a>
                                <a class="btn-xs" v-on:click="deleteAttachment(task.taskMagId, attachment)"><i class="fa fa-trash"></i></a>
                              </div>
                            </td>
                          </tr>
                          
                        </tbody>
                      </table>
                      <p v-else>{{labels.noAttachment}}</p>
                    </div>
                    <div class="col-xs-12 form-group">
                      <div class="fileUpload btn btn-warning">
                        <span>{{labels.new}}</span>
                        <i class="fa fa-upload"></i>
                        <input type="file" class="upload" multiple v-on:change="onChanged"/>
                      </div>                          
                    </div>  
                    <div class="col-xs-12">
                      <div v-for="file in filesToUpload">
                        <p>{{file}}</p>
                      </div>
                    </div>

                    <div class="col-xs-12"><label class="text-info" v-if="uploading">{{labels.uploading}}</label></div>                            
                  </div>
                </div>
              </div>                    
            </div>
            <div class="box-footer">
              <button class="btn btn-primary pull-right" v-on:click.prevent="addAttachment" :disabled="uploading">{{labels.new}}</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<script>

  function initOptions()
  {
    // $.ajax({
    //   url: HOST + '/accountant/code/getTaskOptions/'+SYSTEM_LANGUAGE,
    //   type: 'GET',
    //   dataType: 'json',
    //   success: function (data) {
    //     if(data.status == 200){
    //       for(var i = 0; i < data.data.countries.length; i++){
    //           createForm.countries.push(data.data.countries[i]);
    //           editForm.countries.push(data.data.countries[i]);
    //       }
    //       for(var i = 0; i < data.data.departments.length; i++){
    //           createForm.departments.push(data.data.departments[i]);
    //           editForm.departments.push(data.data.departments[i]);
    //       }
    //       for(var i = 0; i < data.data.positions.length; i++){
    //           createForm.positions.push(data.data.positions[i]);
    //           editForm.positions.push(data.data.positions[i]);
    //       }
    //       for(var i = 0; i < data.data.banks.length; i++){
    //           createForm.banks.push(data.data.banks[i]);
    //           editForm.banks.push(data.data.banks[i]);
    //       }
    //     }
    //     else{
    //       alert(data.message);    
    //     }
    //   }
    // });       
  }

  var searchBar = new Vue({
    el:'#searchBar',
    data: {
      labels:{
        "staffName": lang('taskMag_staffName'),
        "staffList": lang('list_staff')
      },
      searchBtn: lang('searchBtn'),    
      search:'',
      staff:''
    },
    methods:{
      searchUser: function(){
        tableData.param.search = this.search;
      },
      showStaffList: function(){
        MD_staffModal.show(this); 
      },
      onStaffSelected: function(staff){
        this.staff = staff;
        this.staff.name = this.staff.staffNo+' '+this.staff.alias;
        tableData.param.staffId = this.staff.staffId;
      },
      refresh: function(){

        if(tableData)
          tableData.param.staffId = this.staff.staffId;
      }
    }
  });

  var tableData = new Vue({
    el: '#table',
    data: {     
      labels:{
        "addAttachment": lang('taskMag_addAttachment'),
        "viewAttachment": lang('taskMag_viewAttachment'),
        "taskMagId": lang('taskMag_taskMagId'),
        "staffName": lang('taskMag_staffName'),
        "clientName":lang('taskMag_clientName'),
        "taskName":lang('taskMag_taskName'),
        "company":lang('taskMag_company'),
        "value":lang('taskMag_value'),
        "timeRange":lang('taskMag_timeRange'),
        "dueDate":lang('taskMag_dueDate'),
        "status":lang('taskMag_status'),
        "taskAttachments":lang('taskMag_taskAttachments'),
        "lastUpdate":lang('taskMag_lastUpdate'),
        "manHours":lang('taskMag_manHours'),
        "actions":lang('actions')
      },
      taskAttachments:[],
      items: [],
      selected:[],
      colors:COLORS,
      statuses:[],
      param: {
        "pagination":1,
        "search":searchBar.search,
        "lang":SYSTEM_LANGUAGE,
        "clientId":0,
        "taskId":0,
        "staffId":0
      }
    },
    methods:{
      refresh: function(){
        getTableData(this,pagination,URLS['GET_ASSIGNED_TASKS']);
      },
      checkAll: function(){
      },
      deleteTasks: function(){
        
        var result;  
        result = confirm(lang('q_confirmDelete'));  

        if(result == 0)
          return;

        $.ajax({
          url: HOST + '/accountant/task/deleteAssignedTask/',
          type: 'POST',
          dataType: 'json',
          data: {"tasks":this.selected},
          timeout:AJAX_TIMEOUT,
          success:function(data)
          {
            if(checkStatus(data) == false)
              return;  
            tableData.selected = [];
            tableData.refresh(); 
          }
        });         
      },
      deleteItem: function(id){
        
        var result;  
        result = confirm(lang('q_confirmDelete'));  

        if(result == 0)
          return;

        $.ajax({
          url: HOST + '/accountant/task/deleteAssignedTask/',
          type: 'POST',
          dataType: 'json',
          data: {"tasks":[id]},
          timeout:AJAX_TIMEOUT,
          success:function(data)
          {
            if(checkStatus(data) == false)
              return;  
            tableData.selected = [];
            tableData.refresh(); 
          }
        });         
      },
      editItem: function(itemId){
        $("#editModal").modal('show');
        editForm.inputs.clientId = itemId;
        console.log(itemId);

      },
      setStatus: function(statusId, taskMagId){

        $.ajax({
          url: HOST + '/accountant/task/setStatus/',
          type: 'POST',
          dataType: 'json',
          data: {"statusId":statusId,"taskMagId":taskMagId},
          timeout:AJAX_TIMEOUT,
          success:function(data)
          {
            if(checkStatus(data) == false)
              return;  
            tableData.selected = [];
            tableData.refresh(); 
          }
        }); 
      }
    },
    computed: {
      selectAll: {
        get: function () {
          return this.items ? this.selected.length == this.items.length : false;
        },
        set: function (value) {
          var selected = [];

          if (value) {
              this.items.forEach(function (item) {
                  selected.push(item.userId);
              });
          }
          this.selected = selected;
        }
      }
    },
    watch:{
      param: {
        handler(newValue, oldValue){
          getTableData(tableData,pagination,URLS['GET_ASSIGNED_TASKS']);
        },
        deep: true 
      }
    },
    created: function(){
      $.ajax({
        url: HOST + '/accountant/status/getStatuses/',
        type: 'GET',
        dataType: 'json',
        timeout:AJAX_TIMEOUT,
        success:function(data)
        {
          if(checkStatus(data) == false)
            return;  
          tableData.statuses = data.data;
        }
      }); 
          
          
    }
  })

  var pagination = new Vue({
    el: '#tableFooter',
    data: {
      paginationPrefix: lang('paginationPrefix'),
      paginationSuffix: lang('paginationSuffix'),
      total: 0,
      totalPage: 1,
      currentPage: 1
    },
    methods:{

    },
    created:function(){
      getTableData(tableData,this,URLS['GET_ASSIGNED_TASKS']);
    },
    watch:{
      currentPage: {
        handler(newValue, oldValue){
          tableData.param.pagination = newValue;
        },
      }
    }
  }) 

  var createForm = new Vue({
    el: '#createModal',
    data: {
      labels:{
        "new": lang('taskMag_new'),
        "staffName": lang('taskMag_staffName'),
        "clientName":lang('taskMag_clientName'),
        "taskName":lang('taskMag_taskName'),
        "company":lang('taskMag_company'),
        "value":lang('taskMag_value'),
        "startDate":lang('taskMag_startDate'),
        "endDate":lang('taskMag_endDate'),
        "dueDate":lang('taskMag_dueDate'),
        "taskAttachments":lang('taskMag_taskAttachments'),
        "manHours":lang('taskMag_manHours'),
        "clientList":lang('list_client'),
        "staffList":lang('list_staff'), 
        "taskList":lang('list_task')    
      },
      task:'',
      client:'',
      staff:'',
      inputs:{},
      countries:[],
      departments:[],
      banks:[],
      positions:[],
      uploading: false
    },
    methods:{
      createTask: function(){

        this.inputs.taskId = this.task.taskId;
        this.inputs.clientId = this.client.clientId;
        this.inputs.staffId = this.staff.staffId;

        this.uploading = true;
        $.ajax({
          url: HOST + '/accountant/task/assignTask/',
          type: 'POST',
          dataType: 'json',
          data: this.inputs,
          timeout:AJAX_TIMEOUT,
          success:function(data)
          {
            console.log('hello');
            createForm.uploading = false;
            if(checkStatus(data) == false)
              return;  
            else {
              $("#createModal").modal('hide');
              Notify.notify('success', lang('notify_success'), lang('notify_successMessage'));
            }
            tableData.refresh(); 
          }
        }); 
      },
      showClientList: function(){
        MD_clientModal.show(this); 
      },
      onClientSelected: function(client){
        this.client = client;
        this.client.name = this.client.clientCode+' '+this.client.enClientName;
      },
      showStaffList: function(){
        MD_staffModal.show(this); 
      },
      onStaffSelected: function(staff){
        this.staff = staff;
        this.staff.name = this.staff.staffNo+' '+this.staff.alias;
      },
      showTaskList: function(){
        MD_taskModal.show(this); 
      },
      onTaskSelected: function(task){
        this.task = task;
        this.task.name = this.task.taskCode+' '+this.task.alias;
      }
    },
    created: function () {

    }
  }) 

  var editForm = new Vue({
    el: '#editModal',
    data: {
      labels:{
        "edit": lang('taskMag_edit'),
        "staffName": lang('taskMag_staffName'),
        "clientName":lang('taskMag_clientName'),
        "taskName":lang('taskMag_taskName'),
        "company":lang('taskMag_company'),
        "value":lang('taskMag_value'),
        "startDate":lang('taskMag_startDate'),
        "endDate":lang('taskMag_endDate'),
        "dueDate":lang('taskMag_dueDate'),
        "taskAttachments":lang('taskMag_taskAttachments'),
        "manHours":lang('taskMag_manHours'),
        "clientList":lang('list_client'),
        "staffList":lang('list_staff'), 
        "taskList":lang('list_task')       
      },
      task:{},
      client:{},
      staff:{},
      inputs:{},
      uploading: false
    },
    methods:{
      editTask: function(){
        this.uploading = true;
        this.inputs.clientId = this.client.clientId;
        this.inputs.taskId = this.task.taskId;
        this.inputs.staffId = this.staff.staffId;

        $.ajax({
          url: HOST + '/accountant/task/editAssignedTask/',
          type: 'POST',
          dataType: 'json',
          data: this.inputs,
          timeout:AJAX_TIMEOUT,
          error:function(){
            editForm.uploading = false;
          },
          success:function(data)
          {
            console.log('hello');
            editForm.uploading = false;
            if(checkStatus(data) == false)
              return;  
            else {
              $("#editModal").modal('hide');
              Notify.notify('success', lang('notify_success'), lang('notify_successMessage'));
            }
            tableData.refresh(); 
          }
        }); 
      },
      getTaskDetail:function(id){
        this.inputs.taskMagId = id;

        getDetail(id, URLS['GET_ASSIGNED_TASK_DETAIL'],function(data){
          editForm.inputs = data.data[0];

          editForm.client.clientId = editForm.inputs.clientId;
          editForm.client.name = editForm.inputs.clientCode+' '+editForm.inputs.enClientName;

          editForm.staff.staffId = editForm.staff.staffId;
          editForm.staff.name = editForm.inputs.staffNo+' '+editForm.inputs.staffName;

          editForm.task.taskId = editForm.inputs.taskId;
          editForm.task.name = editForm.inputs.taskCode+' '+editForm.inputs.taskName;

          $("#editModal").modal('show');
        });    
      },
      showClientList: function(){
        MD_clientModal.show(this); 
      },
      onClientSelected: function(client){
        this.client = client;
        this.client.name = this.client.clientCode+' '+this.client.enClientName;
      },
      showStaffList: function(){
        MD_staffModal.show(this); 
      },
      onStaffSelected: function(staff){
        this.staff = staff;
        this.staff.name = this.staff.staffNo+' '+this.staff.alias;
      },
      showTaskList: function(){
        MD_taskModal.show(this); 
      },
      onTaskSelected: function(task){
        this.task = task;
        this.task.name = this.task.taskCode+' '+this.task.alias;
      }
    }
  }) 


  var viewAttachmentModal = new Vue({
    el:'#viewAttachmentModal',
    data: {
      labels:{
        "attachment":lang('taskMag_attachment'),
        "uploadTime":lang('taskMag_attachmentTime'),
        "actions":lang('actions'),
        "new":lang('taskMag_addAttachment'),
        "noAttachment":lang('taskMag_noAttachment'),
        "uploading":lang('uploading'),
        "selectFile":lang('selectFile'), 
        "upload":lang('uploadFile')    
      },
      inputs:{},
      taskMagId: 0,
      task:'',
      filesToUpload: [],
      uploading: false
    },
    methods:{
      addAttachment: function(){

        if(this.filesToUpload.length == 0){
          alert(lang("error_pleaseSelectFile"));
          return;
        }

        this.uploading = true;

        // console.log(this.inputs.get("files[]"));

        this.inputs.append('taskMagId', this.taskMagId);

        $.ajax({
          url: HOST + '/accountant/task/addAttachment/',
          type: 'POST',
          dataType: 'json',
          data: this.inputs,
          processData:false,
          contentType:false,
          cache:false,
          timeout:AJAX_TIMEOUT,
          error:function()
          {
            alert(lang("error_uploadFailed"));
            viewAttachmentModal.uploading = false;
          },
          success:function(data)
          {
            console.log('hello');
            viewAttachmentModal.uploading = false;
            if(checkStatus(data) == false)
              return; 
            viewAttachmentModal.refresh(); 
          }
        }); 
      },
      onChanged:function(e){
        var files = e.target.files || e.dataTransfer.files;
        if (!files.length)
          return;

        formData = new FormData();
        this.filesToUpload = [];
        for(var count = 0; count<files.length; count++)
        {
          formData.append("files[]", files[count]);
          this.filesToUpload.push(files[count].name);
        }
        // formData.append("version", 0);
        this.inputs = formData;
        // console.log(this.inputs.get("files[]"));
      },
      viewAttachment: function(itemId, item){
        $("#viewAttachmentModal").modal('show');
        this.taskMagId = itemId;
        this.task = item;
        this.filesToUpload = [];

        console.log(this.task);
      },
      refresh: function(){
        console.log('refresh');
        getDetail(this.taskMagId, URLS['GET_ASSIGNED_TASK_DETAIL'],function(data){
          viewAttachmentModal.task = data.data[0];
          console.log(viewAttachmentModal.task);
          tableData.refresh();
          this.filesToUpload = [];
        }); 

      },
      deleteAttachment: function(itemId, attachment){
        var result;  
        result = confirm(lang('q_confirmDelete'));  

        if(result == 0)
          return;

        $.ajax({
          url: HOST + '/accountant/task/deleteAttachment/',
          type: 'POST',
          dataType: 'json',
          data: {"taskMagId":itemId, "attachment":attachment},
          timeout:AJAX_TIMEOUT,
          error:function()
          {
            viewAttachmentModal.uploading = false;
            viewAttachmentModal.refresh();
            tableData.refresh(); 
          },
          success:function(data)
          {
            if(checkStatus(data) == false)
              return;  

            viewAttachmentModal.refresh();
            tableData.refresh(); 
            this.filesToUpload = [];
          }
        }); 
      }
    },
    created: function () {

    }
  })


</script>
