<div class="row">
  <div class="col-xs-12">
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs" id="tabs">
        <li class="active" style="width: 15%;"><a href="#tab1" data-toggle="tab" aria-expanded="true"><h4 class="text-center">{{labels.taskName}}</h4></a></li>
        <li class="" style="width: 15%;"><a href="#tab2" data-toggle="tab" aria-expanded="true"><h4 class="text-center">{{labels.statusName}}</h4></a></li>
        <!-- <li class="pull-right"></li> -->
      </ul>
      <div class="tab-content">
        <!-- /.tab-pane -->                      
        <div class="tab-pane active" id="tab1">
          <div class="box-header">     
            <div class="pull-left">
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createModal">
                <i class="fa fa-plus"></i>
              </button>
              <button type="button" class="btn btn-danger" onclick="tableData.deleteTasks()">
                <i class="fa fa-trash"></i>
              </button>
            </div>    
            <div class="col-xs-5" id="searchBar">
              <div class="input-group">
                <input type="text" class="form-control" v-model="search" v-on:keyup.enter="searchTask">
                <span class="input-group-btn">
                  <button type="button" class="btn btn-info" v-on:click="searchTask">{{searchBtn}}</button>
                </span>
              </div>
            </div>
          </div>          
          <!-- /.box-header -->
          <div class="box-body">
            <table id="table" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th><input type="checkbox" :checked="false" v-model="selectAll"></th>
                  <th>{{labels.taskId}}</th>
                  <th>{{labels.taskCode}}</th>
                  <th>{{labels.taskName}}</th>
                  <th>{{labels.company}}</th>
                  <th>{{labels.reminderStartTime}}</th>
                  <th>{{labels.reminderFrequency}}</th>
                  <th>{{labels.actions}}</th>
                </tr>
              </thead>
              <tbody>
                <tr v-for="item in items">
                  <td>
                    <input type="checkbox" v-model="selected" :value="item.taskId">
                  </td>
                  <td>{{item.taskId}}</td>
                  <td>{{item.taskCode}}</td>
                  <td>{{item.taskName}}</td>
                  <td>{{item.alias}}</td>
                  <td>{{item.reminderStartTime}}</td>
                  <td>{{item.reminderFrequency}}</td>
                  <td>
                    <div class="">
                      <a class="btn-xs" v-on:click="editForm.getTaskDetail(item.taskId)"><i class="fa fa-edit"></i></a>
                      <a class="btn-xs" v-on:click="deleteItem(item.taskId)"><i class="fa fa-trash"></i></a>
                    </div>
                  </td>
                </tr>
                
              </tbody>

            </table>
          </div>
          <div class="box-footer clearfix" id="tableFooter">
            <div class="col-xs-3">
              <span>{{paginationPrefix}}<b>{{total}}</b>{{paginationSuffix}}</span>
            </div>
            <div class="col-xs-9">
              <pagination v-model="currentPage" :total-page="totalPage" size="sm" align="right" boundary-links style="margin-top:-20px; margin-bottom:-20px;"/>
            </div> 
          </div>        
        </div>
        <div class="tab-pane" id="tab2">
          <div class="box-header">     
            <div class="pull-left">
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createModal2">
                <i class="fa fa-plus"></i>
              </button>
            </div>    
            <div class="col-xs-4" id="searchBar2">
              <div class="input-group">
                <input type="text" class="form-control" v-model="search" v-on:keyup.enter="searchStatuses">
                <span class="input-group-btn">
                  <button type="button" class="btn btn-info" v-on:click="searchStatuses">{{searchBtn}}</button>
                </span>
              </div>
            </div>
          </div>          
          <!-- /.box-header -->
          <div class="box-body">
            <div class="col-xs-12 col-md-5">
              <table id="table2" class="table table-bordered table-hover" style="margin-left:-15px">
                <thead>
                  <tr>
                    <th><input type="checkbox" :checked="false" v-model="selectAll"></th>
                    <th>{{labels.statusId}}</th>
                    <th>{{labels.statusName}}</th>
                    <th>{{labels.actions}}</th>
                  </tr>
                </thead>
                <tbody>
                  <tr v-for="item in items">
                    <td>
                      <input type="checkbox" v-model="selected" :value="item.statusId">
                    </td>
                    <td>{{item.statusId}}</td>
                    <td><span v-bind:class="['badge', item.color]">{{item.statusName}}</span></td>
                    <td>
                      <div class="">
                        <a class="btn-xs" v-on:click="editForm2.getStatusDetail(item.statusId)"><i class="fa fa-edit"></i></a>
<!--                         <a v-if="" class="btn-xs" v-on:click="deleteItem(item.statusId)"><i class="fa fa-trash"></i></a> -->
                      </div>
                    </td>
                  </tr>              
                </tbody>
              </table>
            </div>
          </div>
          <div class="box-footer clearfix" id="tableFooter2">
            <div class="col-xs-2" style="margin-left:-15px">
              <span>{{paginationPrefix}}<b>{{total}}</b>{{paginationSuffix}}</span>
            </div>
            <div class="col-xs-3">
              <pagination v-model="currentPage" :total-page="totalPage" size="sm" align="right" boundary-links style="margin-top:-20px; margin-bottom:-20px;"/>
            </div> 
          </div>  
        </div>
        <!-- /.tab-pane -->  
      </div>                    

    </div>

  </div>
</div>
<!-- /.row -->

<div class="modal fade" id="createModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <!-- form start -->                
        <div class="box-body">     
          <form role="form" id="createForm">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#tab1" data-toggle="tab" aria-expanded="true" id="c_n_tab_1">{{labels.new}}</a></li>
                <li class="pull-right"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></li>
              </ul>
              <div class="tab-content" id="c_tab_content">
                <!-- /.tab-pane -->                      
                <div class="tab-pane active" id="tab1">
                  <div class="row">          
                    <div class="col-xs-8 form-group">
                      <label>{{labels.taskCode}}</label>  
                      <input type="text" class="form-control" v-model="inputs.taskCode" placeholder="">
                    </div> 
                  </div>  
                  <div class="row">          
                    <div class="col-xs-8 form-group">
                      <label>{{labels.taskName}}</label>  
                      <input type="text" class="form-control" v-model="inputs.taskName" placeholder="">
                    </div> 
                  </div>  
                  <div class="row">          
                    <div class="col-xs-8 form-group">
                      <label>{{labels.company}}</label>  
                      <select class="form-control" v-model="inputs.companyId" >
                        <option v-for="company in companies" v-bind:value="company.companyId">{{company.name}}</option>
                      </select>
                    </div>
                  </div>
                  <div class="row">          
                    <div class="col-xs-6 form-group">
                      <label>{{labels.reminderStartTime}}</label>  
                      <select class="form-control" v-model="inputs.reminderStartTime" >
                        <option v-for="reminderStartTime in reminderStartTimes" v-bind:value="reminderStartTime.code">{{reminderStartTime.name}}</option>
                      </select>
                    </div>
                  </div>
                  <div class="row">          
                    <div class="col-xs-6 form-group">
                      <label>{{labels.reminderFrequency}}</label>  
                      <select class="form-control" v-model="inputs.reminderFrequency" >
                        <option v-for="reminderFrequency in reminderFrequencies" v-bind:value="reminderFrequency.code">{{reminderFrequency.name}}</option>
                      </select>
                    </div>
                  </div>
                </div>
                <!-- /.tab-pane -->  
              </div>                    
              <!-- /.box-body -->
            </div>
            <div class="box-footer">
              <button class="btn btn-primary pull-right" v-on:click.prevent="createTask" :disabled="uploading">{{labels.new}}</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>


<div class="modal fade" id="editModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <!-- form start -->                
        <div class="box-body">     
          <form role="form" id="editForm">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#e1_tab1" data-toggle="tab" aria-expanded="true" id="c_n_tab_1">{{labels.edit}}</a></li>
                <li class="pull-right"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></li>
              </ul>
              <div class="tab-content">
                <!-- /.tab-pane -->                      
                <div class="tab-pane active" id="e1_tab1">
                  <input type="hidden" class="form-control" v-model="inputs.taskId">
                  <div class="row">          
                    <div class="col-xs-8 form-group">
                      <label>{{labels.taskCode}}</label>  
                      <input type="text" class="form-control" v-model="inputs.taskCode" placeholder="">
                    </div> 
                  </div> 
                  <div class="row">         
                    <div class="col-xs-8 form-group">
                      <label>{{labels.taskName}}</label> 
                      <input type="text" class="form-control" v-model="inputs.taskName" placeholder="">
                    </div> 
                  </div>  
                  <div class="row">          
                    <div class="col-xs-8 form-group">
                      <label>{{labels.company}}</label>  
                      <select class="form-control" v-model="inputs.companyId" >
                        <option v-for="company in companies" v-bind:value="company.companyId">{{company.name}}</option>
                      </select>
                    </div>
                  </div>
                  <div class="row">          
                    <div class="col-xs-6 form-group">
                      <label>{{labels.reminderStartTime}}</label>  
                      <select class="form-control" v-model="inputs.reminderStartTime" >
                        <option v-for="reminderStartTime in reminderStartTimes" v-bind:value="reminderStartTime.code">{{reminderStartTime.name}}</option>
                      </select>
                    </div>
                  </div>
                  <div class="row">          
                    <div class="col-xs-6 form-group">
                      <label>{{labels.reminderFrequency}}</label>  
                      <select class="form-control" v-model="inputs.reminderFrequency" >
                        <option v-for="reminderFrequency in reminderFrequencies" v-bind:value="reminderFrequency.code">{{reminderFrequency.name}}</option>
                      </select>
                    </div>
                  </div>                
                </div>
                <!-- /.tab-pane -->  

              </div>                    
              <!-- /.box-body -->
            </div>
            <div class="box-footer">
              <button class="btn btn-success pull-right" v-on:click.prevent="editTask" :disabled="uploading">{{labels.edit}}</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>


<div class="modal fade" id="createModal2">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">            
        <div class="box-body">     
          <form role="form">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#c2_tab1" data-toggle="tab" aria-expanded="true">{{labels.new}}</a></li>
                <li class="pull-right"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></li>
              </ul>
              <div class="tab-content">                  
                <div class="tab-pane active" id="c2_tab1">
                  <div class="row">          
                    <div class="col-xs-4 form-group">
                      <label>{{labels.name}}</label>  
                      <input type="text" class="form-control" v-model="inputs.statusName" placeholder="">
                    </div> 
                    <div class="col-xs-4">
                      <label>{{labels.preview}}</label>  
                      <br/>
                      <span span v-bind:class="['badge', colorClass]">{{inputs.statusName}}</span> 
                    </div>     
                  </div>  
                  <div class="row">       
                    <div class="col-xs-4 form-group">
                      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        {{labels.changeColor}}      
                        <span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu" role="menu">
                      <li v-for="color in colors"><a type="button" :class="['btn','btn-flat','btn-block', color.class]" v-on:click="changeColor(color.class)">{{color.color}}</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>                    
            </div>
            <div class="box-footer">
              <button class="btn btn-primary pull-right" v-on:click.prevent="createStatus" :disabled="uploading">{{labels.new}}</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="editModal2">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <!-- form start -->                
        <div class="box-body">     
          <form role="form" id="editForm2">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#e2_tab1" data-toggle="tab" aria-expanded="true">{{labels.edit}}</a></li>
                <li class="pull-right"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></li>
              </ul>
              <div class="tab-content">
                <!-- /.tab-pane -->                      
                <div class="tab-pane active" id="e2_tab1">  
                  <input type="hidden" class="form-control" v-model="inputs.statusId">
                  <div class="row">          
                    <div class="col-xs-4 form-group">
                      <label>{{labels.name}}</label>  
                      <input type="text" class="form-control" v-model="inputs.statusName" placeholder="">
                    </div> 
                    <div class="col-xs-4">
                      <label>{{labels.preview}}</label>  
                      <br/>
                      <span span v-bind:class="['badge', colorClass]">{{inputs.statusName}}</span> 
                    </div>     
                  </div>  
                  <div class="row">       
                    <div class="col-xs-4 form-group">
                      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        {{labels.changeColor}}      
                        <span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu" role="menu">
                      <li v-for="color in colors"><a type="button" :class="['btn','btn-flat','btn-block', color.class]" v-on:click="changeColor(color.class)">{{color.color}}</a></li>
                      </ul>
                    </div>
                  </div>   
                </div>
                <!-- /.tab-pane -->  

              </div>                    
              <!-- /.box-body -->
            </div>
            <div class="box-footer">
              <button class="btn btn-success pull-right" v-on:click.prevent="editStatus" :disabled="uploading">{{labels.edit}}</button>
          </form>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>


<script>

  var COLORS = [
        {"color":"red", "class":"bg-red"},
        {"color":"orange", "class":"bg-orange"},
        {"color":"yellow", "class":"bg-yellow"},
        {"color":"teal", "class":"bg-teal"},
        {"color":"blue", "class":"bg-aqua"},
        {"color":"green", "class":"bg-green"},
        {"color":"maroon", "class":"bg-maroon"},
        {"color":"purple", "class":"bg-purple"},
        {"color":"navy", "class":"bg-navy"},
        {"color":"gray", "class":"bg-gray"}
      ];

  var tabs = new Vue({
    el: '#tabs',
    data: {
      labels:{
        "taskName": lang('task_list'),
        "statusName": lang('status_list')
      }
    }    
  });

  function initOptions(){
    $.ajax({
      url: HOST + '/accountant/code/getCodes/'+SYSTEM_LANGUAGE+'?options[]=reminderStartTime&options[]=reminderFrequency&options[]=company',
      type: 'GET',
      dataType: 'json',
      success: function (data) {
        if(data.status == 200){
          for(var i = 0; i < data.data.length; i++){
            if(data.data[i].type == 'reminderStartTime'){
              createForm.reminderStartTimes.push(data.data[i]);
              editForm.reminderStartTimes.push(data.data[i]);
            }
            if(data.data[i].type == 'reminderFrequency'){
              createForm.reminderFrequencies.push(data.data[i]);
              editForm.reminderFrequencies.push(data.data[i]);
            }

          }
        }
        else{
          alert(data.message);    
        }
      }
    });  
    $.ajax({
      url: HOST + '/accountant/code/getCompanies/'+SYSTEM_LANGUAGE,
      type: 'GET',
      dataType: 'json',
      success: function (data) {
        if(data.status == 200){
          for(var i = 0; i < data.data.length; i++){
            createForm.companies.push(data.data[i]);
            editForm.companies.push(data.data[i]);
          }
        }
        else{
          alert(data.message);    
        }
      }
    });  


  }

  var searchBar = new Vue({
    el:'#searchBar',
    data: {
      searchBtn: lang('searchBtn'),    
      search:''
    },
    methods:{
      searchTask: function(){
        tableData.param.search = this.search;
      }
    }
  });

  var searchBar2 = new Vue({
    el:'#searchBar2',
    data: {
      searchBtn: lang('searchBtn'),    
      search:''
    },
    methods:{
      searchStatuses: function(){
        tableData2.param.search = this.search;
        console.log("hello");
      }
    }
  });

  var tableData = new Vue({
    el: '#table',
    data: {     
      labels:{
        "taskId": lang('task_taskId'),
        "taskCode":lang('task_taskCode'),
        "taskName":lang('task_name'),
        "company":lang('task_company'),
        "reminderStartTime":lang('task_reminderStartTime'),
        "reminderFrequency":lang('task_reminderFrequency'),
        "actions":lang('task_actions')     
      },
      items: [],
      selected:[],
      param: {
        "pagination":1,
        "search":searchBar.search,
        "lang":SYSTEM_LANGUAGE
      }
    },
    methods:{
      refresh: function(){
        getTableData(this,pagination,URLS['GET_TASKS']);
      },
      checkAll: function(){
      },
      deleteTasks: function(){
        
        var result;  
        result = confirm(lang('q_confirmDelete'));  

        if(result == 0)
          return;

        $.ajax({
          url: HOST + '/accountant/task/deleteTasks/',
          type: 'GET',
          dataType: 'json',
          data: {"tasks":this.selected},
          timeout:AJAX_TIMEOUT,
          success:function(data)
          {
            if(checkStatus(data) == false)
              return;  
            tableData.selected = [];
            tableData.refresh(); 
          }
        });         
      },
      deleteItem: function(id){
        
        var result;  
        result = confirm(lang('q_confirmDelete'));  

        if(result == 0)
          return;

        $.ajax({
          url: HOST + '/accountant/task/deleteTasks/',
          type: 'GET',
          dataType: 'json',
          data: {"tasks":[id]},
          timeout:AJAX_TIMEOUT,
          success:function(data)
          {
            if(checkStatus(data) == false)
              return;  
            tableData.selected = [];
            tableData.refresh(); 
          }
        });         
      },


      editItem: function(itemId){
        $("#editModal").modal('show');
        editForm.inputs.taskId = itemId;
        console.log(itemId);

      }
    },
    computed: {
      selectAll: {
        get: function () {
          return this.items ? this.selected.length == this.items.length : false;
        },
        set: function (value) {
          var selected = [];

          if (value) {
              this.items.forEach(function (item) {
                  selected.push(item.taskId);
              });
          }
          this.selected = selected;
        }
      }
    },
    watch:{
      param: {
        handler(newValue, oldValue){
          getTableData(tableData,pagination,URLS['GET_TASKS']);
        },
        deep: true 
      }
    },
    created: function(){
      initOptions();
    }
  });

  var tableData2 = new Vue({
    el: '#table2',
    data: {     
      labels:{
        "statusId": lang('status_statusId'),
        "statusName":lang('status_statusName'),
        "color":lang('status_color'),
        "actions":lang('status_actions'),
        "changeColor":lang('status_changeColor')     
      },
      items: [],
      selected:[],
      param: {
        "pagination":1,
        "search":searchBar2.search,
        "lang":SYSTEM_LANGUAGE
      }
    },
    methods:{
      refresh: function(){
        getTableData(this,pagination2,URLS['GET_STATUSES']);
      },
      checkAll: function(){
      },
      deleteStatuses: function(){
        
        var result;  
        result = confirm(lang('q_confirmDelete'));  

        if(result == 0)
          return;

        $.ajax({
          url: HOST + '/accountant/status/deleteStatuses/',
          type: 'GET',
          dataType: 'json',
          data: {"statuses":this.selected},
          timeout:AJAX_TIMEOUT,
          success:function(data)
          {
            if(checkStatus(data) == false)
              return;  
            tableData2.selected = [];
            tableData2.refresh(); 
          }
        });         
      },
      deleteItem: function(id){
        
        var result;  
        result = confirm(lang('q_confirmDelete'));  

        if(result == 0)
          return;

        $.ajax({
          url: HOST + '/accountant/status/deleteStatuses/',
          type: 'GET',
          dataType: 'json',
          data: {"statuses":[id]},
          timeout:AJAX_TIMEOUT,
          success:function(data)
          {
            if(checkStatus(data) == false)
              return;  
            tableData2.selected = [];
            tableData2.refresh(); 
          }
        });         
      },


      editItem: function(itemId){
        $("#editModal").modal('show');
        editForm.inputs.statusId = itemId;
        console.log(itemId);

      }
    },
    computed: {
      selectAll: {
        get: function () {
          return this.items ? this.selected.length == this.items.length : false;
        },
        set: function (value) {
          var selected = [];

          if (value) {
              this.items.forEach(function (item) {
                  selected.push(item.statusId);
              });
          }
          this.selected = selected;
        }
      }
    },
    watch:{
      param: {
        handler(newValue, oldValue){
          getTableData(tableData2,pagination2,URLS['GET_STATUSES']);
        },
        deep: true 
      }
    },
    created: function(){

    }
  });

  var pagination = new Vue({
    el: '#tableFooter',
    data: {
      paginationPrefix: lang('paginationPrefix'),
      paginationSuffix: lang('paginationSuffix'),
      total: 0,
      totalPage: 1,
      currentPage: 1
    },
    methods:{
      // refreshPagination: function(){
      //   refreshPagination($('#pagination'), this);
      // },
      // onPageClick: function(page){
      //   tableData.param.pagination = page;
      //   console.log("page"+page);
      // }
    },
    created:function(){
      getTableData(tableData,this,URLS['GET_TASKS']);

    },
    watch:{
      currentPage: {
        handler(newValue, oldValue){
          getTableData(tableData,pagination,URLS['GET_TASKS']);
          this.currentPage = newValue;
          tableData.param.pagination = newValue;
        },
      }
    }
  }); 

  var pagination2 = new Vue({
    el: '#tableFooter2',
    data: {
      paginationPrefix: lang('paginationPrefix'),
      paginationSuffix: lang('paginationSuffix'),
      total: 0,
      totalPage: 1,
      currentPage: 1
    },
    methods:{
    },
    created:function(){
      getTableData(tableData2,this,URLS['GET_STATUSES']);

    },
    watch:{
      currentPage: {
        handler(newValue, oldValue){
          getTableData(tableData2,pagination2,URLS['GET_STATUSES']);
          this.currentPage = newValue;
          tableData2.param.pagination = newValue;
        },
      }
    }
  });


  var createForm = new Vue({
    el: '#createModal',
    data: {
      labels:{
        "new":lang('task_new'),
        "taskCode":lang('task_taskCode'),
        "taskName":lang('task_name'),
        "company":lang('task_company'),
        "reminderStartTime":lang('task_reminderStartTime'),
        "reminderFrequency":lang('task_reminderFrequency')
      },
      inputs:{},
      reminderFrequencies:[],
      reminderStartTimes:[],
      companies:[],
      uploading: false
    },
    methods:{
      createTask: function(){
        this.uploading = true;
        $.ajax({
          url: HOST + '/accountant/task/createTask/',
          type: 'POST',
          dataType: 'json',
          data: this.inputs,
          timeout:AJAX_TIMEOUT,
          success:function(data)
          {
            createForm.uploading = false;
            if(checkStatus(data) == false)
              return;  
            else {
              $("#createModal").modal('hide');
              Notify.notify('success', lang('notify_success'), lang('notify_successMessage'));
            }
            tableData.refresh(); 
          }
        }); 
      },
    },
    created: function () {

    }
  });

  var editForm = new Vue({
    el: '#editModal',
    data: {
      labels:{
        "edit":lang('task_edit'),
        "taskCode":lang('task_taskCode'),
        "taskName":lang('task_name'),
        "company":lang('task_company'),
        "reminderStartTime":lang('task_reminderStartTime'),
        "reminderFrequency":lang('task_reminderFrequency')      
      },
      inputs:{},
      reminderFrequencies:[],
      reminderStartTimes:[],
      companies:[],
      uploading: false
    },
    methods:{
      editTask: function(){
        this.uploading = true;
        $.ajax({
          url: HOST + '/accountant/task/editTask/',
          type: 'POST',
          dataType: 'json',
          data: this.inputs,
          timeout:AJAX_TIMEOUT,
          error:function(){
            editForm.uploading = false;
          },
          success:function(data)
          {
            console.log('hello');
            editForm.uploading = false;
            if(checkStatus(data) == false)
              return;  
            else {
              $("#editModal").modal('hide');
              Notify.notify('success', lang('notify_success'), lang('notify_successMessage'));
            }
            tableData.refresh(); 
          }
        }); 
      },
      getTaskDetail:function(id){

        getDetail(id, URLS['GET_TASK_DETAIL'],function(data){
          editForm.inputs = data.data[0];
          console.log(editForm.inputs);
          $("#editModal").modal('show');
        });    
      }
    }
  }); 

  var createForm2 = new Vue({
    el: '#createModal2',
    data: {
      labels:{
        "new":lang('status_new'),
        "preview":lang('status_preview'),
        "color":lang('status_color'),
        "changeColor":lang('status_changeColor'),
        "name":lang('status_statusName')
      },
      colors:COLORS,
      inputs:{},
      colorClass:'',
      uploading: false
    },
    methods:{
      createStatus: function(){
        this.uploading = true;
        $.ajax({
          url: HOST + '/accountant/status/createStatus/',
          type: 'POST',
          dataType: 'json',
          data: this.inputs,
          timeout:AJAX_TIMEOUT,
          success:function(data)
          {
            createForm2.uploading = false;
            if(checkStatus(data) == false)
              return;  
            else {
              $("#createModal2").modal('hide');
              Notify.notify('success', lang('notify_success'), lang('notify_successMessage'));
            }
            tableData2.refresh(); 
          }
        }); 
      },
      changeColor:function(color){
        this.colorClass = color;
        this.inputs.color = color;
      }
    },
    created: function () {

    }
  }); 

  var editForm2 = new Vue({
    el: '#editModal2',
    data: {
      labels:{
        "edit":lang('status_edit'),
        "color":lang('status_color'),
        "name":lang('status_statusName'),
        "changeColor":lang('status_changeColor'),
        "preview":lang('status_preview'),
        "save":lang('save')
      },
      inputs:{},
      colors:COLORS,
      colorClass:'',
      uploading: false
    },
    methods:{
      editStatus: function(){
        this.uploading = true;
        $.ajax({
          url: HOST + '/accountant/status/editStatus/',
          type: 'POST',
          dataType: 'json',
          data: this.inputs,
          timeout:AJAX_TIMEOUT,
          error:function(){
            editForm2.uploading = false;
          },
          success:function(data)
          {
            editForm2.uploading = false;
            if(checkStatus(data) == false)
              return;  
            else {
              $("#editModal2").modal('hide');
              Notify.notify('success', lang('notify_success'), lang('notify_successMessage'));
            }
            tableData2.refresh(); 
          }
        }); 
      },
      getStatusDetail:function(id){

        getDetail(id, URLS['GET_STATUS_DETAIL'],function(data){
          editForm2.inputs = data.data[0];
          editForm2.colorClass = editForm2.inputs.color;
          $("#editModal2").modal('show');
        });    
      },
      changeColor:function(color){
        this.colorClass = color;
        this.inputs.color = color;
      }
    }
  }); 


</script>
