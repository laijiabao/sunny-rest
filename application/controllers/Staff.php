<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');

class Staff extends MY_Controller {

  function __construct() 
  { 
    parent::__construct(); 
    $this->load->model('staff_model');
  } 

  public function index($page = 'home')
  {
    show_404();
  }

  public function getStaffs()
  {
    $this->checkHttpMethod('GET');

    $param['pagination'] = $this->input->get('pagination');
    $param['search'] = $this->input->get('search');
    $param['lang'] = $this->input->get('lang');

    $staffs = $this->staff_model->getStaffs($param);

    $this->success($staffs['data'], $staffs['total']);
  }

  public function getStaff($staffId = null, $lang = 'ch')
  {
    $this->checkHttpMethod('GET');

    if($staffId == null)
      $this->error(405, 'Staff Id cannot be empty');

    $staff = $this->staff_model->getStaff($staffId, $lang);

    // if(!empty($staff['data']))
      $this->success($staff['data']);
    // else
    //   $this->error(405, 'Get staff detail failed');

  }

  public function createStaff()
  {
    $this->checkHttpMethod('POST');

    $this->setParam($staff,'POST','staffNo');
    $this->setParam($staff,'POST','enName');
    $this->setParam($staff,'POST','chName');
    $this->setParam($staff,'POST','alias');
    $this->setParam($staff,'POST','gender');
    $this->setParam($staff,'POST','inTime');
    $this->setParam($staff,'POST','countryId');
    $this->setParam($staff,'POST','companyId');
    $this->setParam($staff,'POST','departmentId');
    $this->setParam($staff,'POST','positionId');
    $this->setParam($staff,'POST','email');
    $this->setParam($staff,'POST','identityNo');
    $this->setParam($staff,'POST','birthday');
    $this->setParam($staff,'POST','MPFDate');
    $this->setParam($staff,'POST','mobileNo');
    $this->setParam($staff,'POST','directNo');
    $this->setParam($staff,'POST','pagingNo');
    $this->setParam($staff,'POST','urgentContactName');
    $this->setParam($staff,'POST','urgentContactNo');
    $this->setParam($staff,'POST','homeTel');

    $this->setParam($staff,'POST','faxNo');
    $this->setParam($staff,'POST','bankId');
    $this->setParam($staff,'POST','bankAccount');
    $this->setParam($staff,'POST','enAddress1');
    $this->setParam($staff,'POST','enAddress2');
    $this->setParam($staff,'POST','enAddress3');
    $this->setParam($staff,'POST','chAddress1');
    $this->setParam($staff,'POST','chAddress2');
    $this->setParam($staff,'POST','chAddress3');


    if(empty($staff['staffNo']))
      $this->error(405, 'Please Input Staff No.');
    if(empty($staff['enName']))
      $this->error(405, 'Please Input English Name');
    if(empty($staff['chName']))
      $this->error(405, 'Please Input Chinese Name');    
    if(empty($staff['alias']))
      $this->error(405, 'Please Input Alias');
    if(empty($staff['gender']))
      $this->error(405, 'Please Select Gender');  
    if(empty($staff['departmentId']))
      $this->error(405, 'Please Select Department');
    if(empty($staff['positionId']))
      $this->error(405, 'Please Select Position');  
    if(empty($staff['email']))
      $this->error(405, 'Please Input Email');
    if(empty($staff['mobileNo']))
      $this->error(405, 'Please Input Mobile No.');
    if(empty($staff['birthday']))
      $this->error(405, 'Please Input Birthday');

    $this->processInput($staff);
    $id = $this->staff_model->createStaff($staff);

    if($id != 0)
      $this->success($id);
    else
       $this->error(405, 'Create Staff Failed');

  }

  public function editStaff()
  {
    $this->checkHttpMethod('POST');

    $staff['staffId'] = $this->input->post('staffId');

    $staff['staffNo'] = $this->input->post('staffNo');
    $staff['enName'] = $this->input->post('enName');
    $staff['chName'] = $this->input->post('chName');
    $staff['alias'] = $this->input->post('alias');
    $staff['gender'] = $this->input->post('gender');
    $staff['inTime'] = $this->input->post('inTime');
    $staff['countryId'] = $this->input->post('countryId');
    $staff['companyId'] = $this->input->post('companyId');
    $staff['departmentId'] = $this->input->post('departmentId');
    $staff['positionId'] = $this->input->post('positionId');
    $staff['email'] = $this->input->post('email');
    $staff['identityNo'] = $this->input->post('identityNo');
    $staff['birthday'] = $this->input->post('birthday');
    $staff['MPFDate'] = $this->input->post('MPFDate');
    $staff['mobileNo'] = $this->input->post('mobileNo');
    $staff['directNo'] = $this->input->post('directNo');
    $staff['pagingNo'] = $this->input->post('pagingNo');
    $staff['urgentContactName'] = $this->input->post('urgentContactName');
    $staff['urgentContactNo'] = $this->input->post('urgentContactNo');
    $staff['homeTel'] = $this->input->post('homeTel');
    $staff['faxNo'] = $this->input->post('faxNo');
    $staff['bankId'] = $this->input->post('bankId');
    $staff['bankAccount'] = $this->input->post('bankAccount');
    $staff['enAddress1'] = $this->input->post('enAddress1');
    $staff['enAddress2'] = $this->input->post('enAddress2');
    $staff['enAddress3'] = $this->input->post('enAddress3');
    $staff['chAddress1'] = $this->input->post('chAddress1');
    $staff['chAddress2'] = $this->input->post('chAddress2');
    $staff['chAddress3'] = $this->input->post('chAddress3');

    if(empty($staff['enName']))
      $this->error(405, 'Please Input English Name');
    if(empty($staff['chName']))
      $this->error(405, 'Please Input Chinese Name');    
    if(empty($staff['alias']))
      $this->error(405, 'Please Input Alias');
    if(empty($staff['gender']))
      $this->error(405, 'Please Select Gender');  
    if(empty($staff['departmentId']))
      $this->error(405, 'Please Select Department');
    if(empty($staff['positionId']))
      $this->error(405, 'Please Select Position');  
    if(empty($staff['email']))
      $this->error(405, 'Please Input Email');
    if(empty($staff['mobileNo']))
      $this->error(405, 'Please Input Mobile No.');
    if(empty($staff['birthday']))
      $this->error(405, 'Please Input Birthday');


    $this->processInput($staff);

    $result = $this->staff_model->editStaff($staff);

    if($result != 0)
      $this->success($result);
    else
       $this->error(405, 'Edit Staff Failed');

  }

  public function deleteStaffs(){

    $staffs = implode(',', $this->input->get('staffs'));
    $result = $this->staff_model->deleteStaffs($staffs);
    if(!empty($result))
      $this->success();
    else
      $this->error(405, 'Delete Staffs Failed');
  }

  public function restoreStaffs(){

    $staffs = implode(',', $this->input->get('staffs'));
    $result = $this->staff_model->restoreStaffs($staffs);
    if(!empty($result))
      $this->success();
    else
      $this->error(405, 'Delete Staffs Failed');
  }

  public function getStaffIdByUserId(){



  }

}