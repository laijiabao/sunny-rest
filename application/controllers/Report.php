<?php
class Report extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('ReportExport_model');
	}
        public function exportReport()
        {
                $this->checkHttpMethod('GET');
                $params = $this->input->get();
                if($params['index'] == '1')
                        $data = $this->ReportExport_model->exportTaskReport($params);


                elseif($params['index'] == '2')
                        $data = $this->ReportExport_model->exportClientReport($params);
                else
                        $this->error(422,'Invalid index input!');
        }
}
