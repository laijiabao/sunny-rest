<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');

class Notification extends MY_Controller {

  function __construct() 
  { 
    parent::__construct(); 
    $this->load->model('notification_model');
  } 

  public function index($page = 'home')
  {
    show_404();
  }

  public function getNotifications()
  {

    $this->checkHttpMethod('GET');
    $result['list'] = $this->notification_model->getNotifications();
    $result['new'] = $this->notification_model->getUnreadNotifications();

    $this->success($result);

  }

  public function testSendNotification()
  {
    $this->checkHttpMethod('POST');

    $taskMagId = $this->input->post('taskMagId');
    $staffId = $this->input->post('staffId');
    $type = $this->input->post('type');
    $days = $this->input->post('days');

    $this->sendNotification($taskMagId, $staffId, $type, $days = 1);
    
  }


  public function checkReminders()
  {
    $reminders = $this->notification_model->checkReminders();

    foreach ($reminders as $reminder) {
      $this->sendNotification($reminder->taskMagId, $reminder->staffId, 'R', $reminder->remainingDays);
    }
  }
}