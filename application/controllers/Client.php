<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');

require 'vendor/autoload.php';


class Client extends MY_Controller {

  function __construct() 
  { 
    parent::__construct(); 
    $this->load->model('client_model');
    $this->load->model('task_model');
  } 

  public function index()
  {
    show_404();
  }

  public function getClients()
  {
    $this->checkHttpMethod('GET');

    $param['pagination'] = $this->input->get('pagination');
    $param['search'] = $this->input->get('search');
    $param['lang'] = $this->input->get('lang');
    $param['filterBy'] = $this->input->get('filterBy');

    $clients = $this->client_model->getClients($param);

    // if(!empty($clients['data']))
      $this->success($clients['data'], $clients['total']);
    // else
    //   $this->error(405, 'Get client list failed');
  }

  public function getClientDetail($clientId = null, $lang = 'ch')
  {
    $this->checkHttpMethod('GET');

    if($clientId == null)
      $this->error(405, 'Client Id cannot be empty');

    $client = $this->client_model->getClientDetail($clientId, $lang);

    if(!empty($client['data']))
      $this->success($client['data']);
    else
      $this->error(405, 'Get client detail failed');

  }

  public function createClient()
  {

    $this->checkHttpMethod('POST');

    $this->setParam($client,'POST','chClientName');
    $this->setParam($client,'POST','enClientName');
    $this->setParam($client,'POST','clientCode');
    $this->setParam($client,'POST','attn');
    $this->setParam($client,'POST','companyNo');
    $this->setParam($client,'POST','brNo');
    $this->setParam($client,'POST','country');
    $this->setParam($client,'POST','type');
    $this->setParam($client,'POST','clientGroup');
    $this->setParam($client,'POST','agencyName');
    $this->setParam($client,'POST','enMailingAddress1');
    $this->setParam($client,'POST','enMailingAddress2');
    $this->setParam($client,'POST','enMailingAddress3');
    $this->setParam($client,'POST','chMailingAddress1');
    $this->setParam($client,'POST','chMailingAddress2');
    $this->setParam($client,'POST','chMailingAddress3');
    $this->setParam($client,'POST','enRegisteredAddress1');
    $this->setParam($client,'POST','enRegisteredAddress2');
    $this->setParam($client,'POST','enRegisteredAddress3');
    $this->setParam($client,'POST','chRegisteredAddress1');
    $this->setParam($client,'POST','chRegisteredAddress2');
    $this->setParam($client,'POST','chRegisteredAddress3');

    $this->setParam($clientDetail,'POST','contactPerson1');
    $this->setParam($clientDetail,'POST','contactPerson2');
    $this->setParam($clientDetail,'POST','position1');
    $this->setParam($clientDetail,'POST','position2');
    $this->setParam($clientDetail,'POST','email1');
    $this->setParam($clientDetail,'POST','email2');
    $this->setParam($clientDetail,'POST','telNo1');
    $this->setParam($clientDetail,'POST','telNo2');
    $this->setParam($clientDetail,'POST','telNo3');
    $this->setParam($clientDetail,'POST','faxNo1');
    $this->setParam($clientDetail,'POST','faxNo2');
    $this->setParam($clientDetail,'POST','faxNo3');

    if(empty($client['clientCode']))
      $this->error(405, 'Please Input client code');

    if(empty($client['enClientName']))
      $this->error(405, 'Please Input client name');



    $id = $this->client_model->createClient($client, $clientDetail);

    if($id != 0)
      $this->success($id);
    else
       $this->error(405, 'Create Client Failed');

  }

  public function editClient()
  {
    $this->checkHttpMethod('POST');

    $this->setParam($client,'POST','clientId');
    $this->setParam($client,'POST','chClientName');
    $this->setParam($client,'POST','enClientName');
    $this->setParam($client,'POST','clientCode');
    $this->setParam($client,'POST','attn');
    $this->setParam($client,'POST','companyNo');
    $this->setParam($client,'POST','brNo');
    $this->setParam($client,'POST','country');
    $this->setParam($client,'POST','type');
    $this->setParam($client,'POST','clientGroup');
    $this->setParam($client,'POST','agencyName');
    $this->setParam($client,'POST','enMailingAddress1');
    $this->setParam($client,'POST','enMailingAddress2');
    $this->setParam($client,'POST','enMailingAddress3');
    $this->setParam($client,'POST','chMailingAddress1');
    $this->setParam($client,'POST','chMailingAddress2');
    $this->setParam($client,'POST','chMailingAddress3');
    $this->setParam($client,'POST','enRegisteredAddress1');
    $this->setParam($client,'POST','enRegisteredAddress2');
    $this->setParam($client,'POST','enRegisteredAddress3');
    $this->setParam($client,'POST','chRegisteredAddress1');
    $this->setParam($client,'POST','chRegisteredAddress2');
    $this->setParam($client,'POST','chRegisteredAddress3');

    $this->setParam($clientDetail,'POST','contactPerson1');
    $this->setParam($clientDetail,'POST','contactPerson2');
    $this->setParam($clientDetail,'POST','position1');
    $this->setParam($clientDetail,'POST','position2');
    $this->setParam($clientDetail,'POST','email1');
    $this->setParam($clientDetail,'POST','email2');
    $this->setParam($clientDetail,'POST','telNo1');
    $this->setParam($clientDetail,'POST','telNo2');
    $this->setParam($clientDetail,'POST','telNo3');
    $this->setParam($clientDetail,'POST','faxNo1');
    $this->setParam($clientDetail,'POST','faxNo2');
    $this->setParam($clientDetail,'POST','faxNo3');


    if(empty($client['clientId']))
      $this->error(405, 'Invalid Client ID');

    if(empty($client['clientCode']))
      $this->error(405, 'Please Input client code');

    if(empty($client['enClientName']))
      $this->error(405, 'Please Input client name');

    $result = $this->client_model->editClient($client, $clientDetail);

    if($result != 0)
      $this->success($result);
    else
       $this->error(405, 'Edit Client Failed');

  }

  public function deleteClients()
  {

    $clients = implode(',', $this->input->get('clients'));
    $result = $this->client_model->deleteClients($clients);
    if(!empty($result))
      $this->success();
    else
      $this->error(405, 'Delete Clients Failed');
  }

  public function restoreClients()
  {

    $clients = implode(',', $this->input->get('clients'));
    $result = $this->client_model->restoreClients($clients);
    if(!empty($result))
      $this->success();
    else
      $this->error(405, 'Delete Clients Failed');
  }

  public function outputInvoice()
  {
    $this->checkHttpMethod('POST');

    $clientId = $this->input->post('clientId');

    if(empty($clientId))
      $this->error(405, 'Invalid Client ID');

    
    $this->setParam($client,'POST','clientId');
    $this->setParam($client,'POST','attn');

    $this->setParam($clientDetail,'POST','contactPerson1');
    $this->setParam($clientDetail,'POST','contactPerson2');
    $this->setParam($clientDetail,'POST','email1');
    $this->setParam($clientDetail,'POST','email2');

    $this->client_model->editClient($client, $clientDetail);

    $this->setParam($client,'POST','enClientName');
    $this->setParam($client,'POST','chClientName');
    $this->setParam($client,'POST','clientCode');


    $selected = implode(',', $this->input->post('selected'));
    $disbursements = $this->input->post('disbursements');
    $companyId = $this->input->post('companyId');

    $tasks = $this->task_model->getInvoiceTasks($selected);

    // echo json_encode($tasks);

    foreach ($tasks as $task) {

      $startDate = date("j F Y", strtotime($task->startDate));
      $endDate = date("j F Y", strtotime($task->endDate));
      $year1 = date("Y", strtotime($task->startDate));
      $year2 = date("Y", strtotime($task->startDate)) + 1;

      $task->enTaskDescription = str_replace('$startDate', $startDate, $task->enTaskDescription);
      $task->enTaskDescription = str_replace('$endDate', $endDate, $task->enTaskDescription);
      $task->enTaskDescription = str_replace('$year1', $year1, $task->enTaskDescription);
      $task->enTaskDescription = str_replace('$year2', $year2, $task->enTaskDescription);

    }

    // echo json_encode($tasks);die();
    
    $this->printInvoice($client, $tasks, $disbursements, $companyId);

    

    // echo $disbursements[0]['disbursement'];
    // echo $selected;

  }



  private function printInvoice($client, $tasks, $disbursements, $companyId){

    // var_dump($this->client_model->createInvoice());

    // $invoice['clientId'] = $client['clientId'];
    // $invoiceContent['client'] = $client;
    // $invoiceContent['tasks'] = $tasks;
    // $invoiceContent['disbursements'] = $disbursements;

    // $invoice['content'] = json_encode($invoiceContent, JSON_UNESCAPED_UNICODE);
    $staffNo = $tasks[0]->staffNo;
    $clientCode = $client['clientCode'];
    $enClientName = $client['enClientName'];
    $chClientName = $client['chClientName'];
    $attn = $client['attn'];
    $date = date("j F Y");

    $invoices = array();

    $numberOfTasks = count($tasks) + count($disbursements);

    foreach ($tasks as $task) {
      $invoice['clientId'] = $client['clientId'];
      $invoice['clientCode'] = $clientCode;
      $invoice['staffNo'] = $staffNo;
      $invoice['numberOfTasks'] = $numberOfTasks;
      $invoice['description'] = $task->enTaskDescription;
      $invoice['taskCode'] = $task->taskCode;
      $invoice['amount'] = $task->value;
      $invoices[] = $invoice;
    }

    foreach ($disbursements as $disbursement) {
      $invoice['clientId'] = $client['clientId'];
      $invoice['clientCode'] = $clientCode;
      $invoice['staffNo'] = $staffNo;
      $invoice['numberOfTasks'] = $numberOfTasks;
      $invoice['description'] = $disbursement['disbursement'];
      $invoice['taskCode'] = $disbursement['taskCode'];
      $invoice['amount'] = $disbursement['value'];
      $invoices[] = $invoice;
    }


    $invoiceNo = $this->client_model->createInvoice($invoices);

    if(empty($invoiceNo))
      $this->error(405, 'Create Invoice Failed');

    $ref = $clientCode.'/'.$staffNo;

    $taskRows = count($tasks);
    $disbursementRows = count($disbursements);
    $total = 0;


    $templateProcessor = null;

    if($companyId == 1)
      $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor(APPPATH.'/fileTemplates/invoiceTemplate.docx');
    else
      $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor(APPPATH.'/fileTemplates/invoiceTemplate2.docx');

    $templateProcessor->setValue('enClientName', $enClientName);
    $templateProcessor->setValue('chClientName', $chClientName);
    $templateProcessor->setValue('attn', $attn);
    $templateProcessor->setValue('date', $date);

    $templateProcessor->setValue('itemNo', 'ITEM NO.');
    $templateProcessor->setValue('service', 'PROFESSIONAL SERVICES');
    $templateProcessor->setValue('hkdollars', "TOTAL<w:br/>\nHK$");

    $templateProcessor->setValue('invoiceNo', $invoiceNo);
    $templateProcessor->setValue('ref', $ref);

    $templateProcessor->cloneRow('row', $taskRows);

    for($i = 0; $i < $taskRows; $i++){

      $templateProcessor->setValue('row#'.($i+1), ($i+1).".");
      $templateProcessor->setValue('rowTask#'.($i+1), $tasks[$i]->enTaskDescription);
      $templateProcessor->setValue('rowMoney#'.($i+1), number_format($tasks[$i]->value,2));

      $total = $total + $tasks[$i]->value;
    }


    $templateProcessor->cloneRow('disbursement', $disbursementRows);

    for($i = 0; $i < $disbursementRows; $i++){
      $templateProcessor->setValue('disbursement#'.($i+1), $disbursements[$i]['disbursement']);
      $templateProcessor->setValue('dPrice#'.($i+1), number_format($disbursements[$i]['value'],2));
      $total = $total + $disbursements[$i]['value'];
    }

    $templateProcessor->setValue('total', number_format($total,2));

    $templateProcessor->setValue('enTotal', $this->numberToEnglish($total));
    $templateProcessor->setValue('zhTotal', $this->numberToChinese($total));

    // $templateProcessor->saveAs(APPPATH.'/fileTemplates/invoice.docx');

    $dir = 'uploadfile/'.date("Y").'/'.date("md").'/';
    if (!file_exists($dir)){
      mkdir ($dir,0777,true);
    }  
    $filePath = $dir.'invoice-'.$invoiceNo.'.docx';

    $templateProcessor->saveAs($filePath);
    $this->success($filePath);

    // header("Content-Disposition: attachment; filename='invoice".$invoiceNo.".docx'");
    // $templateProcessor->saveAs('php://output');    
  
  }

  private function numberToEnglish($num){

    $f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
    $result = $f->format($num);

    $result = strtoupper($result);

    $result = str_replace("-", " ", $result);

    return $result;

  }

  private function numberToChinese($num){

    $f = new NumberFormatter("zh-TW", NumberFormatter::SPELLOUT);

    $result = $f->format($num);

    $result = str_replace("一", "壹", $result);
    $result = str_replace("二", "贰", $result);
    $result = str_replace("三", "叁", $result);
    $result = str_replace("四", "肆", $result);
    $result = str_replace("五", "伍", $result);
    $result = str_replace("六", "陆", $result);
    $result = str_replace("七", "柒", $result);
    $result = str_replace("八", "捌", $result);
    $result = str_replace("九", "玖", $result);
    $result = str_replace("十", "拾", $result);
    $result = str_replace("百", "佰", $result);
    $result = str_replace("千", "仟", $result);
    $result = str_replace("万", "萬", $result);
    $result = str_replace("亿", "億", $result);

    return $result;
  }

  public function getClientInvoiceInfo($clientId = null, $staffId = null, $lang = 'ch'){

    $this->checkHttpMethod('GET');

    if(empty($clientId))
      $this->error(405, 'Invalid Client ID');

    $result['tasks'] = $this->client_model->getClientTasks($clientId, $staffId, $lang);
    $result['client'] = $this->client_model->getClientInvoiceInfo($clientId, $lang);
    $result['disbursementList'] = $this->task_model->getDisbursementList();
    $this->success($result);

  }

  public function exportInvoiceSummary()
  {
    $this->checkHttpMethod('GET');

    $param['year'] = $this->input->get('year');
    $param['month'] = $this->input->get('month');

    $data = $this->client_model->invoiceSummary($param);

    $headers = array(
      "Customer ID",
      "Invoice/CM",
      "Date",
      "Sales\nRepresentative",
      "Accounts\mReceivable\nAccount",
      "Number of\nDistributions",
      "Description",
      "G/L Account",
      "Tax Type",
      "Amount",
      "Job ID"
    );

    $year = $param['year'];
    $month = $param['month']; 

    $this->exportExcel("sales_report_".$year."_".$month.".xls", $data, $headers);

  }

}