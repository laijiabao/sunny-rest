<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Admin-ID, User-ID, Authorization');

class Auth extends MY_Controller {

	function __construct() 
	{ 
		parent::__construct(false); 
		$this->load->model('auth_model');
		$this->load->model('user_model');
		$this->load->library('parser');
	} 

	public function login()
	{
		$this->checkHttpMethod('POST');

        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $result = $this->auth_model->login($username,$password);

		if($result == null)
			$this->error(204, $this->lang->line('error_auth_login_failed'));

        // $userData=null;
        // $userData["userId"] = $result->userId;
        // $userData["username"] = $result->username;
        // $userData["role"] = $result->role;     
        // $userData["session"] = $result->session;
        // $userData["lang"] = $result->lang;
        // $userData["staffId"] = $result->staffId;
        // $_SESSION["userData"] = $userData;
        // $_SESSION["loggedIn"] = true;		

		$this->success($result);
	}

	public function register()
	{
		$this->checkHttpMethod('POST');

		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$this->load->library('form_validation');
		$this->load->helper('security');
		$this->form_validation->set_rules('username','username','trim|xss_clean|required');
		$this->form_validation->set_rules('password','password','trim|required|trim|xss_clean');

		if($this->form_validation->run()){

	    	$salt = $this->createEncrypt(6);

			$user = Array();
			$user['username'] = $username;
			$user['password'] = md5(md5($password).$salt);
			$user['salt'] = $salt;
			$user['session'] = $this->createEncrypt(32);
			$user['role'] = 'S';
			$user['lastLogin'] = date("Y-m-d H:i:s");
			$user['ip'] = $_SERVER["REMOTE_ADDR"];

			$this->load->model('auth_model');
  
			$result = $this->auth_model->register($user);
			if($result == null)
				$this->error(204, 'Register Fail');
			else
				$this->success();

		}else{
			$this->error(204, $this->form_validation->error_array());
		}
	}


	// public function logout()
	// {
	// 	unset($_SESSION['userData']);
	// 	$_SESSION["loggedIn"] = false;	
	// }
}


