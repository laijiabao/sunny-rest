<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');

class Statistic extends MY_Controller {

  function __construct() 
  { 
    parent::__construct(); 
    $this->load->model('statistic_model');
  } 

  public function index($page = 'home')
  {
    show_404();
  }


  public function getRevenue()
  {
    $this->checkHttpMethod('GET');
    $pagination = $this->input->get('pagination');
    $startDate = $this->input->get('startDate');
    $endDate = $this->input->get('endDate');
    $companyId = $this->input->get('companyId');

    $revenue = $this->statistic_model->getRevenue($startDate, $endDate,$companyId);
    $manHours = $this->statistic_model->getManhours($startDate, $endDate,$companyId);
    $clients = $this->statistic_model->getClients($startDate, $endDate,$companyId);
    $tasks = $this->statistic_model->getTasks($startDate, $endDate,$companyId);
    
    $totalRevenue = 0;
    $totalManHours = 0;
    $totalClients = 0;
    $totalTasks = 0;

    foreach ($revenue as $r) {
      $totalRevenue = $totalRevenue + $r->revenue;
    }
    foreach ($manHours as $m) {
      $totalManHours = $totalManHours + $m->manHours;
    }
    foreach ($clients as $c) {
      $totalClients = $totalClients + $c->clients;
    }
    foreach ($tasks as $t) {
      $totalTasks = $totalTasks + $t->tasks;
    }

    $totalRevenue = number_format($totalRevenue,2);
    $totalManHours = number_format($totalManHours,2);

    $result['revenue'] = $revenue;
    $result['totalRevenue'] = $totalRevenue;
    $result['totalClients'] = $totalClients;
    $result['totalManHours'] = $totalManHours;
    $result['totalTasks'] = $totalTasks;

    $this->success($result);

  }

  public function getStatistics(){

    $this->checkHttpMethod('GET');
    $pagination = $this->input->get('pagination');
    $startDate = $this->input->get('startDate');
    $endDate = $this->input->get('endDate');
    $companyId = $this->input->get('companyId');

    $result['clients'] = $this->statistic_model->getClientProfit($pagination, $startDate, $endDate,$companyId);
    $result['staffs'] = $this->statistic_model->getStaffProfit($pagination, $startDate, $endDate,$companyId);
    $result['tasks'] = $this->statistic_model->getTaskProfit($pagination, $startDate, $endDate,$companyId);

    $this->success($result);
  }

  public function getClientStats(){

    $this->checkHttpMethod('GET');
    $pagination = $this->input->get('pagination');
    $startDate = $this->input->get('startDate');
    $endDate = $this->input->get('endDate');
    $companyId = $this->input->get('companyId');

    $result['clients'] = $this->statistic_model->getClientProfit($pagination, $startDate, $endDate,$companyId);

    $this->success($result);
  }

  public function getStaffStats(){

    $this->checkHttpMethod('GET');
    $pagination = $this->input->get('pagination');
    $startDate = $this->input->get('startDate');
    $endDate = $this->input->get('endDate');
    $companyId = $this->input->get('companyId');

    $result['staffs'] = $this->statistic_model->getStaffProfit($pagination, $startDate, $endDate,$companyId);

    $this->success($result);
  }

  public function getTaskStats(){

    $this->checkHttpMethod('GET');
    $pagination = $this->input->get('pagination');
    $startDate = $this->input->get('startDate');
    $endDate = $this->input->get('endDate');
    $companyId = $this->input->get('companyId');

    $result['tasks'] = $this->statistic_model->getTaskProfit($pagination, $startDate, $endDate,$companyId);

    $this->success($result);
  }

  public function getTopStats(){

    $this->checkHttpMethod('GET');
    $n = $this->input->get('n');
    $startDate = $this->input->get('startDate');
    $endDate = $this->input->get('endDate');
    $companyId = $this->input->get('companyId');

    $topStaffs = $this->statistic_model->getTopNStaffs($n, $startDate, $endDate,$companyId);

    $result['topStaffs'] = $topStaffs;

    $this->success($result);
  }

  public function loadDashboard(){

    $this->checkHttpMethod('GET');

    $pagination = $this->input->get('pagination');
    $startDate = $this->input->get('startDate');
    $endDate = $this->input->get('endDate');
    $companyId = $this->input->get('companyId');

    $revenue = $this->statistic_model->getRevenue($startDate, $endDate,$companyId);
    $manHours = $this->statistic_model->getManhours($startDate, $endDate,$companyId);
    $clients = $this->statistic_model->getClients($startDate, $endDate,$companyId);
    $tasks = $this->statistic_model->getTasks($startDate, $endDate,$companyId);
    

    $totalRevenue = 0;
    $totalManHours = 0;
    $totalClients = 0;
    $totalTasks = 0;

    foreach ($revenue as $r) {
      $totalRevenue = $totalRevenue + $r->revenue;
    }
    foreach ($manHours as $m) {
      $totalManHours = $totalManHours + $m->manHours;
    }
    foreach ($clients as $c) {
      $totalClients = $totalClients + $c->clients;
    }
    foreach ($tasks as $t) {
      $totalTasks = $totalTasks + $t->tasks;
    }

    $totalRevenue = number_format($totalRevenue,2);
    $totalManHours = number_format($totalManHours,2);

    $result['revenue']['value'] = $revenue;
    $result['revenue']['totalRevenue'] = $totalRevenue;
    $result['revenue']['totalClients'] = $totalClients;
    $result['revenue']['totalManHours'] = $totalManHours;
    $result['revenue']['totalTasks'] = $totalTasks;

    $result['topStaffs'] = $this->statistic_model->getTopNStaffs(10, $startDate, $endDate,$companyId);
    $result['topClients'] = $this->statistic_model->getTopNClients(10, $startDate, $endDate,$companyId);
    $result['topTasks'] = $this->statistic_model->getTopNTasks(10,$startDate, $endDate,$companyId);

    $result['clients'] = $this->statistic_model->getClientProfit($pagination, $startDate, $endDate,$companyId);
    $result['staffs'] = $this->statistic_model->getStaffProfit($pagination, $startDate, $endDate,$companyId);
    $result['tasks'] = $this->statistic_model->getTaskProfit($pagination, $startDate, $endDate,$companyId);

    $this->success($result);

  }

}