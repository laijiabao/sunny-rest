<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');

class Search extends MY_Controller {

  function __construct() 
  { 
    parent::__construct(); 
    $this->load->model('search_model');
  } 

  public function index()
  {
    show_404();
  }

  public function query($q)
  {
    $this->checkHttpMethod('GET');

    if($q == null)
      return;

    $result = $this->search_model->clientList($q);

    // if(!empty($statuses['data']))
    $this->success($result);
    // else
    //   $this->error(405, 'Get status list failed');
  }

  public function client($q)
  {
    $this->checkHttpMethod('GET');

    if($q == null)
      return;

    $result = $this->search_model->clientList($q);

    $this->success($result);    
  }

  public function staff($q)
  {
    $this->checkHttpMethod('GET');

    if($q == null)
      return;

    $result = $this->search_model->staffList($q);

    $this->success($result);    
  }

  public function task($q)
  {
    $this->checkHttpMethod('GET');

    if($q == null)
      return;

    $result = $this->search_model->taskList($q);

    $this->success($result);    
  }

}