<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Admin-ID, User-ID, Authorization');

class Code extends MY_Controller {

	function __construct() 
	{ 
		parent::__construct(); 
		$this->load->model('code_model');
	} 

	public function index($user_id)
	{
	}

	public function type($type = 'all')
	{

		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			echo json_encode(array('status' => 400,'message' => 'Bad request.'));
			die();
		} 

		$result = $this->code_model->get_type_code_list($type);
		echo json_encode($result, JSON_UNESCAPED_UNICODE);

	}

	public function getCodes($lang = 'ch')
	{

		$this->checkHttpMethod('GET');

		$options = implode(',', $this->input->get('options'));

		$result = $this->code_model->getCodes($options, $lang);

		echo json_encode($result, JSON_UNESCAPED_UNICODE);	

	}

	public function getStaffOptions($lang = 'ch')
	{
		$this->checkHttpMethod('GET');
		$result['data']['countries'] = $this->code_model->getCountries($lang);
		$result['data']['departments'] = $this->code_model->getDepartments($lang);
		$result['data']['positions'] = $this->code_model->getPositions($lang);
		$result['data']['banks'] = $this->code_model->getBanks($lang);

		$this->success($result['data']);
	}


	public function getClientOptions()
	{
		$this->checkHttpMethod('GET');
		$result['data']['countries'] = $this->code_model->getCountries('en');
		$result['data']['clientGroups'] = $this->code_model->getClientGroups();
		$result['data']['types'] = $this->code_model->getClientTypes();

		$this->success($result['data']);
	}


	public function getCompanies($lang = 'ch'){
		$this->checkHttpMethod('GET');
		$result = $this->code_model->getCompanies($lang);

		$this->success($result);

	}

}