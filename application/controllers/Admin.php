<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Admin-ID, User-ID, Authorization');

class Admin extends CI_Controller {

  function __construct() 
  { 
    parent::__construct(); 
  } 

    public function index(){

        $this->load->view('login');       
    }

}