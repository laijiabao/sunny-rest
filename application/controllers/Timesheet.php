<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');

require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class Timesheet extends MY_Controller {

  function __construct() 
  { 
    parent::__construct(); 
    $this->load->model('code_model');
    $this->load->model('task_model');
    $this->load->model('staff_model');
    $this->load->model('client_model');
    $this->load->model('timesheet_model');
  } 

  public function index()
  {
    show_404();
  }

  public function getOptions()
  {
    $this->checkHttpMethod('GET');

   //Get Leave Options

    $data['leaveOptions'] = $this->code_model->getCodes('leaveType');

    //Get Task List
    $data['taskList'] = $this->task_model->getTaskList();

    //Get Company List
    $data['clientList'] = $this->client_model->getClients();

    $this->success($data);
  }

  public function createTask()
  {
    $this->checkHttpMethod('POST');

    $task = array();

    $this->setParam($task,'POST','staffId');
    $this->setParam($task,'POST','leaveType');
    $this->setParam($task,'POST','taskId');
    $this->setParam($task,'POST','clientId');
    $this->setParam($task,'POST','hours');
    $this->setParam($task,'POST','date');
    $this->setParam($task,'POST','isChargeable');

    // if(empty($task['staffId']))
    //   $this->error(405, 'Please Input Staff ID');

    if(empty($task['staffId']))
      $task['staffId'] = $this->session->userdata('userData')['staffId'];


    if(empty($task['leaveType']) && empty($task['clientId']) && empty($task['taskId']))
      $this->error(405, 'Please Input A Valid Task');

    if(empty($task['date']))
      $this->error(405, 'Please Input Date');


    $role = $this->session->userdata('userData')['role']; 


    $today = strtotime(date("Y-m-d"));
    $date = strtotime($task['date']);
    $datediff = round(abs($today - $date) / (60 * 60 * 24));

    if($datediff > 14 && $role != 'A' && $role != 'M'){
      $this->error(405, 'You can only edit timesheet within 2 weeks');
    }

    if(!empty($task['leaveType'])){
      if($task['leaveType'] == 'SLH')
        $task['hours'] = 4;
      else if($task['leaveType'] == 'AL' || $task['leaveType'] == 'SLF')
        $task['hours'] = 8;
    }else{
      if(empty($task['hours']) || $task['hours'] <= 0)
        $this->error(405, 'Please Input Using Hours');
    }

    $id = $this->timesheet_model->createTask($task);

    if($id != 0)
      $this->success($id);
    else
       $this->error(405, 'Create Task Failed');

  }

  public function editTask()
  {
    $this->checkHttpMethod('POST');

    $task['timesheetId'] = $this->input->post('timesheetId');
    $this->setParam($task,'POST','staffId');
    $this->setParam($task,'POST','leaveType');
    $this->setParam($task,'POST','taskId');
    $this->setParam($task,'POST','clientId');
    $this->setParam($task,'POST','hours');
    $this->setParam($task,'POST','date');
    $this->setParam($task,'POST','isChargeable');


    if(empty($task['staffId']))
      $task['staffId'] = $this->session->userdata('userData')['staffId'];

    if(!empty($task['date']))
      $this->error(405, 'You cannot change date');

    $result = $this->timesheet_model->editTask($task);

    // if($result != 0)
    $this->success($result);
    // else
    //    $this->error(405, 'Edit Task Failed');

  }

  public function getTimesheet(){

    $this->checkHttpMethod('GET');

    $param['pagination'] = $this->input->get('pagination');
    $param['search'] = $this->input->get('search');
    $param['lang'] = $this->input->get('lang');
    $param['staffId'] = $this->input->get('staffId');

    if(empty($param['staffId']))
      $param['staffId'] = $this->session->userdata('userData')['staffId'];

    $param['startDate'] = $this->input->get('startDate');
    $param['endDate'] = $this->input->get('endDate');

    if(empty($param['staffId']))
      $this->error(405, 'Staff ID cannot be empty');

    $tasks = $this->timesheet_model->getTimesheet($param);

    $this->success($tasks['data']);

  }

  public function deleteTask(){

    $this->checkHttpMethod('POST');

    $timesheetId = $this->input->post('timesheetId');

    $result = $this->timesheet_model->deleteTask($timesheetId);
    if(!empty($result))
      $this->success();
    else
      $this->error(405, 'Delete Task Failed');
  }


  public function printTimesheet()
  {
    // $this->setParam($param,'GET','startDate');
    // $this->setParam($param,'GET','endDate');
    // $this->setParam($param,'GET','staffId');



    // $staffId = $param['staffId'];
    // $startDate = $param['startDate'];
    // $endDate = $param['endDate'];


    // if(empty($staffId))
    //   $this->error(405, 'Invalid Staff ID');
    // if(empty($startDate))
    //   $this->error(405, 'Invalid Start Date');
    // if(empty($endDate))
    //   $this->error(405, 'Invalid End Date');

    // $this->setParam($param,'GET','staffId');

    // $param['staffId'] = 67;
    $this->setParam($param,'GET','year');
    $this->setParam($param,'GET','from');
    $this->setParam($param,'GET','to');
    $this->setParam($param,'GET','staffId');

    if(empty($param['staffId']))
      $param['staffId'] = $this->session->userdata('userData')['staffId'];

    $staffId = $param['staffId'];
    $year = $param['year'];
    $from = $param['from'];
    $to = $param['to'];


    if(empty($staffId))
      $this->error(405, 'Invalid Staff ID');
    if(empty($year) || !is_numeric($year))
      $this->error(405, 'Invalid Year');
    if(empty($from) || !is_numeric($from) || $from < 1 || $from > 12)
      $this->error(405, 'Invalid Start Month');
    if(empty($to) || !is_numeric($to) || $to < $from || $to > 12)
      $this->error(405, 'Invalid End Month');


    $staff = $this->staff_model->getStaff($staffId);

    //Get Total timesheets to print (Get total month)
    $totalMonths = $to - $from + 1;

    //For each month
    for($i = 0; $i < $totalMonths; $i++){
      //Get start date and end date
      $firstday = date("$year-".($from+$i)."-01");
      $lastday = date('Y-m-d', strtotime("$firstday +1 month -1 day"));

      $_param['staffId'] = $staffId;
      $_param['startDate'] = $firstday;
      $_param['endDate'] = $lastday;

      $this->printOneTimesheet($_param, $staff);
    }
  }

  private function printOneTimesheet($param, $staff){

    $startDate = $param['startDate'];
    $endDate = $param['endDate'];

    $data = $this->timesheet_model->printTimesheet($param);

    $timesheet = $data['timesheet'];
    $standardHours = $data['standardHours'];
    $leaves = $data['leaves'];

    $reader = IOFactory::createReader('Xls');
    $spreadsheet = $reader->load(APPPATH.'/fileTemplates/timesheetTemplate.xls');

    //Write Staff Info
    $staffInfo = 'Name: ('.$staff['data'][0]->staffNo.') '.$staff['data'][0]->alias." Month:\n".date('M - Y',strtotime($startDate));
    $spreadsheet->getActiveSheet()->setCellValue('A2', $staffInfo);
    $spreadsheet->getActiveSheet()->getStyle('A2')->getAlignment()->setWrapText(true);


    //Delete unuse column from template
    $_startDate = strtotime($startDate);
    $_endDate = strtotime($endDate);
    $datediff = round(($_endDate - $_startDate) / (60 * 60 * 24));

    if($datediff < 28){
      $spreadsheet->getActiveSheet()->removeColumn('AF', 1);
    }
    if($datediff < 29){
      $spreadsheet->getActiveSheet()->removeColumn('AG', 1);
    }
    if($datediff < 30){
      $spreadsheet->getActiveSheet()->removeColumn('AH',1);
    }


    $baseRow = 4;
    $r = 0;
    foreach ($timesheet as $dataRow) {
        $row = $baseRow + $r;
        $spreadsheet->getActiveSheet()->insertNewRowBefore($row, 1);

        $spreadsheet->getActiveSheet()->setCellValue('A' . $row, $dataRow->enClientName)
            ->setCellValue('B' . $row, $dataRow->clientCode)
            ->setCellValue('C' . $row, $dataRow->Total)
            ->setCellValue('D' . $row, $dataRow->D1 == "0.0" ? NULL :$dataRow->D1 )
            ->setCellValue('E' . $row, $dataRow->D2 == "0.0" ? NULL :$dataRow->D2 )
            ->setCellValue('F' . $row, $dataRow->D3 == "0.0" ? NULL :$dataRow->D3 )
            ->setCellValue('G' . $row, $dataRow->D4 == "0.0" ? NULL :$dataRow->D4 )
            ->setCellValue('H' . $row, $dataRow->D5 == "0.0" ? NULL :$dataRow->D5 )
            ->setCellValue('I' . $row, $dataRow->D6 == "0.0" ? NULL :$dataRow->D6 )
            ->setCellValue('J' . $row, $dataRow->D7 == "0.0" ? NULL :$dataRow->D7 )
            ->setCellValue('K' . $row, $dataRow->D8 == "0.0" ? NULL :$dataRow->D8 )
            ->setCellValue('L' . $row, $dataRow->D9 == "0.0" ? NULL :$dataRow->D9 )
            ->setCellValue('M' . $row, $dataRow->D10 == "0.0" ? NULL :$dataRow->D10 )
            ->setCellValue('N' . $row, $dataRow->D11 == "0.0" ? NULL :$dataRow->D11 )
            ->setCellValue('O' . $row, $dataRow->D12 == "0.0" ? NULL :$dataRow->D12 )
            ->setCellValue('P' . $row, $dataRow->D13 == "0.0" ? NULL :$dataRow->D13 )
            ->setCellValue('Q' . $row, $dataRow->D14 == "0.0" ? NULL :$dataRow->D14 )
            ->setCellValue('R' . $row, $dataRow->D15 == "0.0" ? NULL :$dataRow->D15 )
            ->setCellValue('S' . $row, $dataRow->D16 == "0.0" ? NULL :$dataRow->D16 )
            ->setCellValue('T' . $row, $dataRow->D17 == "0.0" ? NULL :$dataRow->D17 )
            ->setCellValue('U' . $row, $dataRow->D18 == "0.0" ? NULL :$dataRow->D18 )
            ->setCellValue('V' . $row, $dataRow->D19 == "0.0" ? NULL :$dataRow->D19 )
            ->setCellValue('W' . $row, $dataRow->D20 == "0.0" ? NULL :$dataRow->D20 )
            ->setCellValue('X' . $row, $dataRow->D21 == "0.0" ? NULL :$dataRow->D21 )
            ->setCellValue('Y' . $row, $dataRow->D22 == "0.0" ? NULL :$dataRow->D22 )
            ->setCellValue('Z' . $row, $dataRow->D23 == "0.0" ? NULL :$dataRow->D23 )
            ->setCellValue('AA' . $row, $dataRow->D24 == "0.0" ? NULL :$dataRow->D24 )
            ->setCellValue('AB' . $row, $dataRow->D25 == "0.0" ? NULL :$dataRow->D25 )
            ->setCellValue('AC' . $row, $dataRow->D26 == "0.0" ? NULL :$dataRow->D26 )
            ->setCellValue('AD' . $row, $dataRow->D27 == "0.0" ? NULL :$dataRow->D27 )
            ->setCellValue('AE' . $row, $dataRow->D28 == "0.0" ? NULL :$dataRow->D28 );

        if(!empty($dataRow->D29)){
          $spreadsheet->getActiveSheet()->setCellValue('AF' . $row, $dataRow->D29 == "0.0" ? NULL :$dataRow->D29 );
        }
        if(!empty($dataRow->D30)){
          $spreadsheet->getActiveSheet()->setCellValue('AG' . $row, $dataRow->D30 == "0.0" ? NULL :$dataRow->D30 );
        }
        if(!empty($dataRow->D31)){
          $spreadsheet->getActiveSheet()->setCellValue('AH' . $row, $dataRow->D31 == "0.0" ? NULL :$dataRow->D31 );
        }
        $r = $r + 1;

    }

    $row = $baseRow + $r;
    $spreadsheet->getActiveSheet()->insertNewRowBefore($row, 1);
    $spreadsheet->getActiveSheet()
      ->setCellValue('A' . $row, "Less: Overtime Hours")
      ->setCellValue('B' . $row, "")
      ->setCellValue('C' . $row, "=SUM(D".$row.":AH".$row.")")
      ->setCellValue('D' . $row, "=D".($row-1)."-D".($row+1))
      ->setCellValue('E' . $row, "=E".($row-1)."-E".($row+1))
      ->setCellValue('F' . $row, "=F".($row-1)."-F".($row+1))
      ->setCellValue('G' . $row, "=G".($row-1)."-G".($row+1))
      ->setCellValue('H' . $row, "=H".($row-1)."-H".($row+1))
      ->setCellValue('I' . $row, "=I".($row-1)."-I".($row+1))
      ->setCellValue('J' . $row, "=J".($row-1)."-J".($row+1))
      ->setCellValue('K' . $row, "=K".($row-1)."-K".($row+1))
      ->setCellValue('L' . $row, "=L".($row-1)."-L".($row+1))
      ->setCellValue('M' . $row, "=M".($row-1)."-M".($row+1))
      ->setCellValue('N' . $row, "=N".($row-1)."-N".($row+1))
      ->setCellValue('O' . $row, "=O".($row-1)."-O".($row+1))
      ->setCellValue('P' . $row, "=P".($row-1)."-P".($row+1))
      ->setCellValue('Q' . $row, "=Q".($row-1)."-Q".($row+1))
      ->setCellValue('R' . $row, "=R".($row-1)."-R".($row+1))
      ->setCellValue('S' . $row, "=S".($row-1)."-S".($row+1))
      ->setCellValue('T' . $row, "=T".($row-1)."-T".($row+1))
      ->setCellValue('U' . $row, "=U".($row-1)."-U".($row+1))
      ->setCellValue('V' . $row, "=V".($row-1)."-V".($row+1))
      ->setCellValue('W' . $row, "=W".($row-1)."-W".($row+1))
      ->setCellValue('X' . $row, "=X".($row-1)."-X".($row+1))
      ->setCellValue('Y' . $row, "=Y".($row-1)."-Y".($row+1))
      ->setCellValue('Z' . $row, "=Z".($row-1)."-Z".($row+1))
      ->setCellValue('AA' . $row, "=AA".($row-1)."-AA".($row+1))
      ->setCellValue('AB' . $row, "=AB".($row-1)."-AB".($row+1))
      ->setCellValue('AC' . $row, "=AC".($row-1)."-AC".($row+1))
      ->setCellValue('AD' . $row, "=AD".($row-1)."-AD".($row+1))
      ->setCellValue('AE' . $row, "=AE".($row-1)."-AE".($row+1));

    if($datediff >= 28){
      $spreadsheet->getActiveSheet()->setCellValue('AF' . $row, "=AF".($row-1)."-AF".($row+1));
    }
    if($datediff >= 29){
      $spreadsheet->getActiveSheet()->setCellValue('AG' . $row, "=AG".($row-1)."-AG".($row+1));
    }
    if($datediff >= 30){
      $spreadsheet->getActiveSheet()->setCellValue('AH' . $row, "=AH".($row-1)."-AH".($row+1));
    }
    $r = $r + 1;
    $row = $baseRow + $r;

    $spreadsheet->getActiveSheet()
      ->setCellValue('A' . $row, "Standard Hours")
      ->setCellValue('B' . $row, "")
      ->setCellValue('C' . $row, "=SUM(D".$row.":AH".$row.")")
      ->setCellValue('D' . $row, $standardHours->D1 )
      ->setCellValue('E' . $row, $standardHours->D2 )
      ->setCellValue('F' . $row, $standardHours->D3 )
      ->setCellValue('G' . $row, $standardHours->D4 )
      ->setCellValue('H' . $row, $standardHours->D5 )
      ->setCellValue('I' . $row, $standardHours->D6 )
      ->setCellValue('J' . $row, $standardHours->D7 )
      ->setCellValue('K' . $row, $standardHours->D8 )
      ->setCellValue('L' . $row, $standardHours->D9 )
      ->setCellValue('M' . $row, $standardHours->D10 )
      ->setCellValue('N' . $row, $standardHours->D11 )
      ->setCellValue('O' . $row, $standardHours->D12 )
      ->setCellValue('P' . $row, $standardHours->D13 )
      ->setCellValue('Q' . $row, $standardHours->D14 )
      ->setCellValue('R' . $row, $standardHours->D15 )
      ->setCellValue('S' . $row, $standardHours->D16 )
      ->setCellValue('T' . $row, $standardHours->D17 )
      ->setCellValue('U' . $row, $standardHours->D18 )
      ->setCellValue('V' . $row, $standardHours->D19 )
      ->setCellValue('W' . $row, $standardHours->D20 )
      ->setCellValue('X' . $row, $standardHours->D21 )
      ->setCellValue('Y' . $row, $standardHours->D22 )
      ->setCellValue('Z' . $row, $standardHours->D23 )
      ->setCellValue('AA' . $row, $standardHours->D24 )
      ->setCellValue('AB' . $row, $standardHours->D25 )
      ->setCellValue('AC' . $row, $standardHours->D26 )
      ->setCellValue('AD' . $row, $standardHours->D27 )
      ->setCellValue('AE' . $row, $standardHours->D28 );

    if($datediff >= 28){
      $spreadsheet->getActiveSheet()->setCellValue('AF' . $row, $standardHours->D29 );
    }
    if($datediff >= 29){
      $spreadsheet->getActiveSheet()->setCellValue('AG' . $row, $standardHours->D30 );
    }
    if($datediff >= 30){
      $spreadsheet->getActiveSheet()->setCellValue('AH' . $row, $standardHours->D31 );
    }

    $r = $r + 1;
    $row = $baseRow + $r;

    foreach ($leaves as $dataRow) {
      
      $row = $baseRow + $r;
      $spreadsheet->getActiveSheet()->insertNewRowBefore($row, 1);
      // echo $row;
      $spreadsheet->getActiveSheet()
        ->setCellValue('A' . $row, $dataRow->leaveType)
        ->setCellValue('B' . $row, "")
        ->setCellValue('C' . $row, "=SUM(D".$row.":AH".$row.")")
        ->setCellValue('D' . $row, $dataRow->D1 )
        ->setCellValue('E' . $row, $dataRow->D2 )
        ->setCellValue('F' . $row, $dataRow->D3 )
        ->setCellValue('G' . $row, $dataRow->D4 )
        ->setCellValue('H' . $row, $dataRow->D5 )
        ->setCellValue('I' . $row, $dataRow->D6 )
        ->setCellValue('J' . $row, $dataRow->D7 )
        ->setCellValue('K' . $row, $dataRow->D8 )
        ->setCellValue('L' . $row, $dataRow->D9 )
        ->setCellValue('M' . $row, $dataRow->D10 )
        ->setCellValue('N' . $row, $dataRow->D11 )
        ->setCellValue('O' . $row, $dataRow->D12 )
        ->setCellValue('P' . $row, $dataRow->D13 )
        ->setCellValue('Q' . $row, $dataRow->D14 )
        ->setCellValue('R' . $row, $dataRow->D15 )
        ->setCellValue('S' . $row, $dataRow->D16 )
        ->setCellValue('T' . $row, $dataRow->D17 )
        ->setCellValue('U' . $row, $dataRow->D18 )
        ->setCellValue('V' . $row, $dataRow->D19 )
        ->setCellValue('W' . $row, $dataRow->D20 )
        ->setCellValue('X' . $row, $dataRow->D21 )
        ->setCellValue('Y' . $row, $dataRow->D22 )
        ->setCellValue('Z' . $row, $dataRow->D23 )
        ->setCellValue('AA' . $row, $dataRow->D24 )
        ->setCellValue('AB' . $row, $dataRow->D25 )
        ->setCellValue('AC' . $row, $dataRow->D26 )
        ->setCellValue('AD' . $row, $dataRow->D27 )
        ->setCellValue('AE' . $row, $dataRow->D28 );

      if($datediff >= 28){
        $spreadsheet->getActiveSheet()->setCellValue('AF' . $row, $dataRow->D29 );
      }
      if($datediff >= 29){
        $spreadsheet->getActiveSheet()->setCellValue('AG' . $row, $dataRow->D30 );
      }
      if($datediff >= 30){
        $spreadsheet->getActiveSheet()->setCellValue('AH' . $row, $dataRow->D31 );
      }
      $r = $r + 1;
    }

    $spreadsheet->getActiveSheet()->removeRow($baseRow - 1, 1);

    $writer = IOFactory::createWriter($spreadsheet, "Xls");

    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="timesheet.xls"');
    $writer->save("php://output");

    $writer->save(APPPATH.'/fileTemplates/timesheet.xls');
  }

}

