<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');

class Status extends MY_Controller {

  function __construct() 
  { 
    parent::__construct(); 
    $this->load->model('status_model');
  } 

  public function index()
  {
    show_404();
  }

  public function getStatuses()
  {
    $this->checkHttpMethod('GET');

    $param['pagination'] = $this->input->get('pagination');
    $param['search'] = $this->input->get('search');
    $param['lang'] = $this->input->get('lang');

    $statuses = $this->status_model->getStatuses($param);

    // if(!empty($statuses['data']))
      $this->success($statuses['data'], $statuses['total']);
    // else
    //   $this->error(405, 'Get status list failed');
  }

  public function getStatusDetail($statusId = null, $lang = 'ch')
  {
    $this->checkHttpMethod('GET');

    if($statusId == null)
      $this->error(405, 'Status Id cannot be empty');

    $status = $this->status_model->getStatusDetail($statusId, $lang);

    if(!empty($status['data']))
      $this->success($status['data']);
    else
      $this->error(405, 'Get status detail failed');

  }

  public function createStatus()
  {
    $this->checkHttpMethod('POST');

    $status = array();
    $this->setParam($status,'POST','statusName','enStatusName');
    $this->setParam($status,'POST','statusName','chStatusName');
    $this->setParam($status,'POST','color');

    $id = $this->status_model->createStatus($status);

    if($id != 0)
      $this->success($id);
    else
       $this->error(405, 'Create Status Failed');

  }

  public function editStatus()
  {
    $this->checkHttpMethod('POST');

    $status['statusId'] = $this->input->post('statusId');
    $this->setParam($status,'POST','statusName','enStatusName');
    $this->setParam($status,'POST','statusName','chStatusName');
    $this->setParam($status,'POST','color');

    $result = $this->status_model->editStatus($status);

    if($result != 0)
      $this->success($result);
    else
       $this->error(405, 'Edit Status Failed');

  }

  public function deleteStatuses(){

    $statuses = implode(',', $this->input->get('statuses'));
    $result = $this->status_model->deleteStatuses($statuses);
    if(!empty($result))
      $this->success();
    else
      $this->error(405, 'Delete Statuses Failed');
  }

}