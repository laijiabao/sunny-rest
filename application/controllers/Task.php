<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');

class Task extends MY_Controller {

  function __construct() 
  { 
    parent::__construct(); 
    $this->load->model('task_model');
    $this->load->model('notification_model');
  } 

  public function index()
  {
    show_404();
  }

  public function getTasks()
  {
    $this->checkHttpMethod('GET');

    $param['pagination'] = $this->input->get('pagination');
    $param['search'] = $this->input->get('search');
    $param['filterBy'] = $this->input->get('filterBy');
    $param['lang'] = $this->input->get('lang');

    $tasks = $this->task_model->getTasks($param);

    // if(!empty($tasks['data']))
      $this->success($tasks['data'], $tasks['total']);
    // else
    //   $this->error(405, 'Get task list failed');
  }

  public function getTaskDetail($taskId = null, $lang = 'ch')
  {
    $this->checkHttpMethod('GET');

    if($taskId == null)
      $this->error(405, 'Task Id cannot be empty');

    $task = $this->task_model->getTaskDetail($taskId, $lang);

    if(!empty($task['data']))
      $this->success($task['data']);
    else
      $this->error(405, 'Get task detail failed');

  }

  public function createTask()
  {
    $this->checkHttpMethod('POST');

    $task = array();

    $this->setParam($task,'POST','taskCode');
    $this->setParam($task,'POST','taskName','enTaskName');
    $this->setParam($task,'POST','taskName','chTaskName');
    $this->setParam($task,'POST','taskType');
    $this->setParam($task,'POST','companyId');
    $this->setParam($task,'POST','reminderStartTime');
    $this->setParam($task,'POST','reminderFrequency');


    $id = $this->task_model->createTask($task);

    if($id != 0)
      $this->success($id);
    else
       $this->error(405, 'Create Task Failed');

  }

  public function editTask()
  {
    $this->checkHttpMethod('POST');

    $task['taskId'] = $this->input->post('taskId');
    $this->setParam($task,'POST','taskCode');
    $this->setParam($task,'POST','taskName','enTaskName');
    $this->setParam($task,'POST','taskName','chTaskName');
    $this->setParam($task,'POST','taskType');
    $this->setParam($task,'POST','companyId');
    $this->setParam($task,'POST','reminderStartTime');
    $this->setParam($task,'POST','reminderFrequency');

    $result = $this->task_model->editTask($task);

    if($result != 0)
      $this->success($result);
    else
       $this->error(405, 'Edit Task Failed');

  }

  public function deleteTasks()
  {

    $tasks = implode(',', $this->input->get('tasks'));
    $result = $this->task_model->deleteTasks($tasks);
    if(!empty($result))
      $this->success();
    else
      $this->error(405, 'Delete Tasks Failed');
  }

  public function assignTask()
  {
    $this->checkHttpMethod('POST');

    $task = array();

    $this->setParam($task,'POST','taskId');
    $this->setParam($task,'POST','staffId');
    $this->setParam($task,'POST','clientId');
    $this->setParam($task,'POST','value');
    $this->setParam($task,'POST','manHours');    
    $this->setParam($task,'POST','startDate');    
    $this->setParam($task,'POST','endDate');    
    $this->setParam($task,'POST','dueDate');    
    

    $userData = $_SESSION["userData"];
    $userId = $userData['userId'];
    $task['lastUpdate'] = date("Y-m-d H:i:s");
    $task['statusId'] = 1;
 

    if(empty($task['taskId']))
      $this->error(405, 'Please Select Task');
    if(empty($task['staffId']))
      $this->error(405, 'Please Select Staff');
    if(empty($task['clientId']))
      $this->error(405, 'Please Select Client');
    if(empty($task['value']))
      $this->error(405, 'Please Input Task Value');
    if(empty($task['manHours']))
      $this->error(405, 'Please Input Man Hours');
    if(empty($task['startDate']))
      $this->error(405, 'Please Input Start Date');
    if(empty($task['endDate']))
      $this->error(405, 'Please Input End Date');
    if(empty($task['dueDate']))
      $this->error(405, 'Please Input Due Date');

    $id = $this->task_model->assignTask($task);

    $this->sendNotification($id, $task['staffId'], 'NT');

    if($id != 0)
      $this->success($id);
    else
       $this->error(405, 'Assign Task Failed');

  }

  public function getAssignedTasks()
  {
    $this->checkHttpMethod('GET');

    $param['pagination'] = $this->input->get('pagination');
    $param['search'] = $this->input->get('search');
    $param['lang'] = $this->input->get('lang');

    $this->setParam($param,'GET','pagination');
    $this->setParam($param,'GET','search');
    $this->setParam($param,'GET','lang');
    $this->setParam($param,'GET','clientId');
    $this->setParam($param,'GET','staffId');
    $this->setParam($param,'GET','taskId');


    $tasks = $this->task_model->getAssignedTasks($param);

    foreach($tasks['data'] as &$task){

      $task->taskAttachments = json_decode($task->taskAttachments, JSON_UNESCAPED_UNICODE);
    }

    $this->success($tasks['data'], $tasks['total']);

  }


  public function editAssignedTask()
  {
    $this->checkHttpMethod('POST');

    $task['taskMagId'] = $this->input->post('taskMagId');
    $this->setParam($task,'POST','taskId');
    $this->setParam($task,'POST','staffId');
    $this->setParam($task,'POST','clientId');
    $this->setParam($task,'POST','value');
    $this->setParam($task,'POST','manHours');    
    $this->setParam($task,'POST','startDate');    
    $this->setParam($task,'POST','endDate');    
    $this->setParam($task,'POST','dueDate');   


    $result = $this->task_model->editAssignedTask($task);

    $this->sendNotification($task['taskMagId'], $task['staffId'], 'ET');

    if($result != 0)
      $this->success($result);
    else
       $this->error(405, 'Edit Assigned Task Failed');

  }


  public function addAttachment()
  {
    $this->checkHttpMethod('POST');

    $taskMagId = $this->input->post('taskMagId');

    $forms = $this->uploadFile();

    foreach($forms as &$form){

      $this->setParam($form,'POST','type');
      // echo json_encode($form);
    }
    // $result['hi'] = $forms;
    // echo json_encode($forms);

    $attachment['taskAttachments'] = $forms;
    // $attachment['userId'] = $userId;
    $attachment['taskMagId'] = $taskMagId;

    // echo json_encode($_SESSION["userData"]);


    $result = $this->task_model->addAttachment($attachment);

    if($result != 0)
      $this->success($result);
    else
       $this->error(405, 'Upload Attachment Failed');    
  }


  public function deleteAttachment()
  {
    $this->checkHttpMethod('POST');

    $taskMagId = $this->input->post('taskMagId');
    $attachment = $this->input->post('attachment');

    $result = $this->task_model->deleteAttachment($taskMagId, $attachment);

    if($result != 0)
      $this->success($result);
    else
       $this->error(405, 'Delete Attachment Failed');    

  }


  public function getAssignedTaskDetail($taskMagId)
  {
    $this->checkHttpMethod('GET');
    if(empty($taskMagId))
      $this->error(405, 'Please Input ID');

    $data = $this->task_model->getAssignedTaskDetail($taskMagId);
    $data[0]->taskAttachments = json_decode($data[0]->taskAttachments);
    $this->success($data);

  }


  public function deleteAssignedTask()
  {
    $this->checkHttpMethod('POST');

    $tasks = implode(',', $this->input->post('tasks'));

    $result = $this->task_model->deleteAssignedTask($tasks);

    if($result != 0)
      $this->success($result);
    else
       $this->error(405, 'Delete Task Failed');    

  }

  public function setStatus()
  {
    $this->checkHttpMethod('POST');

    $userData = $_SESSION["userData"];
    $userId = $userData['userId'];

    $taskMagId = $this->input->post('taskMagId');
    $statusId = $this->input->post('statusId');


    if(empty($taskMagId))
      $this->error(405, 'Please Input taskMagId');
    if(empty($statusId))
      $this->error(405, 'Please Input statusId');
    if(empty($userId))
      $this->error(401, 'Please Login');

    $staffId = $this->task_model->setStatus($taskMagId, $statusId, $userId);

    if($statusId == 4)
      $this->sendNotification($taskMagId, $staffId, 'TA');

    if($staffId != 0)
      $this->success($staffId);
    else
       $this->error(405, 'Set Status Failed');   

  }


  private function uploadFile()
  {
    sleep(1);

    $urls = null;

    $dir = 'uploadfile/'.date("Y").'/'.date("md").'/';
    if (!file_exists($dir)){
      mkdir ($dir,0777,true);
    }   
    $config["upload_path"] = $dir;
    $config["allowed_types"] = '*';
    // $config['encrypt_name'] = TRUE;
    $this->load->library('upload', $config);
    $this->upload->initialize($config);

    

    for($count = 0; $count<count($_FILES["files"]["name"]); $count++)
    {
      $_FILES["file"]["name"] = $_FILES["files"]["name"][$count];
      $_FILES["file"]["type"] = $_FILES["files"]["type"][$count];
      $_FILES["file"]["tmp_name"] = $_FILES["files"]["tmp_name"][$count];
      $_FILES["file"]["error"] = $_FILES["files"]["error"][$count];
      $_FILES["file"]["size"] = $_FILES["files"]["size"][$count];

      if($this->upload->do_upload('file'))
      {
        $data = $this->upload->data();   
        $files[$count]['formName'] = $data["file_name"]; 
        $files[$count]['formUrl'] = $dir.$data["file_name"];
        $files[$count]['time'] = date('Y-m-d H:i:s');
      }
    }
    // return $this->upload->display_errors("","");
    return $files;
  }

  public function getOngoingTasks(){

    $this->checkHttpMethod('GET');

    $result = $this->task_model->getOngoingTasks();

    $this->success($result);
  }

  public function test()
  {
    $result['data']['abc'] = $this->task_model->test();
    $result['data']['UPPERCASE_L'] = $this->task_model->test();
    // $result['data']['THIS_IS_UPPER_CASE'] = $this->task_model->test();

    $this->success($result);
  }

}