<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');

class Form extends MY_Controller {

  function __construct() 
  { 
    parent::__construct(); 
    $this->load->model('form_model');
  } 

  public function index()
  {
    show_404();
  }

  public function getForms()
  {
    $this->checkHttpMethod('GET');

    $param['pagination'] = $this->input->get('pagination');
    $param['search'] = $this->input->get('search');
    $param['lang'] = $this->input->get('lang');

    $forms = $this->form_model->getForms($param);

    $this->success($forms['data'], $forms['total']);
  }

  public function getFormDetail($formId = null, $lang = 'ch')
  {
    $this->checkHttpMethod('GET');

    if($formId == null)
      $this->error(405, 'Form Id cannot be empty');

    $form = $this->form_model->getFormDetail($formId, $lang);

    if(!empty($form['data']))
      $this->success($form['data']);
    else
      $this->error(405, 'Get form detail failed');

  }

  public function createForm()
  {
    $this->checkHttpMethod('POST');

    
    if(empty($_SESSION["userData"]))
      $this->error(401, 'Please Login');
    $userData = $_SESSION["userData"];
    $userId = $userData['userId'];


    if(empty($userId))
      $this->error(401, 'Please Login');

    $forms = $this->uploadFile();

    foreach($forms as &$form){

      $this->setParam($form,'POST','type');
      $form['uploadUserId'] = $userId;
      // echo json_encode($form);
    }
    // $result['hi'] = $forms;
    // echo json_encode($forms);

    $result = $this->form_model->createForm($forms);

    if($result != 0)
      $this->success($result);
    else
       $this->error(405, 'Create Form Failed');

  }

  public function editForm()
  {
    $this->checkHttpMethod('POST');

    // $form['formId'] = $this->input->post('formId');
    // $form['formName'] = $this->input->post('formName');


    // $form['chFormName'] = $this->input->post('formName');
    // $form['formType'] = $this->input->post('formType');
    // $form['reminderStartTime'] = $this->input->post('reminderStartTime');
    // $form['reminderFrequency'] = $this->input->post('reminderFrequency');

    $result = $this->form_model->editForm($form);

    if($result != 0)
      $this->success($result);
    else
       $this->error(405, 'Edit Form Failed');

  }

  public function deleteForms(){

    $forms = implode(',', $this->input->get('forms'));
    $result = $this->form_model->deleteForms($forms);

    if(!empty($result)){
      $this->success();

    }else
      $this->error(405, 'Delete Forms Failed');
  }

  private function uploadFile()
  {
    sleep(1);

    $urls = null;

    $dir = 'uploadfile/'.date("Y").'/'.date("md").'/';
    if (!file_exists($dir)){
      mkdir ($dir,0777,true);
    }   
    $config["upload_path"] = $dir;
    $config["allowed_types"] = '*';
    // $config['encrypt_name'] = TRUE;
    $this->load->library('upload', $config);
    $this->upload->initialize($config);

    

    for($count = 0; $count<count($_FILES["files"]["name"]); $count++)
    {
      $_FILES["file"]["name"] = $_FILES["files"]["name"][$count];
      $_FILES["file"]["type"] = $_FILES["files"]["type"][$count];
      $_FILES["file"]["tmp_name"] = $_FILES["files"]["tmp_name"][$count];
      $_FILES["file"]["error"] = $_FILES["files"]["error"][$count];
      $_FILES["file"]["size"] = $_FILES["files"]["size"][$count];

      if($this->upload->do_upload('file'))
      {
        $data = $this->upload->data();   
        $files[$count]['formName'] = $data["file_name"]; 
        $files[$count]['formUrl'] = $dir.$data["file_name"];

      }
    }
    // return $this->upload->display_errors("","");
    return $files;
  }


}