<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');

class User extends MY_Controller {

  function __construct() 
  { 
    parent::__construct(true); 
    $this->load->model('user_model');
  } 

  public function index($page = 'home')
  {
    show_404();
  }

  public function setLang($userId, $lang)
  {
    $this->checkHttpMethod('GET');

    if(empty($userId))
      $this->error(405, $this->lang->line('error_user_invalid_id'));
    if( empty($lang))
      $this->error(405, $this->lang->line('error_user_invalid_lang'));

    $result = $this->user_model->setLang($userId, $lang);

    if($result){

      $userData = $_SESSION["userData"];
      $userData['lang'] = $lang;
      $_SESSION["userData"] = $userData;
      $this->success();

    }else
      $this->error(405, $this->lang->line('error_user_lang_failed'));

  }

  public function getUsers()
  {
    $this->checkHttpMethod('GET');

    $param['page'] = $this->input->get('page');
    $param['search'] = $this->input->get('search');
    $param['lang'] = $this->input->get('lang');
    $param['startDate'] = $this->input->get('startDate');
    $param['endDate'] = $this->input->get('endDate');
    $param['orderBy'] = $this->input->get('orderBy');
    $param['direction'] = $this->input->get('direction');

    $data = $this->user_model->getUsers($param);

    // if($users['data'])
      $this->success($data);
    // else
    //   $this->error(405, $this->lang->line('error_user_list_failed'));
  }


  public function createUser()
  {
    $this->checkHttpMethod('POST');   
    $params = $this->input->post();
    $result = $this->user_model->createUser($params);
    $this->success($result);
  }

  public function editUser()
  {
    $this->checkHttpMethod('POST');

    $param['userId'] = $this->input->post('userId');
    $param['staffId'] = $this->input->post('staffId');
    $param['role'] = $this->input->post('role');
    $param['status'] = $this->input->post('status');

    if(empty($param['userId']))
      $this->error(405,'Invalid User');

    if(empty($param['role']) && empty($param['staffId']) && empty($param['status']))
      $this->error(405,'Invalid Input');


    $result = $this->user_model->editUser($param);

    if(empty($result))
      $this->error(405, 'Edit User Failed');


    $this->success($result);

  }

  public function editPassword()
  {
    $this->checkHttpMethod('POST');
    $param['userId'] = $this->input->post('userId');
    $param['oldPassword'] = $this->input->post('oldPassword');
    $param['newPassword'] = $this->input->post('newPassword');
     $param['confirmedNewPassword'] = $this->input->post('confirmedNewPassword');

    if(empty($param['userId']))
      $param['userId'] = $this->session->userdata('userData')['userId'];

    if(empty($param['oldPassword']) && empty($param['newPassword']) && empty($param['confirmedNewPassword']))
      $this->error(405,'Invalid Input');    

    $result = $this->user_model->editPassword($param);

    if(empty($result))
      $this->error(405, 'Edit User Password Failed');

    $this->success($result);

  }

  public function deleteUsers(){
    $this->checkHttpMethod('POST');
    $params = $this->input->post();
    $result = $this->user_model->deleteUsers($params);
    if(!empty($result))
      $this->success();
    else
      $this->error(405, 'Delete Users Failed');
  }

  public function getRoles($lang = 'ch')
  {
    $this->checkHttpMethod('GET');

    $result = $this->user_model->getRoles($lang);
    $this->success($result);
  }

}