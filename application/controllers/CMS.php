<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CMS extends MY_Controller {


    function __construct() 
    { 
      parent::__construct(); 
      // $this->load->model('util_model');

    } 

    public function index($page = null)
    { 

      // var_dump($this->session->userdata);
        if($this->session->userdata('loggedIn') == false){
          // var_dump($_SESSION);
          redirect('admin');
          
          // var_dump($_SERVER);
        }

        $role = $this->session->userdata('userData')['role'];

        if($page == null){
          if($role == 'A' || $role == 'M')
            $page = 'dashboard';
          else
            $page = 'task';
        }

        if($role != 'A' && $role != 'M'){

          if($page == 'dashboard' || $page == 'staff' || $page == 'user' || $page == 'status')
            show_404();

        }


        if (!file_exists(APPPATH.'views/pages/'.$page.'.php'))
        {
            show_404();
        } 

        
        // echo $page;


        $data['page'] = $page;

        $this->load->view('templates/header');
        $this->load->view('templates/navBar');
        $this->load->view('templates/sidebar');
        // $this->load->view('pages/'.$page);
        $this->load->view('templates/content', $data);
        $this->load->view('templates/modal');
        $this->load->view('templates/footer');
    }

    public function view($page = 'dashboard')
    {
        if (!file_exists(APPPATH.'views/pages/'.$page.'.php'))
        {
            show_404();
        }
        // var_dump($_SESSION);
        if($this->session->userdata('loggedIn') == false){
          
          redirect('admin');
          
          // var_dump($_SERVER);
        }
    
        $this->load->view('pages/'.$page);
        // $this->output->enable_profiler(TRUE);

    }

    public function dashboard(){

      // $data['data'] = $this->util_model->dashboard();
      // $data['status'] = 200;
      // echo json_encode($data);
    }

}
