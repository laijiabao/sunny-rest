var ROLE = 'S';

if(Cookies.get('role') != null)
	ROLE = Cookies.get('role');

function getMenus()
{
	
	switch(ROLE){

		case 'A':
		return new Array('dashboard','user','task','timesheet','client','status','form','staff');
		case 'M': 
		return new Array('dashboard','user','task','timesheet','client','status','form','staff');
		case 'S':
		return new Array('task','client','timesheet','form');

	}
}