
var RECORDS_PER_PAGE = 10;
var VISIBLE_PAGES = 15;
var AJAX_TIMEOUT = 30000;
// var HOST = "https://test.ftinnotech.com";
var HOST = "http://localhost:8888";
var URLS = {
	"GET_USERS": "/accountant/user/getUsers/",
	"GET_STAFFS": "/accountant/staff/getStaffs/",
	"GET_STAFFS_DETAIL": "/accountant/staff/getStaff/",
	"GET_TASKS": "/accountant/task/getTasks/",
	"GET_TASK_DETAIL": "/accountant/task/getTaskDetail/",
	"GET_STATUSES": "/accountant/status/getStatuses/",
	"GET_STATUS_DETAIL": "/accountant/status/getStatusDetail/",
	"GET_CLIENTS": "/accountant/client/getClients/",
	"GET_CLIENT_DETAIL": "/accountant/client/getClientDetail/",
	"GET_FORMS": "/accountant/form/getForms/",
	"GET_ASSIGNED_TASKS": "/accountant/task/getAssignedTasks/",
	"GET_ASSIGNED_TASK_DETAIL": "/accountant/task/getAssignedTaskDetail/"
};

var COLORS = [
    {"color":"red", "class":"bg-red"},
    {"color":"orange", "class":"bg-orange"},
    {"color":"yellow", "class":"bg-yellow"},
    {"color":"teal", "class":"bg-teal"},
    {"color":"blue", "class":"bg-aqua"},
    {"color":"green", "class":"bg-green"},
    {"color":"maroon", "class":"bg-maroon"},
    {"color":"purple", "class":"bg-purple"},
    {"color":"navy", "class":"bg-navy"},
    {"color":"gray", "class":"bg-gray"}
];


function checkStatus(data){

    if(data.status == 200){
    	return true;
    }
    else if(data.status == 401)
    	self.location = "/admin/login";
    else{
    	alert(data.message);
    }

    return false;
}

/**
 *	Refresh pagination
 *
 *  @param element - The pagination dom
 *	@param pagination - The pagination vue instance
 *	@param recordsPerPage - recordsPerPage
 *	@param visiblePages - visiblePages
 */

// function refreshPagination(element, pagination, visiblePages = VISIBLE_PAGES){

// 	// var totalPages = Math.ceil(parseInt(pagination.total)/RECORDS_PER_PAGE);

// 	// if(element)
// 	// 	element.twbsPagination('destroy');
	
// 	// element.twbsPagination({
// 	// 	initiateStartPageClick: false,
// 	// 	totalPages: totalPages,
// 	// 	visiblePages: visiblePages,
// 	// 	first: '<<',
// 	// 	next: '>',
// 	// 	prev: '<',
// 	// 	last: '>>',
// 	// 	onPageClick: function (event, page) {
// 	// 		console.log("pageOnClick" + page);
// 	// 		pagination.onPageClick(page);

// 	// 		console.log(event);
// 	// 	}
// 	// });

// }

// function getUsers(tableData, pagination){

// 	var param = tableData.param;
// 	$.ajax({
// 	  url: HOST + '/accountant/user/getUsers/',
// 	  type: 'GET',
// 	  dataType: 'json',
// 	  data: param,
// 	  timeout:AJAX_TIMEOUT,
// 	  success:function(data)
// 	  {

// 	  	if(checkStatus(data) == false)
// 	  		return;

// 		tableData.items = data.data;
// 		pagination.totalPage = Math.ceil(parseInt(data.total)/RECORDS_PER_PAGE);
// 		// pagination.refreshPagination(data.total, 10);

// 	  }
// 	}); 
// }

function getTableData(tableData, pagination, URL){
	var param = tableData.param;
	$.ajax({
	  url: HOST + URL,
	  type: 'GET',
	  dataType: 'json',
	  data: param,
	  timeout:AJAX_TIMEOUT,
	  success:function(data)
	  {

	  	if(checkStatus(data) == false)
	  		return;

		tableData.items = data.data;
		pagination.total = data.total;
		pagination.totalPage = Math.ceil(parseInt(data.total)/RECORDS_PER_PAGE);

	  }
	}); 
}

function getDetail(id, URL, callback){

	// console.log('calling '+URL);

	$.ajax({
	  url: HOST + URL +id+'/'+SYSTEM_LANGUAGE,
	  type: 'GET',
	  dataType: 'json',
	  timeout:AJAX_TIMEOUT,
	  success:function(data)
	  {
	  	if(checkStatus(data) == false)
	  		return;
	  	callback(data);
	  }
	}); 
}

