var SYSTEM_LANGUAGE = "ch";

if(Cookies.get('lang') != null)
	SYSTEM_LANGUAGE = Cookies.get('lang');

function lang(name, lang = SYSTEM_LANGUAGE){

	switch(lang){
		case "ch":
		return chWording[name];

		case "en":
		return enWording[name];

		default:
		return chWording[name];
	}

}

// Chinese 
var chWording = new Array();

chWording['CMS_title'] = 'W.K Pang任務管理';

chWording['nav_title'] = 'W.K Pang任務管理';
chWording['nav_title_short'] = 'W.K';
chWording['nav_numberOfTasks'] = '正在進行的任務:';
chWording['nav_viewAllTask'] = '查看所有任務';
chWording['nav_hoursLeft'] = ' days left';
chWording['nav_waitingApproval'] = ' 等待審批';
// chWording['nav_viewAllNotifications'] = '查看所有通知';
chWording['nav_numberOfNotifications'] = '最近7天通知數量:';
chWording['nav_notification_editPassword'] = '你已成功修改密碼';
chWording['nav_notification_newStaff'] = '加入新员工: ';
chWording['nav_notification_newTask'] = '你有新的任務:';
chWording['nav_notification_taskStatusChanged'] = '任務狀態更新為:';


chWording['menu_dashboard'] = 'Dashboard';
chWording['menu_user'] = '用戶管理';
chWording['menu_task'] = '任務管理';
chWording['menu_timesheet'] = 'Timesheet';
chWording['menu_client'] = '客戶管理';
chWording['menu_status'] = '任務及狀態';
chWording['menu_form'] = '表單列表';
chWording['menu_staff'] = '員工列表';
chWording['menu_system'] = '系統設置';

chWording['searchBtn'] = '搜尋';
chWording['paginationPrefix'] = '共';
chWording['paginationSuffix'] = '條記錄';
chWording['actions'] = '操作';
chWording['confirm'] = '確定';
chWording['save'] = '保存';
chWording['selectStaff'] = '選擇員工';
chWording['selectFile'] = '選擇文件';
chWording['uploadFile'] = '上載文件';
chWording['uploading'] = '正在上載...';

chWording['q_confirmDelete'] = '確認刪除?';
chWording['q_confirmRestore'] = '確認恢復?';

chWording['user_userId'] = 'ID';
chWording['user_userName'] = '用戶名';
chWording['user_role'] = '用戶類型';
chWording['user_ip'] = 'IP地址';
chWording['user_lastLogin'] = '最後登入';
chWording['user_status'] = '狀態';
chWording['user_actions'] = '操作';

chWording['user_new'] = '新增用戶';
chWording['user_password'] = '密碼';
chWording['user_passwordConf'] = '重複密碼';
chWording['user_edit'] = '編輯用戶資料';
chWording['user_update'] = '確認修改';
chWording['user_editPassword'] = '修改密碼';
chWording['user_oldPassword'] = '舊密碼';
chWording['user_newPassword'] = '新密碼';
chWording['user_newPasswordConf'] = '重複新密碼';

chWording['staff_staffId'] = 'ID';
chWording['staff_staffNo'] = '員工編號';
chWording['staff_name'] = '姓名';
chWording['staff_alias'] = '英文名';
chWording['staff_status'] = '狀態';
chWording['staff_actions'] = '操作';

chWording['staff_basic'] = '基本資料';
chWording['staff_contacts'] = '聯繫方式';
chWording['staff_new'] = '新增員工';
chWording['staff_chName'] = '中文全名';
chWording['staff_enName'] = '英文全名';
chWording['staff_gender'] = '性別';
chWording['staff_department'] = '任職部門';
chWording['staff_position'] = '所屬職位';
chWording['staff_email'] = '電郵';
chWording['staff_birthday'] = '生日';
chWording['staff_identityNo'] = '身份證號碼';
chWording['staff_urgentContactName'] = '緊急聯絡人';
chWording['staff_urgentContactNo'] = '緊急聯絡電話';
chWording['staff_bank'] = '銀行資料';
chWording['staff_bankAccount'] = '銀行卡號';
chWording['staff_country'] = '國家/地區';
chWording['staff_enAddress'] = '英文地址';
chWording['staff_chAddress'] = '中文地址';
chWording['staff_mobileNo'] = '移動電話';
chWording['staff_directNo'] = '電話';
chWording['staff_pagingNo'] = '電話';
chWording['staff_homeNo'] = '家庭電話';
chWording['staff_faxNo'] = '傳真號碼';
chWording['staff_inTime'] = '入職日期';
chWording['staff_leaveTime'] = '離職日期';
chWording['staff_MPFDate'] = 'MPF日期';

chWording['staff_edit'] = '編輯員工資料';
chWording['staff_totalManHours'] = '總工時';
chWording['staff_totalValues'] = '處理金額';

chWording['task_new'] = '新增任務';
chWording['task_taskCode'] = 'Task Code';
chWording['task_company'] = '公司';
chWording['task_edit'] = '編輯任務';
chWording['task_taskId'] = 'ID';
chWording['task_name'] = '任務';
chWording['task_type'] = '任務類型';
chWording['task_reminderStartTime'] = '通知開始時間';
chWording['task_reminderFrequency'] = '通知頻率';
chWording['task_actions'] = '操作';
chWording['task_list'] = '任務列表';

chWording['status_new'] = '新增狀態';
chWording['status_edit'] = '編輯狀態';
chWording['status_statusId'] = 'ID';
chWording['status_statusName'] = '狀態';
chWording['status_color'] = '顏色';
chWording['status_actions'] = '操作';
chWording['status_list'] = '狀態列表';
chWording['status_changeColor'] = '改變顏色';
chWording['status_preview'] = '預覽';

chWording['client_new'] = '新增客戶';
chWording['client_edit'] = '編輯客戶';
chWording['client_basic'] = '基本資料';
chWording['client_clientId'] = 'ID';
chWording['client_clientCode'] = 'Client Code';
chWording['client_companyNo'] = 'Company No.';
chWording['client_brNo'] = 'BR No.';
chWording['client_chClientName'] = 'Company Chinese Name';
chWording['client_enClientName'] = 'Company English Name';
chWording['client_clientGroup'] = 'Client Group';
chWording['client_type'] = 'Company type';
chWording['client_agencyName'] = 'Agency';
chWording['client_status'] = '狀態';
chWording['client_country'] = 'Jurisdiction';
chWording['client_enRegisterAddress'] = 'English Registered Address';
chWording['client_chRegisterAddress'] = 'Chinese Registered Address';
chWording['client_enMailingAddress'] = 'English Mailing Address';
chWording['client_chMailingAddress'] = 'Chinese Mailing Address';
chWording['client_attn'] = 'Attn of invoice';
chWording['client_activeDate'] = 'Active Date';
chWording['client_inactiveDate'] = 'Inactive Date';
chWording['client_telNo1'] = 'Tel no. 1';
chWording['client_telNo2'] = 'Tel no. 2';
chWording['client_telNo3'] = 'Tel no. 3';
chWording['client_faxNo1'] = 'Fax no. 1';
chWording['client_faxNo2'] = 'Fax no. 2';
chWording['client_faxNo3'] = 'Fax no. 3';
chWording['client_contactPerson1'] = 'Contact Person 1';
chWording['client_contactPerson2'] = 'Contact Person 2';
chWording['client_position1'] = 'Position 1';
chWording['client_position2'] = 'Position 2';
chWording['client_email1'] = 'Email 1';
chWording['client_email2'] = 'Email 2';
chWording['client_outputInvoice'] = 'Output Invoice';
chWording['client_taskList'] = '任務列表';
chWording['client_disbursement'] = 'Disbursement';
chWording['client_taskName'] = '任務';
chWording['client_dueDate'] = 'Due Date';
chWording['client_value'] = '金額';
chWording['client_assignedTo'] = '員工';
chWording['client_companyInfo'] = '客戶資料';
chWording['client_companyInvoice'] = '公司';

chWording['client_exportSummary'] = 'Export Invoice Summary';
chWording['client_year'] = '選擇年份';
chWording['client_month'] = '選擇月份';

chWording['form_new'] = '上載文件';
chWording['form_formId'] = 'ID';
chWording['form_formName'] = '文件名';
chWording['form_createTime'] = '上載時間';
chWording['form_updateTime'] = '更新時間';

chWording['taskMag_new'] = '分配任務';
chWording['taskMag_edit'] = '修改任務';
chWording['taskMag_taskMagId'] = 'ID';
chWording['taskMag_staffName'] = '員工';
chWording['taskMag_clientName'] = '客戶';
chWording['taskMag_taskName'] = '任務';
chWording['taskMag_company'] = '公司';
chWording['taskMag_timeRange'] = '時間';
chWording['taskMag_startDate'] = '任務覆蓋期間(開始日期)';
chWording['taskMag_endDate'] = '任務覆蓋期間(結束日期)';
chWording['taskMag_dueDate'] = 'Due Date';
chWording['taskMag_status'] = '狀態';
chWording['taskMag_value'] = '金額';
chWording['taskMag_taskAttachments'] = '附件';
chWording['taskMag_lastUpdate'] = "最後更新";
chWording['taskMag_manHours'] = "Hours";

chWording['taskMag_addAttachment'] = "添加附件";
chWording['taskMag_viewAttachment'] = "查看附件";
chWording['taskMag_noAttachment'] = "暫無附件";
chWording['taskMag_attachment'] = "附件";
chWording['taskMag_attachmentNo'] = "個附件";
chWording['taskMag_attachmentTime'] = "上傳時間";

chWording['list_client'] = "客戶列表";
chWording['list_staff'] = "員工列表";
chWording['list_task'] = "任務列表";


chWording['error_uploadFailed'] = "上傳失敗！";
chWording['error_pleaseSelectFile'] = "請選擇文件！";

//Timesheet
chWording['timesheet_export_csv'] = '以 CSV 格式匯出';
chWording['timesheet_hour']='小時';
chWording['timesheet_event_panel']='事件面板';
chWording['timesheet_event_type']='事件種類';
chWording['timesheet_task_label']='工作';
chWording['timesheet_leave_label']='休假';
chWording['timesheet_task_placeholder']='請選擇工作種類';
chWording['timesheet_leave_placeholder']='請選擇休假種類';
chWording['timesheet_time_using_label']='用時:（小時）';
chWording['timesheet_chargeable_label']='是否為 Chargeable';
chWording['timesheet_create']='創建';
chWording['timesheet_delete']='刪除';
chWording['timesheet_edit']='保存修改';
chWording['timesheet_range']='時間表範圍';
chWording['timesheet_range_indicator']='（起始月份 - 結束月份）';
chWording['timesheet_download']='下載';
chWording['timesheet_create_error']='創建事件錯誤';
chWording['timesheet_create_fail']='創建事件失敗';
chWording['timesheet_update_error']='更新事件錯誤';
chWording['timesheet_update_fail']='更新事件失敗';
chWording['timesheet_delete_error']='刪除事件錯誤';
chWording['timesheet_delete_fail']='刪除事件失敗';
chWording['timesheet_download_fail']='請於瀏覽器右上角設置 “允許彈出窗口” ';
chWording['timesheet_print_message']='如果無法下載，請於瀏覽器右上角設置 “允許該網站彈出窗口” ';


//Dashboard
chWording['dashboard_revenue_title'] = '公司收入';
chWording['dashboard_statistic'] = '統計數據:';
chWording['dashboard_totalRevenue'] = '收入';
chWording['dashboard_totalClients'] = '客户數';
chWording['dashboard_totalManHours'] = '工時';
chWording['dashboard_totalTasks'] = '任務數';

chWording['dashboard_total'] = '總數';
chWording['dashboard_average'] = '平均';
chWording['dashboard_hours'] = '工時';
chWording['dashboard_value'] = '金額';
chWording['dashboard_totalOrder'] = '總數排行TOP 10';
chWording['dashboard_averageOrder'] = '均數排行TOP 10';

chWording['dashboard_staffNo'] = '員工編號';
chWording['dashboard_staffName'] = '名字';
chWording['dashboard_clientCode'] = '客戶編號';
chWording['dashboard_clientName'] = '客戶名稱';
chWording['dashboard_taskCode'] = '任務編號';
chWording['dashboard_taskName'] = '任務名稱';
chWording['dashboard_startDate'] = '開始時間';
chWording['dashboard_endDate'] = '結束時間';
chWording['dashboard_timeRange'] = '日期範圍選擇';

chWording['dashboard_chooseCompany'] = '選擇公司';
chWording['dashboard_allCompany'] = '所有公司';
chWording['dashboard_wkpang'] = 'W.K. Pang & Co.';
chWording['dashboard_chk'] = 'CHK';

chWording['dashboard_staffPerformance'] = '員工統計';
chWording['dashboard_clientPerformance'] = '客戶統計';
chWording['dashboard_taskPerformance'] = '任務統計';

chWording['notify_warning'] = 'Reminder';
chWording['notify_success'] = 'Success';
chWording['notify_info'] = 'Info';
chWording['notify_danger'] = 'Error';

chWording['notify_successMessage'] = '操作成功';

//English

var enWording = new Array();

enWording['CMS_title'] = 'W.K Pang Accounting System';

enWording['nav_title'] = 'W.K Pang Accounting System';
enWording['nav_title_short'] = 'W.K';
enWording['menu_dashboard'] = 'Dashboard';
enWording['menu_user'] = 'User Management';
enWording['menu_task'] = 'Task Management';
enWording['menu_timesheet'] = 'Timesheet';
enWording['menu_client'] = 'Client Management';
enWording['menu_status'] = 'Task & Status Master';
enWording['menu_form'] = 'Form Master';
enWording['menu_staff'] = 'Staff Management';
enWording['menu_system'] = 'System Management';

enWording['searchBtn'] = 'Search';
enWording['paginationPrefix'] = 'Total ';
enWording['paginationSuffix'] = ' Records';
enWording['actions'] = 'Actions';
enWording['confirm'] = 'Confirm';
enWording['save'] = 'Save';
enWording['selectStaff'] = 'Select Staff';
enWording['selectFile'] = 'Select File';
enWording['uploadFile'] = 'Upload File';
enWording['uploading'] = 'Uploading';

enWording['q_confirmDelete'] = 'Are you sure to delete?';

enWording['user_userId'] = 'ID';
enWording['user_userName'] = 'User Name';
enWording['user_role'] = 'User Role';
enWording['user_ip'] = 'IP address';
enWording['user_lastLogin'] = 'Last Login';
enWording['user_status'] = 'Status';
enWording['user_actions'] = 'Actions';

enWording['user_new'] = 'New User';
enWording['user_password'] = 'Password';
enWording['user_passwordConf'] = 'Repeat Password';
enWording['user_edit'] = 'Edit User';
enWording['user_update'] = 'Update';
enWording['user_editPassword'] = 'Edit Password';
enWording['user_oldPassword'] = 'Old Password';
enWording['user_newPassword'] = 'New Password';
enWording['user_newPasswordConf'] = 'New Password Confirm';

enWording['staff_staffId'] = 'ID';
enWording['staff_staffNo'] = 'Staff No';
enWording['staff_name'] = 'Name';
enWording['staff_alias'] = 'Alias';
enWording['staff_status'] = 'Status';
enWording['staff_actions'] = 'Actions';

enWording['staff_basic'] = 'Basic Information';
enWording['staff_contacts'] = 'Contacts';
enWording['staff_new'] = 'New Staff';
enWording['staff_chName'] = 'Chinese Name';
enWording['staff_enName'] = 'English Full Name';
enWording['staff_gender'] = 'Gender';
enWording['staff_department'] = 'Department';
enWording['staff_position'] = 'Position';
enWording['staff_email'] = 'Email';
enWording['staff_identityNo'] = 'IdentityNo';
enWording['staff_urgentContactName'] = 'Urgent Contact';
enWording['staff_urgentContactNo'] = 'Urgent Contact Tel';
enWording['staff_bank'] = 'Bank';
enWording['staff_bankAccount'] = 'Back Account';
enWording['staff_country'] = 'Country/Region';
enWording['staff_enAddress'] = 'English Address';
enWording['staff_chAddress'] = 'Chinese Address';
enWording['staff_mobileNo'] = 'Mobile No.';
enWording['staff_directNo'] = 'Direct No.';
enWording['staff_pagingNo'] = 'Paging No.';
enWording['staff_homeNo'] = 'Home No.';
enWording['staff_faxNo'] = 'Fax No.';
enWording['staff_inTime'] = 'Entry Date';
enWording['staff_leaveTime'] = 'Leave Date';
enWording['staff_MPFDate'] = 'MPF Date';


enWording['staff_edit'] = 'Edit Staff';
enWording['staff_totalManHours'] = 'Man Hour Used';
enWording['staff_totalValues'] = 'Total Value Handled';

enWording['task_new'] = 'New Task';
enWording['task_taskCode'] = 'Task Code';
enWording['task_company'] = 'Company';
enWording['task_edit'] = 'Edit Task';
enWording['task_taskId'] = 'ID';
enWording['task_name'] = 'Task';
enWording['task_type'] = 'Task Type';
enWording['task_reminderStartTime'] = 'Reminder Start Time';
enWording['task_reminderFrequency'] = 'Reminder Frequency';
enWording['task_actions'] = 'Actions';
enWording['task_list'] = 'Task List';

enWording['status_new'] = 'New Status';
enWording['status_edit'] = 'Edit Status';
enWording['status_statusId'] = 'ID';
enWording['status_statusName'] = 'Status';
enWording['status_color'] = 'Status Color';
enWording['status_actions'] = 'Actions';
enWording['status_list'] = 'Status List';
enWording['status_changeColor'] = 'Change Color';
enWording['status_preview'] = 'Preview';



enWording['timesheet_export_csv'] = 'Export as CSV';
enWording['timesheet_hour']='hours';
enWording['timesheet_event_panel']='Event Panel';
enWording['timesheet_event_type']='Event Type';
enWording['timesheet_task_label']='Task';
enWording['timesheet_leave_label']='Leave';
enWording['timesheet_task_placeholder']='Choose a Task Type';
enWording['timesheet_leave_placeholder']='Choose a Leave Type';
enWording['timesheet_time_using_label']='Time Using: (Hours)';
enWording['timesheet_chargeable_label']='Is Chargeable';
enWording['timesheet_create']='Create';
enWording['timesheet_delete']='Delete';
enWording['timesheet_edit']='Save Changes';
enWording['timesheet_range']='Timesheet Range';
enWording['timesheet_range_indicator']='(Start Month - End Month)';
enWording['timesheet_download']='download';
enWording['timesheet_create_error']='Create New Event error';
enWording['timesheet_create_fail']='Create New Event fail';
enWording['timesheet_update_error']='Update Event error';
enWording['timesheet_update_fail']='Update Event fail';
enWording['timesheet_delete_error']='Delete Event error';
enWording['timesheet_delete_fail']='Delete Event fail';
enWording['timesheet_download_fail']='Please allow popups for this website';
enWording['timesheet_print_message']= 'If dwonload failed, please allow popups setting for the website';